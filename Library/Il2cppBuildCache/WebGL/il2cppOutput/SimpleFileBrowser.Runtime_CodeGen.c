﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean SimpleFileBrowser.FileBrowser::get_IsOpen()
extern void FileBrowser_get_IsOpen_mA5D1991D30785FD83AAA533C96803438CDC74C5B (void);
// 0x00000002 System.Void SimpleFileBrowser.FileBrowser::set_IsOpen(System.Boolean)
extern void FileBrowser_set_IsOpen_m4909EA8B84D49A7787D31AD8DD872C3956F6E3BC (void);
// 0x00000003 System.Boolean SimpleFileBrowser.FileBrowser::get_Success()
extern void FileBrowser_get_Success_m31DCB471FECBCF6EFAD502012C1DC58CE90209E7 (void);
// 0x00000004 System.Void SimpleFileBrowser.FileBrowser::set_Success(System.Boolean)
extern void FileBrowser_set_Success_m6D20AE58CDEF9F643CDA6AD3E598158FD09E468C (void);
// 0x00000005 System.String[] SimpleFileBrowser.FileBrowser::get_Result()
extern void FileBrowser_get_Result_mCCFE1FD860688006BA2B97AF3E2926E62E78C814 (void);
// 0x00000006 System.Void SimpleFileBrowser.FileBrowser::set_Result(System.String[])
extern void FileBrowser_set_Result_m0BBF3C3624A4581E4925FA84A1A72A810598ADCC (void);
// 0x00000007 System.Boolean SimpleFileBrowser.FileBrowser::get_AskPermissions()
extern void FileBrowser_get_AskPermissions_mC052343777A666D9D11A058D48FB76C6D251A6A0 (void);
// 0x00000008 System.Void SimpleFileBrowser.FileBrowser::set_AskPermissions(System.Boolean)
extern void FileBrowser_set_AskPermissions_m74169F8EBE9C318835BCBB5B3B448AE0E94E9003 (void);
// 0x00000009 System.Boolean SimpleFileBrowser.FileBrowser::get_SingleClickMode()
extern void FileBrowser_get_SingleClickMode_mDF2CD71D1159B44F8D67DCC5A6B409D7BFF930B8 (void);
// 0x0000000A System.Void SimpleFileBrowser.FileBrowser::set_SingleClickMode(System.Boolean)
extern void FileBrowser_set_SingleClickMode_m068FC8141089C56F72661B7BF9B66FD48480F14B (void);
// 0x0000000B System.Single SimpleFileBrowser.FileBrowser::get_DrivesRefreshInterval()
extern void FileBrowser_get_DrivesRefreshInterval_mD72A4D60E7DA56E0EF979B6351E533951CD98F21 (void);
// 0x0000000C System.Void SimpleFileBrowser.FileBrowser::set_DrivesRefreshInterval(System.Single)
extern void FileBrowser_set_DrivesRefreshInterval_m444758D2DB4CCC57F0474C60328685B4AE7C1DCC (void);
// 0x0000000D System.Boolean SimpleFileBrowser.FileBrowser::get_ShowHiddenFiles()
extern void FileBrowser_get_ShowHiddenFiles_mEB6850AB8F5FFEFB398CC721E468744349D68FC9 (void);
// 0x0000000E System.Void SimpleFileBrowser.FileBrowser::set_ShowHiddenFiles(System.Boolean)
extern void FileBrowser_set_ShowHiddenFiles_m5A4152F2558A434950B600A88859F91C150975BC (void);
// 0x0000000F System.Boolean SimpleFileBrowser.FileBrowser::get_DisplayHiddenFilesToggle()
extern void FileBrowser_get_DisplayHiddenFilesToggle_m22E7770112308552A53A7C9346366A15D42DE081 (void);
// 0x00000010 System.Void SimpleFileBrowser.FileBrowser::set_DisplayHiddenFilesToggle(System.Boolean)
extern void FileBrowser_set_DisplayHiddenFilesToggle_m4EEFC16A76F87218710D9E2C300F4137962A925C (void);
// 0x00000011 System.String SimpleFileBrowser.FileBrowser::get_AllFilesFilterText()
extern void FileBrowser_get_AllFilesFilterText_mC93ACF67D705CFC82178C20ACDDC6AFB6E8B14CF (void);
// 0x00000012 System.Void SimpleFileBrowser.FileBrowser::set_AllFilesFilterText(System.String)
extern void FileBrowser_set_AllFilesFilterText_mD5675A8A16F67EEF79E3A4155D8F35A845DF3D14 (void);
// 0x00000013 System.String SimpleFileBrowser.FileBrowser::get_FoldersFilterText()
extern void FileBrowser_get_FoldersFilterText_m7A35A4AAD9AC262A1CBE5B6E8C6BEA211EAF6E73 (void);
// 0x00000014 System.Void SimpleFileBrowser.FileBrowser::set_FoldersFilterText(System.String)
extern void FileBrowser_set_FoldersFilterText_m2A9450B4354E092F9AF9202E9A6DDC2F282E4373 (void);
// 0x00000015 System.String SimpleFileBrowser.FileBrowser::get_PickFolderQuickLinkText()
extern void FileBrowser_get_PickFolderQuickLinkText_m60FF706C3152065E59FF93D710F19FDE181EF4B4 (void);
// 0x00000016 System.Void SimpleFileBrowser.FileBrowser::set_PickFolderQuickLinkText(System.String)
extern void FileBrowser_set_PickFolderQuickLinkText_m96FEB6CFFD83104583EF5668BA49CF0AB7A18F6B (void);
// 0x00000017 SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser::get_Instance()
extern void FileBrowser_get_Instance_m811EE63D3BED8EF68516D6E00BF969A4EC97ACAA (void);
// 0x00000018 System.String SimpleFileBrowser.FileBrowser::get_CurrentPath()
extern void FileBrowser_get_CurrentPath_m7AEBF65EEE1AAE67FC79949C9565CA382D5C1991 (void);
// 0x00000019 System.Void SimpleFileBrowser.FileBrowser::set_CurrentPath(System.String)
extern void FileBrowser_set_CurrentPath_m9AE094D4E74E17CC9CF900ED71D6316713E0253E (void);
// 0x0000001A System.String SimpleFileBrowser.FileBrowser::get_SearchString()
extern void FileBrowser_get_SearchString_m60BAB1281D2F408F902C290FEE3656CAC11ED288 (void);
// 0x0000001B System.Void SimpleFileBrowser.FileBrowser::set_SearchString(System.String)
extern void FileBrowser_set_SearchString_m20BFE4C41DA445EFA2C00947BE6D18EE3A4EE337 (void);
// 0x0000001C System.Boolean SimpleFileBrowser.FileBrowser::get_AcceptNonExistingFilename()
extern void FileBrowser_get_AcceptNonExistingFilename_mCC73584A920A441DE4DAF2D85CA7FC40A9815DBD (void);
// 0x0000001D System.Void SimpleFileBrowser.FileBrowser::set_AcceptNonExistingFilename(System.Boolean)
extern void FileBrowser_set_AcceptNonExistingFilename_mA5362B26AB27A67FD0BCFEC0B18839A1A8179F74 (void);
// 0x0000001E SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser::get_PickerMode()
extern void FileBrowser_get_PickerMode_mA44EDEDB67BDD91DC42FA68CDF8A554D6498E9FB (void);
// 0x0000001F System.Void SimpleFileBrowser.FileBrowser::set_PickerMode(SimpleFileBrowser.FileBrowser/PickMode)
extern void FileBrowser_set_PickerMode_m3D0E21378984533DDC35DE55B7B1FCB7634863F8 (void);
// 0x00000020 System.Boolean SimpleFileBrowser.FileBrowser::get_AllowMultiSelection()
extern void FileBrowser_get_AllowMultiSelection_m862C120260F12908D02FCE55A1FB9B87F558BEBB (void);
// 0x00000021 System.Void SimpleFileBrowser.FileBrowser::set_AllowMultiSelection(System.Boolean)
extern void FileBrowser_set_AllowMultiSelection_m6FDF012EE485D450F0B68E5A1D9827DF2E52B986 (void);
// 0x00000022 System.Boolean SimpleFileBrowser.FileBrowser::get_MultiSelectionToggleSelectionMode()
extern void FileBrowser_get_MultiSelectionToggleSelectionMode_m975C5D8A411A2755DF57798D39D2188FED2053F8 (void);
// 0x00000023 System.Void SimpleFileBrowser.FileBrowser::set_MultiSelectionToggleSelectionMode(System.Boolean)
extern void FileBrowser_set_MultiSelectionToggleSelectionMode_m176FE9DC46BDD2EB48C108D21831D1B08EB85934 (void);
// 0x00000024 System.String SimpleFileBrowser.FileBrowser::get_Title()
extern void FileBrowser_get_Title_m27BB4C891CE9E16CB8575D73DE6D4994A14261B4 (void);
// 0x00000025 System.Void SimpleFileBrowser.FileBrowser::set_Title(System.String)
extern void FileBrowser_set_Title_m8F51E153087C97D275799A74D1B9248938BA72C7 (void);
// 0x00000026 System.String SimpleFileBrowser.FileBrowser::get_SubmitButtonText()
extern void FileBrowser_get_SubmitButtonText_m20489A29D7521B14D2EF387064647C74D6BEE7FD (void);
// 0x00000027 System.Void SimpleFileBrowser.FileBrowser::set_SubmitButtonText(System.String)
extern void FileBrowser_set_SubmitButtonText_m0B06037B82E56030B481A207250EB04BF448B1DF (void);
// 0x00000028 System.Void SimpleFileBrowser.FileBrowser::Awake()
extern void FileBrowser_Awake_m7ED8D2EAF2FC13A2995A236FED771977A01E29DE (void);
// 0x00000029 System.Void SimpleFileBrowser.FileBrowser::OnRectTransformDimensionsChange()
extern void FileBrowser_OnRectTransformDimensionsChange_m7656558630069B8EAFE99EAC07037AADA4F4A24B (void);
// 0x0000002A System.Void SimpleFileBrowser.FileBrowser::LateUpdate()
extern void FileBrowser_LateUpdate_mB72B0AF4CEE098E1E1617CBC742556A0443A676A (void);
// 0x0000002B System.Void SimpleFileBrowser.FileBrowser::OnApplicationFocus(System.Boolean)
extern void FileBrowser_OnApplicationFocus_m3A55A6E53DF3CB09D533CC71B396CB2C8192007A (void);
// 0x0000002C SimpleFileBrowser.OnItemClickedHandler SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_OnItemClicked()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_OnItemClicked_m3571B66C7BCCCF5A952BC3B77ECA2EC8F7C31AE8 (void);
// 0x0000002D System.Void SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.set_OnItemClicked(SimpleFileBrowser.OnItemClickedHandler)
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_set_OnItemClicked_mAD8C5576E8F5DBEE4E9934E1E961646E18D13AD9 (void);
// 0x0000002E System.Int32 SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_Count()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_Count_m53920F2D439AB6729AACAAE8B10196C0E780DFEF (void);
// 0x0000002F System.Single SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_ItemHeight()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_ItemHeight_m87809A489D1604A41B6264EB197215F5CF1164FB (void);
// 0x00000030 SimpleFileBrowser.ListItem SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.CreateItem()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_CreateItem_m4B3594285FC355021488EDE0424C6A63CEF75909 (void);
// 0x00000031 System.Void SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.SetItemContent(SimpleFileBrowser.ListItem)
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_SetItemContent_mFABF6CE61B6A8A87B1A810287172E11B56BBD44B (void);
// 0x00000032 System.Void SimpleFileBrowser.FileBrowser::InitializeFiletypeIcons()
extern void FileBrowser_InitializeFiletypeIcons_m6EAA858F2C222CBED26172503C41A31D0A2A14D3 (void);
// 0x00000033 System.Void SimpleFileBrowser.FileBrowser::InitializeQuickLinks()
extern void FileBrowser_InitializeQuickLinks_m3E58E20D0485E8685D61E4A5C21A2E81C3FCB1AA (void);
// 0x00000034 System.Void SimpleFileBrowser.FileBrowser::RefreshDriveQuickLinks()
extern void FileBrowser_RefreshDriveQuickLinks_mFA2736938A9141A7AE8D4CCE963BF8599116025F (void);
// 0x00000035 System.Void SimpleFileBrowser.FileBrowser::OnBackButtonPressed()
extern void FileBrowser_OnBackButtonPressed_mE07193E07A9F83C1975D4B56C407B27656458CD6 (void);
// 0x00000036 System.Void SimpleFileBrowser.FileBrowser::OnForwardButtonPressed()
extern void FileBrowser_OnForwardButtonPressed_m2386A29CF07AFCB08198E40F6E3C5164F285EEA0 (void);
// 0x00000037 System.Void SimpleFileBrowser.FileBrowser::OnUpButtonPressed()
extern void FileBrowser_OnUpButtonPressed_m0D3B5941FF82CF88975BB820574CC3DBAF6F37B3 (void);
// 0x00000038 System.Void SimpleFileBrowser.FileBrowser::OnMoreOptionsButtonClicked()
extern void FileBrowser_OnMoreOptionsButtonClicked_mE6946C5641CAD3C5ADAACB66B59D184D586BCFFA (void);
// 0x00000039 System.Void SimpleFileBrowser.FileBrowser::OnContextMenuTriggered(UnityEngine.Vector2)
extern void FileBrowser_OnContextMenuTriggered_m24D91C79FC0A88DFAD5548607BD9B8C145C1E761 (void);
// 0x0000003A System.Void SimpleFileBrowser.FileBrowser::ShowContextMenuAt(UnityEngine.Vector2,System.Boolean)
extern void FileBrowser_ShowContextMenuAt_m40500C533570BA07A1126123B3B097C63C374249 (void);
// 0x0000003B System.Void SimpleFileBrowser.FileBrowser::OnSubmitButtonClicked()
extern void FileBrowser_OnSubmitButtonClicked_m7E848A843F3D025488C0EE6FF5E54EA6D57DCBE3 (void);
// 0x0000003C System.Void SimpleFileBrowser.FileBrowser::OnCancelButtonClicked()
extern void FileBrowser_OnCancelButtonClicked_mB5936764FEC583FA4F56C10619D3CF8BA60E96D8 (void);
// 0x0000003D System.Void SimpleFileBrowser.FileBrowser::OnOperationSuccessful(System.String[])
extern void FileBrowser_OnOperationSuccessful_m5DC93791ECD6CBA227C3FA4436DB0EBE91A27A6C (void);
// 0x0000003E System.Void SimpleFileBrowser.FileBrowser::OnOperationCanceled(System.Boolean)
extern void FileBrowser_OnOperationCanceled_m8D25FC9E848E384F3E878FF801A7993C36DF1947 (void);
// 0x0000003F System.Void SimpleFileBrowser.FileBrowser::OnPathChanged(System.String)
extern void FileBrowser_OnPathChanged_m690981D06142B84FA26FF5904150DB0CD3758D72 (void);
// 0x00000040 System.Void SimpleFileBrowser.FileBrowser::OnSearchStringChanged(System.String)
extern void FileBrowser_OnSearchStringChanged_m455D848F1D77E8509209BEEA041E6F0AF1D0796F (void);
// 0x00000041 System.Void SimpleFileBrowser.FileBrowser::OnFilterChanged()
extern void FileBrowser_OnFilterChanged_m54C7B8ABB2A479ECA7994A22D3FA484E279D8B9B (void);
// 0x00000042 System.Void SimpleFileBrowser.FileBrowser::OnShowHiddenFilesToggleChanged()
extern void FileBrowser_OnShowHiddenFilesToggleChanged_mA0BC97B0151F590135CAD8588EACC5AF6399A794 (void);
// 0x00000043 System.Void SimpleFileBrowser.FileBrowser::OnItemSelected(SimpleFileBrowser.FileBrowserItem,System.Boolean)
extern void FileBrowser_OnItemSelected_m169A9AE1DED1C198827C499C3DE7DE1C25D884F9 (void);
// 0x00000044 System.Void SimpleFileBrowser.FileBrowser::OnItemHeld(SimpleFileBrowser.FileBrowserItem)
extern void FileBrowser_OnItemHeld_m12287097633B233E71C322067B79BED7956395DB (void);
// 0x00000045 System.Char SimpleFileBrowser.FileBrowser::OnValidateFilenameInput(System.String,System.Int32,System.Char)
extern void FileBrowser_OnValidateFilenameInput_mF3FEA1E1138C07A818B30E19B5A988BF742C7922 (void);
// 0x00000046 System.Void SimpleFileBrowser.FileBrowser::OnFilenameInputChanged(System.String)
extern void FileBrowser_OnFilenameInputChanged_m8597E8B6648695042D028063BA767891987F90F4 (void);
// 0x00000047 System.Void SimpleFileBrowser.FileBrowser::Show(System.String,System.String)
extern void FileBrowser_Show_mFFC02C0EB9E1F56896F3AA2EE65AA8CB396FEE78 (void);
// 0x00000048 System.Void SimpleFileBrowser.FileBrowser::Hide()
extern void FileBrowser_Hide_mF5BFFE4E660A4C0BD26E4360676AE6C401B84335 (void);
// 0x00000049 System.Void SimpleFileBrowser.FileBrowser::RefreshFiles(System.Boolean)
extern void FileBrowser_RefreshFiles_m78E8F197E37E572167B58ECF89907B0F937711D3 (void);
// 0x0000004A System.Void SimpleFileBrowser.FileBrowser::SelectAllFiles()
extern void FileBrowser_SelectAllFiles_mE91B7524DDC69BE8825340880B581B63772A070B (void);
// 0x0000004B System.Void SimpleFileBrowser.FileBrowser::DeselectAllFiles()
extern void FileBrowser_DeselectAllFiles_m74394CF1CECC64623176BEDC0E0A85807DE5B526 (void);
// 0x0000004C System.Void SimpleFileBrowser.FileBrowser::CreateNewFolder()
extern void FileBrowser_CreateNewFolder_m93131A2F5C3BBD4AE007C8E0D6AB310F5405E626 (void);
// 0x0000004D System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::CreateNewFolderCoroutine()
extern void FileBrowser_CreateNewFolderCoroutine_mCC4E6964B0EEDC56E8C937CEBBFEAEA262C8581E (void);
// 0x0000004E System.Void SimpleFileBrowser.FileBrowser::RenameSelectedFile()
extern void FileBrowser_RenameSelectedFile_m0BDC939786A45A504F9C74459EC23882ACD3A25B (void);
// 0x0000004F System.Void SimpleFileBrowser.FileBrowser::DeleteSelectedFiles()
extern void FileBrowser_DeleteSelectedFiles_mB3E6B271A957C4519AA8A9AFDBABBB7863520A17 (void);
// 0x00000050 System.Void SimpleFileBrowser.FileBrowser::PersistFileEntrySelection()
extern void FileBrowser_PersistFileEntrySelection_m8A55E481241BD19BA81EC14E6B88F9B1557A9EDA (void);
// 0x00000051 System.Boolean SimpleFileBrowser.FileBrowser::AddQuickLink(UnityEngine.Sprite,System.String,System.String)
extern void FileBrowser_AddQuickLink_mB4F5AF2799B7717685A57C2DDF444E319DE55B81 (void);
// 0x00000052 System.Void SimpleFileBrowser.FileBrowser::EnsureWindowIsWithinBounds()
extern void FileBrowser_EnsureWindowIsWithinBounds_m0C2BE0125CCF95DC08987F4EEC1334949F33560D (void);
// 0x00000053 System.Void SimpleFileBrowser.FileBrowser::OnWindowDimensionsChanged(UnityEngine.Vector2)
extern void FileBrowser_OnWindowDimensionsChanged_mDFABDE888D6BF3701A840871D2A202DAF6279AB3 (void);
// 0x00000054 UnityEngine.Sprite SimpleFileBrowser.FileBrowser::GetIconForFileEntry(SimpleFileBrowser.FileSystemEntry)
extern void FileBrowser_GetIconForFileEntry_m27CDFFF69366A6DD7809BBDA3D49DED0B92DFEC0 (void);
// 0x00000055 System.String SimpleFileBrowser.FileBrowser::GetPathWithoutTrailingDirectorySeparator(System.String)
extern void FileBrowser_GetPathWithoutTrailingDirectorySeparator_m2C5C9B65DF3EDBB6DF9B415815F037C88832C5BE (void);
// 0x00000056 System.Void SimpleFileBrowser.FileBrowser::UpdateFilenameInputFieldWithSelection()
extern void FileBrowser_UpdateFilenameInputFieldWithSelection_mE42C35DBAC32BD4EA372AB25A9F1E8C01BB0A5FF (void);
// 0x00000057 System.Int32 SimpleFileBrowser.FileBrowser::ExtractFilenameFromInput(System.String,System.Int32&,System.Int32&)
extern void FileBrowser_ExtractFilenameFromInput_m839AB82ACA752B225AF295B030B0242D8E0BBF0B (void);
// 0x00000058 System.Int32 SimpleFileBrowser.FileBrowser::FilenameInputToFileEntryIndex(System.String,System.Int32,System.Int32)
extern void FileBrowser_FilenameInputToFileEntryIndex_m885C3DEEE0F74753CA2548DCF5D688464B160B2B (void);
// 0x00000059 System.Boolean SimpleFileBrowser.FileBrowser::VerifyFilenameInput(System.String,System.Int32,System.Int32)
extern void FileBrowser_VerifyFilenameInput_m91328D6BB9E00AA43E5BFDD0978C74846F771CDA (void);
// 0x0000005A System.Int32 SimpleFileBrowser.FileBrowser::CalculateLengthOfDropdownText(System.String)
extern void FileBrowser_CalculateLengthOfDropdownText_m03EEF8FBFE5FCDE52EC3C58157123E6E4F19D026 (void);
// 0x0000005B System.String SimpleFileBrowser.FileBrowser::GetInitialPath(System.String)
extern void FileBrowser_GetInitialPath_m423E2A4E4AA334B2E2181314CB37C2B614DA256F (void);
// 0x0000005C System.Boolean SimpleFileBrowser.FileBrowser::ShowSaveDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowSaveDialog_m7173D28C3661A93392959530E47CEFB5D98668BC (void);
// 0x0000005D System.Boolean SimpleFileBrowser.FileBrowser::ShowLoadDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowLoadDialog_mF6FAC4970BFB711D99DDAAC88482646D3E2F22EF (void);
// 0x0000005E System.Boolean SimpleFileBrowser.FileBrowser::ShowDialogInternal(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowDialogInternal_mF604390B094BB86EB2D3397F33F9E351C19EE726 (void);
// 0x0000005F System.Void SimpleFileBrowser.FileBrowser::HideDialog(System.Boolean)
extern void FileBrowser_HideDialog_m458ECE69A2F2A5830447658A70C93B55790D9278 (void);
// 0x00000060 System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::WaitForSaveDialog(SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_WaitForSaveDialog_mF25AD7B77953DF39D01DA92FB79834A9EFFF6D54 (void);
// 0x00000061 System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::WaitForLoadDialog(SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_WaitForLoadDialog_mA68EC39DDC056025018E9CC48192D2F399E248DD (void);
// 0x00000062 System.Boolean SimpleFileBrowser.FileBrowser::AddQuickLink(System.String,System.String,UnityEngine.Sprite)
extern void FileBrowser_AddQuickLink_mB183F567E4531F2EF7BCD3EE41878D1557C2315B (void);
// 0x00000063 System.Void SimpleFileBrowser.FileBrowser::SetExcludedExtensions(System.String[])
extern void FileBrowser_SetExcludedExtensions_mB83CC9D59130EB004C990C5328155A9629927B9E (void);
// 0x00000064 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.Collections.Generic.IEnumerable`1<System.String>)
extern void FileBrowser_SetFilters_mBBC708C41D6DE39D512917F1D5A778C305688328 (void);
// 0x00000065 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.String[])
extern void FileBrowser_SetFilters_mD0616C3BCDFC45906EE75274FDFA62468B6E6D41 (void);
// 0x00000066 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.Collections.Generic.IEnumerable`1<SimpleFileBrowser.FileBrowser/Filter>)
extern void FileBrowser_SetFilters_m9D8F272840FF79ACB7531F2E008704945C7DFFF3 (void);
// 0x00000067 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,SimpleFileBrowser.FileBrowser/Filter[])
extern void FileBrowser_SetFilters_m0A68D90202F335892C3BEFFAF50688209445E7C5 (void);
// 0x00000068 System.Void SimpleFileBrowser.FileBrowser::SetFiltersPreProcessing(System.Boolean)
extern void FileBrowser_SetFiltersPreProcessing_mD5434AD892C3D13CB90303E66A0B639497374D89 (void);
// 0x00000069 System.Void SimpleFileBrowser.FileBrowser::SetFiltersPostProcessing()
extern void FileBrowser_SetFiltersPostProcessing_mFF54FFC1A0B4B97001FC3EBA2773B7948BEC1AB3 (void);
// 0x0000006A System.Boolean SimpleFileBrowser.FileBrowser::SetDefaultFilter(System.String)
extern void FileBrowser_SetDefaultFilter_m195C08EB42C76D931274E4C25F40D090C7C26E2C (void);
// 0x0000006B SimpleFileBrowser.FileBrowser/Permission SimpleFileBrowser.FileBrowser::CheckPermission()
extern void FileBrowser_CheckPermission_m410430CFDB6701AD4CCBFF9A5330355E1800CE72 (void);
// 0x0000006C SimpleFileBrowser.FileBrowser/Permission SimpleFileBrowser.FileBrowser::RequestPermission()
extern void FileBrowser_RequestPermission_mF5AEA2E34D75233F6A329C09AC4D361D1BFB3234 (void);
// 0x0000006D System.Void SimpleFileBrowser.FileBrowser::.ctor()
extern void FileBrowser__ctor_mE67BF18A7571B5FE7D82A7454AAB098D9A1AACD2 (void);
// 0x0000006E System.Void SimpleFileBrowser.FileBrowser::.cctor()
extern void FileBrowser__cctor_m1B9B68ACDB4CCBFE95C82137BD115FEC79C2E235 (void);
// 0x0000006F System.Void SimpleFileBrowser.FileBrowser::<CreateNewFolderCoroutine>b__212_0(System.String)
extern void FileBrowser_U3CCreateNewFolderCoroutineU3Eb__212_0_m169CB5F3591084983F80B0CF13A9B169009B2837 (void);
// 0x00000070 System.Void SimpleFileBrowser.FileBrowser::<DeleteSelectedFiles>b__214_0()
extern void FileBrowser_U3CDeleteSelectedFilesU3Eb__214_0_m1B38C260089E573783018BAA135BAD4B85DDD622 (void);
// 0x00000071 System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String)
extern void Filter__ctor_m1074FC06CD79E457F25F52026D634FEBF9ACCF10 (void);
// 0x00000072 System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String)
extern void Filter__ctor_m38D854E9B7172B814710C305CA0857452A2DE121 (void);
// 0x00000073 System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String[])
extern void Filter__ctor_m125246E9D958C713FF3D86B5E02C9D6540B58185 (void);
// 0x00000074 System.String SimpleFileBrowser.FileBrowser/Filter::ToString()
extern void Filter_ToString_m9DCF19E38E806A743CA49A330EBEA08814B62753 (void);
// 0x00000075 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::.ctor(System.Object,System.IntPtr)
extern void OnSuccess__ctor_m71AD74DD97BD2E317E6CCD78F0B631AC4D3104D7 (void);
// 0x00000076 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::Invoke(System.String[])
extern void OnSuccess_Invoke_m7AC28204C5A1715F07274D5F73B66E39CD7D1317 (void);
// 0x00000077 System.IAsyncResult SimpleFileBrowser.FileBrowser/OnSuccess::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern void OnSuccess_BeginInvoke_m99BB5894F15FCDD0F04BD6321CB1E7F19EF28FFE (void);
// 0x00000078 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::EndInvoke(System.IAsyncResult)
extern void OnSuccess_EndInvoke_mB36C87C496674F8F56D681D6E25384CAB0F18CBC (void);
// 0x00000079 System.Void SimpleFileBrowser.FileBrowser/OnCancel::.ctor(System.Object,System.IntPtr)
extern void OnCancel__ctor_m1B12099B6EC65CE377D0A854F92BC24145CCF42C (void);
// 0x0000007A System.Void SimpleFileBrowser.FileBrowser/OnCancel::Invoke()
extern void OnCancel_Invoke_m91F1A69EF5776DCA7D9A5C5EC9A03C925CDEBD49 (void);
// 0x0000007B System.IAsyncResult SimpleFileBrowser.FileBrowser/OnCancel::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCancel_BeginInvoke_m21D3022399AFFCF3CFC9926B0597AE814B2E380C (void);
// 0x0000007C System.Void SimpleFileBrowser.FileBrowser/OnCancel::EndInvoke(System.IAsyncResult)
extern void OnCancel_EndInvoke_m11BD247D9B4669B70D8D3116C309BB8EE08023F4 (void);
// 0x0000007D System.Void SimpleFileBrowser.FileBrowser/<>c::.cctor()
extern void U3CU3Ec__cctor_m366263627B30E5A7F4830C9CFAF7D47DBF886CF7 (void);
// 0x0000007E System.Void SimpleFileBrowser.FileBrowser/<>c::.ctor()
extern void U3CU3Ec__ctor_m0C7FD05A63A799955E2C175B16DD6E2FA642261E (void);
// 0x0000007F System.Int32 SimpleFileBrowser.FileBrowser/<>c::<RefreshFiles>b__208_0(SimpleFileBrowser.FileSystemEntry,SimpleFileBrowser.FileSystemEntry)
extern void U3CU3Ec_U3CRefreshFilesU3Eb__208_0_mE257F311C6FC4C025CB3BFE2C3B1E6E21F4B5DE8 (void);
// 0x00000080 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::.ctor(System.Int32)
extern void U3CCreateNewFolderCoroutineU3Ed__212__ctor_m9416FA4180A784E6A783386ED3724C5E1DFB84B0 (void);
// 0x00000081 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::System.IDisposable.Dispose()
extern void U3CCreateNewFolderCoroutineU3Ed__212_System_IDisposable_Dispose_m861842100F9A6B23C05F9DE57224BC80A09D897D (void);
// 0x00000082 System.Boolean SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::MoveNext()
extern void U3CCreateNewFolderCoroutineU3Ed__212_MoveNext_m3C8E1659613DF5B2E6B9595D445DDB2B9FAE48AB (void);
// 0x00000083 System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateNewFolderCoroutineU3Ed__212_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC77AE5F347747A0E3CBD1D2E97411A49FC1DCC47 (void);
// 0x00000084 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::System.Collections.IEnumerator.Reset()
extern void U3CCreateNewFolderCoroutineU3Ed__212_System_Collections_IEnumerator_Reset_m36AB2D43644CE8533A09A453FC2987FFEAD355A3 (void);
// 0x00000085 System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::System.Collections.IEnumerator.get_Current()
extern void U3CCreateNewFolderCoroutineU3Ed__212_System_Collections_IEnumerator_get_Current_mC7C28B566ABB43074905E5D00C91BC91AE057106 (void);
// 0x00000086 System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass213_0::.ctor()
extern void U3CU3Ec__DisplayClass213_0__ctor_mFF5EA87575F29C29592FF99C4F6EA9C4F5E50FCE (void);
// 0x00000087 System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass213_0::<RenameSelectedFile>b__0(System.String)
extern void U3CU3Ec__DisplayClass213_0_U3CRenameSelectedFileU3Eb__0_mB759C986F3523C0EB6F69B06CA4D680C0805CBD0 (void);
// 0x00000088 System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::.ctor(System.Int32)
extern void U3CWaitForSaveDialogU3Ed__231__ctor_m4D13DBAAD39E45924438AE6BF60C5550BEED425F (void);
// 0x00000089 System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::System.IDisposable.Dispose()
extern void U3CWaitForSaveDialogU3Ed__231_System_IDisposable_Dispose_m915ACB6811934B1C52C24E797972E85DECA210DA (void);
// 0x0000008A System.Boolean SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::MoveNext()
extern void U3CWaitForSaveDialogU3Ed__231_MoveNext_mD3F0EDC2048CE261FFD12CE666289AF1E818CC71 (void);
// 0x0000008B System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSaveDialogU3Ed__231_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB128DE990CFBCB0D31510A56597565A527520CCD (void);
// 0x0000008C System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSaveDialogU3Ed__231_System_Collections_IEnumerator_Reset_m8773BD2220230AACB064062C84F4B5775FF52FC4 (void);
// 0x0000008D System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSaveDialogU3Ed__231_System_Collections_IEnumerator_get_Current_mC7BA754F2B85A434AEBEAF359E35A59C1785C68F (void);
// 0x0000008E System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::.ctor(System.Int32)
extern void U3CWaitForLoadDialogU3Ed__232__ctor_m5F9902D83C49E785B9A2431C0258EE15D6C17B3C (void);
// 0x0000008F System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::System.IDisposable.Dispose()
extern void U3CWaitForLoadDialogU3Ed__232_System_IDisposable_Dispose_m341495EE610B41B00EACF073111B65F6953FB20C (void);
// 0x00000090 System.Boolean SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::MoveNext()
extern void U3CWaitForLoadDialogU3Ed__232_MoveNext_mA785D39AE2F2FFD91FA6A593FD428C7A16C25C1C (void);
// 0x00000091 System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForLoadDialogU3Ed__232_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B7ECD9F510B0D843F6DBACD9B305A2FE5AE2694 (void);
// 0x00000092 System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::System.Collections.IEnumerator.Reset()
extern void U3CWaitForLoadDialogU3Ed__232_System_Collections_IEnumerator_Reset_mD7EBFD14824CD4612FBD67C4DA21FC8871DF2B80 (void);
// 0x00000093 System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForLoadDialogU3Ed__232_System_Collections_IEnumerator_get_Current_mDB33A1EF5FF53F8969143599B3BF9CD4D120F8E4 (void);
// 0x00000094 System.Void SimpleFileBrowser.FileBrowserContextMenu::Show(System.Boolean,System.Boolean,System.Boolean,System.Boolean,UnityEngine.Vector2,System.Boolean)
extern void FileBrowserContextMenu_Show_m048F7147CB835F992FF2017662D8557D74937ED1 (void);
// 0x00000095 System.Void SimpleFileBrowser.FileBrowserContextMenu::Hide()
extern void FileBrowserContextMenu_Hide_m74048A668955B61CF3C72AAA635387BD82631F64 (void);
// 0x00000096 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnSelectAllButtonClicked()
extern void FileBrowserContextMenu_OnSelectAllButtonClicked_mA30D36E00D69D1EF1F7083A1D3F9C4E6E6BDA4B0 (void);
// 0x00000097 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnDeselectAllButtonClicked()
extern void FileBrowserContextMenu_OnDeselectAllButtonClicked_mB45F767EE9CD38BA3F1235D0D4B3CDC74D5562F9 (void);
// 0x00000098 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnCreateFolderButtonClicked()
extern void FileBrowserContextMenu_OnCreateFolderButtonClicked_mD6CBE83296EB54560F9E3F933AFD521312D07376 (void);
// 0x00000099 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnDeleteButtonClicked()
extern void FileBrowserContextMenu_OnDeleteButtonClicked_m99ACE4623BD9BFAFC79B94AA484D60CBEE2D91AB (void);
// 0x0000009A System.Void SimpleFileBrowser.FileBrowserContextMenu::OnRenameButtonClicked()
extern void FileBrowserContextMenu_OnRenameButtonClicked_m37DEA61748E86D2F41D6013F1106CE72AC6D5D00 (void);
// 0x0000009B System.Void SimpleFileBrowser.FileBrowserContextMenu::.ctor()
extern void FileBrowserContextMenu__ctor_m0CE266A05467D4C59C3BBD3E3D9A65AF477D995F (void);
// 0x0000009C System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IPointerEnterHandler.OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m8C73CE93FA4E67CCCC8481664F14D16975025F88 (void);
// 0x0000009D System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IPointerExitHandler.OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m6AB7CACBBF9735A5D824DB4A72D9925BF3F11FF1 (void);
// 0x0000009E System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IBeginDragHandler.OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_m27DAB75B178336A69E5B81F8779D2DA1F8E467B0 (void);
// 0x0000009F System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IEndDragHandler.OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_mAC258EBDA04A00D20023D84799524BDEFBADFCE7 (void);
// 0x000000A0 System.Void SimpleFileBrowser.FileBrowserCursorHandler::ShowDefaultCursor()
extern void FileBrowserCursorHandler_ShowDefaultCursor_m9CD6A3784F2ABC645DF9E5B572175EDE5F04EDD6 (void);
// 0x000000A1 System.Void SimpleFileBrowser.FileBrowserCursorHandler::ShowResizeCursor()
extern void FileBrowserCursorHandler_ShowResizeCursor_m6189764CC19F2BAF0DF7F36C571518D43120DF6D (void);
// 0x000000A2 System.Void SimpleFileBrowser.FileBrowserCursorHandler::.ctor()
extern void FileBrowserCursorHandler__ctor_m700BE779271111CF7F82596D9B059379BF6D9B79 (void);
// 0x000000A3 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::Show(SimpleFileBrowser.FileBrowser,System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry>,System.Collections.Generic.List`1<System.Int32>,SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed)
extern void FileBrowserDeleteConfirmationPanel_Show_m7A414AF936D5796ED4E75EECC83A2F28F39B188F (void);
// 0x000000A4 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::OnCanvasDimensionsChanged(UnityEngine.Vector2)
extern void FileBrowserDeleteConfirmationPanel_OnCanvasDimensionsChanged_m494AFB7F1030E6F1B6F4F9766ECF188DEF2ABDF1 (void);
// 0x000000A5 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::YesButtonClicked()
extern void FileBrowserDeleteConfirmationPanel_YesButtonClicked_m137962390E987A7EE4F6510EF754C17E52AB7DDD (void);
// 0x000000A6 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::NoButtonClicked()
extern void FileBrowserDeleteConfirmationPanel_NoButtonClicked_m1A36579C3C7A9CD5E1712DA12D699ACE4FC9E856 (void);
// 0x000000A7 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::.ctor()
extern void FileBrowserDeleteConfirmationPanel__ctor_m70D71C94A46C8D66FF99880FFE8635E9A86925B8 (void);
// 0x000000A8 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::.ctor(System.Object,System.IntPtr)
extern void OnDeletionConfirmed__ctor_mB63BE1ED5553DAC5AE773722E1A1DF2D1D218082 (void);
// 0x000000A9 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::Invoke()
extern void OnDeletionConfirmed_Invoke_mC6D0FBA98BB13C38C0608939A4D7B90D74149D33 (void);
// 0x000000AA System.IAsyncResult SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDeletionConfirmed_BeginInvoke_m01E257578F12D0E1F92B58568CE2BC6D27B99DCE (void);
// 0x000000AB System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::EndInvoke(System.IAsyncResult)
extern void OnDeletionConfirmed_EndInvoke_mC32E70F5F89EDEBBA1C9CFFE8084C6D9C163E154 (void);
// 0x000000AC System.Boolean SimpleFileBrowser.FileSystemEntry::get_IsDirectory()
extern void FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F (void);
// 0x000000AD System.Void SimpleFileBrowser.FileSystemEntry::.ctor(System.String,System.String,System.Boolean)
extern void FileSystemEntry__ctor_mF0D709ED4A4F496A5E8CC475F8E1FC967AD49B9E (void);
// 0x000000AE System.Void SimpleFileBrowser.FileSystemEntry::.ctor(System.IO.FileSystemInfo)
extern void FileSystemEntry__ctor_m74190ABC21BA187496BA91DDAB1FE9C58BAE7D08 (void);
// 0x000000AF System.Boolean SimpleFileBrowser.FileBrowserHelpers::FileExists(System.String)
extern void FileBrowserHelpers_FileExists_m9D52915118B1AEB6D91D0F7D57AF305FF6688044 (void);
// 0x000000B0 System.Boolean SimpleFileBrowser.FileBrowserHelpers::DirectoryExists(System.String)
extern void FileBrowserHelpers_DirectoryExists_m6BA2B800C6CA754CB1BC3AE06E5BB11B21BB66AE (void);
// 0x000000B1 System.Boolean SimpleFileBrowser.FileBrowserHelpers::IsDirectory(System.String)
extern void FileBrowserHelpers_IsDirectory_m0E85C3971D327E8F1BFD3B6F986FF567DEDAB11B (void);
// 0x000000B2 System.String SimpleFileBrowser.FileBrowserHelpers::GetDirectoryName(System.String)
extern void FileBrowserHelpers_GetDirectoryName_m99B775DBBFAA040EC24F8CACBC4337F2D479881B (void);
// 0x000000B3 SimpleFileBrowser.FileSystemEntry[] SimpleFileBrowser.FileBrowserHelpers::GetEntriesInDirectory(System.String)
extern void FileBrowserHelpers_GetEntriesInDirectory_m9FFDF203835C5C9B2418E9D0DD8BDECB29B07656 (void);
// 0x000000B4 System.String SimpleFileBrowser.FileBrowserHelpers::CreateFileInDirectory(System.String,System.String)
extern void FileBrowserHelpers_CreateFileInDirectory_m1C7DD9921217C8E1E5D641C1D8CBF87040F34335 (void);
// 0x000000B5 System.String SimpleFileBrowser.FileBrowserHelpers::CreateFolderInDirectory(System.String,System.String)
extern void FileBrowserHelpers_CreateFolderInDirectory_m42BAE776231D65524E9F2B5C4CD37D27427448C8 (void);
// 0x000000B6 System.Void SimpleFileBrowser.FileBrowserHelpers::WriteBytesToFile(System.String,System.Byte[])
extern void FileBrowserHelpers_WriteBytesToFile_mC398A6AB4FE293E59E25A506C1773E6101A3F186 (void);
// 0x000000B7 System.Void SimpleFileBrowser.FileBrowserHelpers::WriteTextToFile(System.String,System.String)
extern void FileBrowserHelpers_WriteTextToFile_mFDEC4257FD9346411B0CFEA1A2F738F3C924266C (void);
// 0x000000B8 System.Void SimpleFileBrowser.FileBrowserHelpers::AppendBytesToFile(System.String,System.Byte[])
extern void FileBrowserHelpers_AppendBytesToFile_m58453582275E21279E214CE9898E435C61AF5F8F (void);
// 0x000000B9 System.Void SimpleFileBrowser.FileBrowserHelpers::AppendTextToFile(System.String,System.String)
extern void FileBrowserHelpers_AppendTextToFile_mEE9EB50ACDE0288954557048A86DC7EA4D02D13E (void);
// 0x000000BA System.Void SimpleFileBrowser.FileBrowserHelpers::AppendFileToFile(System.String,System.String)
extern void FileBrowserHelpers_AppendFileToFile_mA5235981BDE3C6BB928F39525218E150685B8160 (void);
// 0x000000BB System.Byte[] SimpleFileBrowser.FileBrowserHelpers::ReadBytesFromFile(System.String)
extern void FileBrowserHelpers_ReadBytesFromFile_m20F09F56CC0DF8AF5F55EABACFE0BCDBB9A76E64 (void);
// 0x000000BC System.String SimpleFileBrowser.FileBrowserHelpers::ReadTextFromFile(System.String)
extern void FileBrowserHelpers_ReadTextFromFile_m035E5869B8DB91920790A367F0C0DA3649E52975 (void);
// 0x000000BD System.Void SimpleFileBrowser.FileBrowserHelpers::CopyFile(System.String,System.String)
extern void FileBrowserHelpers_CopyFile_mCF4B10B93E0D2DE1A028956B1C00AF8618FC57EB (void);
// 0x000000BE System.Void SimpleFileBrowser.FileBrowserHelpers::CopyDirectory(System.String,System.String)
extern void FileBrowserHelpers_CopyDirectory_m32E8286BECDD086893DD79B46C1B31A24381A0A6 (void);
// 0x000000BF System.Void SimpleFileBrowser.FileBrowserHelpers::CopyDirectoryRecursively(System.IO.DirectoryInfo,System.String)
extern void FileBrowserHelpers_CopyDirectoryRecursively_mA7F7A1DC8EE65626F11A6A19E1B9F871A9507A38 (void);
// 0x000000C0 System.Void SimpleFileBrowser.FileBrowserHelpers::MoveFile(System.String,System.String)
extern void FileBrowserHelpers_MoveFile_mA9DEB625A976831CD765B701F18CF0DBAE5CB59C (void);
// 0x000000C1 System.Void SimpleFileBrowser.FileBrowserHelpers::MoveDirectory(System.String,System.String)
extern void FileBrowserHelpers_MoveDirectory_mC400BA412A8CA381828DBACA19BDC2694E77206A (void);
// 0x000000C2 System.String SimpleFileBrowser.FileBrowserHelpers::RenameFile(System.String,System.String)
extern void FileBrowserHelpers_RenameFile_m2175A6933C34270587AEAFDA3B790E64CECE89B2 (void);
// 0x000000C3 System.String SimpleFileBrowser.FileBrowserHelpers::RenameDirectory(System.String,System.String)
extern void FileBrowserHelpers_RenameDirectory_m0EB0855EC112F9D79416E986E75DE00885AD1D7A (void);
// 0x000000C4 System.Void SimpleFileBrowser.FileBrowserHelpers::DeleteFile(System.String)
extern void FileBrowserHelpers_DeleteFile_m42F1CEE70904E259557F27B72D8AE6584ECB1AB2 (void);
// 0x000000C5 System.Void SimpleFileBrowser.FileBrowserHelpers::DeleteDirectory(System.String)
extern void FileBrowserHelpers_DeleteDirectory_mCE4FA56EDEDA054174A483910A8061EE4C67E7BD (void);
// 0x000000C6 System.String SimpleFileBrowser.FileBrowserHelpers::GetFilename(System.String)
extern void FileBrowserHelpers_GetFilename_m7BA7650CF4DAFD74BDDDED8EFAB7FD4C1E3F0A06 (void);
// 0x000000C7 System.Int64 SimpleFileBrowser.FileBrowserHelpers::GetFilesize(System.String)
extern void FileBrowserHelpers_GetFilesize_m2B7C3C550C55A4AAAC8CD511367050643943B24B (void);
// 0x000000C8 System.DateTime SimpleFileBrowser.FileBrowserHelpers::GetLastModifiedDate(System.String)
extern void FileBrowserHelpers_GetLastModifiedDate_m4379195FAB7835E2F549FFFBD185B2EED5A7041D (void);
// 0x000000C9 UnityEngine.RectTransform SimpleFileBrowser.FileBrowserItem::get_TransformComponent()
extern void FileBrowserItem_get_TransformComponent_mEDD8CD474DBBF4B58E11D1BE82C255CAE65141CA (void);
// 0x000000CA System.String SimpleFileBrowser.FileBrowserItem::get_Name()
extern void FileBrowserItem_get_Name_m1F09E0A5E84870485231447173A5AF496DC0E01F (void);
// 0x000000CB System.Boolean SimpleFileBrowser.FileBrowserItem::get_IsDirectory()
extern void FileBrowserItem_get_IsDirectory_mC40A8B51825BC34EB34D45C72D35A1CA6E1A89BB (void);
// 0x000000CC System.Void SimpleFileBrowser.FileBrowserItem::set_IsDirectory(System.Boolean)
extern void FileBrowserItem_set_IsDirectory_mDB0D5D005A9C6F689AF30D6A63E8AE555FFF6DF6 (void);
// 0x000000CD System.Void SimpleFileBrowser.FileBrowserItem::SetFileBrowser(SimpleFileBrowser.FileBrowser)
extern void FileBrowserItem_SetFileBrowser_m0BC28D0644C40780C1C4ECA5D3A768B23D10DD4C (void);
// 0x000000CE System.Void SimpleFileBrowser.FileBrowserItem::SetFile(UnityEngine.Sprite,System.String,System.Boolean)
extern void FileBrowserItem_SetFile_m1F1468944380FAE443BB546ADF30AC56B3C6616D (void);
// 0x000000CF System.Void SimpleFileBrowser.FileBrowserItem::Update()
extern void FileBrowserItem_Update_m3A16C87E26FCD94FB63A74DBD6F10ED09E6D7FC4 (void);
// 0x000000D0 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerClick_m324DF382072BE76D6FFDBE62E8C2DFABD47A6065 (void);
// 0x000000D1 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerDown_m4EA10AF080820CE9779BDE00E61CFF69C9BE4BF8 (void);
// 0x000000D2 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerUp_m7CEDD0C86CCBB748225E07597536AB8FB9D8436A (void);
// 0x000000D3 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerEnter_mCEE10023A5B9C505A3B18ADA2B9D83CF7F74A7B7 (void);
// 0x000000D4 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerExit_m6D8F537184562578F627250C4CC9880154FBA04F (void);
// 0x000000D5 System.Void SimpleFileBrowser.FileBrowserItem::SetSelected(System.Boolean)
extern void FileBrowserItem_SetSelected_mF1A4735B8F234281F0698E5C0DA0B9575C0D4C28 (void);
// 0x000000D6 System.Void SimpleFileBrowser.FileBrowserItem::SetHidden(System.Boolean)
extern void FileBrowserItem_SetHidden_m91D66EE00C0EEAF1FFEB6C9220BF363310271F28 (void);
// 0x000000D7 System.Void SimpleFileBrowser.FileBrowserItem::.ctor()
extern void FileBrowserItem__ctor_m78CFC23F9073522F0BBFB7F9792BAA0A46362454 (void);
// 0x000000D8 System.Void SimpleFileBrowser.FileBrowserMovement::Initialize(SimpleFileBrowser.FileBrowser)
extern void FileBrowserMovement_Initialize_m471ABE4C29F9D705461783AB33F2CC921AC51478 (void);
// 0x000000D9 System.Void SimpleFileBrowser.FileBrowserMovement::OnDragStarted(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnDragStarted_m3E0201898C42B961B9D8194C95DDB8EEA8555B65 (void);
// 0x000000DA System.Void SimpleFileBrowser.FileBrowserMovement::OnDrag(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnDrag_mD8D2918E9F95EFFDCC104E321E5B957850743AF8 (void);
// 0x000000DB System.Void SimpleFileBrowser.FileBrowserMovement::OnEndDrag(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnEndDrag_m443E3696DBB3571E87FC1B616BDE956C5F3A506E (void);
// 0x000000DC System.Void SimpleFileBrowser.FileBrowserMovement::OnResizeStarted(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnResizeStarted_mB31A17958B7A24A16CBE1B21D9B22451A2C0D486 (void);
// 0x000000DD System.Void SimpleFileBrowser.FileBrowserMovement::OnResize(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnResize_m49E6F628633974DE0F8E1737D127FB03BE774853 (void);
// 0x000000DE System.Void SimpleFileBrowser.FileBrowserMovement::OnEndResize(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnEndResize_mFB53FC3931D90175FE576DA10B16F94CD75BEC27 (void);
// 0x000000DF System.Void SimpleFileBrowser.FileBrowserMovement::.ctor()
extern void FileBrowserMovement__ctor_mB4D1B389EACB5128B7FC728B439AFE488642E95D (void);
// 0x000000E0 System.String SimpleFileBrowser.FileBrowserQuickLink::get_TargetPath()
extern void FileBrowserQuickLink_get_TargetPath_mD1CA8CA83A2B19226D00B8E77BC024A57D0C4C91 (void);
// 0x000000E1 System.Void SimpleFileBrowser.FileBrowserQuickLink::SetQuickLink(UnityEngine.Sprite,System.String,System.String)
extern void FileBrowserQuickLink_SetQuickLink_mDEDE06D8E3C9A90193FB71E57336A68E92895350 (void);
// 0x000000E2 System.Void SimpleFileBrowser.FileBrowserQuickLink::.ctor()
extern void FileBrowserQuickLink__ctor_mCED0CE4E2EDD195AAB3F2BBA553F9EFC9715D8A5 (void);
// 0x000000E3 System.Void SimpleFileBrowser.FileBrowserRenamedItem::Show(System.String,UnityEngine.Color,UnityEngine.Sprite,SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted)
extern void FileBrowserRenamedItem_Show_m67CF833D01BEDC56743DF4EAC46AF60E8FFFB5DB (void);
// 0x000000E4 System.Void SimpleFileBrowser.FileBrowserRenamedItem::OnInputFieldEndEdit(System.String)
extern void FileBrowserRenamedItem_OnInputFieldEndEdit_mB5AC8C49EEF73640F34CB94F2D5FA62F1C48C366 (void);
// 0x000000E5 System.Void SimpleFileBrowser.FileBrowserRenamedItem::.ctor()
extern void FileBrowserRenamedItem__ctor_m32B6017C97863E0349A0D4B61D355CD9EFECE11B (void);
// 0x000000E6 System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::.ctor(System.Object,System.IntPtr)
extern void OnRenameCompleted__ctor_m019658E790544491DDBD4F208F8B422DC77764EB (void);
// 0x000000E7 System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::Invoke(System.String)
extern void OnRenameCompleted_Invoke_m8A1B85475DE7D5362CB711D63543CDD6A68A7CDB (void);
// 0x000000E8 System.IAsyncResult SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void OnRenameCompleted_BeginInvoke_m090E447625A7D2DCE9AE24887390FB68BAD85E31 (void);
// 0x000000E9 System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::EndInvoke(System.IAsyncResult)
extern void OnRenameCompleted_EndInvoke_mEC5511E093D8247D914A8802352DC8DA57CFB1B2 (void);
// 0x000000EA System.Void SimpleFileBrowser.NonDrawingGraphic::SetMaterialDirty()
extern void NonDrawingGraphic_SetMaterialDirty_m329A03BC12E9779085DC0D91E2DE2582127E8395 (void);
// 0x000000EB System.Void SimpleFileBrowser.NonDrawingGraphic::SetVerticesDirty()
extern void NonDrawingGraphic_SetVerticesDirty_mD732D935D4951495843EC5A9EA1DB19665415793 (void);
// 0x000000EC System.Void SimpleFileBrowser.NonDrawingGraphic::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void NonDrawingGraphic_OnPopulateMesh_m1C0A9BA5BE3AE3AAC6CBFE8F742E4AB51823198C (void);
// 0x000000ED System.Void SimpleFileBrowser.NonDrawingGraphic::.ctor()
extern void NonDrawingGraphic__ctor_m995D20F1C7EA51EC17D9E9D5F8F10A226597D6BB (void);
// 0x000000EE System.Void SimpleFileBrowser.OnItemClickedHandler::.ctor(System.Object,System.IntPtr)
extern void OnItemClickedHandler__ctor_m2E672645F74ACB3CB0B7F152205B647BCB399FAC (void);
// 0x000000EF System.Void SimpleFileBrowser.OnItemClickedHandler::Invoke(SimpleFileBrowser.ListItem)
extern void OnItemClickedHandler_Invoke_mE3A8562310E72EC8ADF01FA3693D1B02A010B9D3 (void);
// 0x000000F0 System.IAsyncResult SimpleFileBrowser.OnItemClickedHandler::BeginInvoke(SimpleFileBrowser.ListItem,System.AsyncCallback,System.Object)
extern void OnItemClickedHandler_BeginInvoke_mAC5A0CEC312EBB2FEB0BBACA665E2FB8B43226E3 (void);
// 0x000000F1 System.Void SimpleFileBrowser.OnItemClickedHandler::EndInvoke(System.IAsyncResult)
extern void OnItemClickedHandler_EndInvoke_m89D2C7968FA251487A47AFC4D2FFAFB65C892E3C (void);
// 0x000000F2 SimpleFileBrowser.OnItemClickedHandler SimpleFileBrowser.IListViewAdapter::get_OnItemClicked()
// 0x000000F3 System.Void SimpleFileBrowser.IListViewAdapter::set_OnItemClicked(SimpleFileBrowser.OnItemClickedHandler)
// 0x000000F4 System.Int32 SimpleFileBrowser.IListViewAdapter::get_Count()
// 0x000000F5 System.Single SimpleFileBrowser.IListViewAdapter::get_ItemHeight()
// 0x000000F6 SimpleFileBrowser.ListItem SimpleFileBrowser.IListViewAdapter::CreateItem()
// 0x000000F7 System.Void SimpleFileBrowser.IListViewAdapter::SetItemContent(SimpleFileBrowser.ListItem)
// 0x000000F8 System.Object SimpleFileBrowser.ListItem::get_Tag()
extern void ListItem_get_Tag_mA71AE4254B5B31A3B822DB8F1674D50AB25A3041 (void);
// 0x000000F9 System.Void SimpleFileBrowser.ListItem::set_Tag(System.Object)
extern void ListItem_set_Tag_m74393E301D898520AB53B5AA336ACFBC97EC787E (void);
// 0x000000FA System.Int32 SimpleFileBrowser.ListItem::get_Position()
extern void ListItem_get_Position_mB4639770E22F328A1496CDCA7FCB936D65E1FFAE (void);
// 0x000000FB System.Void SimpleFileBrowser.ListItem::set_Position(System.Int32)
extern void ListItem_set_Position_mD18CBDB9DDB4D5C2E68E5CFA0B2C7D2B94540DFE (void);
// 0x000000FC System.Void SimpleFileBrowser.ListItem::SetAdapter(SimpleFileBrowser.IListViewAdapter)
extern void ListItem_SetAdapter_m5A6BE935C41666EC29E6AFA47207C30F2AB2FD0D (void);
// 0x000000FD System.Void SimpleFileBrowser.ListItem::OnClick()
extern void ListItem_OnClick_m950C0BA1BCA2C8B4EE9DE7A8044E822169B83669 (void);
// 0x000000FE System.Void SimpleFileBrowser.ListItem::.ctor()
extern void ListItem__ctor_m108C9F53A120596095F9DDD0A2ABFD0EF0B2084C (void);
// 0x000000FF System.Void SimpleFileBrowser.RecycledListView::Start()
extern void RecycledListView_Start_mBE55ACC0C6D315D3337AD031F6F9CC15421AA23A (void);
// 0x00000100 System.Void SimpleFileBrowser.RecycledListView::SetAdapter(SimpleFileBrowser.IListViewAdapter)
extern void RecycledListView_SetAdapter_m06A0E4AF885C03B6B9F6A3E1933F83A3E90541B7 (void);
// 0x00000101 System.Void SimpleFileBrowser.RecycledListView::UpdateList()
extern void RecycledListView_UpdateList_m0637428860547148FA94CA0267BBC6E06911F29F (void);
// 0x00000102 System.Void SimpleFileBrowser.RecycledListView::OnViewportDimensionsChanged()
extern void RecycledListView_OnViewportDimensionsChanged_m62B43716804A2EF7DB7F8E45508B2A780D06A4C2 (void);
// 0x00000103 System.Void SimpleFileBrowser.RecycledListView::UpdateItemsInTheList(System.Boolean)
extern void RecycledListView_UpdateItemsInTheList_mFFE94878442D42C7B7F49A08025E4511FDFCCBC0 (void);
// 0x00000104 System.Void SimpleFileBrowser.RecycledListView::CreateItemsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_CreateItemsBetweenIndices_m1EBA714FEB4E87540C618C5C01A2176140CF34B0 (void);
// 0x00000105 System.Void SimpleFileBrowser.RecycledListView::CreateItemAtIndex(System.Int32)
extern void RecycledListView_CreateItemAtIndex_m848A2F8857745DC016A54F1CC80B138A364664BB (void);
// 0x00000106 System.Void SimpleFileBrowser.RecycledListView::DestroyItemsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_DestroyItemsBetweenIndices_m9DD81FA19B89E35FCC06583C85EB8299D7FD8137 (void);
// 0x00000107 System.Void SimpleFileBrowser.RecycledListView::UpdateItemContentsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_UpdateItemContentsBetweenIndices_m7C71B732BD2850BAC39B8CBE1AEE1A592451A0F4 (void);
// 0x00000108 System.Void SimpleFileBrowser.RecycledListView::.ctor()
extern void RecycledListView__ctor_m360F5EF4D831B9AEAC4D7E023CABA6F2931F1F7E (void);
// 0x00000109 System.Void SimpleFileBrowser.RecycledListView::<Start>b__10_0(UnityEngine.Vector2)
extern void RecycledListView_U3CStartU3Eb__10_0_m24E1DAB94C847738A00D58ED91844281DAFC0696 (void);
static Il2CppMethodPointer s_methodPointers[265] = 
{
	FileBrowser_get_IsOpen_mA5D1991D30785FD83AAA533C96803438CDC74C5B,
	FileBrowser_set_IsOpen_m4909EA8B84D49A7787D31AD8DD872C3956F6E3BC,
	FileBrowser_get_Success_m31DCB471FECBCF6EFAD502012C1DC58CE90209E7,
	FileBrowser_set_Success_m6D20AE58CDEF9F643CDA6AD3E598158FD09E468C,
	FileBrowser_get_Result_mCCFE1FD860688006BA2B97AF3E2926E62E78C814,
	FileBrowser_set_Result_m0BBF3C3624A4581E4925FA84A1A72A810598ADCC,
	FileBrowser_get_AskPermissions_mC052343777A666D9D11A058D48FB76C6D251A6A0,
	FileBrowser_set_AskPermissions_m74169F8EBE9C318835BCBB5B3B448AE0E94E9003,
	FileBrowser_get_SingleClickMode_mDF2CD71D1159B44F8D67DCC5A6B409D7BFF930B8,
	FileBrowser_set_SingleClickMode_m068FC8141089C56F72661B7BF9B66FD48480F14B,
	FileBrowser_get_DrivesRefreshInterval_mD72A4D60E7DA56E0EF979B6351E533951CD98F21,
	FileBrowser_set_DrivesRefreshInterval_m444758D2DB4CCC57F0474C60328685B4AE7C1DCC,
	FileBrowser_get_ShowHiddenFiles_mEB6850AB8F5FFEFB398CC721E468744349D68FC9,
	FileBrowser_set_ShowHiddenFiles_m5A4152F2558A434950B600A88859F91C150975BC,
	FileBrowser_get_DisplayHiddenFilesToggle_m22E7770112308552A53A7C9346366A15D42DE081,
	FileBrowser_set_DisplayHiddenFilesToggle_m4EEFC16A76F87218710D9E2C300F4137962A925C,
	FileBrowser_get_AllFilesFilterText_mC93ACF67D705CFC82178C20ACDDC6AFB6E8B14CF,
	FileBrowser_set_AllFilesFilterText_mD5675A8A16F67EEF79E3A4155D8F35A845DF3D14,
	FileBrowser_get_FoldersFilterText_m7A35A4AAD9AC262A1CBE5B6E8C6BEA211EAF6E73,
	FileBrowser_set_FoldersFilterText_m2A9450B4354E092F9AF9202E9A6DDC2F282E4373,
	FileBrowser_get_PickFolderQuickLinkText_m60FF706C3152065E59FF93D710F19FDE181EF4B4,
	FileBrowser_set_PickFolderQuickLinkText_m96FEB6CFFD83104583EF5668BA49CF0AB7A18F6B,
	FileBrowser_get_Instance_m811EE63D3BED8EF68516D6E00BF969A4EC97ACAA,
	FileBrowser_get_CurrentPath_m7AEBF65EEE1AAE67FC79949C9565CA382D5C1991,
	FileBrowser_set_CurrentPath_m9AE094D4E74E17CC9CF900ED71D6316713E0253E,
	FileBrowser_get_SearchString_m60BAB1281D2F408F902C290FEE3656CAC11ED288,
	FileBrowser_set_SearchString_m20BFE4C41DA445EFA2C00947BE6D18EE3A4EE337,
	FileBrowser_get_AcceptNonExistingFilename_mCC73584A920A441DE4DAF2D85CA7FC40A9815DBD,
	FileBrowser_set_AcceptNonExistingFilename_mA5362B26AB27A67FD0BCFEC0B18839A1A8179F74,
	FileBrowser_get_PickerMode_mA44EDEDB67BDD91DC42FA68CDF8A554D6498E9FB,
	FileBrowser_set_PickerMode_m3D0E21378984533DDC35DE55B7B1FCB7634863F8,
	FileBrowser_get_AllowMultiSelection_m862C120260F12908D02FCE55A1FB9B87F558BEBB,
	FileBrowser_set_AllowMultiSelection_m6FDF012EE485D450F0B68E5A1D9827DF2E52B986,
	FileBrowser_get_MultiSelectionToggleSelectionMode_m975C5D8A411A2755DF57798D39D2188FED2053F8,
	FileBrowser_set_MultiSelectionToggleSelectionMode_m176FE9DC46BDD2EB48C108D21831D1B08EB85934,
	FileBrowser_get_Title_m27BB4C891CE9E16CB8575D73DE6D4994A14261B4,
	FileBrowser_set_Title_m8F51E153087C97D275799A74D1B9248938BA72C7,
	FileBrowser_get_SubmitButtonText_m20489A29D7521B14D2EF387064647C74D6BEE7FD,
	FileBrowser_set_SubmitButtonText_m0B06037B82E56030B481A207250EB04BF448B1DF,
	FileBrowser_Awake_m7ED8D2EAF2FC13A2995A236FED771977A01E29DE,
	FileBrowser_OnRectTransformDimensionsChange_m7656558630069B8EAFE99EAC07037AADA4F4A24B,
	FileBrowser_LateUpdate_mB72B0AF4CEE098E1E1617CBC742556A0443A676A,
	FileBrowser_OnApplicationFocus_m3A55A6E53DF3CB09D533CC71B396CB2C8192007A,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_OnItemClicked_m3571B66C7BCCCF5A952BC3B77ECA2EC8F7C31AE8,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_set_OnItemClicked_mAD8C5576E8F5DBEE4E9934E1E961646E18D13AD9,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_Count_m53920F2D439AB6729AACAAE8B10196C0E780DFEF,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_ItemHeight_m87809A489D1604A41B6264EB197215F5CF1164FB,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_CreateItem_m4B3594285FC355021488EDE0424C6A63CEF75909,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_SetItemContent_mFABF6CE61B6A8A87B1A810287172E11B56BBD44B,
	FileBrowser_InitializeFiletypeIcons_m6EAA858F2C222CBED26172503C41A31D0A2A14D3,
	FileBrowser_InitializeQuickLinks_m3E58E20D0485E8685D61E4A5C21A2E81C3FCB1AA,
	FileBrowser_RefreshDriveQuickLinks_mFA2736938A9141A7AE8D4CCE963BF8599116025F,
	FileBrowser_OnBackButtonPressed_mE07193E07A9F83C1975D4B56C407B27656458CD6,
	FileBrowser_OnForwardButtonPressed_m2386A29CF07AFCB08198E40F6E3C5164F285EEA0,
	FileBrowser_OnUpButtonPressed_m0D3B5941FF82CF88975BB820574CC3DBAF6F37B3,
	FileBrowser_OnMoreOptionsButtonClicked_mE6946C5641CAD3C5ADAACB66B59D184D586BCFFA,
	FileBrowser_OnContextMenuTriggered_m24D91C79FC0A88DFAD5548607BD9B8C145C1E761,
	FileBrowser_ShowContextMenuAt_m40500C533570BA07A1126123B3B097C63C374249,
	FileBrowser_OnSubmitButtonClicked_m7E848A843F3D025488C0EE6FF5E54EA6D57DCBE3,
	FileBrowser_OnCancelButtonClicked_mB5936764FEC583FA4F56C10619D3CF8BA60E96D8,
	FileBrowser_OnOperationSuccessful_m5DC93791ECD6CBA227C3FA4436DB0EBE91A27A6C,
	FileBrowser_OnOperationCanceled_m8D25FC9E848E384F3E878FF801A7993C36DF1947,
	FileBrowser_OnPathChanged_m690981D06142B84FA26FF5904150DB0CD3758D72,
	FileBrowser_OnSearchStringChanged_m455D848F1D77E8509209BEEA041E6F0AF1D0796F,
	FileBrowser_OnFilterChanged_m54C7B8ABB2A479ECA7994A22D3FA484E279D8B9B,
	FileBrowser_OnShowHiddenFilesToggleChanged_mA0BC97B0151F590135CAD8588EACC5AF6399A794,
	FileBrowser_OnItemSelected_m169A9AE1DED1C198827C499C3DE7DE1C25D884F9,
	FileBrowser_OnItemHeld_m12287097633B233E71C322067B79BED7956395DB,
	FileBrowser_OnValidateFilenameInput_mF3FEA1E1138C07A818B30E19B5A988BF742C7922,
	FileBrowser_OnFilenameInputChanged_m8597E8B6648695042D028063BA767891987F90F4,
	FileBrowser_Show_mFFC02C0EB9E1F56896F3AA2EE65AA8CB396FEE78,
	FileBrowser_Hide_mF5BFFE4E660A4C0BD26E4360676AE6C401B84335,
	FileBrowser_RefreshFiles_m78E8F197E37E572167B58ECF89907B0F937711D3,
	FileBrowser_SelectAllFiles_mE91B7524DDC69BE8825340880B581B63772A070B,
	FileBrowser_DeselectAllFiles_m74394CF1CECC64623176BEDC0E0A85807DE5B526,
	FileBrowser_CreateNewFolder_m93131A2F5C3BBD4AE007C8E0D6AB310F5405E626,
	FileBrowser_CreateNewFolderCoroutine_mCC4E6964B0EEDC56E8C937CEBBFEAEA262C8581E,
	FileBrowser_RenameSelectedFile_m0BDC939786A45A504F9C74459EC23882ACD3A25B,
	FileBrowser_DeleteSelectedFiles_mB3E6B271A957C4519AA8A9AFDBABBB7863520A17,
	FileBrowser_PersistFileEntrySelection_m8A55E481241BD19BA81EC14E6B88F9B1557A9EDA,
	FileBrowser_AddQuickLink_mB4F5AF2799B7717685A57C2DDF444E319DE55B81,
	FileBrowser_EnsureWindowIsWithinBounds_m0C2BE0125CCF95DC08987F4EEC1334949F33560D,
	FileBrowser_OnWindowDimensionsChanged_mDFABDE888D6BF3701A840871D2A202DAF6279AB3,
	FileBrowser_GetIconForFileEntry_m27CDFFF69366A6DD7809BBDA3D49DED0B92DFEC0,
	FileBrowser_GetPathWithoutTrailingDirectorySeparator_m2C5C9B65DF3EDBB6DF9B415815F037C88832C5BE,
	FileBrowser_UpdateFilenameInputFieldWithSelection_mE42C35DBAC32BD4EA372AB25A9F1E8C01BB0A5FF,
	FileBrowser_ExtractFilenameFromInput_m839AB82ACA752B225AF295B030B0242D8E0BBF0B,
	FileBrowser_FilenameInputToFileEntryIndex_m885C3DEEE0F74753CA2548DCF5D688464B160B2B,
	FileBrowser_VerifyFilenameInput_m91328D6BB9E00AA43E5BFDD0978C74846F771CDA,
	FileBrowser_CalculateLengthOfDropdownText_m03EEF8FBFE5FCDE52EC3C58157123E6E4F19D026,
	FileBrowser_GetInitialPath_m423E2A4E4AA334B2E2181314CB37C2B614DA256F,
	FileBrowser_ShowSaveDialog_m7173D28C3661A93392959530E47CEFB5D98668BC,
	FileBrowser_ShowLoadDialog_mF6FAC4970BFB711D99DDAAC88482646D3E2F22EF,
	FileBrowser_ShowDialogInternal_mF604390B094BB86EB2D3397F33F9E351C19EE726,
	FileBrowser_HideDialog_m458ECE69A2F2A5830447658A70C93B55790D9278,
	FileBrowser_WaitForSaveDialog_mF25AD7B77953DF39D01DA92FB79834A9EFFF6D54,
	FileBrowser_WaitForLoadDialog_mA68EC39DDC056025018E9CC48192D2F399E248DD,
	FileBrowser_AddQuickLink_mB183F567E4531F2EF7BCD3EE41878D1557C2315B,
	FileBrowser_SetExcludedExtensions_mB83CC9D59130EB004C990C5328155A9629927B9E,
	FileBrowser_SetFilters_mBBC708C41D6DE39D512917F1D5A778C305688328,
	FileBrowser_SetFilters_mD0616C3BCDFC45906EE75274FDFA62468B6E6D41,
	FileBrowser_SetFilters_m9D8F272840FF79ACB7531F2E008704945C7DFFF3,
	FileBrowser_SetFilters_m0A68D90202F335892C3BEFFAF50688209445E7C5,
	FileBrowser_SetFiltersPreProcessing_mD5434AD892C3D13CB90303E66A0B639497374D89,
	FileBrowser_SetFiltersPostProcessing_mFF54FFC1A0B4B97001FC3EBA2773B7948BEC1AB3,
	FileBrowser_SetDefaultFilter_m195C08EB42C76D931274E4C25F40D090C7C26E2C,
	FileBrowser_CheckPermission_m410430CFDB6701AD4CCBFF9A5330355E1800CE72,
	FileBrowser_RequestPermission_mF5AEA2E34D75233F6A329C09AC4D361D1BFB3234,
	FileBrowser__ctor_mE67BF18A7571B5FE7D82A7454AAB098D9A1AACD2,
	FileBrowser__cctor_m1B9B68ACDB4CCBFE95C82137BD115FEC79C2E235,
	FileBrowser_U3CCreateNewFolderCoroutineU3Eb__212_0_m169CB5F3591084983F80B0CF13A9B169009B2837,
	FileBrowser_U3CDeleteSelectedFilesU3Eb__214_0_m1B38C260089E573783018BAA135BAD4B85DDD622,
	Filter__ctor_m1074FC06CD79E457F25F52026D634FEBF9ACCF10,
	Filter__ctor_m38D854E9B7172B814710C305CA0857452A2DE121,
	Filter__ctor_m125246E9D958C713FF3D86B5E02C9D6540B58185,
	Filter_ToString_m9DCF19E38E806A743CA49A330EBEA08814B62753,
	OnSuccess__ctor_m71AD74DD97BD2E317E6CCD78F0B631AC4D3104D7,
	OnSuccess_Invoke_m7AC28204C5A1715F07274D5F73B66E39CD7D1317,
	OnSuccess_BeginInvoke_m99BB5894F15FCDD0F04BD6321CB1E7F19EF28FFE,
	OnSuccess_EndInvoke_mB36C87C496674F8F56D681D6E25384CAB0F18CBC,
	OnCancel__ctor_m1B12099B6EC65CE377D0A854F92BC24145CCF42C,
	OnCancel_Invoke_m91F1A69EF5776DCA7D9A5C5EC9A03C925CDEBD49,
	OnCancel_BeginInvoke_m21D3022399AFFCF3CFC9926B0597AE814B2E380C,
	OnCancel_EndInvoke_m11BD247D9B4669B70D8D3116C309BB8EE08023F4,
	U3CU3Ec__cctor_m366263627B30E5A7F4830C9CFAF7D47DBF886CF7,
	U3CU3Ec__ctor_m0C7FD05A63A799955E2C175B16DD6E2FA642261E,
	U3CU3Ec_U3CRefreshFilesU3Eb__208_0_mE257F311C6FC4C025CB3BFE2C3B1E6E21F4B5DE8,
	U3CCreateNewFolderCoroutineU3Ed__212__ctor_m9416FA4180A784E6A783386ED3724C5E1DFB84B0,
	U3CCreateNewFolderCoroutineU3Ed__212_System_IDisposable_Dispose_m861842100F9A6B23C05F9DE57224BC80A09D897D,
	U3CCreateNewFolderCoroutineU3Ed__212_MoveNext_m3C8E1659613DF5B2E6B9595D445DDB2B9FAE48AB,
	U3CCreateNewFolderCoroutineU3Ed__212_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC77AE5F347747A0E3CBD1D2E97411A49FC1DCC47,
	U3CCreateNewFolderCoroutineU3Ed__212_System_Collections_IEnumerator_Reset_m36AB2D43644CE8533A09A453FC2987FFEAD355A3,
	U3CCreateNewFolderCoroutineU3Ed__212_System_Collections_IEnumerator_get_Current_mC7C28B566ABB43074905E5D00C91BC91AE057106,
	U3CU3Ec__DisplayClass213_0__ctor_mFF5EA87575F29C29592FF99C4F6EA9C4F5E50FCE,
	U3CU3Ec__DisplayClass213_0_U3CRenameSelectedFileU3Eb__0_mB759C986F3523C0EB6F69B06CA4D680C0805CBD0,
	U3CWaitForSaveDialogU3Ed__231__ctor_m4D13DBAAD39E45924438AE6BF60C5550BEED425F,
	U3CWaitForSaveDialogU3Ed__231_System_IDisposable_Dispose_m915ACB6811934B1C52C24E797972E85DECA210DA,
	U3CWaitForSaveDialogU3Ed__231_MoveNext_mD3F0EDC2048CE261FFD12CE666289AF1E818CC71,
	U3CWaitForSaveDialogU3Ed__231_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB128DE990CFBCB0D31510A56597565A527520CCD,
	U3CWaitForSaveDialogU3Ed__231_System_Collections_IEnumerator_Reset_m8773BD2220230AACB064062C84F4B5775FF52FC4,
	U3CWaitForSaveDialogU3Ed__231_System_Collections_IEnumerator_get_Current_mC7BA754F2B85A434AEBEAF359E35A59C1785C68F,
	U3CWaitForLoadDialogU3Ed__232__ctor_m5F9902D83C49E785B9A2431C0258EE15D6C17B3C,
	U3CWaitForLoadDialogU3Ed__232_System_IDisposable_Dispose_m341495EE610B41B00EACF073111B65F6953FB20C,
	U3CWaitForLoadDialogU3Ed__232_MoveNext_mA785D39AE2F2FFD91FA6A593FD428C7A16C25C1C,
	U3CWaitForLoadDialogU3Ed__232_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B7ECD9F510B0D843F6DBACD9B305A2FE5AE2694,
	U3CWaitForLoadDialogU3Ed__232_System_Collections_IEnumerator_Reset_mD7EBFD14824CD4612FBD67C4DA21FC8871DF2B80,
	U3CWaitForLoadDialogU3Ed__232_System_Collections_IEnumerator_get_Current_mDB33A1EF5FF53F8969143599B3BF9CD4D120F8E4,
	FileBrowserContextMenu_Show_m048F7147CB835F992FF2017662D8557D74937ED1,
	FileBrowserContextMenu_Hide_m74048A668955B61CF3C72AAA635387BD82631F64,
	FileBrowserContextMenu_OnSelectAllButtonClicked_mA30D36E00D69D1EF1F7083A1D3F9C4E6E6BDA4B0,
	FileBrowserContextMenu_OnDeselectAllButtonClicked_mB45F767EE9CD38BA3F1235D0D4B3CDC74D5562F9,
	FileBrowserContextMenu_OnCreateFolderButtonClicked_mD6CBE83296EB54560F9E3F933AFD521312D07376,
	FileBrowserContextMenu_OnDeleteButtonClicked_m99ACE4623BD9BFAFC79B94AA484D60CBEE2D91AB,
	FileBrowserContextMenu_OnRenameButtonClicked_m37DEA61748E86D2F41D6013F1106CE72AC6D5D00,
	FileBrowserContextMenu__ctor_m0CE266A05467D4C59C3BBD3E3D9A65AF477D995F,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m8C73CE93FA4E67CCCC8481664F14D16975025F88,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m6AB7CACBBF9735A5D824DB4A72D9925BF3F11FF1,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_m27DAB75B178336A69E5B81F8779D2DA1F8E467B0,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_mAC258EBDA04A00D20023D84799524BDEFBADFCE7,
	FileBrowserCursorHandler_ShowDefaultCursor_m9CD6A3784F2ABC645DF9E5B572175EDE5F04EDD6,
	FileBrowserCursorHandler_ShowResizeCursor_m6189764CC19F2BAF0DF7F36C571518D43120DF6D,
	FileBrowserCursorHandler__ctor_m700BE779271111CF7F82596D9B059379BF6D9B79,
	FileBrowserDeleteConfirmationPanel_Show_m7A414AF936D5796ED4E75EECC83A2F28F39B188F,
	FileBrowserDeleteConfirmationPanel_OnCanvasDimensionsChanged_m494AFB7F1030E6F1B6F4F9766ECF188DEF2ABDF1,
	FileBrowserDeleteConfirmationPanel_YesButtonClicked_m137962390E987A7EE4F6510EF754C17E52AB7DDD,
	FileBrowserDeleteConfirmationPanel_NoButtonClicked_m1A36579C3C7A9CD5E1712DA12D699ACE4FC9E856,
	FileBrowserDeleteConfirmationPanel__ctor_m70D71C94A46C8D66FF99880FFE8635E9A86925B8,
	OnDeletionConfirmed__ctor_mB63BE1ED5553DAC5AE773722E1A1DF2D1D218082,
	OnDeletionConfirmed_Invoke_mC6D0FBA98BB13C38C0608939A4D7B90D74149D33,
	OnDeletionConfirmed_BeginInvoke_m01E257578F12D0E1F92B58568CE2BC6D27B99DCE,
	OnDeletionConfirmed_EndInvoke_mC32E70F5F89EDEBBA1C9CFFE8084C6D9C163E154,
	FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F,
	FileSystemEntry__ctor_mF0D709ED4A4F496A5E8CC475F8E1FC967AD49B9E,
	FileSystemEntry__ctor_m74190ABC21BA187496BA91DDAB1FE9C58BAE7D08,
	FileBrowserHelpers_FileExists_m9D52915118B1AEB6D91D0F7D57AF305FF6688044,
	FileBrowserHelpers_DirectoryExists_m6BA2B800C6CA754CB1BC3AE06E5BB11B21BB66AE,
	FileBrowserHelpers_IsDirectory_m0E85C3971D327E8F1BFD3B6F986FF567DEDAB11B,
	FileBrowserHelpers_GetDirectoryName_m99B775DBBFAA040EC24F8CACBC4337F2D479881B,
	FileBrowserHelpers_GetEntriesInDirectory_m9FFDF203835C5C9B2418E9D0DD8BDECB29B07656,
	FileBrowserHelpers_CreateFileInDirectory_m1C7DD9921217C8E1E5D641C1D8CBF87040F34335,
	FileBrowserHelpers_CreateFolderInDirectory_m42BAE776231D65524E9F2B5C4CD37D27427448C8,
	FileBrowserHelpers_WriteBytesToFile_mC398A6AB4FE293E59E25A506C1773E6101A3F186,
	FileBrowserHelpers_WriteTextToFile_mFDEC4257FD9346411B0CFEA1A2F738F3C924266C,
	FileBrowserHelpers_AppendBytesToFile_m58453582275E21279E214CE9898E435C61AF5F8F,
	FileBrowserHelpers_AppendTextToFile_mEE9EB50ACDE0288954557048A86DC7EA4D02D13E,
	FileBrowserHelpers_AppendFileToFile_mA5235981BDE3C6BB928F39525218E150685B8160,
	FileBrowserHelpers_ReadBytesFromFile_m20F09F56CC0DF8AF5F55EABACFE0BCDBB9A76E64,
	FileBrowserHelpers_ReadTextFromFile_m035E5869B8DB91920790A367F0C0DA3649E52975,
	FileBrowserHelpers_CopyFile_mCF4B10B93E0D2DE1A028956B1C00AF8618FC57EB,
	FileBrowserHelpers_CopyDirectory_m32E8286BECDD086893DD79B46C1B31A24381A0A6,
	FileBrowserHelpers_CopyDirectoryRecursively_mA7F7A1DC8EE65626F11A6A19E1B9F871A9507A38,
	FileBrowserHelpers_MoveFile_mA9DEB625A976831CD765B701F18CF0DBAE5CB59C,
	FileBrowserHelpers_MoveDirectory_mC400BA412A8CA381828DBACA19BDC2694E77206A,
	FileBrowserHelpers_RenameFile_m2175A6933C34270587AEAFDA3B790E64CECE89B2,
	FileBrowserHelpers_RenameDirectory_m0EB0855EC112F9D79416E986E75DE00885AD1D7A,
	FileBrowserHelpers_DeleteFile_m42F1CEE70904E259557F27B72D8AE6584ECB1AB2,
	FileBrowserHelpers_DeleteDirectory_mCE4FA56EDEDA054174A483910A8061EE4C67E7BD,
	FileBrowserHelpers_GetFilename_m7BA7650CF4DAFD74BDDDED8EFAB7FD4C1E3F0A06,
	FileBrowserHelpers_GetFilesize_m2B7C3C550C55A4AAAC8CD511367050643943B24B,
	FileBrowserHelpers_GetLastModifiedDate_m4379195FAB7835E2F549FFFBD185B2EED5A7041D,
	FileBrowserItem_get_TransformComponent_mEDD8CD474DBBF4B58E11D1BE82C255CAE65141CA,
	FileBrowserItem_get_Name_m1F09E0A5E84870485231447173A5AF496DC0E01F,
	FileBrowserItem_get_IsDirectory_mC40A8B51825BC34EB34D45C72D35A1CA6E1A89BB,
	FileBrowserItem_set_IsDirectory_mDB0D5D005A9C6F689AF30D6A63E8AE555FFF6DF6,
	FileBrowserItem_SetFileBrowser_m0BC28D0644C40780C1C4ECA5D3A768B23D10DD4C,
	FileBrowserItem_SetFile_m1F1468944380FAE443BB546ADF30AC56B3C6616D,
	FileBrowserItem_Update_m3A16C87E26FCD94FB63A74DBD6F10ED09E6D7FC4,
	FileBrowserItem_OnPointerClick_m324DF382072BE76D6FFDBE62E8C2DFABD47A6065,
	FileBrowserItem_OnPointerDown_m4EA10AF080820CE9779BDE00E61CFF69C9BE4BF8,
	FileBrowserItem_OnPointerUp_m7CEDD0C86CCBB748225E07597536AB8FB9D8436A,
	FileBrowserItem_OnPointerEnter_mCEE10023A5B9C505A3B18ADA2B9D83CF7F74A7B7,
	FileBrowserItem_OnPointerExit_m6D8F537184562578F627250C4CC9880154FBA04F,
	FileBrowserItem_SetSelected_mF1A4735B8F234281F0698E5C0DA0B9575C0D4C28,
	FileBrowserItem_SetHidden_m91D66EE00C0EEAF1FFEB6C9220BF363310271F28,
	FileBrowserItem__ctor_m78CFC23F9073522F0BBFB7F9792BAA0A46362454,
	FileBrowserMovement_Initialize_m471ABE4C29F9D705461783AB33F2CC921AC51478,
	FileBrowserMovement_OnDragStarted_m3E0201898C42B961B9D8194C95DDB8EEA8555B65,
	FileBrowserMovement_OnDrag_mD8D2918E9F95EFFDCC104E321E5B957850743AF8,
	FileBrowserMovement_OnEndDrag_m443E3696DBB3571E87FC1B616BDE956C5F3A506E,
	FileBrowserMovement_OnResizeStarted_mB31A17958B7A24A16CBE1B21D9B22451A2C0D486,
	FileBrowserMovement_OnResize_m49E6F628633974DE0F8E1737D127FB03BE774853,
	FileBrowserMovement_OnEndResize_mFB53FC3931D90175FE576DA10B16F94CD75BEC27,
	FileBrowserMovement__ctor_mB4D1B389EACB5128B7FC728B439AFE488642E95D,
	FileBrowserQuickLink_get_TargetPath_mD1CA8CA83A2B19226D00B8E77BC024A57D0C4C91,
	FileBrowserQuickLink_SetQuickLink_mDEDE06D8E3C9A90193FB71E57336A68E92895350,
	FileBrowserQuickLink__ctor_mCED0CE4E2EDD195AAB3F2BBA553F9EFC9715D8A5,
	FileBrowserRenamedItem_Show_m67CF833D01BEDC56743DF4EAC46AF60E8FFFB5DB,
	FileBrowserRenamedItem_OnInputFieldEndEdit_mB5AC8C49EEF73640F34CB94F2D5FA62F1C48C366,
	FileBrowserRenamedItem__ctor_m32B6017C97863E0349A0D4B61D355CD9EFECE11B,
	OnRenameCompleted__ctor_m019658E790544491DDBD4F208F8B422DC77764EB,
	OnRenameCompleted_Invoke_m8A1B85475DE7D5362CB711D63543CDD6A68A7CDB,
	OnRenameCompleted_BeginInvoke_m090E447625A7D2DCE9AE24887390FB68BAD85E31,
	OnRenameCompleted_EndInvoke_mEC5511E093D8247D914A8802352DC8DA57CFB1B2,
	NonDrawingGraphic_SetMaterialDirty_m329A03BC12E9779085DC0D91E2DE2582127E8395,
	NonDrawingGraphic_SetVerticesDirty_mD732D935D4951495843EC5A9EA1DB19665415793,
	NonDrawingGraphic_OnPopulateMesh_m1C0A9BA5BE3AE3AAC6CBFE8F742E4AB51823198C,
	NonDrawingGraphic__ctor_m995D20F1C7EA51EC17D9E9D5F8F10A226597D6BB,
	OnItemClickedHandler__ctor_m2E672645F74ACB3CB0B7F152205B647BCB399FAC,
	OnItemClickedHandler_Invoke_mE3A8562310E72EC8ADF01FA3693D1B02A010B9D3,
	OnItemClickedHandler_BeginInvoke_mAC5A0CEC312EBB2FEB0BBACA665E2FB8B43226E3,
	OnItemClickedHandler_EndInvoke_m89D2C7968FA251487A47AFC4D2FFAFB65C892E3C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ListItem_get_Tag_mA71AE4254B5B31A3B822DB8F1674D50AB25A3041,
	ListItem_set_Tag_m74393E301D898520AB53B5AA336ACFBC97EC787E,
	ListItem_get_Position_mB4639770E22F328A1496CDCA7FCB936D65E1FFAE,
	ListItem_set_Position_mD18CBDB9DDB4D5C2E68E5CFA0B2C7D2B94540DFE,
	ListItem_SetAdapter_m5A6BE935C41666EC29E6AFA47207C30F2AB2FD0D,
	ListItem_OnClick_m950C0BA1BCA2C8B4EE9DE7A8044E822169B83669,
	ListItem__ctor_m108C9F53A120596095F9DDD0A2ABFD0EF0B2084C,
	RecycledListView_Start_mBE55ACC0C6D315D3337AD031F6F9CC15421AA23A,
	RecycledListView_SetAdapter_m06A0E4AF885C03B6B9F6A3E1933F83A3E90541B7,
	RecycledListView_UpdateList_m0637428860547148FA94CA0267BBC6E06911F29F,
	RecycledListView_OnViewportDimensionsChanged_m62B43716804A2EF7DB7F8E45508B2A780D06A4C2,
	RecycledListView_UpdateItemsInTheList_mFFE94878442D42C7B7F49A08025E4511FDFCCBC0,
	RecycledListView_CreateItemsBetweenIndices_m1EBA714FEB4E87540C618C5C01A2176140CF34B0,
	RecycledListView_CreateItemAtIndex_m848A2F8857745DC016A54F1CC80B138A364664BB,
	RecycledListView_DestroyItemsBetweenIndices_m9DD81FA19B89E35FCC06583C85EB8299D7FD8137,
	RecycledListView_UpdateItemContentsBetweenIndices_m7C71B732BD2850BAC39B8CBE1AEE1A592451A0F4,
	RecycledListView__ctor_m360F5EF4D831B9AEAC4D7E023CABA6F2931F1F7E,
	RecycledListView_U3CStartU3Eb__10_0_m24E1DAB94C847738A00D58ED91844281DAFC0696,
};
extern void FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F_AdjustorThunk (void);
extern void FileSystemEntry__ctor_mF0D709ED4A4F496A5E8CC475F8E1FC967AD49B9E_AdjustorThunk (void);
extern void FileSystemEntry__ctor_m74190ABC21BA187496BA91DDAB1FE9C58BAE7D08_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x060000AC, FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F_AdjustorThunk },
	{ 0x060000AD, FileSystemEntry__ctor_mF0D709ED4A4F496A5E8CC475F8E1FC967AD49B9E_AdjustorThunk },
	{ 0x060000AE, FileSystemEntry__ctor_m74190ABC21BA187496BA91DDAB1FE9C58BAE7D08_AdjustorThunk },
};
static const int32_t s_InvokerIndices[265] = 
{
	2823,
	2785,
	2823,
	2785,
	2816,
	2783,
	2823,
	2785,
	2823,
	2785,
	2825,
	2788,
	2823,
	2785,
	2823,
	2785,
	2816,
	2783,
	2816,
	2783,
	2816,
	2783,
	2816,
	1761,
	1519,
	1761,
	1519,
	1780,
	1536,
	1750,
	1510,
	1780,
	1536,
	1780,
	1536,
	1761,
	1519,
	1761,
	1519,
	1793,
	1793,
	1793,
	1536,
	1761,
	1519,
	1750,
	1782,
	1761,
	1519,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1545,
	1010,
	1793,
	1793,
	1519,
	1536,
	1519,
	1519,
	1793,
	1793,
	991,
	1519,
	488,
	1519,
	989,
	1793,
	1536,
	1793,
	1793,
	1793,
	1761,
	1793,
	1793,
	1793,
	605,
	1793,
	1545,
	1247,
	1253,
	1793,
	501,
	505,
	602,
	1176,
	1253,
	1849,
	1849,
	1834,
	2785,
	1897,
	1897,
	2319,
	2783,
	2602,
	2602,
	2602,
	2602,
	2785,
	2830,
	2741,
	2811,
	2811,
	1793,
	2830,
	1519,
	1793,
	1519,
	989,
	989,
	1761,
	988,
	1519,
	549,
	1519,
	988,
	1793,
	773,
	1519,
	2830,
	1793,
	713,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	1793,
	1519,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	168,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1519,
	1519,
	1519,
	1519,
	1793,
	1793,
	1793,
	446,
	1545,
	1793,
	1793,
	1793,
	988,
	1793,
	773,
	1519,
	1780,
	670,
	1519,
	2741,
	2741,
	2741,
	2716,
	2716,
	2480,
	2480,
	2596,
	2596,
	2596,
	2596,
	2596,
	2716,
	2716,
	2596,
	2596,
	2596,
	2596,
	2596,
	2480,
	2480,
	2783,
	2783,
	2716,
	2682,
	2630,
	1761,
	1761,
	1780,
	1536,
	1519,
	670,
	1793,
	1519,
	1519,
	1519,
	1519,
	1519,
	1536,
	1536,
	1793,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	1793,
	1761,
	669,
	1793,
	426,
	1519,
	1793,
	988,
	1519,
	549,
	1519,
	1793,
	1793,
	1519,
	1793,
	988,
	1519,
	549,
	1519,
	1761,
	1519,
	1750,
	1782,
	1761,
	1519,
	1761,
	1519,
	1750,
	1510,
	1519,
	1793,
	1793,
	1793,
	1519,
	1793,
	1793,
	1536,
	918,
	1510,
	918,
	918,
	1793,
	1545,
};
extern const CustomAttributesCacheGenerator g_SimpleFileBrowser_Runtime_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SimpleFileBrowser_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_SimpleFileBrowser_Runtime_CodeGenModule = 
{
	"SimpleFileBrowser.Runtime.dll",
	265,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_SimpleFileBrowser_Runtime_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
