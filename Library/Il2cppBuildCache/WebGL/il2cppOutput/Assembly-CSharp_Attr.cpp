﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct  RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct  HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_ShowLoadDialogCoroutine_m67FA4AFF637FCD4C4A5EA99D38AE652FCA400514(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_0_0_0_var), NULL);
	}
}
static void mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_0_m3D4E196FAB3FED9950537F5704CF5B6B525DF6E7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_1_mBD72798DD4C4E98D97A33D2B94524AB6E77A75DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_2_m2337882BE0CA863007C70EA0897E8328A7C54E29(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_4_m2E8D5C592A0F3AF59C8C3300DBDB10E7D49B5575(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13__ctor_m4B6600EB7805E7F4F86F09C4C321ADA942A57780(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_IDisposable_Dispose_m01FB2C32275EB18BBBD4F60A45455486495EE968(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2AECCA9C2680930F4A4A1DE1A9DDD485BDB3925D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_Reset_m76BB3A7BCE2E32D7D2806770112C1AA06E7EC596(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_get_Current_mDF35685524E8BB7D14D1AA103D95FD7F4DBF22F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA_CustomAttributesCacheGenerator_U3CPushedFaceCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA_CustomAttributesCacheGenerator_OBJObjectBuilder_get_PushedFaceCount_m9A51064310CB6F80EE840BA9067EE95EF8220F06(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA_CustomAttributesCacheGenerator_OBJObjectBuilder_set_PushedFaceCount_mE5FC98B476DA97661D6B440D1CE8781C82AC3042(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringExtensions_t8EE5329B03429310F4FF4C39B400D1A409C8BAA1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t8EE5329B03429310F4FF4C39B400D1A409C8BAA1_CustomAttributesCacheGenerator_StringExtensions_Clean_mBD817F18A962673D4157FDD426B9324C092BD734(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32RGBR_mE26BFB16967BE261B57740558FC02FD3AD82EE78(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32RGBA_mA6987C56F059E266336BFBF30067DA149B825F39(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32RGB_m968635C4AFB130CBB829DC65251E1E388F1B1D7E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32BGR_mFA7375B3F8EDA89B1F6BE3BD82E148804E8855EE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t9F13E8AF7EFAA846676626D2C10E60654ADC2A34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t9F13E8AF7EFAA846676626D2C10E60654ADC2A34_CustomAttributesCacheGenerator_ColorExtensions_FlipRB_m621188E25D11376AF450BD484FA3F50475370785(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t9F13E8AF7EFAA846676626D2C10E60654ADC2A34_CustomAttributesCacheGenerator_ColorExtensions_FlipRB_m4689B607045C6100B80EB112A617E94235A9469D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_zUp(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x6F\x61\x64\x20\x74\x68\x65\x20\x4F\x42\x4A\x20\x66\x69\x6C\x65\x20\x61\x73\x73\x75\x6D\x69\x74\x67\x20\x69\x74\x73\x20\x76\x65\x72\x74\x69\x63\x61\x6C\x20\x61\x78\x69\x73\x20\x69\x73\x20\x5A\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x59"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_litDiffuse(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x73\x69\x64\x65\x72\x20\x64\x69\x66\x66\x75\x73\x65\x20\x6D\x61\x70\x20\x61\x73\x20\x61\x6C\x72\x65\x61\x64\x79\x20\x6C\x69\x74\x20\x28\x64\x69\x73\x61\x62\x6C\x65\x20\x6C\x69\x67\x68\x74\x69\x6E\x67\x29\x20\x69\x66\x20\x6E\x6F\x20\x6F\x74\x68\x65\x72\x20\x74\x65\x78\x74\x75\x72\x65\x20\x69\x73\x20\x70\x72\x65\x73\x65\x6E\x74"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_convertToDoubleSided(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x73\x69\x64\x65\x72\x20\x74\x6F\x20\x64\x6F\x75\x62\x6C\x65\x2D\x73\x69\x64\x65\x64\x20\x28\x64\x75\x70\x6C\x69\x63\x61\x74\x65\x20\x61\x6E\x64\x20\x66\x6C\x69\x70\x20\x66\x61\x63\x65\x73\x20\x61\x6E\x64\x20\x6E\x6F\x72\x6D\x61\x6C\x73"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_modelScaling(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x63\x61\x6C\x69\x6E\x67\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x6F\x64\x65\x6C\x20\x28\x31\x20\x3D\x20\x6E\x6F\x20\x72\x65\x73\x63\x61\x6C\x69\x6E\x67\x29"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_reuseLoaded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x75\x73\x65\x20\x61\x20\x6D\x6F\x64\x65\x6C\x20\x69\x6E\x20\x6D\x65\x6D\x6F\x72\x79\x20\x69\x66\x20\x61\x6C\x72\x65\x61\x64\x79\x20\x6C\x6F\x61\x64\x65\x64"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_inheritLayer(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x68\x65\x72\x69\x74\x20\x70\x61\x72\x65\x6E\x74\x20\x6C\x61\x79\x65\x72"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_buildColliders(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x74\x65\x20\x6D\x65\x73\x68\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x73"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_colliderConvex(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x74\x65\x20\x63\x6F\x6E\x76\x65\x78\x20\x6D\x65\x73\x68\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x73\x20\x28\x6F\x6E\x6C\x79\x20\x61\x63\x74\x69\x76\x65\x20\x69\x66\x20\x62\x75\x69\x6C\x64\x43\x6F\x6C\x6C\x69\x64\x65\x72\x73\x20\x3D\x20\x74\x72\x75\x65\x29\xA\x4E\x6F\x74\x65\x3A\x20\x69\x74\x20\x63\x6F\x75\x6C\x64\x20\x6E\x6F\x74\x20\x77\x6F\x72\x6B\x20\x66\x6F\x72\x20\x6D\x65\x73\x68\x65\x73\x20\x77\x69\x74\x68\x20\x74\x6F\x6F\x20\x6D\x61\x6E\x79\x20\x73\x6D\x6F\x6F\x74\x68\x20\x73\x75\x72\x66\x61\x63\x65\x20\x72\x65\x67\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_colliderTrigger(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x73\x68\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x73\x20\x61\x73\x20\x74\x72\x69\x67\x67\x65\x72\x20\x28\x6F\x6E\x6C\x79\x20\x61\x63\x74\x69\x76\x65\x20\x69\x66\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x43\x6F\x6E\x76\x65\x78\x20\x3D\x20\x74\x72\x75\x65\x29"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_use32bitIndices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x33\x32\x20\x62\x69\x74\x20\x69\x6E\x64\x69\x63\x65\x73\x20\x77\x68\x65\x6E\x20\x6E\x65\x65\x64\x65\x64\x2C\x20\x69\x66\x20\x61\x76\x61\x69\x6C\x61\x62\x6C\x65"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_hideWhileLoading(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x69\x64\x65\x20\x74\x68\x65\x20\x6C\x6F\x61\x64\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x64\x75\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x6C\x6F\x61\x64\x69\x6E\x67\x20\x70\x72\x6F\x63\x65\x73\x73"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_localPosition(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x63\x61\x6C\x20\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x69\x6D\x70\x6F\x72\x74\x65\x64\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x69\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_localEulerAngles(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\xA\x28\x45\x75\x6C\x65\x72\x20\x61\x6E\x67\x6C\x65\x73\x29"), NULL);
	}
}
static void ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_localScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x69\x6E\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\xA\x28\x5B\x31\x2C\x31\x2C\x31\x5D\x20\x3D\x20\x6E\x6F\x20\x72\x65\x73\x63\x61\x6C\x69\x6E\x67\x29"), NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_ModelCreated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_ModelLoaded(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_ModelError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_add_ModelCreated_m8992EE812B09EB58B47864AB996D16B47EA07FB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_remove_ModelCreated_mA5DD5078F249FC7D57A468B487C89C9FC0CD0085(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_add_ModelLoaded_m7A065F03EFAA5AC2FE5FD2D792BB0FFA310F8142(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_remove_ModelLoaded_m2A7830BB84800200D10E269230C57DF7EF9B58E5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_add_ModelError_m56EDA25051268763FDDC531159F60AEC088B53CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_remove_ModelError_m525B768D2BB19243FE1905EC21D1548E6147068E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_Load_m4DC773597C6E42E7B9AE846A83E8DE6866D56353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_0_0_0_var), NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_Build_mF8D000CD4CC7267CE431AA748594A985C4E890AF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_0_0_0_var), NULL);
	}
}
static void Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_LoadMaterialTexture_m440076E0E24E8754B2044227984452E07818025E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_0_0_0_var), NULL);
	}
}
static void U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32__ctor_m73C8A633B599ED1F4ED404A8ECDF82DCD633BCA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_IDisposable_Dispose_m76F0AB7C4A259585D6C69D2D48DD59B16ED7F944(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4814EA0F29D8615752EFF8176625E0CF12F5E10C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_Collections_IEnumerator_Reset_m53352D1E2DF974F4FF214A8F561008C0B688519B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_Collections_IEnumerator_get_Current_m33F97DBD0C22371E96278B6FF2B29A7B86EB09A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36__ctor_m2746753CE6057324F628EF2130856DA6E70D389D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_IDisposable_Dispose_mBEEA3AF4CC543B059B5992CC6AB36554A98BFEE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234701C996636E960294EDAF55C3CAA01DCD2092(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_Collections_IEnumerator_Reset_m786116A825908E278B23C5A6FEA64FCAE7BB5BEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_Collections_IEnumerator_get_Current_m367ECE887DF4A49C3B21942EA6A37D688242EA51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42__ctor_m4EA7B131CF7AB5224995803A3878296B0A70CB0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_IDisposable_Dispose_m8D9EC2C3145CDE449264D604FFBF8A74DAC0CFD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52FA484524B04EF7CB4E0B3DCABF955F37778AED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_Reset_m9849C2B35AD8DF82C95FB6A427C4298EE351A212(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_get_Current_mA9EAD8C0E049278489EF63EDD9A8B8E72899A510(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_LoadModelFile_m78B90897589F239C23519364BE7691CC1BF85829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_0_0_0_var), NULL);
	}
}
static void LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_LoadMaterialLibrary_mC4CDB8A952C1B852AE2EB4DF8254A6990EAE5755(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_0_0_0_var), NULL);
	}
}
static void LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_ParseGeometryData_m4AA82D03093B53A6B2F002D8091BFD3FDF007FE1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_0_0_0_var), NULL);
	}
}
static void LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_LoadOrDownloadText_mB967181C6B7DF4C1FD7F423BDDC08A0F721158B0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_0_0_0_var), NULL);
	}
}
static void U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3__ctor_m31EC5B02C9FFDD0107481311078DC237D33FC350(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_IDisposable_Dispose_m302B51DB67B5231710E448287F3A65CBCADD3773(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m157DEF0B8196F4E4A4532C9A4D456025AFA21240(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_Reset_m8A5292170989E075719F3393BD0DA6B1AEFA1BD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_get_Current_m033DB07B0490629CDC70C9E82462D27750683230(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4__ctor_m857E4E65341EE9147C0AE4B475AACD135325D97B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_IDisposable_Dispose_m338D35DA7855937147A8B8807F490E0CD56334FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DE19B3479375B272D8F6EA4AE9F7650A7C96686(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_Reset_mA169DB740A8A7154826E4EF0736B2DF07DC1AF8B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_get_Current_m433F960B7017A11D26D2CF999E1E72077C29C412(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8__ctor_mC96B3D3DCFE50A6FD8A6894A512EDC67D0308430(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_IDisposable_Dispose_mACAB1D384D2A3973F6604BC082A4841937B5EAD2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m987832127609D559655B0F45011479F84CCF5AD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_Reset_mB570E836E9EE3A72D1E67E98807C52514F030F17(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_get_Current_mC2D7EBA1B6E5B1872CBFC18737136E4F44D453F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16__ctor_m449E2AF3B9297CB1F6B68DC54427D53B0D481A41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_IDisposable_Dispose_mF22941E1A5FA87FF66CFA4CBB0EF3FB497B04DFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m319547258507AB779A3F29F8C08FE499C9D29026(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_Reset_m68C9FCF8FF223384464DF36D0CEC510BFE410240(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_get_Current_mC1D95606D9F1B0C1FA62787A1700AEF6430FFB76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x63\x72\x65\x61\x74\x65\x64\xA\x28\x6C\x65\x61\x76\x65\x20\x69\x74\x20\x62\x6C\x61\x6E\x6B\x20\x74\x6F\x20\x75\x73\x65\x20\x69\x74\x73\x20\x66\x69\x6C\x65\x20\x6E\x61\x6D\x65\x29"), NULL);
	}
}
static void ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6_CustomAttributesCacheGenerator_path(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x74\x68\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x70\x72\x6F\x6A\x65\x63\x74\x20\x66\x6F\x6C\x64\x65\x72"), NULL);
	}
}
static void ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6_CustomAttributesCacheGenerator_skip(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x73\x6B\x69\x70\x20\x74\x68\x69\x73\x20\x6D\x6F\x64\x65\x6C"), NULL);
	}
}
static void MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_autoLoadOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x61\x64\x20\x6D\x6F\x64\x65\x6C\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x69\x73\x74\x20\x6F\x6E\x20\x73\x74\x61\x72\x74"), NULL);
	}
}
static void MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_objectsList(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65\x6C\x73\x20\x74\x6F\x20\x6C\x6F\x61\x64\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x75\x70"), NULL);
	}
}
static void MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_defaultImportOptions(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x20\x69\x6D\x70\x6F\x72\x74\x20\x6F\x70\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_pathSettings(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportingStart(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportingComplete(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_CreatedModel(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportedModel(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportingStart_mF967C93A8772C6AF9DF70DB5AD1503B276B8C3B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportingStart_m2F7F74D1CA7C50EEF9BB842B790C461AF6693100(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportingComplete_m9A892024861058A6FF56116A60B365B91076E0CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportingComplete_m9F2D2CCDB062B2F027A0DD7EBA9C446E3275C68E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_CreatedModel_m178EB892D4E4FE81E6CA3ADBF19F5849423315C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_CreatedModel_m2BA274F4B150A6A181E5AFE20479075FB177612E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportedModel_m6C50C797294CB6EB2D8CB556321954E2BD0959C9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportedModel_m16878B820F61B0039349B22718E6D8F884931B0E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportError_mD47F0F2C636D79952DA5DBA5F5A76FE002BAFD40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportError_m93DF9FAC2DF5F4642BD530AA12D665BA8B684082(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_0_0_0_var), NULL);
	}
}
static void ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator_progressText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x66\x6F\x72\x20\x61\x63\x74\x69\x76\x69\x74\x79\x20\x6D\x65\x73\x73\x61\x67\x65\x73"), NULL);
	}
}
static void ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator_progressSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6F\x76\x65\x72\x61\x6C\x6C\x20\x70\x72\x6F\x67\x72\x65\x73\x73"), NULL);
	}
}
static void ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator_progressImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x6E\x65\x6C\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x49\x6D\x61\x67\x65\x20\x54\x79\x70\x65\x20\x73\x65\x74\x20\x74\x6F\x20\x46\x69\x6C\x6C\x65\x64"), NULL);
	}
}
static void PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346_CustomAttributesCacheGenerator_defaultRootPath(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x20\x72\x6F\x6F\x74\x20\x70\x61\x74\x68\x20\x66\x6F\x72\x20\x6D\x6F\x64\x65\x6C\x73"), NULL);
	}
}
static void PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346_CustomAttributesCacheGenerator_mobileRootPath(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x6F\x74\x20\x70\x61\x74\x68\x20\x66\x6F\x72\x20\x6D\x6F\x64\x65\x6C\x73\x20\x6F\x6E\x20\x6D\x6F\x62\x69\x6C\x65\x20\x64\x65\x76\x69\x63\x65\x73"), NULL);
	}
}
static void Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_U3COriginalIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_get_Position_m39B376C28A912E63492FFDC60D91460C36EDB7BC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_set_Position_m34AEC05B85BDA60FCEDD74055FA4659C17212D3C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_get_OriginalIndex_m993575EDF4FDA693778501A1FEC370E83BDDF8D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_set_OriginalIndex_m8BB3858C9BA804333E0DCD3CB9669D543551F661(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_filePath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_objectName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_importOptions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_pathSettings(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE_CustomAttributesCacheGenerator_objScalingText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x66\x6F\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x6F\x76\x65\x72\x61\x6C\x6C\x20\x73\x63\x61\x6C\x65"), NULL);
	}
}
static void CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE_CustomAttributesCacheGenerator_configFile(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x20\x58\x4D\x4C\x20\x66\x69\x6C\x65\x20\x28\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x61\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x20\x64\x61\x74\x61\x20\x66\x6F\x6C\x64\x65\x72\x29"), NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t516260B5AD68685A397D253FF6E9898F2AB75C4A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[141] = 
{
	U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73_CustomAttributesCacheGenerator,
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488_CustomAttributesCacheGenerator,
	StringExtensions_t8EE5329B03429310F4FF4C39B400D1A409C8BAA1_CustomAttributesCacheGenerator,
	BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator,
	ColorExtensions_t9F13E8AF7EFAA846676626D2C10E60654ADC2A34_CustomAttributesCacheGenerator,
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator,
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator,
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator,
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator,
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator,
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator,
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator,
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t516260B5AD68685A397D253FF6E9898F2AB75C4A_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA_CustomAttributesCacheGenerator_U3CPushedFaceCountU3Ek__BackingField,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_zUp,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_litDiffuse,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_convertToDoubleSided,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_modelScaling,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_reuseLoaded,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_inheritLayer,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_buildColliders,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_colliderConvex,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_colliderTrigger,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_use32bitIndices,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_hideWhileLoading,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_localPosition,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_localEulerAngles,
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F_CustomAttributesCacheGenerator_localScale,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_ModelCreated,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_ModelLoaded,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_ModelError,
	ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6_CustomAttributesCacheGenerator_name,
	ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6_CustomAttributesCacheGenerator_path,
	ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6_CustomAttributesCacheGenerator_skip,
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_autoLoadOnStart,
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_objectsList,
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_defaultImportOptions,
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C_CustomAttributesCacheGenerator_pathSettings,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportingStart,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportingComplete,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_CreatedModel,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportedModel,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ImportError,
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator_progressText,
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator_progressSlider,
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875_CustomAttributesCacheGenerator_progressImage,
	PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346_CustomAttributesCacheGenerator_defaultRootPath,
	PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346_CustomAttributesCacheGenerator_mobileRootPath,
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField,
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_U3COriginalIndexU3Ek__BackingField,
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_filePath,
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_objectName,
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_importOptions,
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A_CustomAttributesCacheGenerator_pathSettings,
	CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE_CustomAttributesCacheGenerator_objScalingText,
	CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE_CustomAttributesCacheGenerator_configFile,
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_ShowLoadDialogCoroutine_m67FA4AFF637FCD4C4A5EA99D38AE652FCA400514,
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_0_m3D4E196FAB3FED9950537F5704CF5B6B525DF6E7,
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_1_mBD72798DD4C4E98D97A33D2B94524AB6E77A75DC,
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_2_m2337882BE0CA863007C70EA0897E8328A7C54E29,
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339_CustomAttributesCacheGenerator_mainMenu_U3CconfigureListenersU3Eb__6_4_m2E8D5C592A0F3AF59C8C3300DBDB10E7D49B5575,
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13__ctor_m4B6600EB7805E7F4F86F09C4C321ADA942A57780,
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_IDisposable_Dispose_m01FB2C32275EB18BBBD4F60A45455486495EE968,
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2AECCA9C2680930F4A4A1DE1A9DDD485BDB3925D,
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_Reset_m76BB3A7BCE2E32D7D2806770112C1AA06E7EC596,
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50_CustomAttributesCacheGenerator_U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_get_Current_mDF35685524E8BB7D14D1AA103D95FD7F4DBF22F1,
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA_CustomAttributesCacheGenerator_OBJObjectBuilder_get_PushedFaceCount_m9A51064310CB6F80EE840BA9067EE95EF8220F06,
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA_CustomAttributesCacheGenerator_OBJObjectBuilder_set_PushedFaceCount_mE5FC98B476DA97661D6B440D1CE8781C82AC3042,
	StringExtensions_t8EE5329B03429310F4FF4C39B400D1A409C8BAA1_CustomAttributesCacheGenerator_StringExtensions_Clean_mBD817F18A962673D4157FDD426B9324C092BD734,
	BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32RGBR_mE26BFB16967BE261B57740558FC02FD3AD82EE78,
	BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32RGBA_mA6987C56F059E266336BFBF30067DA149B825F39,
	BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32RGB_m968635C4AFB130CBB829DC65251E1E388F1B1D7E,
	BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E_CustomAttributesCacheGenerator_BinaryExtensions_ReadColor32BGR_mFA7375B3F8EDA89B1F6BE3BD82E148804E8855EE,
	ColorExtensions_t9F13E8AF7EFAA846676626D2C10E60654ADC2A34_CustomAttributesCacheGenerator_ColorExtensions_FlipRB_m621188E25D11376AF450BD484FA3F50475370785,
	ColorExtensions_t9F13E8AF7EFAA846676626D2C10E60654ADC2A34_CustomAttributesCacheGenerator_ColorExtensions_FlipRB_m4689B607045C6100B80EB112A617E94235A9469D,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_add_ModelCreated_m8992EE812B09EB58B47864AB996D16B47EA07FB6,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_remove_ModelCreated_mA5DD5078F249FC7D57A468B487C89C9FC0CD0085,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_add_ModelLoaded_m7A065F03EFAA5AC2FE5FD2D792BB0FFA310F8142,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_remove_ModelLoaded_m2A7830BB84800200D10E269230C57DF7EF9B58E5,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_add_ModelError_m56EDA25051268763FDDC531159F60AEC088B53CE,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_remove_ModelError_m525B768D2BB19243FE1905EC21D1548E6147068E,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_Load_m4DC773597C6E42E7B9AE846A83E8DE6866D56353,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_Build_mF8D000CD4CC7267CE431AA748594A985C4E890AF,
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_CustomAttributesCacheGenerator_Loader_LoadMaterialTexture_m440076E0E24E8754B2044227984452E07818025E,
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32__ctor_m73C8A633B599ED1F4ED404A8ECDF82DCD633BCA2,
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_IDisposable_Dispose_m76F0AB7C4A259585D6C69D2D48DD59B16ED7F944,
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4814EA0F29D8615752EFF8176625E0CF12F5E10C,
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_Collections_IEnumerator_Reset_m53352D1E2DF974F4FF214A8F561008C0B688519B,
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903_CustomAttributesCacheGenerator_U3CLoadU3Ed__32_System_Collections_IEnumerator_get_Current_m33F97DBD0C22371E96278B6FF2B29A7B86EB09A1,
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36__ctor_m2746753CE6057324F628EF2130856DA6E70D389D,
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_IDisposable_Dispose_mBEEA3AF4CC543B059B5992CC6AB36554A98BFEE0,
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234701C996636E960294EDAF55C3CAA01DCD2092,
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_Collections_IEnumerator_Reset_m786116A825908E278B23C5A6FEA64FCAE7BB5BEA,
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9_CustomAttributesCacheGenerator_U3CBuildU3Ed__36_System_Collections_IEnumerator_get_Current_m367ECE887DF4A49C3B21942EA6A37D688242EA51,
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42__ctor_m4EA7B131CF7AB5224995803A3878296B0A70CB0A,
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_IDisposable_Dispose_m8D9EC2C3145CDE449264D604FFBF8A74DAC0CFD8,
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52FA484524B04EF7CB4E0B3DCABF955F37778AED,
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_Reset_m9849C2B35AD8DF82C95FB6A427C4298EE351A212,
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7_CustomAttributesCacheGenerator_U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_get_Current_mA9EAD8C0E049278489EF63EDD9A8B8E72899A510,
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_LoadModelFile_m78B90897589F239C23519364BE7691CC1BF85829,
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_LoadMaterialLibrary_mC4CDB8A952C1B852AE2EB4DF8254A6990EAE5755,
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_ParseGeometryData_m4AA82D03093B53A6B2F002D8091BFD3FDF007FE1,
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543_CustomAttributesCacheGenerator_LoaderObj_LoadOrDownloadText_mB967181C6B7DF4C1FD7F423BDDC08A0F721158B0,
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3__ctor_m31EC5B02C9FFDD0107481311078DC237D33FC350,
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_IDisposable_Dispose_m302B51DB67B5231710E448287F3A65CBCADD3773,
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m157DEF0B8196F4E4A4532C9A4D456025AFA21240,
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_Reset_m8A5292170989E075719F3393BD0DA6B1AEFA1BD3,
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56_CustomAttributesCacheGenerator_U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_get_Current_m033DB07B0490629CDC70C9E82462D27750683230,
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4__ctor_m857E4E65341EE9147C0AE4B475AACD135325D97B,
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_IDisposable_Dispose_m338D35DA7855937147A8B8807F490E0CD56334FA,
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DE19B3479375B272D8F6EA4AE9F7650A7C96686,
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_Reset_mA169DB740A8A7154826E4EF0736B2DF07DC1AF8B,
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F_CustomAttributesCacheGenerator_U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_get_Current_m433F960B7017A11D26D2CF999E1E72077C29C412,
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8__ctor_mC96B3D3DCFE50A6FD8A6894A512EDC67D0308430,
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_IDisposable_Dispose_mACAB1D384D2A3973F6604BC082A4841937B5EAD2,
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m987832127609D559655B0F45011479F84CCF5AD3,
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_Reset_mB570E836E9EE3A72D1E67E98807C52514F030F17,
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5_CustomAttributesCacheGenerator_U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_get_Current_mC2D7EBA1B6E5B1872CBFC18737136E4F44D453F4,
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16__ctor_m449E2AF3B9297CB1F6B68DC54427D53B0D481A41,
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_IDisposable_Dispose_mF22941E1A5FA87FF66CFA4CBB0EF3FB497B04DFA,
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m319547258507AB779A3F29F8C08FE499C9D29026,
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_Reset_m68C9FCF8FF223384464DF36D0CEC510BFE410240,
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF_CustomAttributesCacheGenerator_U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_get_Current_mC1D95606D9F1B0C1FA62787A1700AEF6430FFB76,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportingStart_mF967C93A8772C6AF9DF70DB5AD1503B276B8C3B4,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportingStart_m2F7F74D1CA7C50EEF9BB842B790C461AF6693100,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportingComplete_m9A892024861058A6FF56116A60B365B91076E0CD,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportingComplete_m9F2D2CCDB062B2F027A0DD7EBA9C446E3275C68E,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_CreatedModel_m178EB892D4E4FE81E6CA3ADBF19F5849423315C2,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_CreatedModel_m2BA274F4B150A6A181E5AFE20479075FB177612E,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportedModel_m6C50C797294CB6EB2D8CB556321954E2BD0959C9,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportedModel_m16878B820F61B0039349B22718E6D8F884931B0E,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_add_ImportError_mD47F0F2C636D79952DA5DBA5F5A76FE002BAFD40,
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D_CustomAttributesCacheGenerator_ObjectImporter_remove_ImportError_m93DF9FAC2DF5F4642BD530AA12D665BA8B684082,
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_get_Position_m39B376C28A912E63492FFDC60D91460C36EDB7BC,
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_set_Position_m34AEC05B85BDA60FCEDD74055FA4659C17212D3C,
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_get_OriginalIndex_m993575EDF4FDA693778501A1FEC370E83BDDF8D5,
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA_CustomAttributesCacheGenerator_Vertex_set_OriginalIndex_m8BB3858C9BA804333E0DCD3CB9669D543551F661,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
