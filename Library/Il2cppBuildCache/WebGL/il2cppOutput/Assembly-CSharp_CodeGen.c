﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Texture2D MTLLoader::TextureLoadFunction(System.String,System.Boolean)
extern void MTLLoader_TextureLoadFunction_m09A9871A19D6F5351732639D001CAD4A9B3C2EFF (void);
// 0x00000002 UnityEngine.Texture2D MTLLoader::TryLoadTexture(System.String,System.Boolean)
extern void MTLLoader_TryLoadTexture_mE77138911E4861BF99DA17D8977866B448902DE4 (void);
// 0x00000003 System.Int32 MTLLoader::GetArgValueCount(System.String)
extern void MTLLoader_GetArgValueCount_m200C2986F85BFC80A83F78F3AAA0230B33B088B6 (void);
// 0x00000004 System.Int32 MTLLoader::GetTexNameIndex(System.String[])
extern void MTLLoader_GetTexNameIndex_m665BC7D4E4EE57BD635E6269A9347C427DD5CA32 (void);
// 0x00000005 System.Single MTLLoader::GetArgValue(System.String[],System.String,System.Single)
extern void MTLLoader_GetArgValue_mFAE340D18A3669E1FE0F9AE2EF80D3ED6AAE5E55 (void);
// 0x00000006 System.String MTLLoader::GetTexPathFromMapStatement(System.String,System.String[])
extern void MTLLoader_GetTexPathFromMapStatement_m7B30B10BDD96DA432C4429112F015D60AB168EF2 (void);
// 0x00000007 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> MTLLoader::Load(System.IO.Stream)
extern void MTLLoader_Load_m069D9E2F221F03FA1D2A972F52C133DE1C1310CC (void);
// 0x00000008 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> MTLLoader::Load(System.String)
extern void MTLLoader_Load_mDC4DE42F6E4FB4E2E7A1418934C36FE303CF9F6C (void);
// 0x00000009 System.Void MTLLoader::.ctor()
extern void MTLLoader__ctor_m09ABDFD4907D9F5CBEE0A7BD231730FC3BF89CE9 (void);
// 0x0000000A System.Void ObjFromFile::OnGUI()
extern void ObjFromFile_OnGUI_m0323BCECC6C3175D2BBC6A35B35D0F33EBDE3D89 (void);
// 0x0000000B System.Void ObjFromFile::.ctor()
extern void ObjFromFile__ctor_mE9BBFB0301921CD1F9363610DDC3BE99A859E538 (void);
// 0x0000000C System.Void ObjFromStream::Start()
extern void ObjFromStream_Start_mD4140981D260A0DE3D04CEBB8332DDCEC1C9DFA2 (void);
// 0x0000000D System.Void ObjFromStream::.ctor()
extern void ObjFromStream__ctor_mCD88711D645EB1E8513AB99C3D5B5769F0B6EC47 (void);
// 0x0000000E System.Void mainMenu::Start()
extern void mainMenu_Start_m3409ECFCEA4CA479FCD8643BF891EC0FF0D4DCBC (void);
// 0x0000000F System.Void mainMenu::configureListeners()
extern void mainMenu_configureListeners_m795DBAFAD89DC8095BA60A2077FF7BC5FDE0D730 (void);
// 0x00000010 System.Void mainMenu::loadSelectedModel()
extern void mainMenu_loadSelectedModel_mEE82704C795A12668CD6E3212BE6D53EC8E0F8AB (void);
// 0x00000011 System.Void mainMenu::updateDropdown()
extern void mainMenu_updateDropdown_mF168432B305F0A58C5598871F7D4D76D49893944 (void);
// 0x00000012 System.Void mainMenu::LoadSceneButton()
extern void mainMenu_LoadSceneButton_m2DBD38292ED5A396C8B3FB75B7862BB55A318556 (void);
// 0x00000013 System.Void mainMenu::SetActiveSceneButton()
extern void mainMenu_SetActiveSceneButton_m79B1B3686AD54780B9BD2341490D7FF4C819256A (void);
// 0x00000014 System.Void mainMenu::OpenFileChooser(System.Boolean)
extern void mainMenu_OpenFileChooser_mB10D677DD5BF860883CED35439CB350B86E31868 (void);
// 0x00000015 System.Void mainMenu::generateModel()
extern void mainMenu_generateModel_m832EBD44B172C138039F65574485CB7B7C8C7565 (void);
// 0x00000016 System.Collections.IEnumerator mainMenu::ShowLoadDialogCoroutine(System.Boolean)
extern void mainMenu_ShowLoadDialogCoroutine_m67FA4AFF637FCD4C4A5EA99D38AE652FCA400514 (void);
// 0x00000017 System.Void mainMenu::dicom2Mesh(System.String,System.String,System.Int32,System.Single,System.Single,System.Boolean)
extern void mainMenu_dicom2Mesh_m41BAC2220FB7404CFE8B17DBE6A8CD5D7A94B2E4 (void);
// 0x00000018 System.String mainMenu::constructParameters(System.String,System.String,System.Int32,System.Single,System.Single,System.Boolean)
extern void mainMenu_constructParameters_m58E431D26E90896CD12550FE7FB3579790DD1A8F (void);
// 0x00000019 System.Void mainMenu::OutputHandler(System.Object,System.Diagnostics.DataReceivedEventArgs)
extern void mainMenu_OutputHandler_m59D0ED6CF7BE844476440D66EB333B24D5AE30E5 (void);
// 0x0000001A System.Void mainMenu::Update()
extern void mainMenu_Update_m5D44F29640B47BD5A96523B2DF21656B31D13830 (void);
// 0x0000001B System.Void mainMenu::.ctor()
extern void mainMenu__ctor_mE3659A1D9717CE7DB207A80A81E608028AF0ECD0 (void);
// 0x0000001C System.Void mainMenu::<configureListeners>b__6_0()
extern void mainMenu_U3CconfigureListenersU3Eb__6_0_m3D4E196FAB3FED9950537F5704CF5B6B525DF6E7 (void);
// 0x0000001D System.Void mainMenu::<configureListeners>b__6_1()
extern void mainMenu_U3CconfigureListenersU3Eb__6_1_mBD72798DD4C4E98D97A33D2B94524AB6E77A75DC (void);
// 0x0000001E System.Void mainMenu::<configureListeners>b__6_2()
extern void mainMenu_U3CconfigureListenersU3Eb__6_2_m2337882BE0CA863007C70EA0897E8328A7C54E29 (void);
// 0x0000001F System.Void mainMenu::<configureListeners>b__6_4()
extern void mainMenu_U3CconfigureListenersU3Eb__6_4_m2E8D5C592A0F3AF59C8C3300DBDB10E7D49B5575 (void);
// 0x00000020 System.Void mainMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_m02255B54D95C964A5FBAFC2F817558D4B14AEC82 (void);
// 0x00000021 System.Void mainMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_mB1F8AA87B45A8B57A6DC8FB74E2F5B950984FFC9 (void);
// 0x00000022 System.Void mainMenu/<>c::<configureListeners>b__6_3()
extern void U3CU3Ec_U3CconfigureListenersU3Eb__6_3_m0CA4536117ABEEC6E4C1C6948683AB7ED9460E5C (void);
// 0x00000023 System.Void mainMenu/<ShowLoadDialogCoroutine>d__13::.ctor(System.Int32)
extern void U3CShowLoadDialogCoroutineU3Ed__13__ctor_m4B6600EB7805E7F4F86F09C4C321ADA942A57780 (void);
// 0x00000024 System.Void mainMenu/<ShowLoadDialogCoroutine>d__13::System.IDisposable.Dispose()
extern void U3CShowLoadDialogCoroutineU3Ed__13_System_IDisposable_Dispose_m01FB2C32275EB18BBBD4F60A45455486495EE968 (void);
// 0x00000025 System.Boolean mainMenu/<ShowLoadDialogCoroutine>d__13::MoveNext()
extern void U3CShowLoadDialogCoroutineU3Ed__13_MoveNext_m591957769AF4C012013C30EF0519C233912CB711 (void);
// 0x00000026 System.Object mainMenu/<ShowLoadDialogCoroutine>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2AECCA9C2680930F4A4A1DE1A9DDD485BDB3925D (void);
// 0x00000027 System.Void mainMenu/<ShowLoadDialogCoroutine>d__13::System.Collections.IEnumerator.Reset()
extern void U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_Reset_m76BB3A7BCE2E32D7D2806770112C1AA06E7EC596 (void);
// 0x00000028 System.Object mainMenu/<ShowLoadDialogCoroutine>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_get_Current_mDF35685524E8BB7D14D1AA103D95FD7F4DBF22F1 (void);
// 0x00000029 System.Int32 B83.Image.BMP.BitmapInfoHeader::get_absWidth()
extern void BitmapInfoHeader_get_absWidth_m29DD5E462E03A235354F22118E1DF9DFE6E751AD (void);
// 0x0000002A System.Int32 B83.Image.BMP.BitmapInfoHeader::get_absHeight()
extern void BitmapInfoHeader_get_absHeight_m46F0A269C8ED63B5071CD2236A16E8E4D8DED021 (void);
// 0x0000002B UnityEngine.Texture2D B83.Image.BMP.BMPImage::ToTexture2D()
extern void BMPImage_ToTexture2D_m1319F6AA5B9059D7006BD3ADA18D5A7B65B8CD1A (void);
// 0x0000002C System.Void B83.Image.BMP.BMPImage::.ctor()
extern void BMPImage__ctor_m4BE9499052B860FC015D2E12501EE738D9B9B403 (void);
// 0x0000002D B83.Image.BMP.BMPImage B83.Image.BMP.BMPLoader::LoadBMP(System.String)
extern void BMPLoader_LoadBMP_mF30DA3DEFA407675C366CBB70CD89D1CA6D5DFEA (void);
// 0x0000002E B83.Image.BMP.BMPImage B83.Image.BMP.BMPLoader::LoadBMP(System.Byte[])
extern void BMPLoader_LoadBMP_mCC3AD2FAC3C38F05E95D29AF1D30F739FC678D0E (void);
// 0x0000002F B83.Image.BMP.BMPImage B83.Image.BMP.BMPLoader::LoadBMP(System.IO.Stream)
extern void BMPLoader_LoadBMP_m898201054F07D1F209BE2F6E65F1C65E8D4B5E5D (void);
// 0x00000030 B83.Image.BMP.BMPImage B83.Image.BMP.BMPLoader::LoadBMP(System.IO.BinaryReader)
extern void BMPLoader_LoadBMP_mAE1BD831832DD4C73D6EC4FCA8FED3734B447E03 (void);
// 0x00000031 System.Void B83.Image.BMP.BMPLoader::Read32BitImage(System.IO.BinaryReader,B83.Image.BMP.BMPImage)
extern void BMPLoader_Read32BitImage_m828FA1D3A522A9435AF2BF0BCF0AE02FB70ABD02 (void);
// 0x00000032 System.Void B83.Image.BMP.BMPLoader::Read24BitImage(System.IO.BinaryReader,B83.Image.BMP.BMPImage)
extern void BMPLoader_Read24BitImage_m6AB565B5E2D4BF08B51C01F8964B91E7E75739D8 (void);
// 0x00000033 System.Void B83.Image.BMP.BMPLoader::Read16BitImage(System.IO.BinaryReader,B83.Image.BMP.BMPImage)
extern void BMPLoader_Read16BitImage_mCDF3EA0452D745636BEA76F779174376C4152788 (void);
// 0x00000034 System.Void B83.Image.BMP.BMPLoader::ReadIndexedImage(System.IO.BinaryReader,B83.Image.BMP.BMPImage)
extern void BMPLoader_ReadIndexedImage_m8415B6D10B5AAC890733B8F701FB713A57F6A132 (void);
// 0x00000035 System.Void B83.Image.BMP.BMPLoader::ReadIndexedImageRLE4(System.IO.BinaryReader,B83.Image.BMP.BMPImage)
extern void BMPLoader_ReadIndexedImageRLE4_m507FD2AA1A8D5D7D73D32070A84351136F7EB62F (void);
// 0x00000036 System.Void B83.Image.BMP.BMPLoader::ReadIndexedImageRLE8(System.IO.BinaryReader,B83.Image.BMP.BMPImage)
extern void BMPLoader_ReadIndexedImageRLE8_m74D4DA30FDE797A6554425C97CA819F0FEC8BF30 (void);
// 0x00000037 System.Int32 B83.Image.BMP.BMPLoader::GetShiftCount(System.UInt32)
extern void BMPLoader_GetShiftCount_m564FC367120D0F4D8D88FB72C0C7022A20892825 (void);
// 0x00000038 System.UInt32 B83.Image.BMP.BMPLoader::GetMask(System.Int32)
extern void BMPLoader_GetMask_mD2190CA042F9B1597A0936312BA4A93F75BB0CEC (void);
// 0x00000039 System.Boolean B83.Image.BMP.BMPLoader::ReadFileHeader(System.IO.BinaryReader,B83.Image.BMP.BMPFileHeader&)
extern void BMPLoader_ReadFileHeader_m9A6EE25392F2CDCD6EE4B05B80DFCCEB14CF8D7C (void);
// 0x0000003A System.Boolean B83.Image.BMP.BMPLoader::ReadInfoHeader(System.IO.BinaryReader,B83.Image.BMP.BitmapInfoHeader&)
extern void BMPLoader_ReadInfoHeader_mB6EB8C42C47E850D6BA2174387DF3D851BA3E7B1 (void);
// 0x0000003B System.Collections.Generic.List`1<UnityEngine.Color32> B83.Image.BMP.BMPLoader::ReadPalette(System.IO.BinaryReader,B83.Image.BMP.BMPImage,System.Boolean)
extern void BMPLoader_ReadPalette_mABC3EF3B4AD0C037A2EB019EEA3D18B092FE7E47 (void);
// 0x0000003C System.Void B83.Image.BMP.BMPLoader::.ctor()
extern void BMPLoader__ctor_m58293ED2D5394FDC53883B9F28AEA4AC4F5FC7E9 (void);
// 0x0000003D System.Void B83.Image.BMP.BitStreamReader::.ctor(System.IO.BinaryReader)
extern void BitStreamReader__ctor_m8310D226B2DB1FBEDF06E91B7FBDCB292315DE8B (void);
// 0x0000003E System.Void B83.Image.BMP.BitStreamReader::.ctor(System.IO.Stream)
extern void BitStreamReader__ctor_m08AF2B47A4153DDAE4A63DBD64DFE4C9D777366F (void);
// 0x0000003F System.Byte B83.Image.BMP.BitStreamReader::ReadBit()
extern void BitStreamReader_ReadBit_m33D70EC31D7A8CF4E40EAF6D9866CACAE0EA3F89 (void);
// 0x00000040 System.UInt64 B83.Image.BMP.BitStreamReader::ReadBits(System.Int32)
extern void BitStreamReader_ReadBits_m6D9B837B8D3D59FC018BAF8315A192721FCB6E23 (void);
// 0x00000041 System.Void B83.Image.BMP.BitStreamReader::Flush()
extern void BitStreamReader_Flush_m8D202513A3237F9AA99E17DB5F1A52E293C75546 (void);
// 0x00000042 System.Void Dummiesman.CharWordReader::.ctor(System.IO.StreamReader,System.Int32)
extern void CharWordReader__ctor_m0F497C1D870CF796766AF6876339CD2DF1549C88 (void);
// 0x00000043 System.Void Dummiesman.CharWordReader::SkipWhitespaces()
extern void CharWordReader_SkipWhitespaces_m6C6C7EF0950407BAF326F48C81B9B7D2CE2B5037 (void);
// 0x00000044 System.Void Dummiesman.CharWordReader::SkipWhitespaces(System.Boolean&)
extern void CharWordReader_SkipWhitespaces_mCEC5201CDF4C4D10442D51A743710BC29FC209B2 (void);
// 0x00000045 System.Void Dummiesman.CharWordReader::SkipUntilNewLine()
extern void CharWordReader_SkipUntilNewLine_m7E0311B86AB6FC198152523F9578820A2FB09726 (void);
// 0x00000046 System.Void Dummiesman.CharWordReader::ReadUntilWhiteSpace()
extern void CharWordReader_ReadUntilWhiteSpace_m2C75BB0AA47DDCF6E3D7DB032ED112B7EBC22509 (void);
// 0x00000047 System.Void Dummiesman.CharWordReader::ReadUntilNewLine()
extern void CharWordReader_ReadUntilNewLine_mD4426E6A4B64982C8E47CD53662EF1D2477D87DF (void);
// 0x00000048 System.Boolean Dummiesman.CharWordReader::Is(System.String)
extern void CharWordReader_Is_m3C81A7727B4E4EDD7119D852D85A2BE98A70C629 (void);
// 0x00000049 System.String Dummiesman.CharWordReader::GetString(System.Int32)
extern void CharWordReader_GetString_m8106220DF5E5557185BB859FB70AF05C56302418 (void);
// 0x0000004A UnityEngine.Vector3 Dummiesman.CharWordReader::ReadVector()
extern void CharWordReader_ReadVector_mBF5D6281BED528D7A0BBF6D084C8DFBECABDF15C (void);
// 0x0000004B System.Int32 Dummiesman.CharWordReader::ReadInt()
extern void CharWordReader_ReadInt_m91505746F5627EE4F9655A006451DE36027768E9 (void);
// 0x0000004C System.Single Dummiesman.CharWordReader::ReadFloat()
extern void CharWordReader_ReadFloat_mCA65124A5DBD9FEBCE10315A6E032D89833D1FEA (void);
// 0x0000004D System.Single Dummiesman.CharWordReader::ReadFloatEnd()
extern void CharWordReader_ReadFloatEnd_m11C69AAC8D104C091F81D49832DFB199DBEF417A (void);
// 0x0000004E System.Void Dummiesman.CharWordReader::SkipNewLineSymbols()
extern void CharWordReader_SkipNewLineSymbols_mC1F4132F600407CD94A42B0CF68BF8BAA0A154E6 (void);
// 0x0000004F System.Void Dummiesman.CharWordReader::MoveNext()
extern void CharWordReader_MoveNext_m2E9862FB91C9B46BF25B9B4D15653A0108419EEE (void);
// 0x00000050 System.Void Dummiesman.OBJLoader::LoadMaterialLibrary(System.String)
extern void OBJLoader_LoadMaterialLibrary_m3E4D33857ED7B8787F8C8DBB987B0FC9BC17141C (void);
// 0x00000051 UnityEngine.GameObject Dummiesman.OBJLoader::Load(System.IO.Stream)
extern void OBJLoader_Load_m84C0006E0F986946481D5ECE4B230D418A49C75B (void);
// 0x00000052 UnityEngine.GameObject Dummiesman.OBJLoader::Load(System.IO.Stream,System.IO.Stream)
extern void OBJLoader_Load_mB6A0EFCC5E89697C11BC735544CE16ADED3F172C (void);
// 0x00000053 UnityEngine.GameObject Dummiesman.OBJLoader::Load(System.String,System.String)
extern void OBJLoader_Load_m71B83AFC549B39742BC5437885A7B3E174EAB313 (void);
// 0x00000054 UnityEngine.GameObject Dummiesman.OBJLoader::Load(System.String)
extern void OBJLoader_Load_mADD589597CC52044C238A2FF2FC814B4B25AB4AB (void);
// 0x00000055 System.Void Dummiesman.OBJLoader::.ctor()
extern void OBJLoader__ctor_m2A29D87A50D4D692C023E1B349E9010606C60CDA (void);
// 0x00000056 System.Void Dummiesman.OBJLoader/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m0B8617E43AD163CDE3E3B02DA8E15906900C2F28 (void);
// 0x00000057 System.Void Dummiesman.OBJLoader/<>c__DisplayClass7_0::<Load>b__0(System.String)
extern void U3CU3Ec__DisplayClass7_0_U3CLoadU3Eb__0_m4D0A804BD38A2A47113C3DE8F0FC9293ECCFB04A (void);
// 0x00000058 System.Void Dummiesman.OBJLoaderHelper::EnableMaterialTransparency(UnityEngine.Material)
extern void OBJLoaderHelper_EnableMaterialTransparency_m0F838E8E9D1FEB86417DD35FDEC89B3C71B83A17 (void);
// 0x00000059 System.Single Dummiesman.OBJLoaderHelper::FastFloatParse(System.String)
extern void OBJLoaderHelper_FastFloatParse_m0ABA03819C1D38E22041897A5218372939CCF62E (void);
// 0x0000005A System.Int32 Dummiesman.OBJLoaderHelper::FastIntParse(System.String)
extern void OBJLoaderHelper_FastIntParse_mA807DC7E1B18EC766C5E657D5FE04A92199A5CCB (void);
// 0x0000005B UnityEngine.Material Dummiesman.OBJLoaderHelper::CreateNullMaterial()
extern void OBJLoaderHelper_CreateNullMaterial_m435BF547B1AE26265789C607A0BB3A6A012647AA (void);
// 0x0000005C UnityEngine.Vector3 Dummiesman.OBJLoaderHelper::VectorFromStrArray(System.String[])
extern void OBJLoaderHelper_VectorFromStrArray_m108BABF5B4B4FBE77149F0EA9BBE0D5D4B8A3763 (void);
// 0x0000005D UnityEngine.Color Dummiesman.OBJLoaderHelper::ColorFromStrArray(System.String[],System.Single)
extern void OBJLoaderHelper_ColorFromStrArray_m54B3D99C6EB38AF997E9F5F3E8420701856C2974 (void);
// 0x0000005E System.Int32 Dummiesman.OBJObjectBuilder::get_PushedFaceCount()
extern void OBJObjectBuilder_get_PushedFaceCount_m9A51064310CB6F80EE840BA9067EE95EF8220F06 (void);
// 0x0000005F System.Void Dummiesman.OBJObjectBuilder::set_PushedFaceCount(System.Int32)
extern void OBJObjectBuilder_set_PushedFaceCount_mE5FC98B476DA97661D6B440D1CE8781C82AC3042 (void);
// 0x00000060 UnityEngine.GameObject Dummiesman.OBJObjectBuilder::Build()
extern void OBJObjectBuilder_Build_m9292D5C63F5E1E3C7B5E32A3CFCD7E956F2B4C7E (void);
// 0x00000061 System.Void Dummiesman.OBJObjectBuilder::SetMaterial(System.String)
extern void OBJObjectBuilder_SetMaterial_m130EF05B8B56A9888B2BD253F8267858E6AA8188 (void);
// 0x00000062 System.Void Dummiesman.OBJObjectBuilder::PushFace(System.String,System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Int32>)
extern void OBJObjectBuilder_PushFace_mBAC6F523C1189692256B58C31B54BA10C265C54B (void);
// 0x00000063 System.Void Dummiesman.OBJObjectBuilder::.ctor(System.String,Dummiesman.OBJLoader)
extern void OBJObjectBuilder__ctor_m6CE8E4E7C197E1B4C3E4DA2276BB189984677A37 (void);
// 0x00000064 System.Boolean Dummiesman.OBJObjectBuilder/ObjLoopHash::Equals(System.Object)
extern void ObjLoopHash_Equals_m4045656E38151C1B8BE3C2563FD6B4999377FA68 (void);
// 0x00000065 System.Int32 Dummiesman.OBJObjectBuilder/ObjLoopHash::GetHashCode()
extern void ObjLoopHash_GetHashCode_mB03E4094552B962AA7C2698972047EE2FF365E23 (void);
// 0x00000066 System.Void Dummiesman.OBJObjectBuilder/ObjLoopHash::.ctor()
extern void ObjLoopHash__ctor_m269DD59F077394D2A398ECF25AF3B4A3FF27B0DC (void);
// 0x00000067 System.String Dummiesman.StringExtensions::Clean(System.String)
extern void StringExtensions_Clean_mBD817F18A962673D4157FDD426B9324C092BD734 (void);
// 0x00000068 UnityEngine.Color32 Dummiesman.BinaryExtensions::ReadColor32RGBR(System.IO.BinaryReader)
extern void BinaryExtensions_ReadColor32RGBR_mE26BFB16967BE261B57740558FC02FD3AD82EE78 (void);
// 0x00000069 UnityEngine.Color32 Dummiesman.BinaryExtensions::ReadColor32RGBA(System.IO.BinaryReader)
extern void BinaryExtensions_ReadColor32RGBA_mA6987C56F059E266336BFBF30067DA149B825F39 (void);
// 0x0000006A UnityEngine.Color32 Dummiesman.BinaryExtensions::ReadColor32RGB(System.IO.BinaryReader)
extern void BinaryExtensions_ReadColor32RGB_m968635C4AFB130CBB829DC65251E1E388F1B1D7E (void);
// 0x0000006B UnityEngine.Color32 Dummiesman.BinaryExtensions::ReadColor32BGR(System.IO.BinaryReader)
extern void BinaryExtensions_ReadColor32BGR_mFA7375B3F8EDA89B1F6BE3BD82E148804E8855EE (void);
// 0x0000006C UnityEngine.Texture2D Dummiesman.DDSLoader::Load(System.IO.Stream)
extern void DDSLoader_Load_m4C08D89BDB06C305DAC028014E475D9A6CB30019 (void);
// 0x0000006D UnityEngine.Texture2D Dummiesman.DDSLoader::Load(System.String)
extern void DDSLoader_Load_m6B482F4E3C4F4EADEDB438FDF18D471E0B042852 (void);
// 0x0000006E UnityEngine.Texture2D Dummiesman.DDSLoader::Load(System.Byte[])
extern void DDSLoader_Load_m49BDEFE41B6816E47C28FEC5E465C799F3D5DAA6 (void);
// 0x0000006F System.Void Dummiesman.ImageLoader::SetNormalMap(UnityEngine.Texture2D&)
extern void ImageLoader_SetNormalMap_m54B7B74AB389DC11798E442B267A85883D7AD959 (void);
// 0x00000070 UnityEngine.Texture2D Dummiesman.ImageLoader::LoadTexture(System.IO.Stream,Dummiesman.ImageLoader/TextureFormat)
extern void ImageLoader_LoadTexture_mC9D1F44DADB2E2AE8DFA5915F6F3F8D84A2F6182 (void);
// 0x00000071 UnityEngine.Texture2D Dummiesman.ImageLoader::LoadTexture(System.String)
extern void ImageLoader_LoadTexture_m72598D61C94895B3DFB7A2D4184AF036C0C8147F (void);
// 0x00000072 System.Void Dummiesman.ImageLoader::.ctor()
extern void ImageLoader__ctor_m03610F404B6E496BAA27809B55B8830604AB2BE6 (void);
// 0x00000073 UnityEngine.Texture2D Dummiesman.ImageLoaderHelper::VerifyFormat(UnityEngine.Texture2D)
extern void ImageLoaderHelper_VerifyFormat_m5B08C89C83028E94CAB14CFC745CED42CBBFB4EA (void);
// 0x00000074 System.Void Dummiesman.ImageLoaderHelper::FillPixelArray(UnityEngine.Color32[],System.Byte[],System.Int32,System.Boolean)
extern void ImageLoaderHelper_FillPixelArray_mB67884D323AD1B9161110D78DB97128D6D1D8110 (void);
// 0x00000075 System.Void Dummiesman.ImageLoaderHelper::.ctor()
extern void ImageLoaderHelper__ctor_mDA60FBFAF4077A5B055E32B8385DCDE3A2A7273C (void);
// 0x00000076 UnityEngine.Texture2D Dummiesman.ImageUtils::ConvertToNormalMap(UnityEngine.Texture2D)
extern void ImageUtils_ConvertToNormalMap_m383343212117BBB20A1580F2C3758485016B665F (void);
// 0x00000077 System.Int32 Dummiesman.TGALoader::GetBits(System.Byte,System.Int32,System.Int32)
extern void TGALoader_GetBits_m87D2DCCEF149DF8DC8841957243E783748F4CA0C (void);
// 0x00000078 UnityEngine.Color32[] Dummiesman.TGALoader::LoadRawTGAData(System.IO.BinaryReader,System.Int32,System.Int32,System.Int32)
extern void TGALoader_LoadRawTGAData_mC63040F4276598D8B0DA88EC184EBB36256B5248 (void);
// 0x00000079 UnityEngine.Color32[] Dummiesman.TGALoader::LoadRLETGAData(System.IO.BinaryReader,System.Int32,System.Int32,System.Int32)
extern void TGALoader_LoadRLETGAData_m65192B81EB466963D935CC8A6590C7F7DA92118C (void);
// 0x0000007A UnityEngine.Texture2D Dummiesman.TGALoader::Load(System.String)
extern void TGALoader_Load_m2752C133CE7C6F43E0BD596932AB44AC75BC77EF (void);
// 0x0000007B UnityEngine.Texture2D Dummiesman.TGALoader::Load(System.Byte[])
extern void TGALoader_Load_m7492EAE7244471443B83B6C01CFC841F4F14060F (void);
// 0x0000007C UnityEngine.Texture2D Dummiesman.TGALoader::Load(System.IO.Stream)
extern void TGALoader_Load_m3884F2B51C2235C5B97DB6A306607386069FD5D6 (void);
// 0x0000007D System.Void Dummiesman.TGALoader::.ctor()
extern void TGALoader__ctor_m5F5C44E5FBDB9CE459B21FB87445ED7096A9D653 (void);
// 0x0000007E UnityEngine.Color Dummiesman.Extensions.ColorExtensions::FlipRB(UnityEngine.Color)
extern void ColorExtensions_FlipRB_m621188E25D11376AF450BD484FA3F50475370785 (void);
// 0x0000007F UnityEngine.Color32 Dummiesman.Extensions.ColorExtensions::FlipRB(UnityEngine.Color32)
extern void ColorExtensions_FlipRB_m4689B607045C6100B80EB112A617E94235A9469D (void);
// 0x00000080 System.String AsImpL.DataSet::get_CurrGroupName()
extern void DataSet_get_CurrGroupName_m09420C11302898F7072C80D1F42B7091712319F3 (void);
// 0x00000081 System.Boolean AsImpL.DataSet::get_IsEmpty()
extern void DataSet_get_IsEmpty_mFD6E338266F0FFE1066ADA35058F04B92263863F (void);
// 0x00000082 System.String AsImpL.DataSet::GetFaceIndicesKey(AsImpL.DataSet/FaceIndices)
extern void DataSet_GetFaceIndicesKey_m506350EA2878556157D1C2AA5E4DDC82FA4FC4F6 (void);
// 0x00000083 System.String AsImpL.DataSet::FixMaterialName(System.String)
extern void DataSet_FixMaterialName_m6707E70C463D382835F0FD96D12E0E798B99B546 (void);
// 0x00000084 System.Void AsImpL.DataSet::.ctor()
extern void DataSet__ctor_m6971FE870DE2FAA395813D3BDA2BEA0317DD25D2 (void);
// 0x00000085 System.Void AsImpL.DataSet::AddObject(System.String)
extern void DataSet_AddObject_m9FB7257A4317C83E028FAE578D437362F0A0DCE3 (void);
// 0x00000086 System.Void AsImpL.DataSet::AddGroup(System.String)
extern void DataSet_AddGroup_m926F9EF063AAD403FF441A30BFD43FA82204232F (void);
// 0x00000087 System.Void AsImpL.DataSet::AddMaterialName(System.String)
extern void DataSet_AddMaterialName_m00AB8E080F912D78138FAEC6C3FC8927D7EFA66E (void);
// 0x00000088 System.Void AsImpL.DataSet::AddVertex(UnityEngine.Vector3)
extern void DataSet_AddVertex_m2F16605FD673B64FD9396DD8D42A89D0750A61A2 (void);
// 0x00000089 System.Void AsImpL.DataSet::AddUV(UnityEngine.Vector2)
extern void DataSet_AddUV_m4AD05B643C4AD1F13D5CD4B47CD46B7D3C0AEF07 (void);
// 0x0000008A System.Void AsImpL.DataSet::AddNormal(UnityEngine.Vector3)
extern void DataSet_AddNormal_m1F6BA0C73EC0CD34E2DA0DDDA5C971EB52F756AB (void);
// 0x0000008B System.Void AsImpL.DataSet::AddColor(UnityEngine.Color)
extern void DataSet_AddColor_mB1B92705CF49601C203B31674FDE5D0F48BC1850 (void);
// 0x0000008C System.Void AsImpL.DataSet::AddFaceIndices(AsImpL.DataSet/FaceIndices)
extern void DataSet_AddFaceIndices_m0CBA3502005CB31A4C846180272BDABF99869F08 (void);
// 0x0000008D System.Void AsImpL.DataSet::PrintSummary()
extern void DataSet_PrintSummary_m60F659DECDB02450F2E2D5FD98E3372A114A01C6 (void);
// 0x0000008E System.Void AsImpL.DataSet/ObjectData::.ctor()
extern void ObjectData__ctor_mBA3C40BDF5ED93C4430FB30B35566A7DBB0E829E (void);
// 0x0000008F System.Void AsImpL.DataSet/FaceGroupData::.ctor()
extern void FaceGroupData__ctor_m83350BD5E0DCE710F2C5945C556B10A62C57CF39 (void);
// 0x00000090 System.Boolean AsImpL.DataSet/FaceGroupData::get_IsEmpty()
extern void FaceGroupData_get_IsEmpty_m3B5E451CCF4DA4CD8CF12C97EBCDE9E614F04A74 (void);
// 0x00000091 System.Void AsImpL.MaterialData::.ctor()
extern void MaterialData__ctor_mAA0A3C9950A0FD380AFFFB5F626DDF959433C8FC (void);
// 0x00000092 System.Void AsImpL.ModelUtil::SetupMaterialWithBlendMode(UnityEngine.Material,AsImpL.ModelUtil/MtlBlendMode)
extern void ModelUtil_SetupMaterialWithBlendMode_mA969B6E72F767FAB6BC8DC11BD7C3A5F1CBCDFFE (void);
// 0x00000093 System.Boolean AsImpL.ModelUtil::ScanTransparentPixels(UnityEngine.Texture2D,AsImpL.ModelUtil/MtlBlendMode&)
extern void ModelUtil_ScanTransparentPixels_mA7EBE50D52111C4CC321B0C26E2B5C7812BA2F7C (void);
// 0x00000094 System.Void AsImpL.ModelUtil::DetectMtlBlendFadeOrCutout(System.Single,AsImpL.ModelUtil/MtlBlendMode&,System.Boolean&)
extern void ModelUtil_DetectMtlBlendFadeOrCutout_mAB0DCDFB4D96563DD1118C9E0C0E7672BC0FDE9D (void);
// 0x00000095 UnityEngine.Texture2D AsImpL.ModelUtil::HeightToNormalMap(UnityEngine.Texture2D,System.Single)
extern void ModelUtil_HeightToNormalMap_m7298B601A0ABDC8FB2F81E51BEE8C86A4BDE126E (void);
// 0x00000096 System.Int32 AsImpL.ModelUtil::WrapInt(System.Int32,System.Int32)
extern void ModelUtil_WrapInt_mBF9311FADF1F38241F513B44B534B67309D4BB73 (void);
// 0x00000097 System.Void AsImpL.ModelUtil::.ctor()
extern void ModelUtil__ctor_m96FFE76BCD72F106DE400FB1530A7FEDD102C83C (void);
// 0x00000098 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> AsImpL.ObjectBuilder::get_ImportedMaterials()
extern void ObjectBuilder_get_ImportedMaterials_mECB8544D6C7486E6C776C53118B70798E0838A93 (void);
// 0x00000099 System.Int32 AsImpL.ObjectBuilder::get_NumImportedMaterials()
extern void ObjectBuilder_get_NumImportedMaterials_m5BB878C4A084ABA6D28F19FE23CC8E8B13DE31E8 (void);
// 0x0000009A System.Void AsImpL.ObjectBuilder::InitBuildMaterials(System.Collections.Generic.List`1<AsImpL.MaterialData>,System.Boolean)
extern void ObjectBuilder_InitBuildMaterials_m5F4B83DF9A3109192A0469EED7BD44BE07C25B2B (void);
// 0x0000009B System.Boolean AsImpL.ObjectBuilder::BuildMaterials(AsImpL.ObjectBuilder/ProgressInfo)
extern void ObjectBuilder_BuildMaterials_mF67A04083D61A87FB2CC07B6987A2299C585CD93 (void);
// 0x0000009C System.Void AsImpL.ObjectBuilder::StartBuildObjectAsync(AsImpL.DataSet,UnityEngine.GameObject,System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>)
extern void ObjectBuilder_StartBuildObjectAsync_m7BED00FA4B0E6C48518BB244E19AD4CC1B9DCC22 (void);
// 0x0000009D System.Boolean AsImpL.ObjectBuilder::BuildObjectAsync(AsImpL.ObjectBuilder/ProgressInfo&)
extern void ObjectBuilder_BuildObjectAsync_m7F473E66A3F43267F3483E9BE01245A109456D40 (void);
// 0x0000009E System.Void AsImpL.ObjectBuilder::Solve(UnityEngine.Mesh)
extern void ObjectBuilder_Solve_mED61C100AB65B37F38EC1FB5538D012B1817EA75 (void);
// 0x0000009F System.Void AsImpL.ObjectBuilder::BuildMeshCollider(UnityEngine.GameObject,System.Boolean,System.Boolean,System.Boolean,System.Single)
extern void ObjectBuilder_BuildMeshCollider_mBBB25A024702EBFE6BDC3814D1A5332518D416B9 (void);
// 0x000000A0 System.Boolean AsImpL.ObjectBuilder::BuildNextObject(UnityEngine.GameObject,System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>)
extern void ObjectBuilder_BuildNextObject_mDF514126A63577FE4CFFB335C3DFE929CC476223 (void);
// 0x000000A1 UnityEngine.GameObject AsImpL.ObjectBuilder::ImportSubObject(UnityEngine.GameObject,AsImpL.DataSet/ObjectData,System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>)
extern void ObjectBuilder_ImportSubObject_mC1EDF7C09EB5BF7877624D9A7171103B69E2C4B2 (void);
// 0x000000A2 UnityEngine.Material AsImpL.ObjectBuilder::BuildMaterial(AsImpL.MaterialData)
extern void ObjectBuilder_BuildMaterial_m79CC542900FF22C01F063862D1A8427BF8D4FB1E (void);
// 0x000000A3 System.Boolean AsImpL.ObjectBuilder::Using32bitIndices()
extern void ObjectBuilder_Using32bitIndices_mC5FE3465AF75D46401E6C0DC81E609A8C13E8E96 (void);
// 0x000000A4 System.Void AsImpL.ObjectBuilder::.ctor()
extern void ObjectBuilder__ctor_mD90CC57A26B89DDE8DAAF91EBBDECEEA78603730 (void);
// 0x000000A5 System.Void AsImpL.ObjectBuilder::.cctor()
extern void ObjectBuilder__cctor_mB3CABC142B0C11FB0846FC39391EB4B8B58793E5 (void);
// 0x000000A6 System.Void AsImpL.ObjectBuilder/ProgressInfo::.ctor()
extern void ProgressInfo__ctor_m272DD2F266BC13188B09796694C9755B1FE35D96 (void);
// 0x000000A7 System.Void AsImpL.ObjectBuilder/BuildStatus::.ctor()
extern void BuildStatus__ctor_mB75F2A47F94B76F79DDEE8D6CBE9CF73A45E9D5A (void);
// 0x000000A8 System.Void AsImpL.Triangulator::Triangulate(AsImpL.DataSet,AsImpL.DataSet/FaceIndices[])
extern void Triangulator_Triangulate_m5FF55D863BEC0E02F3EAE424E1DF0276B8A9EEB4 (void);
// 0x000000A9 UnityEngine.Vector3 AsImpL.Triangulator::FindPlaneNormal(AsImpL.DataSet,AsImpL.DataSet/FaceIndices[])
extern void Triangulator_FindPlaneNormal_m5CB314C194FE01D8AF494F764AED1A76E3BEA57C (void);
// 0x000000AA System.Void AsImpL.ImportOptions::.ctor()
extern void ImportOptions__ctor_m0770080AEBEDA39A2BBDCC8A649B4CA525F5729D (void);
// 0x000000AB System.Boolean AsImpL.Loader::get_ConvertVertAxis()
extern void Loader_get_ConvertVertAxis_mFCC4AF705C2593AE1FF34CAA91CBBE7943812644 (void);
// 0x000000AC System.Void AsImpL.Loader::set_ConvertVertAxis(System.Boolean)
extern void Loader_set_ConvertVertAxis_m1CB277ED557387444660F635BEB7C27D6828A0CD (void);
// 0x000000AD System.Single AsImpL.Loader::get_Scaling()
extern void Loader_get_Scaling_m9769BCADA294F639830D107EACF9D4934240A18D (void);
// 0x000000AE System.Void AsImpL.Loader::set_Scaling(System.Single)
extern void Loader_set_Scaling_mCCB9C2711E159841C6EB17DF20FCBFAED75BE55F (void);
// 0x000000AF System.Boolean AsImpL.Loader::get_HasMaterialLibrary()
// 0x000000B0 System.Void AsImpL.Loader::add_ModelCreated(System.Action`2<UnityEngine.GameObject,System.String>)
extern void Loader_add_ModelCreated_m8992EE812B09EB58B47864AB996D16B47EA07FB6 (void);
// 0x000000B1 System.Void AsImpL.Loader::remove_ModelCreated(System.Action`2<UnityEngine.GameObject,System.String>)
extern void Loader_remove_ModelCreated_mA5DD5078F249FC7D57A468B487C89C9FC0CD0085 (void);
// 0x000000B2 System.Void AsImpL.Loader::add_ModelLoaded(System.Action`2<UnityEngine.GameObject,System.String>)
extern void Loader_add_ModelLoaded_m7A065F03EFAA5AC2FE5FD2D792BB0FFA310F8142 (void);
// 0x000000B3 System.Void AsImpL.Loader::remove_ModelLoaded(System.Action`2<UnityEngine.GameObject,System.String>)
extern void Loader_remove_ModelLoaded_m2A7830BB84800200D10E269230C57DF7EF9B58E5 (void);
// 0x000000B4 System.Void AsImpL.Loader::add_ModelError(System.Action`1<System.String>)
extern void Loader_add_ModelError_m56EDA25051268763FDDC531159F60AEC088B53CE (void);
// 0x000000B5 System.Void AsImpL.Loader::remove_ModelError(System.Action`1<System.String>)
extern void Loader_remove_ModelError_m525B768D2BB19243FE1905EC21D1548E6147068E (void);
// 0x000000B6 UnityEngine.GameObject AsImpL.Loader::GetModelByPath(System.String)
extern void Loader_GetModelByPath_m671847AF7A9E9BB030997667BC357890676124D2 (void);
// 0x000000B7 System.Collections.IEnumerator AsImpL.Loader::Load(System.String,System.String,UnityEngine.Transform)
extern void Loader_Load_m4DC773597C6E42E7B9AE846A83E8DE6866D56353 (void);
// 0x000000B8 System.String[] AsImpL.Loader::ParseTexturePaths(System.String)
// 0x000000B9 System.Collections.IEnumerator AsImpL.Loader::LoadModelFile(System.String)
// 0x000000BA System.Collections.IEnumerator AsImpL.Loader::LoadMaterialLibrary(System.String)
// 0x000000BB System.Collections.IEnumerator AsImpL.Loader::Build(System.String,System.String,UnityEngine.Transform)
extern void Loader_Build_mF8D000CD4CC7267CE431AA748594A985C4E890AF (void);
// 0x000000BC System.String AsImpL.Loader::GetDirName(System.String)
extern void Loader_GetDirName_mE2DCBDCA6616BE07ABF1CF62A645272C461861D9 (void);
// 0x000000BD System.Void AsImpL.Loader::OnLoaded(UnityEngine.GameObject,System.String)
extern void Loader_OnLoaded_mB2D8D687F460831EDDF804F00CD4E4B933D71440 (void);
// 0x000000BE System.Void AsImpL.Loader::OnCreated(UnityEngine.GameObject,System.String)
extern void Loader_OnCreated_mBDF98C3B64E87029C98E1A87CA7386064EFFF53B (void);
// 0x000000BF System.Void AsImpL.Loader::OnLoadFailed(System.String)
extern void Loader_OnLoadFailed_mBC5020778D01655CA808A0430335DE3B2533C923 (void);
// 0x000000C0 System.String AsImpL.Loader::GetTextureUrl(System.String,System.String)
extern void Loader_GetTextureUrl_m9098D5BBCF874362012451C841519DBBF034209F (void);
// 0x000000C1 System.Collections.IEnumerator AsImpL.Loader::LoadMaterialTexture(System.String,System.String)
extern void Loader_LoadMaterialTexture_m440076E0E24E8754B2044227984452E07818025E (void);
// 0x000000C2 UnityEngine.Texture2D AsImpL.Loader::LoadTexture(UnityEngine.Networking.UnityWebRequest)
extern void Loader_LoadTexture_mAC0A6DC5EFDA49DD4B3F89FDF3889D06B6A2AD07 (void);
// 0x000000C3 System.Void AsImpL.Loader::.ctor()
extern void Loader__ctor_m2D29856FA5DBE97CB21A92B8DA0A8603E8E4C8FC (void);
// 0x000000C4 System.Void AsImpL.Loader::.cctor()
extern void Loader__cctor_m0A29251BB34EC2906BE556B705DA194493BD6819 (void);
// 0x000000C5 System.Void AsImpL.Loader/<Load>d__32::.ctor(System.Int32)
extern void U3CLoadU3Ed__32__ctor_m73C8A633B599ED1F4ED404A8ECDF82DCD633BCA2 (void);
// 0x000000C6 System.Void AsImpL.Loader/<Load>d__32::System.IDisposable.Dispose()
extern void U3CLoadU3Ed__32_System_IDisposable_Dispose_m76F0AB7C4A259585D6C69D2D48DD59B16ED7F944 (void);
// 0x000000C7 System.Boolean AsImpL.Loader/<Load>d__32::MoveNext()
extern void U3CLoadU3Ed__32_MoveNext_m69237F8C341CDC08F7231248E0E2A235714AC1E8 (void);
// 0x000000C8 System.Object AsImpL.Loader/<Load>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4814EA0F29D8615752EFF8176625E0CF12F5E10C (void);
// 0x000000C9 System.Void AsImpL.Loader/<Load>d__32::System.Collections.IEnumerator.Reset()
extern void U3CLoadU3Ed__32_System_Collections_IEnumerator_Reset_m53352D1E2DF974F4FF214A8F561008C0B688519B (void);
// 0x000000CA System.Object AsImpL.Loader/<Load>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CLoadU3Ed__32_System_Collections_IEnumerator_get_Current_m33F97DBD0C22371E96278B6FF2B29A7B86EB09A1 (void);
// 0x000000CB System.Void AsImpL.Loader/<Build>d__36::.ctor(System.Int32)
extern void U3CBuildU3Ed__36__ctor_m2746753CE6057324F628EF2130856DA6E70D389D (void);
// 0x000000CC System.Void AsImpL.Loader/<Build>d__36::System.IDisposable.Dispose()
extern void U3CBuildU3Ed__36_System_IDisposable_Dispose_mBEEA3AF4CC543B059B5992CC6AB36554A98BFEE0 (void);
// 0x000000CD System.Boolean AsImpL.Loader/<Build>d__36::MoveNext()
extern void U3CBuildU3Ed__36_MoveNext_m2AB412717C10187EFFDE5CFEAFB0E326C4F3DBC7 (void);
// 0x000000CE System.Void AsImpL.Loader/<Build>d__36::<>m__Finally1()
extern void U3CBuildU3Ed__36_U3CU3Em__Finally1_m2C742BFD2FEC9407C659BD90D0132EF6401464A0 (void);
// 0x000000CF System.Object AsImpL.Loader/<Build>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBuildU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234701C996636E960294EDAF55C3CAA01DCD2092 (void);
// 0x000000D0 System.Void AsImpL.Loader/<Build>d__36::System.Collections.IEnumerator.Reset()
extern void U3CBuildU3Ed__36_System_Collections_IEnumerator_Reset_m786116A825908E278B23C5A6FEA64FCAE7BB5BEA (void);
// 0x000000D1 System.Object AsImpL.Loader/<Build>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CBuildU3Ed__36_System_Collections_IEnumerator_get_Current_m367ECE887DF4A49C3B21942EA6A37D688242EA51 (void);
// 0x000000D2 System.Void AsImpL.Loader/<LoadMaterialTexture>d__42::.ctor(System.Int32)
extern void U3CLoadMaterialTextureU3Ed__42__ctor_m4EA7B131CF7AB5224995803A3878296B0A70CB0A (void);
// 0x000000D3 System.Void AsImpL.Loader/<LoadMaterialTexture>d__42::System.IDisposable.Dispose()
extern void U3CLoadMaterialTextureU3Ed__42_System_IDisposable_Dispose_m8D9EC2C3145CDE449264D604FFBF8A74DAC0CFD8 (void);
// 0x000000D4 System.Boolean AsImpL.Loader/<LoadMaterialTexture>d__42::MoveNext()
extern void U3CLoadMaterialTextureU3Ed__42_MoveNext_m070794AC59F67C9D99887807AB40A5BF46223924 (void);
// 0x000000D5 System.Void AsImpL.Loader/<LoadMaterialTexture>d__42::<>m__Finally1()
extern void U3CLoadMaterialTextureU3Ed__42_U3CU3Em__Finally1_m04E13DEF8FBEA253C51C19FCA13D4692C0200BE4 (void);
// 0x000000D6 System.Object AsImpL.Loader/<LoadMaterialTexture>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadMaterialTextureU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52FA484524B04EF7CB4E0B3DCABF955F37778AED (void);
// 0x000000D7 System.Void AsImpL.Loader/<LoadMaterialTexture>d__42::System.Collections.IEnumerator.Reset()
extern void U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_Reset_m9849C2B35AD8DF82C95FB6A427C4298EE351A212 (void);
// 0x000000D8 System.Object AsImpL.Loader/<LoadMaterialTexture>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_get_Current_mA9EAD8C0E049278489EF63EDD9A8B8E72899A510 (void);
// 0x000000D9 System.String[] AsImpL.LoaderObj::ParseTexturePaths(System.String)
extern void LoaderObj_ParseTexturePaths_m0A42A45F24C53DEE743213A0ACE943FAC4A853F2 (void);
// 0x000000DA System.Collections.IEnumerator AsImpL.LoaderObj::LoadModelFile(System.String)
extern void LoaderObj_LoadModelFile_m78B90897589F239C23519364BE7691CC1BF85829 (void);
// 0x000000DB System.Collections.IEnumerator AsImpL.LoaderObj::LoadMaterialLibrary(System.String)
extern void LoaderObj_LoadMaterialLibrary_mC4CDB8A952C1B852AE2EB4DF8254A6990EAE5755 (void);
// 0x000000DC System.Void AsImpL.LoaderObj::GetFaceIndicesByOneFaceLine(AsImpL.DataSet/FaceIndices[],System.String[],System.Boolean)
extern void LoaderObj_GetFaceIndicesByOneFaceLine_m75789C09856C63F4E2DC410FCD4053DE9CD02979 (void);
// 0x000000DD UnityEngine.Vector3 AsImpL.LoaderObj::ConvertVec3(System.Single,System.Single,System.Single)
extern void LoaderObj_ConvertVec3_m0A9148B32F2D11757630F18E67299B90A6374E3E (void);
// 0x000000DE System.Single AsImpL.LoaderObj::ParseFloat(System.String)
extern void LoaderObj_ParseFloat_m09B6A4FAFD5B64045AE84F74F8E83F2F673D11C0 (void);
// 0x000000DF System.Collections.IEnumerator AsImpL.LoaderObj::ParseGeometryData(System.String)
extern void LoaderObj_ParseGeometryData_m4AA82D03093B53A6B2F002D8091BFD3FDF007FE1 (void);
// 0x000000E0 System.String AsImpL.LoaderObj::ParseMaterialLibName(System.String)
extern void LoaderObj_ParseMaterialLibName_mAAF7C7C29F8BC9C30A93FD0488234893B289D438 (void);
// 0x000000E1 System.Boolean AsImpL.LoaderObj::get_HasMaterialLibrary()
extern void LoaderObj_get_HasMaterialLibrary_mD5284050AB5BAC6CC93926A851E29EFEA465B777 (void);
// 0x000000E2 System.Void AsImpL.LoaderObj::ParseMaterialData(System.String)
extern void LoaderObj_ParseMaterialData_m0540619C5B10A1D4287575D4864058454DEDC603 (void);
// 0x000000E3 System.Void AsImpL.LoaderObj::ParseMaterialData(System.String[],System.Collections.Generic.List`1<AsImpL.MaterialData>)
extern void LoaderObj_ParseMaterialData_m7B82DDA18F38556CE0580956FF4BE2F88D0B27E2 (void);
// 0x000000E4 System.Void AsImpL.LoaderObj::ParseBumpParameters(System.String[],AsImpL.MaterialData)
extern void LoaderObj_ParseBumpParameters_m5AEE0312F23063508C8DCA3D09E9CD3CF8DC7584 (void);
// 0x000000E5 UnityEngine.Color AsImpL.LoaderObj::StringsToColor(System.String[])
extern void LoaderObj_StringsToColor_m09A21991578C1579084759320824488EE2BD7A0F (void);
// 0x000000E6 System.Collections.IEnumerator AsImpL.LoaderObj::LoadOrDownloadText(System.String,System.Boolean)
extern void LoaderObj_LoadOrDownloadText_mB967181C6B7DF4C1FD7F423BDDC08A0F721158B0 (void);
// 0x000000E7 System.Void AsImpL.LoaderObj::.ctor()
extern void LoaderObj__ctor_m664CEF04C4257C4DA19EB6361F1ADD761FD279C3 (void);
// 0x000000E8 System.Void AsImpL.LoaderObj/BumpParamDef::.ctor(System.String,System.String,System.Int32,System.Int32)
extern void BumpParamDef__ctor_m31C8BD508D8BAAB93F428707C999F74C7345E686 (void);
// 0x000000E9 System.Void AsImpL.LoaderObj/<LoadModelFile>d__3::.ctor(System.Int32)
extern void U3CLoadModelFileU3Ed__3__ctor_m31EC5B02C9FFDD0107481311078DC237D33FC350 (void);
// 0x000000EA System.Void AsImpL.LoaderObj/<LoadModelFile>d__3::System.IDisposable.Dispose()
extern void U3CLoadModelFileU3Ed__3_System_IDisposable_Dispose_m302B51DB67B5231710E448287F3A65CBCADD3773 (void);
// 0x000000EB System.Boolean AsImpL.LoaderObj/<LoadModelFile>d__3::MoveNext()
extern void U3CLoadModelFileU3Ed__3_MoveNext_m18D28E36003CA86A4EF36443EC6B156621ED7B80 (void);
// 0x000000EC System.Object AsImpL.LoaderObj/<LoadModelFile>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadModelFileU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m157DEF0B8196F4E4A4532C9A4D456025AFA21240 (void);
// 0x000000ED System.Void AsImpL.LoaderObj/<LoadModelFile>d__3::System.Collections.IEnumerator.Reset()
extern void U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_Reset_m8A5292170989E075719F3393BD0DA6B1AEFA1BD3 (void);
// 0x000000EE System.Object AsImpL.LoaderObj/<LoadModelFile>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_get_Current_m033DB07B0490629CDC70C9E82462D27750683230 (void);
// 0x000000EF System.Void AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::.ctor(System.Int32)
extern void U3CLoadMaterialLibraryU3Ed__4__ctor_m857E4E65341EE9147C0AE4B475AACD135325D97B (void);
// 0x000000F0 System.Void AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::System.IDisposable.Dispose()
extern void U3CLoadMaterialLibraryU3Ed__4_System_IDisposable_Dispose_m338D35DA7855937147A8B8807F490E0CD56334FA (void);
// 0x000000F1 System.Boolean AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::MoveNext()
extern void U3CLoadMaterialLibraryU3Ed__4_MoveNext_mBE76692CB7FA17B8BDA2EA80C5C1A3697AA7274B (void);
// 0x000000F2 System.Object AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadMaterialLibraryU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DE19B3479375B272D8F6EA4AE9F7650A7C96686 (void);
// 0x000000F3 System.Void AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::System.Collections.IEnumerator.Reset()
extern void U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_Reset_mA169DB740A8A7154826E4EF0736B2DF07DC1AF8B (void);
// 0x000000F4 System.Object AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_get_Current_m433F960B7017A11D26D2CF999E1E72077C29C412 (void);
// 0x000000F5 System.Void AsImpL.LoaderObj/<ParseGeometryData>d__8::.ctor(System.Int32)
extern void U3CParseGeometryDataU3Ed__8__ctor_mC96B3D3DCFE50A6FD8A6894A512EDC67D0308430 (void);
// 0x000000F6 System.Void AsImpL.LoaderObj/<ParseGeometryData>d__8::System.IDisposable.Dispose()
extern void U3CParseGeometryDataU3Ed__8_System_IDisposable_Dispose_mACAB1D384D2A3973F6604BC082A4841937B5EAD2 (void);
// 0x000000F7 System.Boolean AsImpL.LoaderObj/<ParseGeometryData>d__8::MoveNext()
extern void U3CParseGeometryDataU3Ed__8_MoveNext_mF117DA3476FF7F0ACD2B2A4AFB463FF59DDC22EF (void);
// 0x000000F8 System.Object AsImpL.LoaderObj/<ParseGeometryData>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CParseGeometryDataU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m987832127609D559655B0F45011479F84CCF5AD3 (void);
// 0x000000F9 System.Void AsImpL.LoaderObj/<ParseGeometryData>d__8::System.Collections.IEnumerator.Reset()
extern void U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_Reset_mB570E836E9EE3A72D1E67E98807C52514F030F17 (void);
// 0x000000FA System.Object AsImpL.LoaderObj/<ParseGeometryData>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_get_Current_mC2D7EBA1B6E5B1872CBFC18737136E4F44D453F4 (void);
// 0x000000FB System.Void AsImpL.LoaderObj/<LoadOrDownloadText>d__16::.ctor(System.Int32)
extern void U3CLoadOrDownloadTextU3Ed__16__ctor_m449E2AF3B9297CB1F6B68DC54427D53B0D481A41 (void);
// 0x000000FC System.Void AsImpL.LoaderObj/<LoadOrDownloadText>d__16::System.IDisposable.Dispose()
extern void U3CLoadOrDownloadTextU3Ed__16_System_IDisposable_Dispose_mF22941E1A5FA87FF66CFA4CBB0EF3FB497B04DFA (void);
// 0x000000FD System.Boolean AsImpL.LoaderObj/<LoadOrDownloadText>d__16::MoveNext()
extern void U3CLoadOrDownloadTextU3Ed__16_MoveNext_m40AED1D063767C8DC8C67B80468A1471688E34AA (void);
// 0x000000FE System.Object AsImpL.LoaderObj/<LoadOrDownloadText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadOrDownloadTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m319547258507AB779A3F29F8C08FE499C9D29026 (void);
// 0x000000FF System.Void AsImpL.LoaderObj/<LoadOrDownloadText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_Reset_m68C9FCF8FF223384464DF36D0CEC510BFE410240 (void);
// 0x00000100 System.Object AsImpL.LoaderObj/<LoadOrDownloadText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_get_Current_mC1D95606D9F1B0C1FA62787A1700AEF6430FFB76 (void);
// 0x00000101 UnityEngine.Texture2D AsImpL.TextureLoader::LoadTextureFromUrl(System.String)
extern void TextureLoader_LoadTextureFromUrl_m253D4C7F1B376EB1542A2073E295F3B05375E641 (void);
// 0x00000102 UnityEngine.Texture2D AsImpL.TextureLoader::LoadTexture(System.String)
extern void TextureLoader_LoadTexture_mBD892E20EEAD731B6032E7C4934011B5756D98D0 (void);
// 0x00000103 UnityEngine.Texture2D AsImpL.TextureLoader::LoadTGA(System.String)
extern void TextureLoader_LoadTGA_mC6B13F465CAA35F3A5894D06781DF8781371DEAE (void);
// 0x00000104 UnityEngine.Texture2D AsImpL.TextureLoader::LoadDDSManual(System.String)
extern void TextureLoader_LoadDDSManual_mE0C492E2CE870E9F9746FC88FA2693D51384A90E (void);
// 0x00000105 UnityEngine.Texture2D AsImpL.TextureLoader::LoadTGA(System.IO.Stream)
extern void TextureLoader_LoadTGA_mBADD6D4522B23C756F6D18E879325C54D3A7D4B5 (void);
// 0x00000106 AsImpL.TextureLoader/TgaHeader AsImpL.TextureLoader::LoadTgaHeader(System.IO.BinaryReader)
extern void TextureLoader_LoadTgaHeader_mDF48D34A9DEA349EC09275104C360DBB3129BB31 (void);
// 0x00000107 System.Void AsImpL.TextureLoader::.ctor()
extern void TextureLoader__ctor_mAE7A4998857ECA4B8585F720B05632F65E568A95 (void);
// 0x00000108 System.Void AsImpL.TextureLoader/TgaHeader::.ctor()
extern void TgaHeader__ctor_m2E1A7D00698B384A622BF8F829CD511756A2EA70 (void);
// 0x00000109 System.Void AsImpL.SingleLoadingProgress::.ctor()
extern void SingleLoadingProgress__ctor_m0217A2F9D64DF860F4EA62E0F274E54A0C490674 (void);
// 0x0000010A System.Void AsImpL.LoadingProgress::.ctor()
extern void LoadingProgress__ctor_mF6D959443EFC179A4C4E37C35854579426F9F716 (void);
// 0x0000010B System.Void AsImpL.ModelImportInfo::.ctor()
extern void ModelImportInfo__ctor_mF4479545CA11639353E1123548A4C27EAAB5AFC6 (void);
// 0x0000010C System.String AsImpL.MultiObjectImporter::get_RootPath()
extern void MultiObjectImporter_get_RootPath_mC352AFD7AABA876D92BE718CC202361D4128B5AC (void);
// 0x0000010D System.Void AsImpL.MultiObjectImporter::ImportModelListAsync(AsImpL.ModelImportInfo[])
extern void MultiObjectImporter_ImportModelListAsync_m5D6D7DBDF0F9F4BA9EB2EFD92C857678A2FD20B3 (void);
// 0x0000010E System.Void AsImpL.MultiObjectImporter::Start()
extern void MultiObjectImporter_Start_m479F65DE89933BD9CF129A23756DC6C5E144F363 (void);
// 0x0000010F System.Void AsImpL.MultiObjectImporter::.ctor()
extern void MultiObjectImporter__ctor_mD441A20FEC84DED9A104C77158C0AFB58AB0ECEA (void);
// 0x00000110 System.Void AsImpL.ObjectImporter::add_ImportingStart(System.Action)
extern void ObjectImporter_add_ImportingStart_mF967C93A8772C6AF9DF70DB5AD1503B276B8C3B4 (void);
// 0x00000111 System.Void AsImpL.ObjectImporter::remove_ImportingStart(System.Action)
extern void ObjectImporter_remove_ImportingStart_m2F7F74D1CA7C50EEF9BB842B790C461AF6693100 (void);
// 0x00000112 System.Void AsImpL.ObjectImporter::add_ImportingComplete(System.Action)
extern void ObjectImporter_add_ImportingComplete_m9A892024861058A6FF56116A60B365B91076E0CD (void);
// 0x00000113 System.Void AsImpL.ObjectImporter::remove_ImportingComplete(System.Action)
extern void ObjectImporter_remove_ImportingComplete_m9F2D2CCDB062B2F027A0DD7EBA9C446E3275C68E (void);
// 0x00000114 System.Void AsImpL.ObjectImporter::add_CreatedModel(System.Action`2<UnityEngine.GameObject,System.String>)
extern void ObjectImporter_add_CreatedModel_m178EB892D4E4FE81E6CA3ADBF19F5849423315C2 (void);
// 0x00000115 System.Void AsImpL.ObjectImporter::remove_CreatedModel(System.Action`2<UnityEngine.GameObject,System.String>)
extern void ObjectImporter_remove_CreatedModel_m2BA274F4B150A6A181E5AFE20479075FB177612E (void);
// 0x00000116 System.Void AsImpL.ObjectImporter::add_ImportedModel(System.Action`2<UnityEngine.GameObject,System.String>)
extern void ObjectImporter_add_ImportedModel_m6C50C797294CB6EB2D8CB556321954E2BD0959C9 (void);
// 0x00000117 System.Void AsImpL.ObjectImporter::remove_ImportedModel(System.Action`2<UnityEngine.GameObject,System.String>)
extern void ObjectImporter_remove_ImportedModel_m16878B820F61B0039349B22718E6D8F884931B0E (void);
// 0x00000118 System.Void AsImpL.ObjectImporter::add_ImportError(System.Action`1<System.String>)
extern void ObjectImporter_add_ImportError_mD47F0F2C636D79952DA5DBA5F5A76FE002BAFD40 (void);
// 0x00000119 System.Void AsImpL.ObjectImporter::remove_ImportError(System.Action`1<System.String>)
extern void ObjectImporter_remove_ImportError_m93DF9FAC2DF5F4642BD530AA12D665BA8B684082 (void);
// 0x0000011A System.Int32 AsImpL.ObjectImporter::get_NumImportRequests()
extern void ObjectImporter_get_NumImportRequests_m5A38CAA9856617DF3BF130604227EBB315B3E895 (void);
// 0x0000011B AsImpL.Loader AsImpL.ObjectImporter::CreateLoader(System.String)
extern void ObjectImporter_CreateLoader_m3FE9BA7749342EC1862ECB8D4131CC2E14246E86 (void);
// 0x0000011C System.Void AsImpL.ObjectImporter::ImportModelAsync(System.String,System.String,UnityEngine.Transform,AsImpL.ImportOptions)
extern void ObjectImporter_ImportModelAsync_mF2D0DBEB7082C165F1F9F35E3C74E0A762D11731 (void);
// 0x0000011D System.Void AsImpL.ObjectImporter::UpdateStatus()
extern void ObjectImporter_UpdateStatus_m3D69476B9E79362A7ECD7C6FA321C177FFD53EDE (void);
// 0x0000011E System.Void AsImpL.ObjectImporter::Update()
extern void ObjectImporter_Update_m47D34B4B4BD725F1264BC92361224D5B88866D6B (void);
// 0x0000011F System.Void AsImpL.ObjectImporter::OnImportingComplete()
extern void ObjectImporter_OnImportingComplete_mBE4241253EA259B294AC427BE790B0E6181BB3BA (void);
// 0x00000120 System.Void AsImpL.ObjectImporter::OnModelCreated(UnityEngine.GameObject,System.String)
extern void ObjectImporter_OnModelCreated_mAB12B1AA902EF43171AA43BCA80E2E3E56DF41C8 (void);
// 0x00000121 System.Void AsImpL.ObjectImporter::OnImported(UnityEngine.GameObject,System.String)
extern void ObjectImporter_OnImported_m845D1192567B2D7AA8C851B4AB4E6F8199E7CDBB (void);
// 0x00000122 System.Void AsImpL.ObjectImporter::OnImportError(System.String)
extern void ObjectImporter_OnImportError_m5023EB7AD6E364B6C82B51EDBC2E5800AF8D40A5 (void);
// 0x00000123 System.Void AsImpL.ObjectImporter::.ctor()
extern void ObjectImporter__ctor_m26565F09B5CDD627B75A039763A481E5B59D6C05 (void);
// 0x00000124 System.Void AsImpL.ObjectImporterUI::Awake()
extern void ObjectImporterUI_Awake_m5CC4408989784EC94E2D2C5C44D48548AF56E764 (void);
// 0x00000125 System.Void AsImpL.ObjectImporterUI::OnEnable()
extern void ObjectImporterUI_OnEnable_m7A64184436CB20E91C2D5FE4A4DD3FE880929608 (void);
// 0x00000126 System.Void AsImpL.ObjectImporterUI::OnDisable()
extern void ObjectImporterUI_OnDisable_m29E8EF0623BB91DE5C4E9E2BBA8996F31D948C25 (void);
// 0x00000127 System.Void AsImpL.ObjectImporterUI::Update()
extern void ObjectImporterUI_Update_mBE47D8ED6B6E5E21F37BA0ED43CE721756057A86 (void);
// 0x00000128 System.Void AsImpL.ObjectImporterUI::OnImportStart()
extern void ObjectImporterUI_OnImportStart_m901718D2E90A2DCEDFFA2AE830C0B3F808F1B052 (void);
// 0x00000129 System.Void AsImpL.ObjectImporterUI::OnImportComplete()
extern void ObjectImporterUI_OnImportComplete_m3B3447B4DCFB45AD5BEF777BD68F8BEFA71DE7BE (void);
// 0x0000012A System.Void AsImpL.ObjectImporterUI::.ctor()
extern void ObjectImporterUI__ctor_m4C9E65824CA2EDAC149375BC4319AAA165728C34 (void);
// 0x0000012B System.String AsImpL.PathSettings::get_RootPath()
extern void PathSettings_get_RootPath_m7F43F3D368D119097AADD19DA8C7EF037B8B94F4 (void);
// 0x0000012C AsImpL.PathSettings AsImpL.PathSettings::FindPathComponent(UnityEngine.GameObject)
extern void PathSettings_FindPathComponent_m21149B93770E417A3A3F64105C8B6ABEBEFDB5EC (void);
// 0x0000012D System.String AsImpL.PathSettings::FullPath(System.String)
extern void PathSettings_FullPath_mC1DAA758C6D67B1A0EA7E28698FCA308F51B31F4 (void);
// 0x0000012E System.Void AsImpL.PathSettings::.ctor()
extern void PathSettings__ctor_m94C07886C54AC7BA19A1ABE8025F4E34D49FA30D (void);
// 0x0000012F System.Void AsImpL.MathUtil.Triangle::.ctor(AsImpL.MathUtil.Vertex,AsImpL.MathUtil.Vertex,AsImpL.MathUtil.Vertex)
extern void Triangle__ctor_m67A27B9F99280B61520B2071A9FF83DC9378BD48 (void);
// 0x00000130 UnityEngine.Vector3 AsImpL.MathUtil.Vertex::get_Position()
extern void Vertex_get_Position_m39B376C28A912E63492FFDC60D91460C36EDB7BC (void);
// 0x00000131 System.Void AsImpL.MathUtil.Vertex::set_Position(UnityEngine.Vector3)
extern void Vertex_set_Position_m34AEC05B85BDA60FCEDD74055FA4659C17212D3C (void);
// 0x00000132 System.Int32 AsImpL.MathUtil.Vertex::get_OriginalIndex()
extern void Vertex_get_OriginalIndex_m993575EDF4FDA693778501A1FEC370E83BDDF8D5 (void);
// 0x00000133 System.Void AsImpL.MathUtil.Vertex::set_OriginalIndex(System.Int32)
extern void Vertex_set_OriginalIndex_m8BB3858C9BA804333E0DCD3CB9669D543551F661 (void);
// 0x00000134 AsImpL.MathUtil.Vertex AsImpL.MathUtil.Vertex::get_PreviousVertex()
extern void Vertex_get_PreviousVertex_mB01B95271B099F86C43206E0DC5952B1726906E7 (void);
// 0x00000135 System.Void AsImpL.MathUtil.Vertex::set_PreviousVertex(AsImpL.MathUtil.Vertex)
extern void Vertex_set_PreviousVertex_mF77E162C66D380C4D6A6DCF0EB50FF5CA769DACA (void);
// 0x00000136 AsImpL.MathUtil.Vertex AsImpL.MathUtil.Vertex::get_NextVertex()
extern void Vertex_get_NextVertex_m469C0B8C2BCE7DA5F19F4E3B6B659CE900C1A31D (void);
// 0x00000137 System.Void AsImpL.MathUtil.Vertex::set_NextVertex(AsImpL.MathUtil.Vertex)
extern void Vertex_set_NextVertex_mDE848CF323CEA421E9E5E6416E3D253C5E4C47F4 (void);
// 0x00000138 System.Single AsImpL.MathUtil.Vertex::get_TriangleArea()
extern void Vertex_get_TriangleArea_m36C0DC59C1A063F9B5D794D9785F4CC877DE2523 (void);
// 0x00000139 System.Void AsImpL.MathUtil.Vertex::.ctor(System.Int32,UnityEngine.Vector3)
extern void Vertex__ctor_mDDBB7CCC2200DA1F64BFC0CD68066E1645FA843A (void);
// 0x0000013A UnityEngine.Vector2 AsImpL.MathUtil.Vertex::GetPosOnPlane(UnityEngine.Vector3)
extern void Vertex_GetPosOnPlane_m04E821C5285EEEEF867CF8DF0B8D45A4793882C3 (void);
// 0x0000013B System.Void AsImpL.MathUtil.Vertex::ComputeTriangleArea()
extern void Vertex_ComputeTriangleArea_m71AF6EFBAE09FC3AF4CE4BA8E66EFF68A1363AFF (void);
// 0x0000013C System.Int32 AsImpL.MathUtil.MathUtility::ClampListIndex(System.Int32,System.Int32)
extern void MathUtility_ClampListIndex_m2F4D71FC9998358588DC470485115B601E949DB7 (void);
// 0x0000013D System.Boolean AsImpL.MathUtil.MathUtility::IsPointInTriangle(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void MathUtility_IsPointInTriangle_mDE459C9808B2590D267B63D2AF225CA2A70E646F (void);
// 0x0000013E System.Boolean AsImpL.MathUtil.MathUtility::IsTriangleOrientedClockwise(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void MathUtility_IsTriangleOrientedClockwise_m2D894EA67B697240A5898F190010C344393D271B (void);
// 0x0000013F UnityEngine.Vector3 AsImpL.MathUtil.MathUtility::ComputeNormal(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MathUtility_ComputeNormal_m6714E9F666C9DD7E724E0C5B87907F1CFF0CDD64 (void);
// 0x00000140 System.Collections.Generic.List`1<AsImpL.MathUtil.Triangle> AsImpL.MathUtil.Triangulation::TriangulateConvexPolygon(System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>,System.Boolean)
extern void Triangulation_TriangulateConvexPolygon_m75287D6100A633732759F6A64AD45CDE6388B00B (void);
// 0x00000141 System.Collections.Generic.List`1<AsImpL.MathUtil.Triangle> AsImpL.MathUtil.Triangulation::TriangulateByEarClipping(System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>,UnityEngine.Vector3,System.String,System.Boolean)
extern void Triangulation_TriangulateByEarClipping_m826EEE5D578C55327325F1B60C75A8860081D2E0 (void);
// 0x00000142 AsImpL.MathUtil.Triangle AsImpL.MathUtil.Triangulation::ClipTriangle(AsImpL.MathUtil.Vertex,System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>)
extern void Triangulation_ClipTriangle_mC2AC86F736D5A979F4E6C14E8C3CDA2205809D81 (void);
// 0x00000143 AsImpL.MathUtil.Triangle AsImpL.MathUtil.Triangulation::ClipEar(AsImpL.MathUtil.Vertex,System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>,System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>,UnityEngine.Vector3)
extern void Triangulation_ClipEar_mF5E5DACB0C1539C39CE5509A9FBE461E017731E4 (void);
// 0x00000144 AsImpL.MathUtil.Vertex AsImpL.MathUtil.Triangulation::FindMaxAreaEarVertex(System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>)
extern void Triangulation_FindMaxAreaEarVertex_m694E4F9C83C1D3C9317D7D0B4225772604C27AA4 (void);
// 0x00000145 System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex> AsImpL.MathUtil.Triangulation::FindEarVertices(System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>,UnityEngine.Vector3)
extern void Triangulation_FindEarVertices_m467C24BE20B4073C0D68A5E9EBB33B1EE82509CD (void);
// 0x00000146 System.Boolean AsImpL.MathUtil.Triangulation::IsVertexReflex(AsImpL.MathUtil.Vertex,UnityEngine.Vector3)
extern void Triangulation_IsVertexReflex_mFC56A1AEF4B1FB63B55EE04C03B697DCF4450A9C (void);
// 0x00000147 System.Boolean AsImpL.MathUtil.Triangulation::IsVertexEar(AsImpL.MathUtil.Vertex,System.Collections.Generic.List`1<AsImpL.MathUtil.Vertex>,UnityEngine.Vector3)
extern void Triangulation_IsVertexEar_m03BC0E0B69A400B551DE69E52848D93372AA36F6 (void);
// 0x00000148 System.Void AsImpL.Examples.AsImpLSample::Awake()
extern void AsImpLSample_Awake_m9213473518EE4612BAAE21816377B73D911FE3E3 (void);
// 0x00000149 System.Void AsImpL.Examples.AsImpLSample::Start()
extern void AsImpLSample_Start_mEB1576451F33DE46139970CDA0850C97C949E835 (void);
// 0x0000014A System.Void AsImpL.Examples.AsImpLSample::OnValidate()
extern void AsImpLSample_OnValidate_m55E6D2681207CDE66EBD73018DD28EB0343F81AD (void);
// 0x0000014B System.Void AsImpL.Examples.AsImpLSample::.ctor()
extern void AsImpLSample__ctor_m7255D45E95809F19DE0095CCA6A0DFF9474AC692 (void);
// 0x0000014C System.Void AsImpL.Examples.CustomObjImporter::Awake()
extern void CustomObjImporter_Awake_mFCE0E48186A9F9C7EECAE6D64CD6E448BA67EE0B (void);
// 0x0000014D System.Void AsImpL.Examples.CustomObjImporter::Start()
extern void CustomObjImporter_Start_m7F705D94878648A67A5918199BF3B276C5CDEF85 (void);
// 0x0000014E System.Void AsImpL.Examples.CustomObjImporter::SetScaling(System.Single)
extern void CustomObjImporter_SetScaling_m2DE8FCBBDA98DC68E7533333B145C1B9CA6C5E3E (void);
// 0x0000014F System.Void AsImpL.Examples.CustomObjImporter::OnImportingComplete()
extern void CustomObjImporter_OnImportingComplete_m910B6C2E1EBAF35E3376DDA751064FF97C380223 (void);
// 0x00000150 System.Void AsImpL.Examples.CustomObjImporter::Save()
extern void CustomObjImporter_Save_m6DF18BA68F237CE152A2AB971E94066801123169 (void);
// 0x00000151 System.Void AsImpL.Examples.CustomObjImporter::Reload()
extern void CustomObjImporter_Reload_mD976DF7FF7981079726FAC888C3443DA7775AB71 (void);
// 0x00000152 System.Void AsImpL.Examples.CustomObjImporter::UpdateObject(UnityEngine.GameObject,AsImpL.ModelImportInfo)
extern void CustomObjImporter_UpdateObject_mB6188F9105A92BF837F4D049BE5AD10A8BA17A9A (void);
// 0x00000153 System.Void AsImpL.Examples.CustomObjImporter::UpdateImportInfo(AsImpL.ModelImportInfo,UnityEngine.GameObject)
extern void CustomObjImporter_UpdateImportInfo_mF2EF3A83BABB335E30C94EB0F7B3D2F2FEF177ED (void);
// 0x00000154 System.Void AsImpL.Examples.CustomObjImporter::UpdateObjectList()
extern void CustomObjImporter_UpdateObjectList_m214B2E5D2783BE9D062B9A65C4376010312D8D21 (void);
// 0x00000155 System.Void AsImpL.Examples.CustomObjImporter::UpdateScene()
extern void CustomObjImporter_UpdateScene_mC140D9F3FC34135B2E4862E3BA046512A458EA33 (void);
// 0x00000156 System.Void AsImpL.Examples.CustomObjImporter::.ctor()
extern void CustomObjImporter__ctor_mA2B7B242D4C4C4F7DBB20D612388B299FD36335C (void);
// 0x00000157 System.Void AsImpL.Examples.CustomObjImporter/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m7F74B42E1B3AE6A908151C457ACFEE8356AD3033 (void);
// 0x00000158 System.Boolean AsImpL.Examples.CustomObjImporter/<>c__DisplayClass11_0::<UpdateObjectList>b__0(AsImpL.ModelImportInfo)
extern void U3CU3Ec__DisplayClass11_0_U3CUpdateObjectListU3Eb__0_mB7CCD3905A3E817C10B709D71062BC44E40B5F4C (void);
// 0x00000159 System.Void AsImpL.Examples.EditorLikeCameraController::Update()
extern void EditorLikeCameraController_Update_m7A3C952401647189EED7383A4B5664C1087B06BE (void);
// 0x0000015A System.Void AsImpL.Examples.EditorLikeCameraController::.ctor()
extern void EditorLikeCameraController__ctor_mE1F2D1A5C3ECE0BAC7FF49BAB3B96B665AEC7A29 (void);
// 0x0000015B System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
static Il2CppMethodPointer s_methodPointers[347] = 
{
	MTLLoader_TextureLoadFunction_m09A9871A19D6F5351732639D001CAD4A9B3C2EFF,
	MTLLoader_TryLoadTexture_mE77138911E4861BF99DA17D8977866B448902DE4,
	MTLLoader_GetArgValueCount_m200C2986F85BFC80A83F78F3AAA0230B33B088B6,
	MTLLoader_GetTexNameIndex_m665BC7D4E4EE57BD635E6269A9347C427DD5CA32,
	MTLLoader_GetArgValue_mFAE340D18A3669E1FE0F9AE2EF80D3ED6AAE5E55,
	MTLLoader_GetTexPathFromMapStatement_m7B30B10BDD96DA432C4429112F015D60AB168EF2,
	MTLLoader_Load_m069D9E2F221F03FA1D2A972F52C133DE1C1310CC,
	MTLLoader_Load_mDC4DE42F6E4FB4E2E7A1418934C36FE303CF9F6C,
	MTLLoader__ctor_m09ABDFD4907D9F5CBEE0A7BD231730FC3BF89CE9,
	ObjFromFile_OnGUI_m0323BCECC6C3175D2BBC6A35B35D0F33EBDE3D89,
	ObjFromFile__ctor_mE9BBFB0301921CD1F9363610DDC3BE99A859E538,
	ObjFromStream_Start_mD4140981D260A0DE3D04CEBB8332DDCEC1C9DFA2,
	ObjFromStream__ctor_mCD88711D645EB1E8513AB99C3D5B5769F0B6EC47,
	mainMenu_Start_m3409ECFCEA4CA479FCD8643BF891EC0FF0D4DCBC,
	mainMenu_configureListeners_m795DBAFAD89DC8095BA60A2077FF7BC5FDE0D730,
	mainMenu_loadSelectedModel_mEE82704C795A12668CD6E3212BE6D53EC8E0F8AB,
	mainMenu_updateDropdown_mF168432B305F0A58C5598871F7D4D76D49893944,
	mainMenu_LoadSceneButton_m2DBD38292ED5A396C8B3FB75B7862BB55A318556,
	mainMenu_SetActiveSceneButton_m79B1B3686AD54780B9BD2341490D7FF4C819256A,
	mainMenu_OpenFileChooser_mB10D677DD5BF860883CED35439CB350B86E31868,
	mainMenu_generateModel_m832EBD44B172C138039F65574485CB7B7C8C7565,
	mainMenu_ShowLoadDialogCoroutine_m67FA4AFF637FCD4C4A5EA99D38AE652FCA400514,
	mainMenu_dicom2Mesh_m41BAC2220FB7404CFE8B17DBE6A8CD5D7A94B2E4,
	mainMenu_constructParameters_m58E431D26E90896CD12550FE7FB3579790DD1A8F,
	mainMenu_OutputHandler_m59D0ED6CF7BE844476440D66EB333B24D5AE30E5,
	mainMenu_Update_m5D44F29640B47BD5A96523B2DF21656B31D13830,
	mainMenu__ctor_mE3659A1D9717CE7DB207A80A81E608028AF0ECD0,
	mainMenu_U3CconfigureListenersU3Eb__6_0_m3D4E196FAB3FED9950537F5704CF5B6B525DF6E7,
	mainMenu_U3CconfigureListenersU3Eb__6_1_mBD72798DD4C4E98D97A33D2B94524AB6E77A75DC,
	mainMenu_U3CconfigureListenersU3Eb__6_2_m2337882BE0CA863007C70EA0897E8328A7C54E29,
	mainMenu_U3CconfigureListenersU3Eb__6_4_m2E8D5C592A0F3AF59C8C3300DBDB10E7D49B5575,
	U3CU3Ec__cctor_m02255B54D95C964A5FBAFC2F817558D4B14AEC82,
	U3CU3Ec__ctor_mB1F8AA87B45A8B57A6DC8FB74E2F5B950984FFC9,
	U3CU3Ec_U3CconfigureListenersU3Eb__6_3_m0CA4536117ABEEC6E4C1C6948683AB7ED9460E5C,
	U3CShowLoadDialogCoroutineU3Ed__13__ctor_m4B6600EB7805E7F4F86F09C4C321ADA942A57780,
	U3CShowLoadDialogCoroutineU3Ed__13_System_IDisposable_Dispose_m01FB2C32275EB18BBBD4F60A45455486495EE968,
	U3CShowLoadDialogCoroutineU3Ed__13_MoveNext_m591957769AF4C012013C30EF0519C233912CB711,
	U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2AECCA9C2680930F4A4A1DE1A9DDD485BDB3925D,
	U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_Reset_m76BB3A7BCE2E32D7D2806770112C1AA06E7EC596,
	U3CShowLoadDialogCoroutineU3Ed__13_System_Collections_IEnumerator_get_Current_mDF35685524E8BB7D14D1AA103D95FD7F4DBF22F1,
	BitmapInfoHeader_get_absWidth_m29DD5E462E03A235354F22118E1DF9DFE6E751AD,
	BitmapInfoHeader_get_absHeight_m46F0A269C8ED63B5071CD2236A16E8E4D8DED021,
	BMPImage_ToTexture2D_m1319F6AA5B9059D7006BD3ADA18D5A7B65B8CD1A,
	BMPImage__ctor_m4BE9499052B860FC015D2E12501EE738D9B9B403,
	BMPLoader_LoadBMP_mF30DA3DEFA407675C366CBB70CD89D1CA6D5DFEA,
	BMPLoader_LoadBMP_mCC3AD2FAC3C38F05E95D29AF1D30F739FC678D0E,
	BMPLoader_LoadBMP_m898201054F07D1F209BE2F6E65F1C65E8D4B5E5D,
	BMPLoader_LoadBMP_mAE1BD831832DD4C73D6EC4FCA8FED3734B447E03,
	BMPLoader_Read32BitImage_m828FA1D3A522A9435AF2BF0BCF0AE02FB70ABD02,
	BMPLoader_Read24BitImage_m6AB565B5E2D4BF08B51C01F8964B91E7E75739D8,
	BMPLoader_Read16BitImage_mCDF3EA0452D745636BEA76F779174376C4152788,
	BMPLoader_ReadIndexedImage_m8415B6D10B5AAC890733B8F701FB713A57F6A132,
	BMPLoader_ReadIndexedImageRLE4_m507FD2AA1A8D5D7D73D32070A84351136F7EB62F,
	BMPLoader_ReadIndexedImageRLE8_m74D4DA30FDE797A6554425C97CA819F0FEC8BF30,
	BMPLoader_GetShiftCount_m564FC367120D0F4D8D88FB72C0C7022A20892825,
	BMPLoader_GetMask_mD2190CA042F9B1597A0936312BA4A93F75BB0CEC,
	BMPLoader_ReadFileHeader_m9A6EE25392F2CDCD6EE4B05B80DFCCEB14CF8D7C,
	BMPLoader_ReadInfoHeader_mB6EB8C42C47E850D6BA2174387DF3D851BA3E7B1,
	BMPLoader_ReadPalette_mABC3EF3B4AD0C037A2EB019EEA3D18B092FE7E47,
	BMPLoader__ctor_m58293ED2D5394FDC53883B9F28AEA4AC4F5FC7E9,
	BitStreamReader__ctor_m8310D226B2DB1FBEDF06E91B7FBDCB292315DE8B,
	BitStreamReader__ctor_m08AF2B47A4153DDAE4A63DBD64DFE4C9D777366F,
	BitStreamReader_ReadBit_m33D70EC31D7A8CF4E40EAF6D9866CACAE0EA3F89,
	BitStreamReader_ReadBits_m6D9B837B8D3D59FC018BAF8315A192721FCB6E23,
	BitStreamReader_Flush_m8D202513A3237F9AA99E17DB5F1A52E293C75546,
	CharWordReader__ctor_m0F497C1D870CF796766AF6876339CD2DF1549C88,
	CharWordReader_SkipWhitespaces_m6C6C7EF0950407BAF326F48C81B9B7D2CE2B5037,
	CharWordReader_SkipWhitespaces_mCEC5201CDF4C4D10442D51A743710BC29FC209B2,
	CharWordReader_SkipUntilNewLine_m7E0311B86AB6FC198152523F9578820A2FB09726,
	CharWordReader_ReadUntilWhiteSpace_m2C75BB0AA47DDCF6E3D7DB032ED112B7EBC22509,
	CharWordReader_ReadUntilNewLine_mD4426E6A4B64982C8E47CD53662EF1D2477D87DF,
	CharWordReader_Is_m3C81A7727B4E4EDD7119D852D85A2BE98A70C629,
	CharWordReader_GetString_m8106220DF5E5557185BB859FB70AF05C56302418,
	CharWordReader_ReadVector_mBF5D6281BED528D7A0BBF6D084C8DFBECABDF15C,
	CharWordReader_ReadInt_m91505746F5627EE4F9655A006451DE36027768E9,
	CharWordReader_ReadFloat_mCA65124A5DBD9FEBCE10315A6E032D89833D1FEA,
	CharWordReader_ReadFloatEnd_m11C69AAC8D104C091F81D49832DFB199DBEF417A,
	CharWordReader_SkipNewLineSymbols_mC1F4132F600407CD94A42B0CF68BF8BAA0A154E6,
	CharWordReader_MoveNext_m2E9862FB91C9B46BF25B9B4D15653A0108419EEE,
	OBJLoader_LoadMaterialLibrary_m3E4D33857ED7B8787F8C8DBB987B0FC9BC17141C,
	OBJLoader_Load_m84C0006E0F986946481D5ECE4B230D418A49C75B,
	OBJLoader_Load_mB6A0EFCC5E89697C11BC735544CE16ADED3F172C,
	OBJLoader_Load_m71B83AFC549B39742BC5437885A7B3E174EAB313,
	OBJLoader_Load_mADD589597CC52044C238A2FF2FC814B4B25AB4AB,
	OBJLoader__ctor_m2A29D87A50D4D692C023E1B349E9010606C60CDA,
	U3CU3Ec__DisplayClass7_0__ctor_m0B8617E43AD163CDE3E3B02DA8E15906900C2F28,
	U3CU3Ec__DisplayClass7_0_U3CLoadU3Eb__0_m4D0A804BD38A2A47113C3DE8F0FC9293ECCFB04A,
	OBJLoaderHelper_EnableMaterialTransparency_m0F838E8E9D1FEB86417DD35FDEC89B3C71B83A17,
	OBJLoaderHelper_FastFloatParse_m0ABA03819C1D38E22041897A5218372939CCF62E,
	OBJLoaderHelper_FastIntParse_mA807DC7E1B18EC766C5E657D5FE04A92199A5CCB,
	OBJLoaderHelper_CreateNullMaterial_m435BF547B1AE26265789C607A0BB3A6A012647AA,
	OBJLoaderHelper_VectorFromStrArray_m108BABF5B4B4FBE77149F0EA9BBE0D5D4B8A3763,
	OBJLoaderHelper_ColorFromStrArray_m54B3D99C6EB38AF997E9F5F3E8420701856C2974,
	OBJObjectBuilder_get_PushedFaceCount_m9A51064310CB6F80EE840BA9067EE95EF8220F06,
	OBJObjectBuilder_set_PushedFaceCount_mE5FC98B476DA97661D6B440D1CE8781C82AC3042,
	OBJObjectBuilder_Build_m9292D5C63F5E1E3C7B5E32A3CFCD7E956F2B4C7E,
	OBJObjectBuilder_SetMaterial_m130EF05B8B56A9888B2BD253F8267858E6AA8188,
	OBJObjectBuilder_PushFace_mBAC6F523C1189692256B58C31B54BA10C265C54B,
	OBJObjectBuilder__ctor_m6CE8E4E7C197E1B4C3E4DA2276BB189984677A37,
	ObjLoopHash_Equals_m4045656E38151C1B8BE3C2563FD6B4999377FA68,
	ObjLoopHash_GetHashCode_mB03E4094552B962AA7C2698972047EE2FF365E23,
	ObjLoopHash__ctor_m269DD59F077394D2A398ECF25AF3B4A3FF27B0DC,
	StringExtensions_Clean_mBD817F18A962673D4157FDD426B9324C092BD734,
	BinaryExtensions_ReadColor32RGBR_mE26BFB16967BE261B57740558FC02FD3AD82EE78,
	BinaryExtensions_ReadColor32RGBA_mA6987C56F059E266336BFBF30067DA149B825F39,
	BinaryExtensions_ReadColor32RGB_m968635C4AFB130CBB829DC65251E1E388F1B1D7E,
	BinaryExtensions_ReadColor32BGR_mFA7375B3F8EDA89B1F6BE3BD82E148804E8855EE,
	DDSLoader_Load_m4C08D89BDB06C305DAC028014E475D9A6CB30019,
	DDSLoader_Load_m6B482F4E3C4F4EADEDB438FDF18D471E0B042852,
	DDSLoader_Load_m49BDEFE41B6816E47C28FEC5E465C799F3D5DAA6,
	ImageLoader_SetNormalMap_m54B7B74AB389DC11798E442B267A85883D7AD959,
	ImageLoader_LoadTexture_mC9D1F44DADB2E2AE8DFA5915F6F3F8D84A2F6182,
	ImageLoader_LoadTexture_m72598D61C94895B3DFB7A2D4184AF036C0C8147F,
	ImageLoader__ctor_m03610F404B6E496BAA27809B55B8830604AB2BE6,
	ImageLoaderHelper_VerifyFormat_m5B08C89C83028E94CAB14CFC745CED42CBBFB4EA,
	ImageLoaderHelper_FillPixelArray_mB67884D323AD1B9161110D78DB97128D6D1D8110,
	ImageLoaderHelper__ctor_mDA60FBFAF4077A5B055E32B8385DCDE3A2A7273C,
	ImageUtils_ConvertToNormalMap_m383343212117BBB20A1580F2C3758485016B665F,
	TGALoader_GetBits_m87D2DCCEF149DF8DC8841957243E783748F4CA0C,
	TGALoader_LoadRawTGAData_mC63040F4276598D8B0DA88EC184EBB36256B5248,
	TGALoader_LoadRLETGAData_m65192B81EB466963D935CC8A6590C7F7DA92118C,
	TGALoader_Load_m2752C133CE7C6F43E0BD596932AB44AC75BC77EF,
	TGALoader_Load_m7492EAE7244471443B83B6C01CFC841F4F14060F,
	TGALoader_Load_m3884F2B51C2235C5B97DB6A306607386069FD5D6,
	TGALoader__ctor_m5F5C44E5FBDB9CE459B21FB87445ED7096A9D653,
	ColorExtensions_FlipRB_m621188E25D11376AF450BD484FA3F50475370785,
	ColorExtensions_FlipRB_m4689B607045C6100B80EB112A617E94235A9469D,
	DataSet_get_CurrGroupName_m09420C11302898F7072C80D1F42B7091712319F3,
	DataSet_get_IsEmpty_mFD6E338266F0FFE1066ADA35058F04B92263863F,
	DataSet_GetFaceIndicesKey_m506350EA2878556157D1C2AA5E4DDC82FA4FC4F6,
	DataSet_FixMaterialName_m6707E70C463D382835F0FD96D12E0E798B99B546,
	DataSet__ctor_m6971FE870DE2FAA395813D3BDA2BEA0317DD25D2,
	DataSet_AddObject_m9FB7257A4317C83E028FAE578D437362F0A0DCE3,
	DataSet_AddGroup_m926F9EF063AAD403FF441A30BFD43FA82204232F,
	DataSet_AddMaterialName_m00AB8E080F912D78138FAEC6C3FC8927D7EFA66E,
	DataSet_AddVertex_m2F16605FD673B64FD9396DD8D42A89D0750A61A2,
	DataSet_AddUV_m4AD05B643C4AD1F13D5CD4B47CD46B7D3C0AEF07,
	DataSet_AddNormal_m1F6BA0C73EC0CD34E2DA0DDDA5C971EB52F756AB,
	DataSet_AddColor_mB1B92705CF49601C203B31674FDE5D0F48BC1850,
	DataSet_AddFaceIndices_m0CBA3502005CB31A4C846180272BDABF99869F08,
	DataSet_PrintSummary_m60F659DECDB02450F2E2D5FD98E3372A114A01C6,
	ObjectData__ctor_mBA3C40BDF5ED93C4430FB30B35566A7DBB0E829E,
	FaceGroupData__ctor_m83350BD5E0DCE710F2C5945C556B10A62C57CF39,
	FaceGroupData_get_IsEmpty_m3B5E451CCF4DA4CD8CF12C97EBCDE9E614F04A74,
	MaterialData__ctor_mAA0A3C9950A0FD380AFFFB5F626DDF959433C8FC,
	ModelUtil_SetupMaterialWithBlendMode_mA969B6E72F767FAB6BC8DC11BD7C3A5F1CBCDFFE,
	ModelUtil_ScanTransparentPixels_mA7EBE50D52111C4CC321B0C26E2B5C7812BA2F7C,
	ModelUtil_DetectMtlBlendFadeOrCutout_mAB0DCDFB4D96563DD1118C9E0C0E7672BC0FDE9D,
	ModelUtil_HeightToNormalMap_m7298B601A0ABDC8FB2F81E51BEE8C86A4BDE126E,
	ModelUtil_WrapInt_mBF9311FADF1F38241F513B44B534B67309D4BB73,
	ModelUtil__ctor_m96FFE76BCD72F106DE400FB1530A7FEDD102C83C,
	ObjectBuilder_get_ImportedMaterials_mECB8544D6C7486E6C776C53118B70798E0838A93,
	ObjectBuilder_get_NumImportedMaterials_m5BB878C4A084ABA6D28F19FE23CC8E8B13DE31E8,
	ObjectBuilder_InitBuildMaterials_m5F4B83DF9A3109192A0469EED7BD44BE07C25B2B,
	ObjectBuilder_BuildMaterials_mF67A04083D61A87FB2CC07B6987A2299C585CD93,
	ObjectBuilder_StartBuildObjectAsync_m7BED00FA4B0E6C48518BB244E19AD4CC1B9DCC22,
	ObjectBuilder_BuildObjectAsync_m7F473E66A3F43267F3483E9BE01245A109456D40,
	ObjectBuilder_Solve_mED61C100AB65B37F38EC1FB5538D012B1817EA75,
	ObjectBuilder_BuildMeshCollider_mBBB25A024702EBFE6BDC3814D1A5332518D416B9,
	ObjectBuilder_BuildNextObject_mDF514126A63577FE4CFFB335C3DFE929CC476223,
	ObjectBuilder_ImportSubObject_mC1EDF7C09EB5BF7877624D9A7171103B69E2C4B2,
	ObjectBuilder_BuildMaterial_m79CC542900FF22C01F063862D1A8427BF8D4FB1E,
	ObjectBuilder_Using32bitIndices_mC5FE3465AF75D46401E6C0DC81E609A8C13E8E96,
	ObjectBuilder__ctor_mD90CC57A26B89DDE8DAAF91EBBDECEEA78603730,
	ObjectBuilder__cctor_mB3CABC142B0C11FB0846FC39391EB4B8B58793E5,
	ProgressInfo__ctor_m272DD2F266BC13188B09796694C9755B1FE35D96,
	BuildStatus__ctor_mB75F2A47F94B76F79DDEE8D6CBE9CF73A45E9D5A,
	Triangulator_Triangulate_m5FF55D863BEC0E02F3EAE424E1DF0276B8A9EEB4,
	Triangulator_FindPlaneNormal_m5CB314C194FE01D8AF494F764AED1A76E3BEA57C,
	ImportOptions__ctor_m0770080AEBEDA39A2BBDCC8A649B4CA525F5729D,
	Loader_get_ConvertVertAxis_mFCC4AF705C2593AE1FF34CAA91CBBE7943812644,
	Loader_set_ConvertVertAxis_m1CB277ED557387444660F635BEB7C27D6828A0CD,
	Loader_get_Scaling_m9769BCADA294F639830D107EACF9D4934240A18D,
	Loader_set_Scaling_mCCB9C2711E159841C6EB17DF20FCBFAED75BE55F,
	NULL,
	Loader_add_ModelCreated_m8992EE812B09EB58B47864AB996D16B47EA07FB6,
	Loader_remove_ModelCreated_mA5DD5078F249FC7D57A468B487C89C9FC0CD0085,
	Loader_add_ModelLoaded_m7A065F03EFAA5AC2FE5FD2D792BB0FFA310F8142,
	Loader_remove_ModelLoaded_m2A7830BB84800200D10E269230C57DF7EF9B58E5,
	Loader_add_ModelError_m56EDA25051268763FDDC531159F60AEC088B53CE,
	Loader_remove_ModelError_m525B768D2BB19243FE1905EC21D1548E6147068E,
	Loader_GetModelByPath_m671847AF7A9E9BB030997667BC357890676124D2,
	Loader_Load_m4DC773597C6E42E7B9AE846A83E8DE6866D56353,
	NULL,
	NULL,
	NULL,
	Loader_Build_mF8D000CD4CC7267CE431AA748594A985C4E890AF,
	Loader_GetDirName_mE2DCBDCA6616BE07ABF1CF62A645272C461861D9,
	Loader_OnLoaded_mB2D8D687F460831EDDF804F00CD4E4B933D71440,
	Loader_OnCreated_mBDF98C3B64E87029C98E1A87CA7386064EFFF53B,
	Loader_OnLoadFailed_mBC5020778D01655CA808A0430335DE3B2533C923,
	Loader_GetTextureUrl_m9098D5BBCF874362012451C841519DBBF034209F,
	Loader_LoadMaterialTexture_m440076E0E24E8754B2044227984452E07818025E,
	Loader_LoadTexture_mAC0A6DC5EFDA49DD4B3F89FDF3889D06B6A2AD07,
	Loader__ctor_m2D29856FA5DBE97CB21A92B8DA0A8603E8E4C8FC,
	Loader__cctor_m0A29251BB34EC2906BE556B705DA194493BD6819,
	U3CLoadU3Ed__32__ctor_m73C8A633B599ED1F4ED404A8ECDF82DCD633BCA2,
	U3CLoadU3Ed__32_System_IDisposable_Dispose_m76F0AB7C4A259585D6C69D2D48DD59B16ED7F944,
	U3CLoadU3Ed__32_MoveNext_m69237F8C341CDC08F7231248E0E2A235714AC1E8,
	U3CLoadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4814EA0F29D8615752EFF8176625E0CF12F5E10C,
	U3CLoadU3Ed__32_System_Collections_IEnumerator_Reset_m53352D1E2DF974F4FF214A8F561008C0B688519B,
	U3CLoadU3Ed__32_System_Collections_IEnumerator_get_Current_m33F97DBD0C22371E96278B6FF2B29A7B86EB09A1,
	U3CBuildU3Ed__36__ctor_m2746753CE6057324F628EF2130856DA6E70D389D,
	U3CBuildU3Ed__36_System_IDisposable_Dispose_mBEEA3AF4CC543B059B5992CC6AB36554A98BFEE0,
	U3CBuildU3Ed__36_MoveNext_m2AB412717C10187EFFDE5CFEAFB0E326C4F3DBC7,
	U3CBuildU3Ed__36_U3CU3Em__Finally1_m2C742BFD2FEC9407C659BD90D0132EF6401464A0,
	U3CBuildU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234701C996636E960294EDAF55C3CAA01DCD2092,
	U3CBuildU3Ed__36_System_Collections_IEnumerator_Reset_m786116A825908E278B23C5A6FEA64FCAE7BB5BEA,
	U3CBuildU3Ed__36_System_Collections_IEnumerator_get_Current_m367ECE887DF4A49C3B21942EA6A37D688242EA51,
	U3CLoadMaterialTextureU3Ed__42__ctor_m4EA7B131CF7AB5224995803A3878296B0A70CB0A,
	U3CLoadMaterialTextureU3Ed__42_System_IDisposable_Dispose_m8D9EC2C3145CDE449264D604FFBF8A74DAC0CFD8,
	U3CLoadMaterialTextureU3Ed__42_MoveNext_m070794AC59F67C9D99887807AB40A5BF46223924,
	U3CLoadMaterialTextureU3Ed__42_U3CU3Em__Finally1_m04E13DEF8FBEA253C51C19FCA13D4692C0200BE4,
	U3CLoadMaterialTextureU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52FA484524B04EF7CB4E0B3DCABF955F37778AED,
	U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_Reset_m9849C2B35AD8DF82C95FB6A427C4298EE351A212,
	U3CLoadMaterialTextureU3Ed__42_System_Collections_IEnumerator_get_Current_mA9EAD8C0E049278489EF63EDD9A8B8E72899A510,
	LoaderObj_ParseTexturePaths_m0A42A45F24C53DEE743213A0ACE943FAC4A853F2,
	LoaderObj_LoadModelFile_m78B90897589F239C23519364BE7691CC1BF85829,
	LoaderObj_LoadMaterialLibrary_mC4CDB8A952C1B852AE2EB4DF8254A6990EAE5755,
	LoaderObj_GetFaceIndicesByOneFaceLine_m75789C09856C63F4E2DC410FCD4053DE9CD02979,
	LoaderObj_ConvertVec3_m0A9148B32F2D11757630F18E67299B90A6374E3E,
	LoaderObj_ParseFloat_m09B6A4FAFD5B64045AE84F74F8E83F2F673D11C0,
	LoaderObj_ParseGeometryData_m4AA82D03093B53A6B2F002D8091BFD3FDF007FE1,
	LoaderObj_ParseMaterialLibName_mAAF7C7C29F8BC9C30A93FD0488234893B289D438,
	LoaderObj_get_HasMaterialLibrary_mD5284050AB5BAC6CC93926A851E29EFEA465B777,
	LoaderObj_ParseMaterialData_m0540619C5B10A1D4287575D4864058454DEDC603,
	LoaderObj_ParseMaterialData_m7B82DDA18F38556CE0580956FF4BE2F88D0B27E2,
	LoaderObj_ParseBumpParameters_m5AEE0312F23063508C8DCA3D09E9CD3CF8DC7584,
	LoaderObj_StringsToColor_m09A21991578C1579084759320824488EE2BD7A0F,
	LoaderObj_LoadOrDownloadText_mB967181C6B7DF4C1FD7F423BDDC08A0F721158B0,
	LoaderObj__ctor_m664CEF04C4257C4DA19EB6361F1ADD761FD279C3,
	BumpParamDef__ctor_m31C8BD508D8BAAB93F428707C999F74C7345E686,
	U3CLoadModelFileU3Ed__3__ctor_m31EC5B02C9FFDD0107481311078DC237D33FC350,
	U3CLoadModelFileU3Ed__3_System_IDisposable_Dispose_m302B51DB67B5231710E448287F3A65CBCADD3773,
	U3CLoadModelFileU3Ed__3_MoveNext_m18D28E36003CA86A4EF36443EC6B156621ED7B80,
	U3CLoadModelFileU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m157DEF0B8196F4E4A4532C9A4D456025AFA21240,
	U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_Reset_m8A5292170989E075719F3393BD0DA6B1AEFA1BD3,
	U3CLoadModelFileU3Ed__3_System_Collections_IEnumerator_get_Current_m033DB07B0490629CDC70C9E82462D27750683230,
	U3CLoadMaterialLibraryU3Ed__4__ctor_m857E4E65341EE9147C0AE4B475AACD135325D97B,
	U3CLoadMaterialLibraryU3Ed__4_System_IDisposable_Dispose_m338D35DA7855937147A8B8807F490E0CD56334FA,
	U3CLoadMaterialLibraryU3Ed__4_MoveNext_mBE76692CB7FA17B8BDA2EA80C5C1A3697AA7274B,
	U3CLoadMaterialLibraryU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DE19B3479375B272D8F6EA4AE9F7650A7C96686,
	U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_Reset_mA169DB740A8A7154826E4EF0736B2DF07DC1AF8B,
	U3CLoadMaterialLibraryU3Ed__4_System_Collections_IEnumerator_get_Current_m433F960B7017A11D26D2CF999E1E72077C29C412,
	U3CParseGeometryDataU3Ed__8__ctor_mC96B3D3DCFE50A6FD8A6894A512EDC67D0308430,
	U3CParseGeometryDataU3Ed__8_System_IDisposable_Dispose_mACAB1D384D2A3973F6604BC082A4841937B5EAD2,
	U3CParseGeometryDataU3Ed__8_MoveNext_mF117DA3476FF7F0ACD2B2A4AFB463FF59DDC22EF,
	U3CParseGeometryDataU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m987832127609D559655B0F45011479F84CCF5AD3,
	U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_Reset_mB570E836E9EE3A72D1E67E98807C52514F030F17,
	U3CParseGeometryDataU3Ed__8_System_Collections_IEnumerator_get_Current_mC2D7EBA1B6E5B1872CBFC18737136E4F44D453F4,
	U3CLoadOrDownloadTextU3Ed__16__ctor_m449E2AF3B9297CB1F6B68DC54427D53B0D481A41,
	U3CLoadOrDownloadTextU3Ed__16_System_IDisposable_Dispose_mF22941E1A5FA87FF66CFA4CBB0EF3FB497B04DFA,
	U3CLoadOrDownloadTextU3Ed__16_MoveNext_m40AED1D063767C8DC8C67B80468A1471688E34AA,
	U3CLoadOrDownloadTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m319547258507AB779A3F29F8C08FE499C9D29026,
	U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_Reset_m68C9FCF8FF223384464DF36D0CEC510BFE410240,
	U3CLoadOrDownloadTextU3Ed__16_System_Collections_IEnumerator_get_Current_mC1D95606D9F1B0C1FA62787A1700AEF6430FFB76,
	TextureLoader_LoadTextureFromUrl_m253D4C7F1B376EB1542A2073E295F3B05375E641,
	TextureLoader_LoadTexture_mBD892E20EEAD731B6032E7C4934011B5756D98D0,
	TextureLoader_LoadTGA_mC6B13F465CAA35F3A5894D06781DF8781371DEAE,
	TextureLoader_LoadDDSManual_mE0C492E2CE870E9F9746FC88FA2693D51384A90E,
	TextureLoader_LoadTGA_mBADD6D4522B23C756F6D18E879325C54D3A7D4B5,
	TextureLoader_LoadTgaHeader_mDF48D34A9DEA349EC09275104C360DBB3129BB31,
	TextureLoader__ctor_mAE7A4998857ECA4B8585F720B05632F65E568A95,
	TgaHeader__ctor_m2E1A7D00698B384A622BF8F829CD511756A2EA70,
	SingleLoadingProgress__ctor_m0217A2F9D64DF860F4EA62E0F274E54A0C490674,
	LoadingProgress__ctor_mF6D959443EFC179A4C4E37C35854579426F9F716,
	ModelImportInfo__ctor_mF4479545CA11639353E1123548A4C27EAAB5AFC6,
	MultiObjectImporter_get_RootPath_mC352AFD7AABA876D92BE718CC202361D4128B5AC,
	MultiObjectImporter_ImportModelListAsync_m5D6D7DBDF0F9F4BA9EB2EFD92C857678A2FD20B3,
	MultiObjectImporter_Start_m479F65DE89933BD9CF129A23756DC6C5E144F363,
	MultiObjectImporter__ctor_mD441A20FEC84DED9A104C77158C0AFB58AB0ECEA,
	ObjectImporter_add_ImportingStart_mF967C93A8772C6AF9DF70DB5AD1503B276B8C3B4,
	ObjectImporter_remove_ImportingStart_m2F7F74D1CA7C50EEF9BB842B790C461AF6693100,
	ObjectImporter_add_ImportingComplete_m9A892024861058A6FF56116A60B365B91076E0CD,
	ObjectImporter_remove_ImportingComplete_m9F2D2CCDB062B2F027A0DD7EBA9C446E3275C68E,
	ObjectImporter_add_CreatedModel_m178EB892D4E4FE81E6CA3ADBF19F5849423315C2,
	ObjectImporter_remove_CreatedModel_m2BA274F4B150A6A181E5AFE20479075FB177612E,
	ObjectImporter_add_ImportedModel_m6C50C797294CB6EB2D8CB556321954E2BD0959C9,
	ObjectImporter_remove_ImportedModel_m16878B820F61B0039349B22718E6D8F884931B0E,
	ObjectImporter_add_ImportError_mD47F0F2C636D79952DA5DBA5F5A76FE002BAFD40,
	ObjectImporter_remove_ImportError_m93DF9FAC2DF5F4642BD530AA12D665BA8B684082,
	ObjectImporter_get_NumImportRequests_m5A38CAA9856617DF3BF130604227EBB315B3E895,
	ObjectImporter_CreateLoader_m3FE9BA7749342EC1862ECB8D4131CC2E14246E86,
	ObjectImporter_ImportModelAsync_mF2D0DBEB7082C165F1F9F35E3C74E0A762D11731,
	ObjectImporter_UpdateStatus_m3D69476B9E79362A7ECD7C6FA321C177FFD53EDE,
	ObjectImporter_Update_m47D34B4B4BD725F1264BC92361224D5B88866D6B,
	ObjectImporter_OnImportingComplete_mBE4241253EA259B294AC427BE790B0E6181BB3BA,
	ObjectImporter_OnModelCreated_mAB12B1AA902EF43171AA43BCA80E2E3E56DF41C8,
	ObjectImporter_OnImported_m845D1192567B2D7AA8C851B4AB4E6F8199E7CDBB,
	ObjectImporter_OnImportError_m5023EB7AD6E364B6C82B51EDBC2E5800AF8D40A5,
	ObjectImporter__ctor_m26565F09B5CDD627B75A039763A481E5B59D6C05,
	ObjectImporterUI_Awake_m5CC4408989784EC94E2D2C5C44D48548AF56E764,
	ObjectImporterUI_OnEnable_m7A64184436CB20E91C2D5FE4A4DD3FE880929608,
	ObjectImporterUI_OnDisable_m29E8EF0623BB91DE5C4E9E2BBA8996F31D948C25,
	ObjectImporterUI_Update_mBE47D8ED6B6E5E21F37BA0ED43CE721756057A86,
	ObjectImporterUI_OnImportStart_m901718D2E90A2DCEDFFA2AE830C0B3F808F1B052,
	ObjectImporterUI_OnImportComplete_m3B3447B4DCFB45AD5BEF777BD68F8BEFA71DE7BE,
	ObjectImporterUI__ctor_m4C9E65824CA2EDAC149375BC4319AAA165728C34,
	PathSettings_get_RootPath_m7F43F3D368D119097AADD19DA8C7EF037B8B94F4,
	PathSettings_FindPathComponent_m21149B93770E417A3A3F64105C8B6ABEBEFDB5EC,
	PathSettings_FullPath_mC1DAA758C6D67B1A0EA7E28698FCA308F51B31F4,
	PathSettings__ctor_m94C07886C54AC7BA19A1ABE8025F4E34D49FA30D,
	Triangle__ctor_m67A27B9F99280B61520B2071A9FF83DC9378BD48,
	Vertex_get_Position_m39B376C28A912E63492FFDC60D91460C36EDB7BC,
	Vertex_set_Position_m34AEC05B85BDA60FCEDD74055FA4659C17212D3C,
	Vertex_get_OriginalIndex_m993575EDF4FDA693778501A1FEC370E83BDDF8D5,
	Vertex_set_OriginalIndex_m8BB3858C9BA804333E0DCD3CB9669D543551F661,
	Vertex_get_PreviousVertex_mB01B95271B099F86C43206E0DC5952B1726906E7,
	Vertex_set_PreviousVertex_mF77E162C66D380C4D6A6DCF0EB50FF5CA769DACA,
	Vertex_get_NextVertex_m469C0B8C2BCE7DA5F19F4E3B6B659CE900C1A31D,
	Vertex_set_NextVertex_mDE848CF323CEA421E9E5E6416E3D253C5E4C47F4,
	Vertex_get_TriangleArea_m36C0DC59C1A063F9B5D794D9785F4CC877DE2523,
	Vertex__ctor_mDDBB7CCC2200DA1F64BFC0CD68066E1645FA843A,
	Vertex_GetPosOnPlane_m04E821C5285EEEEF867CF8DF0B8D45A4793882C3,
	Vertex_ComputeTriangleArea_m71AF6EFBAE09FC3AF4CE4BA8E66EFF68A1363AFF,
	MathUtility_ClampListIndex_m2F4D71FC9998358588DC470485115B601E949DB7,
	MathUtility_IsPointInTriangle_mDE459C9808B2590D267B63D2AF225CA2A70E646F,
	MathUtility_IsTriangleOrientedClockwise_m2D894EA67B697240A5898F190010C344393D271B,
	MathUtility_ComputeNormal_m6714E9F666C9DD7E724E0C5B87907F1CFF0CDD64,
	Triangulation_TriangulateConvexPolygon_m75287D6100A633732759F6A64AD45CDE6388B00B,
	Triangulation_TriangulateByEarClipping_m826EEE5D578C55327325F1B60C75A8860081D2E0,
	Triangulation_ClipTriangle_mC2AC86F736D5A979F4E6C14E8C3CDA2205809D81,
	Triangulation_ClipEar_mF5E5DACB0C1539C39CE5509A9FBE461E017731E4,
	Triangulation_FindMaxAreaEarVertex_m694E4F9C83C1D3C9317D7D0B4225772604C27AA4,
	Triangulation_FindEarVertices_m467C24BE20B4073C0D68A5E9EBB33B1EE82509CD,
	Triangulation_IsVertexReflex_mFC56A1AEF4B1FB63B55EE04C03B697DCF4450A9C,
	Triangulation_IsVertexEar_m03BC0E0B69A400B551DE69E52848D93372AA36F6,
	AsImpLSample_Awake_m9213473518EE4612BAAE21816377B73D911FE3E3,
	AsImpLSample_Start_mEB1576451F33DE46139970CDA0850C97C949E835,
	AsImpLSample_OnValidate_m55E6D2681207CDE66EBD73018DD28EB0343F81AD,
	AsImpLSample__ctor_m7255D45E95809F19DE0095CCA6A0DFF9474AC692,
	CustomObjImporter_Awake_mFCE0E48186A9F9C7EECAE6D64CD6E448BA67EE0B,
	CustomObjImporter_Start_m7F705D94878648A67A5918199BF3B276C5CDEF85,
	CustomObjImporter_SetScaling_m2DE8FCBBDA98DC68E7533333B145C1B9CA6C5E3E,
	CustomObjImporter_OnImportingComplete_m910B6C2E1EBAF35E3376DDA751064FF97C380223,
	CustomObjImporter_Save_m6DF18BA68F237CE152A2AB971E94066801123169,
	CustomObjImporter_Reload_mD976DF7FF7981079726FAC888C3443DA7775AB71,
	CustomObjImporter_UpdateObject_mB6188F9105A92BF837F4D049BE5AD10A8BA17A9A,
	CustomObjImporter_UpdateImportInfo_mF2EF3A83BABB335E30C94EB0F7B3D2F2FEF177ED,
	CustomObjImporter_UpdateObjectList_m214B2E5D2783BE9D062B9A65C4376010312D8D21,
	CustomObjImporter_UpdateScene_mC140D9F3FC34135B2E4862E3BA046512A458EA33,
	CustomObjImporter__ctor_mA2B7B242D4C4C4F7DBB20D612388B299FD36335C,
	U3CU3Ec__DisplayClass11_0__ctor_m7F74B42E1B3AE6A908151C457ACFEE8356AD3033,
	U3CU3Ec__DisplayClass11_0_U3CUpdateObjectListU3Eb__0_mB7CCD3905A3E817C10B709D71062BC44E40B5F4C,
	EditorLikeCameraController_Update_m7A3C952401647189EED7383A4B5664C1087B06BE,
	EditorLikeCameraController__ctor_mE1F2D1A5C3ECE0BAC7FF49BAB3B96B665AEC7A29,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
};
extern void BitmapInfoHeader_get_absWidth_m29DD5E462E03A235354F22118E1DF9DFE6E751AD_AdjustorThunk (void);
extern void BitmapInfoHeader_get_absHeight_m46F0A269C8ED63B5071CD2236A16E8E4D8DED021_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000029, BitmapInfoHeader_get_absWidth_m29DD5E462E03A235354F22118E1DF9DFE6E751AD_AdjustorThunk },
	{ 0x0600002A, BitmapInfoHeader_get_absHeight_m46F0A269C8ED63B5071CD2236A16E8E4D8DED021_AdjustorThunk },
};
static const int32_t s_InvokerIndices[347] = 
{
	774,
	774,
	1176,
	1176,
	615,
	773,
	1253,
	1253,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1536,
	1793,
	1254,
	152,
	108,
	2596,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	2830,
	1793,
	1793,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	1750,
	1750,
	1761,
	1793,
	1253,
	1253,
	1253,
	1253,
	2596,
	2596,
	2596,
	2596,
	2596,
	2596,
	2668,
	2668,
	2526,
	2526,
	2292,
	1793,
	1519,
	1519,
	1780,
	1221,
	1793,
	986,
	1793,
	1482,
	1793,
	1793,
	1793,
	1363,
	1250,
	1791,
	1750,
	1782,
	1782,
	1793,
	1793,
	1519,
	1253,
	773,
	773,
	1253,
	1793,
	1793,
	1519,
	2783,
	2757,
	2672,
	2816,
	2770,
	2397,
	1750,
	1510,
	1761,
	1519,
	446,
	989,
	1363,
	1750,
	1793,
	2716,
	2625,
	2625,
	2625,
	2625,
	2716,
	2716,
	2716,
	2778,
	2477,
	2716,
	1793,
	2716,
	2199,
	1793,
	2716,
	2250,
	2112,
	2112,
	2716,
	2716,
	2716,
	1793,
	2619,
	2624,
	1761,
	1780,
	2725,
	2716,
	1793,
	1519,
	1519,
	1519,
	1547,
	1545,
	1547,
	1487,
	1557,
	1793,
	1793,
	1793,
	1780,
	1793,
	2593,
	2526,
	2385,
	2482,
	2431,
	1793,
	1761,
	1750,
	991,
	1363,
	669,
	1317,
	2783,
	2033,
	822,
	549,
	1253,
	1780,
	1793,
	2830,
	1793,
	1793,
	2596,
	2569,
	1793,
	1780,
	1536,
	1782,
	1538,
	1780,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	2716,
	549,
	1253,
	1253,
	1253,
	549,
	1253,
	989,
	989,
	1519,
	773,
	773,
	1253,
	1793,
	2830,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	1510,
	1793,
	1780,
	1793,
	1761,
	1793,
	1761,
	1510,
	1793,
	1780,
	1793,
	1761,
	1793,
	1761,
	1253,
	1253,
	1253,
	670,
	618,
	1422,
	1253,
	1253,
	1780,
	1519,
	989,
	989,
	1074,
	774,
	1793,
	440,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	1510,
	1793,
	1780,
	1761,
	1793,
	1761,
	2716,
	2716,
	2716,
	2716,
	2716,
	2716,
	1793,
	1793,
	1793,
	1793,
	1793,
	1761,
	1519,
	1793,
	1793,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	1519,
	1750,
	1253,
	446,
	1793,
	1793,
	1793,
	989,
	989,
	1519,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1761,
	2716,
	1253,
	1793,
	669,
	1791,
	1547,
	1750,
	1510,
	1761,
	1519,
	1761,
	1519,
	1782,
	943,
	1441,
	1793,
	2431,
	2171,
	2329,
	2339,
	2481,
	2128,
	2480,
	2121,
	2716,
	2484,
	2530,
	2321,
	1793,
	1793,
	1793,
	1793,
	1793,
	1793,
	1538,
	1793,
	1793,
	1793,
	989,
	989,
	1793,
	1793,
	1793,
	1793,
	1363,
	1793,
	1793,
	2672,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	347,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
