﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31;
// System.Action`1<System.String>
struct Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3;
// System.Action`2<UnityEngine.GameObject,System.String>
struct Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0;
// System.Comparison`1<SimpleFileBrowser.FileSystemEntry>
struct Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38;
// System.Collections.Generic.Dictionary`2<System.Int32,SimpleFileBrowser.ListItem>
struct Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Int32>>
struct Dictionary_2_t0141BC6966873E9827CA2F0856EF6D6EA51AD929;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F;
// System.Collections.Generic.Dictionary`2<System.String,Dummiesman.OBJObjectBuilder>
struct Dictionary_2_t38706229FB7F5DD826CC94AD9B6EEB0049FEF20C;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite>
struct Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D;
// System.Collections.Generic.Dictionary`2<Dummiesman.OBJObjectBuilder/ObjLoopHash,System.Int32>
struct Dictionary_2_tEC067FFDB4677DBEDC3664ED8FDF94D9F8891FAF;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t2090386F6F1AD36902CC49C47D33DBC66C60B100;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t62770D319A98A721900E1C08EC156D59926CDC42;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_tC96EF7224041A1435F414F0A974F5E415FFCC528;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t092EF97BABC8AD77EFF4A451CB7124FD24E1E10E;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t5660F2E7C674760C0F595E987D232818F4E0AA0A;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_tEAD99CB0B6FC23ECDE82646A3710D24E183A26C5;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t2890FC9B45E7B56EDFEC06B764D49D1EDB7E4ADA;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t5BDB9EBC3BFFC71A97904CD3E01ED89BEBEE00AD;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t4870461507D94C55EB84820C99AC6C495DCE4A53;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t613569DE3BDA144DA5A8D56AFFCA0A1F03DCD96C;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_tBAE9A2CDB8174D2A78A46C57B54E9D86245D3BC8;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t9E4CEC2DA9A249AE1B4E40E3D2B396741E347F60;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t7FBE64714A4E50EF106796C42BB2493D33F6C7CA;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_tA4599B6CC5BFC12FBD61E3E846515E4DEBA873EF;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_tF2F90BDFC6B14457DE9485B3A5C065C31BE80AD0;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_tD45A9BFBDD99A872DA88945877EBDFD3542C9E23;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_tB6C6DD6D13924F282523CD3468E286DA3742C74C;
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_tCE3CE3D7F67C20FF5576ED2A6E74518A0756E2DE;
// System.Collections.Generic.HashSet`1<System.Char>
struct HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_tBC81A7DE12BDADB43C5817DF67BD79E70CFFFC54;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserItem>
struct List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserQuickLink>
struct List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry>
struct List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<AsImpL.Loader>
struct List_1_tF8B1775B627E4B4F8DA9A10979A72AA4553651A8;
// System.Collections.Generic.List`1<AsImpL.MaterialData>
struct List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76;
// System.Collections.Generic.List`1<AsImpL.ModelImportInfo>
struct List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447;
// System.Collections.Generic.List`1<AsImpL.SingleLoadingProgress>
struct List_1_tBA02C63B2CCA4D235A278271A2AEECE53A29EAB8;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_tECEEA56321275CFF8DECB929786CE364F743B07D;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A;
// System.Collections.Generic.List`1<AsImpL.DataSet/FaceGroupData>
struct List_1_t138CB7BF5449ED9D4E18705A315F237223D3CF77;
// System.Collections.Generic.List`1<AsImpL.DataSet/FaceIndices>
struct List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754;
// System.Collections.Generic.List`1<AsImpL.DataSet/ObjectData>
struct List_1_t74926B717449DBB4C29EBE546DD95522A79A9A7C;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t88A4BE98895C19A1F134BA69882646898AC2BD70;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowser/Filter>
struct List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct List_1_t75FFBEBE24171F12D0459DE4BA90E0FD3E22A60E;
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct List_1_t81B435AD26EAEDC4948F109696316554CD0DC100;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t7DE371FC4173D0882831B9DD0945CA448A3BAB31;
// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_t0AABBBAF16CED490518BA49ED7BC02D9A9475166;
// System.Collections.Generic.Stack`1<SimpleFileBrowser.ListItem>
struct Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color32[]
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// SimpleFileBrowser.FileSystemEntry[]
struct FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// SimpleFileBrowser.FileBrowser/FiletypeIcon[]
struct FiletypeIconU5BU5D_tCB87AC1C627D7517B2694613417FDB428E27F395;
// SimpleFileBrowser.FileBrowser/QuickLink[]
struct QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876;
// System.IO.BinaryReader
struct BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// AsImpL.DataSet
struct DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB;
// UnityEngine.UI.Dropdown
struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C;
// SimpleFileBrowser.FileBrowser
struct FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8;
// SimpleFileBrowser.FileBrowserContextMenu
struct FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC;
// SimpleFileBrowser.FileBrowserCursorHandler
struct FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C;
// SimpleFileBrowser.FileBrowserDeleteConfirmationPanel
struct FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C;
// SimpleFileBrowser.FileBrowserItem
struct FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025;
// SimpleFileBrowser.FileBrowserMovement
struct FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19;
// SimpleFileBrowser.FileBrowserQuickLink
struct FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C;
// SimpleFileBrowser.FileBrowserRenamedItem
struct FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4;
// System.IO.FileInfo
struct FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// SimpleFileBrowser.IListViewAdapter
struct IListViewAdapter_t043B79411048E065CEDC01867299EFF50F726D00;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// AsImpL.ImportOptions
struct ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F;
// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// SimpleFileBrowser.ListItem
struct ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED;
// AsImpL.Loader
struct Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B;
// AsImpL.LoaderObj
struct LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543;
// AsImpL.LoadingProgress
struct LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// AsImpL.MaterialData
struct MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Dummiesman.OBJLoader
struct OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896;
// Dummiesman.OBJObjectBuilder
struct OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA;
// AsImpL.ObjectBuilder
struct ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D;
// AsImpL.ObjectImporter
struct ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D;
// AsImpL.PathSettings
struct PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// SimpleFileBrowser.RecycledListView
struct RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767;
// System.Text.RegularExpressions.Regex
struct Regex_t90F443D398F44965EA241A652ED75DF5BA072A1F;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA;
// UnityEngine.UI.Scrollbar
struct Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// AsImpL.SingleLoadingProgress
struct SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.IO.StreamReader
struct StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.UI.Toggle
struct Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// AsImpL.MathUtil.Vertex
struct Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// mainMenu
struct mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026;
// AsImpL.DataSet/FaceGroupData
struct FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3;
// AsImpL.DataSet/ObjectData
struct ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t6C4DB59340B55DE906C54EE3FF7DAE4DE24A1276;
// SimpleFileBrowser.FileBrowser/Filter
struct Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8;
// SimpleFileBrowser.FileBrowser/OnCancel
struct OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E;
// SimpleFileBrowser.FileBrowser/OnSuccess
struct OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C;
// SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed
struct OnDeletionConfirmed_t54FFE37F4F1E4680F19B875155B0796C976BE677;
// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted
struct OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74;
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// AsImpL.ObjectBuilder/BuildStatus
struct BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96;
// AsImpL.ObjectBuilder/ProgressInfo
struct ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t9D6C059892DE030746D2873EB8871415BAC79311;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t6DAE64211C37E996B257BF2C54707DAD3474D69C;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tA4A6A2336A9B9FEE31F8F5344576B3BB0A7B3F34;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t125C1CA6D0148380915E597AC8ADBB93EFB0EE29;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t27A8B301052E9C6A4A7D38F95293CA129C39373F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t48E12CFDCFDEA0CD7D83F9DDE1E341DBCC855005;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t7B9EFE80B7D7F16F3E7B8FA75FEF45B00E0C0075;

struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com;
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com;
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke;
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_t3D12990088F3C42D819ACC9CFEED729104DC4287 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tE7281C0E269ACF224AB82F00FE8D46ED8C621032 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tC81EA6223B5DC14CA590D1BADEDD82F8EB8B39EF 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tD63710BA729AB26F68A06F88FD5557BC1A838A68 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_t1A04780FFD77F371B4AE0F3CA161423BCEF4EDAF 
{
public:

public:
};


// System.Object


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528  : public RuntimeObject
{
public:

public:
};


// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};


// B83.Image.BMP.BMPLoader
struct  BMPLoader_t7887312A90AEBE67CF364032B76634D8E4E5C828  : public RuntimeObject
{
public:
	// System.Boolean B83.Image.BMP.BMPLoader::ReadPaletteAlpha
	bool ___ReadPaletteAlpha_1;
	// System.Boolean B83.Image.BMP.BMPLoader::ForceAlphaReadWhenPossible
	bool ___ForceAlphaReadWhenPossible_2;

public:
	inline static int32_t get_offset_of_ReadPaletteAlpha_1() { return static_cast<int32_t>(offsetof(BMPLoader_t7887312A90AEBE67CF364032B76634D8E4E5C828, ___ReadPaletteAlpha_1)); }
	inline bool get_ReadPaletteAlpha_1() const { return ___ReadPaletteAlpha_1; }
	inline bool* get_address_of_ReadPaletteAlpha_1() { return &___ReadPaletteAlpha_1; }
	inline void set_ReadPaletteAlpha_1(bool value)
	{
		___ReadPaletteAlpha_1 = value;
	}

	inline static int32_t get_offset_of_ForceAlphaReadWhenPossible_2() { return static_cast<int32_t>(offsetof(BMPLoader_t7887312A90AEBE67CF364032B76634D8E4E5C828, ___ForceAlphaReadWhenPossible_2)); }
	inline bool get_ForceAlphaReadWhenPossible_2() const { return ___ForceAlphaReadWhenPossible_2; }
	inline bool* get_address_of_ForceAlphaReadWhenPossible_2() { return &___ForceAlphaReadWhenPossible_2; }
	inline void set_ForceAlphaReadWhenPossible_2(bool value)
	{
		___ForceAlphaReadWhenPossible_2 = value;
	}
};


// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_tD033B949E13F0BF6E82C34E1EB18F422A0F66105  : public RuntimeObject
{
public:

public:
};


// Dummiesman.BinaryExtensions
struct  BinaryExtensions_tB6A330A950CA5E7CCC2BB553E50664A89C174D5E  : public RuntimeObject
{
public:

public:
};


// B83.Image.BMP.BitStreamReader
struct  BitStreamReader_t346F29AEEFCB12190328AD5372F70B676DA34EC3  : public RuntimeObject
{
public:
	// System.IO.BinaryReader B83.Image.BMP.BitStreamReader::m_Reader
	BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * ___m_Reader_0;
	// System.Byte B83.Image.BMP.BitStreamReader::m_Data
	uint8_t ___m_Data_1;
	// System.Int32 B83.Image.BMP.BitStreamReader::m_Bits
	int32_t ___m_Bits_2;

public:
	inline static int32_t get_offset_of_m_Reader_0() { return static_cast<int32_t>(offsetof(BitStreamReader_t346F29AEEFCB12190328AD5372F70B676DA34EC3, ___m_Reader_0)); }
	inline BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * get_m_Reader_0() const { return ___m_Reader_0; }
	inline BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 ** get_address_of_m_Reader_0() { return &___m_Reader_0; }
	inline void set_m_Reader_0(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * value)
	{
		___m_Reader_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Reader_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Data_1() { return static_cast<int32_t>(offsetof(BitStreamReader_t346F29AEEFCB12190328AD5372F70B676DA34EC3, ___m_Data_1)); }
	inline uint8_t get_m_Data_1() const { return ___m_Data_1; }
	inline uint8_t* get_address_of_m_Data_1() { return &___m_Data_1; }
	inline void set_m_Data_1(uint8_t value)
	{
		___m_Data_1 = value;
	}

	inline static int32_t get_offset_of_m_Bits_2() { return static_cast<int32_t>(offsetof(BitStreamReader_t346F29AEEFCB12190328AD5372F70B676DA34EC3, ___m_Bits_2)); }
	inline int32_t get_m_Bits_2() const { return ___m_Bits_2; }
	inline int32_t* get_address_of_m_Bits_2() { return &___m_Bits_2; }
	inline void set_m_Bits_2(int32_t value)
	{
		___m_Bits_2 = value;
	}
};


// Dummiesman.CharWordReader
struct  CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9  : public RuntimeObject
{
public:
	// System.Char[] Dummiesman.CharWordReader::word
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___word_0;
	// System.Int32 Dummiesman.CharWordReader::wordSize
	int32_t ___wordSize_1;
	// System.Boolean Dummiesman.CharWordReader::endReached
	bool ___endReached_2;
	// System.IO.StreamReader Dummiesman.CharWordReader::reader
	StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * ___reader_3;
	// System.Int32 Dummiesman.CharWordReader::bufferSize
	int32_t ___bufferSize_4;
	// System.Char[] Dummiesman.CharWordReader::buffer
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___buffer_5;
	// System.Char Dummiesman.CharWordReader::currentChar
	Il2CppChar ___currentChar_6;
	// System.Int32 Dummiesman.CharWordReader::currentPosition
	int32_t ___currentPosition_7;
	// System.Int32 Dummiesman.CharWordReader::maxPosition
	int32_t ___maxPosition_8;

public:
	inline static int32_t get_offset_of_word_0() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___word_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_word_0() const { return ___word_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_word_0() { return &___word_0; }
	inline void set_word_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___word_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___word_0), (void*)value);
	}

	inline static int32_t get_offset_of_wordSize_1() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___wordSize_1)); }
	inline int32_t get_wordSize_1() const { return ___wordSize_1; }
	inline int32_t* get_address_of_wordSize_1() { return &___wordSize_1; }
	inline void set_wordSize_1(int32_t value)
	{
		___wordSize_1 = value;
	}

	inline static int32_t get_offset_of_endReached_2() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___endReached_2)); }
	inline bool get_endReached_2() const { return ___endReached_2; }
	inline bool* get_address_of_endReached_2() { return &___endReached_2; }
	inline void set_endReached_2(bool value)
	{
		___endReached_2 = value;
	}

	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___reader_3)); }
	inline StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * get_reader_3() const { return ___reader_3; }
	inline StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(StreamReader_tA857ACC7ABF9AA4638E1291E6D2539C14D2963D3 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reader_3), (void*)value);
	}

	inline static int32_t get_offset_of_bufferSize_4() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___bufferSize_4)); }
	inline int32_t get_bufferSize_4() const { return ___bufferSize_4; }
	inline int32_t* get_address_of_bufferSize_4() { return &___bufferSize_4; }
	inline void set_bufferSize_4(int32_t value)
	{
		___bufferSize_4 = value;
	}

	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___buffer_5)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_buffer_5() const { return ___buffer_5; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buffer_5), (void*)value);
	}

	inline static int32_t get_offset_of_currentChar_6() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___currentChar_6)); }
	inline Il2CppChar get_currentChar_6() const { return ___currentChar_6; }
	inline Il2CppChar* get_address_of_currentChar_6() { return &___currentChar_6; }
	inline void set_currentChar_6(Il2CppChar value)
	{
		___currentChar_6 = value;
	}

	inline static int32_t get_offset_of_currentPosition_7() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___currentPosition_7)); }
	inline int32_t get_currentPosition_7() const { return ___currentPosition_7; }
	inline int32_t* get_address_of_currentPosition_7() { return &___currentPosition_7; }
	inline void set_currentPosition_7(int32_t value)
	{
		___currentPosition_7 = value;
	}

	inline static int32_t get_offset_of_maxPosition_8() { return static_cast<int32_t>(offsetof(CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9, ___maxPosition_8)); }
	inline int32_t get_maxPosition_8() const { return ___maxPosition_8; }
	inline int32_t* get_address_of_maxPosition_8() { return &___maxPosition_8; }
	inline void set_maxPosition_8(int32_t value)
	{
		___maxPosition_8 = value;
	}
};


// Dummiesman.Extensions.ColorExtensions
struct  ColorExtensions_t9F13E8AF7EFAA846676626D2C10E60654ADC2A34  : public RuntimeObject
{
public:

public:
};


// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7  : public RuntimeObject
{
public:

public:
};


// Dummiesman.DDSLoader
struct  DDSLoader_t06BDF806480E03C2939D09627442AF8AA1EB6642  : public RuntimeObject
{
public:

public:
};


// AsImpL.DataSet
struct  DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<AsImpL.DataSet/ObjectData> AsImpL.DataSet::objectList
	List_1_t74926B717449DBB4C29EBE546DD95522A79A9A7C * ___objectList_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> AsImpL.DataSet::vertList
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___vertList_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> AsImpL.DataSet::uvList
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___uvList_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> AsImpL.DataSet::normalList
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___normalList_3;
	// System.Collections.Generic.List`1<UnityEngine.Color> AsImpL.DataSet::colorList
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___colorList_4;
	// System.Int32 AsImpL.DataSet::unnamedGroupIndex
	int32_t ___unnamedGroupIndex_5;
	// AsImpL.DataSet/ObjectData AsImpL.DataSet::currObjData
	ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71 * ___currObjData_6;
	// AsImpL.DataSet/FaceGroupData AsImpL.DataSet::currGroup
	FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3 * ___currGroup_7;
	// System.Boolean AsImpL.DataSet::noFaceDefined
	bool ___noFaceDefined_8;

public:
	inline static int32_t get_offset_of_objectList_0() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___objectList_0)); }
	inline List_1_t74926B717449DBB4C29EBE546DD95522A79A9A7C * get_objectList_0() const { return ___objectList_0; }
	inline List_1_t74926B717449DBB4C29EBE546DD95522A79A9A7C ** get_address_of_objectList_0() { return &___objectList_0; }
	inline void set_objectList_0(List_1_t74926B717449DBB4C29EBE546DD95522A79A9A7C * value)
	{
		___objectList_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectList_0), (void*)value);
	}

	inline static int32_t get_offset_of_vertList_1() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___vertList_1)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_vertList_1() const { return ___vertList_1; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_vertList_1() { return &___vertList_1; }
	inline void set_vertList_1(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___vertList_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vertList_1), (void*)value);
	}

	inline static int32_t get_offset_of_uvList_2() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___uvList_2)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_uvList_2() const { return ___uvList_2; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_uvList_2() { return &___uvList_2; }
	inline void set_uvList_2(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___uvList_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uvList_2), (void*)value);
	}

	inline static int32_t get_offset_of_normalList_3() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___normalList_3)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_normalList_3() const { return ___normalList_3; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_normalList_3() { return &___normalList_3; }
	inline void set_normalList_3(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___normalList_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normalList_3), (void*)value);
	}

	inline static int32_t get_offset_of_colorList_4() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___colorList_4)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_colorList_4() const { return ___colorList_4; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_colorList_4() { return &___colorList_4; }
	inline void set_colorList_4(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___colorList_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorList_4), (void*)value);
	}

	inline static int32_t get_offset_of_unnamedGroupIndex_5() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___unnamedGroupIndex_5)); }
	inline int32_t get_unnamedGroupIndex_5() const { return ___unnamedGroupIndex_5; }
	inline int32_t* get_address_of_unnamedGroupIndex_5() { return &___unnamedGroupIndex_5; }
	inline void set_unnamedGroupIndex_5(int32_t value)
	{
		___unnamedGroupIndex_5 = value;
	}

	inline static int32_t get_offset_of_currObjData_6() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___currObjData_6)); }
	inline ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71 * get_currObjData_6() const { return ___currObjData_6; }
	inline ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71 ** get_address_of_currObjData_6() { return &___currObjData_6; }
	inline void set_currObjData_6(ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71 * value)
	{
		___currObjData_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currObjData_6), (void*)value);
	}

	inline static int32_t get_offset_of_currGroup_7() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___currGroup_7)); }
	inline FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3 * get_currGroup_7() const { return ___currGroup_7; }
	inline FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3 ** get_address_of_currGroup_7() { return &___currGroup_7; }
	inline void set_currGroup_7(FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3 * value)
	{
		___currGroup_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currGroup_7), (void*)value);
	}

	inline static int32_t get_offset_of_noFaceDefined_8() { return static_cast<int32_t>(offsetof(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863, ___noFaceDefined_8)); }
	inline bool get_noFaceDefined_8() const { return ___noFaceDefined_8; }
	inline bool* get_address_of_noFaceDefined_8() { return &___noFaceDefined_8; }
	inline void set_noFaceDefined_8(bool value)
	{
		___noFaceDefined_8 = value;
	}
};


// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_tBAE9A2CDB8174D2A78A46C57B54E9D86245D3BC8 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t9E4CEC2DA9A249AE1B4E40E3D2B396741E347F60 * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t613569DE3BDA144DA5A8D56AFFCA0A1F03DCD96C * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_t7FBE64714A4E50EF106796C42BB2493D33F6C7CA * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t4870461507D94C55EB84820C99AC6C495DCE4A53 * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_t2890FC9B45E7B56EDFEC06B764D49D1EDB7E4ADA * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t2090386F6F1AD36902CC49C47D33DBC66C60B100 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t092EF97BABC8AD77EFF4A451CB7124FD24E1E10E * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_tEAD99CB0B6FC23ECDE82646A3710D24E183A26C5 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t5660F2E7C674760C0F595E987D232818F4E0AA0A * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_tA4599B6CC5BFC12FBD61E3E846515E4DEBA873EF * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_tB6C6DD6D13924F282523CD3468E286DA3742C74C * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_tF2F90BDFC6B14457DE9485B3A5C065C31BE80AD0 * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_tC96EF7224041A1435F414F0A974F5E415FFCC528 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_t5BDB9EBC3BFFC71A97904CD3E01ED89BEBEE00AD * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_tD45A9BFBDD99A872DA88945877EBDFD3542C9E23 * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_t62770D319A98A721900E1C08EC156D59926CDC42 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_t7DE371FC4173D0882831B9DD0945CA448A3BAB31 * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___s_InternalTransformList_18;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_tBAE9A2CDB8174D2A78A46C57B54E9D86245D3BC8 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_tBAE9A2CDB8174D2A78A46C57B54E9D86245D3BC8 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_tBAE9A2CDB8174D2A78A46C57B54E9D86245D3BC8 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerEnterHandler_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t9E4CEC2DA9A249AE1B4E40E3D2B396741E347F60 * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t9E4CEC2DA9A249AE1B4E40E3D2B396741E347F60 ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t9E4CEC2DA9A249AE1B4E40E3D2B396741E347F60 * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerExitHandler_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t613569DE3BDA144DA5A8D56AFFCA0A1F03DCD96C * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t613569DE3BDA144DA5A8D56AFFCA0A1F03DCD96C ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t613569DE3BDA144DA5A8D56AFFCA0A1F03DCD96C * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerDownHandler_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_t7FBE64714A4E50EF106796C42BB2493D33F6C7CA * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_t7FBE64714A4E50EF106796C42BB2493D33F6C7CA ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_t7FBE64714A4E50EF106796C42BB2493D33F6C7CA * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerUpHandler_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t4870461507D94C55EB84820C99AC6C495DCE4A53 * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t4870461507D94C55EB84820C99AC6C495DCE4A53 ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t4870461507D94C55EB84820C99AC6C495DCE4A53 * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PointerClickHandler_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_t2890FC9B45E7B56EDFEC06B764D49D1EDB7E4ADA * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_t2890FC9B45E7B56EDFEC06B764D49D1EDB7E4ADA ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_t2890FC9B45E7B56EDFEC06B764D49D1EDB7E4ADA * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InitializePotentialDragHandler_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t2090386F6F1AD36902CC49C47D33DBC66C60B100 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t2090386F6F1AD36902CC49C47D33DBC66C60B100 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t2090386F6F1AD36902CC49C47D33DBC66C60B100 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_BeginDragHandler_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t092EF97BABC8AD77EFF4A451CB7124FD24E1E10E * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t092EF97BABC8AD77EFF4A451CB7124FD24E1E10E ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t092EF97BABC8AD77EFF4A451CB7124FD24E1E10E * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DragHandler_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_tEAD99CB0B6FC23ECDE82646A3710D24E183A26C5 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_tEAD99CB0B6FC23ECDE82646A3710D24E183A26C5 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_tEAD99CB0B6FC23ECDE82646A3710D24E183A26C5 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EndDragHandler_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t5660F2E7C674760C0F595E987D232818F4E0AA0A * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t5660F2E7C674760C0F595E987D232818F4E0AA0A ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t5660F2E7C674760C0F595E987D232818F4E0AA0A * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DropHandler_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_tA4599B6CC5BFC12FBD61E3E846515E4DEBA873EF * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_tA4599B6CC5BFC12FBD61E3E846515E4DEBA873EF ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_tA4599B6CC5BFC12FBD61E3E846515E4DEBA873EF * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ScrollHandler_10), (void*)value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_tB6C6DD6D13924F282523CD3468E286DA3742C74C * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_tB6C6DD6D13924F282523CD3468E286DA3742C74C ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_tB6C6DD6D13924F282523CD3468E286DA3742C74C * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UpdateSelectedHandler_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_tF2F90BDFC6B14457DE9485B3A5C065C31BE80AD0 * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_tF2F90BDFC6B14457DE9485B3A5C065C31BE80AD0 ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_tF2F90BDFC6B14457DE9485B3A5C065C31BE80AD0 * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SelectHandler_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_tC96EF7224041A1435F414F0A974F5E415FFCC528 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_tC96EF7224041A1435F414F0A974F5E415FFCC528 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_tC96EF7224041A1435F414F0A974F5E415FFCC528 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DeselectHandler_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_t5BDB9EBC3BFFC71A97904CD3E01ED89BEBEE00AD * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_t5BDB9EBC3BFFC71A97904CD3E01ED89BEBEE00AD ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_t5BDB9EBC3BFFC71A97904CD3E01ED89BEBEE00AD * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_MoveHandler_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_tD45A9BFBDD99A872DA88945877EBDFD3542C9E23 * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_tD45A9BFBDD99A872DA88945877EBDFD3542C9E23 ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_tD45A9BFBDD99A872DA88945877EBDFD3542C9E23 * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SubmitHandler_15), (void*)value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_t62770D319A98A721900E1C08EC156D59926CDC42 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_t62770D319A98A721900E1C08EC156D59926CDC42 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_t62770D319A98A721900E1C08EC156D59926CDC42 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CancelHandler_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_t7DE371FC4173D0882831B9DD0945CA448A3BAB31 * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_t7DE371FC4173D0882831B9DD0945CA448A3BAB31 ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_t7DE371FC4173D0882831B9DD0945CA448A3BAB31 * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_HandlerListPool_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalTransformList_18), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowserHelpers
struct  FileBrowserHelpers_t2E079075C38746CC9599F4635A704970DF3AA4E1  : public RuntimeObject
{
public:

public:
};


// Dummiesman.ImageLoader
struct  ImageLoader_t055F10A43BA181BB3017E3EC9CB88A6D2ECBA2CE  : public RuntimeObject
{
public:

public:
};


// Dummiesman.ImageLoaderHelper
struct  ImageLoaderHelper_t9689B253D821F39DF411F9DD51E8B321DFF40901  : public RuntimeObject
{
public:

public:
};


// Dummiesman.ImageUtils
struct  ImageUtils_t3F02A1314784D18DFB731A4BD2EB4C9722D6C024  : public RuntimeObject
{
public:

public:
};


// AsImpL.LoadingProgress
struct  LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<AsImpL.SingleLoadingProgress> AsImpL.LoadingProgress::singleProgress
	List_1_tBA02C63B2CCA4D235A278271A2AEECE53A29EAB8 * ___singleProgress_0;

public:
	inline static int32_t get_offset_of_singleProgress_0() { return static_cast<int32_t>(offsetof(LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5, ___singleProgress_0)); }
	inline List_1_tBA02C63B2CCA4D235A278271A2AEECE53A29EAB8 * get_singleProgress_0() const { return ___singleProgress_0; }
	inline List_1_tBA02C63B2CCA4D235A278271A2AEECE53A29EAB8 ** get_address_of_singleProgress_0() { return &___singleProgress_0; }
	inline void set_singleProgress_0(List_1_tBA02C63B2CCA4D235A278271A2AEECE53A29EAB8 * value)
	{
		___singleProgress_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___singleProgress_0), (void*)value);
	}
};


// MTLLoader
struct  MTLLoader_tDE3C3EB24E2C96C6A4C6F1D2D19AAABC9EC93E82  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> MTLLoader::SearchPaths
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___SearchPaths_0;
	// System.IO.FileInfo MTLLoader::_objFileInfo
	FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * ____objFileInfo_1;

public:
	inline static int32_t get_offset_of_SearchPaths_0() { return static_cast<int32_t>(offsetof(MTLLoader_tDE3C3EB24E2C96C6A4C6F1D2D19AAABC9EC93E82, ___SearchPaths_0)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_SearchPaths_0() const { return ___SearchPaths_0; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_SearchPaths_0() { return &___SearchPaths_0; }
	inline void set_SearchPaths_0(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___SearchPaths_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SearchPaths_0), (void*)value);
	}

	inline static int32_t get_offset_of__objFileInfo_1() { return static_cast<int32_t>(offsetof(MTLLoader_tDE3C3EB24E2C96C6A4C6F1D2D19AAABC9EC93E82, ____objFileInfo_1)); }
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * get__objFileInfo_1() const { return ____objFileInfo_1; }
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 ** get_address_of__objFileInfo_1() { return &____objFileInfo_1; }
	inline void set__objFileInfo_1(FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * value)
	{
		____objFileInfo_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____objFileInfo_1), (void*)value);
	}
};


// AsImpL.MathUtil.MathUtility
struct  MathUtility_t8F551E08A08D1EE70D680CC8B30511FA4827F647  : public RuntimeObject
{
public:

public:
};


// AsImpL.ModelImportInfo
struct  ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6  : public RuntimeObject
{
public:
	// System.String AsImpL.ModelImportInfo::name
	String_t* ___name_0;
	// System.String AsImpL.ModelImportInfo::path
	String_t* ___path_1;
	// System.Boolean AsImpL.ModelImportInfo::skip
	bool ___skip_2;
	// AsImpL.ImportOptions AsImpL.ModelImportInfo::loaderOptions
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * ___loaderOptions_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_1), (void*)value);
	}

	inline static int32_t get_offset_of_skip_2() { return static_cast<int32_t>(offsetof(ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6, ___skip_2)); }
	inline bool get_skip_2() const { return ___skip_2; }
	inline bool* get_address_of_skip_2() { return &___skip_2; }
	inline void set_skip_2(bool value)
	{
		___skip_2 = value;
	}

	inline static int32_t get_offset_of_loaderOptions_3() { return static_cast<int32_t>(offsetof(ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6, ___loaderOptions_3)); }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * get_loaderOptions_3() const { return ___loaderOptions_3; }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F ** get_address_of_loaderOptions_3() { return &___loaderOptions_3; }
	inline void set_loaderOptions_3(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * value)
	{
		___loaderOptions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loaderOptions_3), (void*)value);
	}
};


// AsImpL.ModelUtil
struct  ModelUtil_tE0B2759106EB2B342DFFE8E6C0075F60D4477EB2  : public RuntimeObject
{
public:

public:
};


// Dummiesman.OBJLoaderHelper
struct  OBJLoaderHelper_t8ED9B962F331F0AE3B00E7C80F8D98248768D84E  : public RuntimeObject
{
public:

public:
};


// Dummiesman.OBJObjectBuilder
struct  OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA  : public RuntimeObject
{
public:
	// System.Int32 Dummiesman.OBJObjectBuilder::<PushedFaceCount>k__BackingField
	int32_t ___U3CPushedFaceCountU3Ek__BackingField_0;
	// Dummiesman.OBJLoader Dummiesman.OBJObjectBuilder::_loader
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 * ____loader_1;
	// System.String Dummiesman.OBJObjectBuilder::_name
	String_t* ____name_2;
	// System.Collections.Generic.Dictionary`2<Dummiesman.OBJObjectBuilder/ObjLoopHash,System.Int32> Dummiesman.OBJObjectBuilder::_globalIndexRemap
	Dictionary_2_tEC067FFDB4677DBEDC3664ED8FDF94D9F8891FAF * ____globalIndexRemap_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Int32>> Dummiesman.OBJObjectBuilder::_materialIndices
	Dictionary_2_t0141BC6966873E9827CA2F0856EF6D6EA51AD929 * ____materialIndices_4;
	// System.Collections.Generic.List`1<System.Int32> Dummiesman.OBJObjectBuilder::_currentIndexList
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ____currentIndexList_5;
	// System.String Dummiesman.OBJObjectBuilder::_lastMaterial
	String_t* ____lastMaterial_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Dummiesman.OBJObjectBuilder::_vertices
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ____vertices_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Dummiesman.OBJObjectBuilder::_normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ____normals_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Dummiesman.OBJObjectBuilder::_uvs
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ____uvs_9;
	// System.Boolean Dummiesman.OBJObjectBuilder::recalculateNormals
	bool ___recalculateNormals_10;

public:
	inline static int32_t get_offset_of_U3CPushedFaceCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ___U3CPushedFaceCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CPushedFaceCountU3Ek__BackingField_0() const { return ___U3CPushedFaceCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPushedFaceCountU3Ek__BackingField_0() { return &___U3CPushedFaceCountU3Ek__BackingField_0; }
	inline void set_U3CPushedFaceCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CPushedFaceCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of__loader_1() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____loader_1)); }
	inline OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 * get__loader_1() const { return ____loader_1; }
	inline OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 ** get_address_of__loader_1() { return &____loader_1; }
	inline void set__loader_1(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 * value)
	{
		____loader_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____loader_1), (void*)value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____name_2), (void*)value);
	}

	inline static int32_t get_offset_of__globalIndexRemap_3() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____globalIndexRemap_3)); }
	inline Dictionary_2_tEC067FFDB4677DBEDC3664ED8FDF94D9F8891FAF * get__globalIndexRemap_3() const { return ____globalIndexRemap_3; }
	inline Dictionary_2_tEC067FFDB4677DBEDC3664ED8FDF94D9F8891FAF ** get_address_of__globalIndexRemap_3() { return &____globalIndexRemap_3; }
	inline void set__globalIndexRemap_3(Dictionary_2_tEC067FFDB4677DBEDC3664ED8FDF94D9F8891FAF * value)
	{
		____globalIndexRemap_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____globalIndexRemap_3), (void*)value);
	}

	inline static int32_t get_offset_of__materialIndices_4() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____materialIndices_4)); }
	inline Dictionary_2_t0141BC6966873E9827CA2F0856EF6D6EA51AD929 * get__materialIndices_4() const { return ____materialIndices_4; }
	inline Dictionary_2_t0141BC6966873E9827CA2F0856EF6D6EA51AD929 ** get_address_of__materialIndices_4() { return &____materialIndices_4; }
	inline void set__materialIndices_4(Dictionary_2_t0141BC6966873E9827CA2F0856EF6D6EA51AD929 * value)
	{
		____materialIndices_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____materialIndices_4), (void*)value);
	}

	inline static int32_t get_offset_of__currentIndexList_5() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____currentIndexList_5)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get__currentIndexList_5() const { return ____currentIndexList_5; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of__currentIndexList_5() { return &____currentIndexList_5; }
	inline void set__currentIndexList_5(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		____currentIndexList_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentIndexList_5), (void*)value);
	}

	inline static int32_t get_offset_of__lastMaterial_6() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____lastMaterial_6)); }
	inline String_t* get__lastMaterial_6() const { return ____lastMaterial_6; }
	inline String_t** get_address_of__lastMaterial_6() { return &____lastMaterial_6; }
	inline void set__lastMaterial_6(String_t* value)
	{
		____lastMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastMaterial_6), (void*)value);
	}

	inline static int32_t get_offset_of__vertices_7() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____vertices_7)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get__vertices_7() const { return ____vertices_7; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of__vertices_7() { return &____vertices_7; }
	inline void set__vertices_7(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		____vertices_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____vertices_7), (void*)value);
	}

	inline static int32_t get_offset_of__normals_8() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____normals_8)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get__normals_8() const { return ____normals_8; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of__normals_8() { return &____normals_8; }
	inline void set__normals_8(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		____normals_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____normals_8), (void*)value);
	}

	inline static int32_t get_offset_of__uvs_9() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ____uvs_9)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get__uvs_9() const { return ____uvs_9; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of__uvs_9() { return &____uvs_9; }
	inline void set__uvs_9(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		____uvs_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____uvs_9), (void*)value);
	}

	inline static int32_t get_offset_of_recalculateNormals_10() { return static_cast<int32_t>(offsetof(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA, ___recalculateNormals_10)); }
	inline bool get_recalculateNormals_10() const { return ___recalculateNormals_10; }
	inline bool* get_address_of_recalculateNormals_10() { return &___recalculateNormals_10; }
	inline void set_recalculateNormals_10(bool value)
	{
		___recalculateNormals_10 = value;
	}
};


// AsImpL.ObjectBuilder
struct  ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D  : public RuntimeObject
{
public:
	// AsImpL.ImportOptions AsImpL.ObjectBuilder::buildOptions
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * ___buildOptions_0;
	// AsImpL.ObjectBuilder/BuildStatus AsImpL.ObjectBuilder::buildStatus
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96 * ___buildStatus_1;
	// AsImpL.DataSet AsImpL.ObjectBuilder::currDataSet
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 * ___currDataSet_2;
	// UnityEngine.GameObject AsImpL.ObjectBuilder::currParentObj
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___currParentObj_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> AsImpL.ObjectBuilder::currMaterials
	Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F * ___currMaterials_4;
	// System.Collections.Generic.List`1<AsImpL.MaterialData> AsImpL.ObjectBuilder::materialData
	List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * ___materialData_5;

public:
	inline static int32_t get_offset_of_buildOptions_0() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D, ___buildOptions_0)); }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * get_buildOptions_0() const { return ___buildOptions_0; }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F ** get_address_of_buildOptions_0() { return &___buildOptions_0; }
	inline void set_buildOptions_0(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * value)
	{
		___buildOptions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buildOptions_0), (void*)value);
	}

	inline static int32_t get_offset_of_buildStatus_1() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D, ___buildStatus_1)); }
	inline BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96 * get_buildStatus_1() const { return ___buildStatus_1; }
	inline BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96 ** get_address_of_buildStatus_1() { return &___buildStatus_1; }
	inline void set_buildStatus_1(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96 * value)
	{
		___buildStatus_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buildStatus_1), (void*)value);
	}

	inline static int32_t get_offset_of_currDataSet_2() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D, ___currDataSet_2)); }
	inline DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 * get_currDataSet_2() const { return ___currDataSet_2; }
	inline DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 ** get_address_of_currDataSet_2() { return &___currDataSet_2; }
	inline void set_currDataSet_2(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 * value)
	{
		___currDataSet_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currDataSet_2), (void*)value);
	}

	inline static int32_t get_offset_of_currParentObj_3() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D, ___currParentObj_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_currParentObj_3() const { return ___currParentObj_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_currParentObj_3() { return &___currParentObj_3; }
	inline void set_currParentObj_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___currParentObj_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currParentObj_3), (void*)value);
	}

	inline static int32_t get_offset_of_currMaterials_4() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D, ___currMaterials_4)); }
	inline Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F * get_currMaterials_4() const { return ___currMaterials_4; }
	inline Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F ** get_address_of_currMaterials_4() { return &___currMaterials_4; }
	inline void set_currMaterials_4(Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F * value)
	{
		___currMaterials_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currMaterials_4), (void*)value);
	}

	inline static int32_t get_offset_of_materialData_5() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D, ___materialData_5)); }
	inline List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * get_materialData_5() const { return ___materialData_5; }
	inline List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 ** get_address_of_materialData_5() { return &___materialData_5; }
	inline void set_materialData_5(List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * value)
	{
		___materialData_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materialData_5), (void*)value);
	}
};

struct ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D_StaticFields
{
public:
	// System.Int32 AsImpL.ObjectBuilder::MAX_VERTICES_LIMIT_FOR_A_MESH
	int32_t ___MAX_VERTICES_LIMIT_FOR_A_MESH_6;
	// System.Int32 AsImpL.ObjectBuilder::MAX_INDICES_LIMIT_FOR_A_MESH
	int32_t ___MAX_INDICES_LIMIT_FOR_A_MESH_7;
	// System.Int32 AsImpL.ObjectBuilder::MAX_VERT_COUNT
	int32_t ___MAX_VERT_COUNT_8;

public:
	inline static int32_t get_offset_of_MAX_VERTICES_LIMIT_FOR_A_MESH_6() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D_StaticFields, ___MAX_VERTICES_LIMIT_FOR_A_MESH_6)); }
	inline int32_t get_MAX_VERTICES_LIMIT_FOR_A_MESH_6() const { return ___MAX_VERTICES_LIMIT_FOR_A_MESH_6; }
	inline int32_t* get_address_of_MAX_VERTICES_LIMIT_FOR_A_MESH_6() { return &___MAX_VERTICES_LIMIT_FOR_A_MESH_6; }
	inline void set_MAX_VERTICES_LIMIT_FOR_A_MESH_6(int32_t value)
	{
		___MAX_VERTICES_LIMIT_FOR_A_MESH_6 = value;
	}

	inline static int32_t get_offset_of_MAX_INDICES_LIMIT_FOR_A_MESH_7() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D_StaticFields, ___MAX_INDICES_LIMIT_FOR_A_MESH_7)); }
	inline int32_t get_MAX_INDICES_LIMIT_FOR_A_MESH_7() const { return ___MAX_INDICES_LIMIT_FOR_A_MESH_7; }
	inline int32_t* get_address_of_MAX_INDICES_LIMIT_FOR_A_MESH_7() { return &___MAX_INDICES_LIMIT_FOR_A_MESH_7; }
	inline void set_MAX_INDICES_LIMIT_FOR_A_MESH_7(int32_t value)
	{
		___MAX_INDICES_LIMIT_FOR_A_MESH_7 = value;
	}

	inline static int32_t get_offset_of_MAX_VERT_COUNT_8() { return static_cast<int32_t>(offsetof(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D_StaticFields, ___MAX_VERT_COUNT_8)); }
	inline int32_t get_MAX_VERT_COUNT_8() const { return ___MAX_VERT_COUNT_8; }
	inline int32_t* get_address_of_MAX_VERT_COUNT_8() { return &___MAX_VERT_COUNT_8; }
	inline void set_MAX_VERT_COUNT_8(int32_t value)
	{
		___MAX_VERT_COUNT_8 = value;
	}
};


// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t9B5A044582C34098C71FC3C8CD413369CDE0DA33  : public RuntimeObject
{
public:

public:
};

struct RaycasterManager_t9B5A044582C34098C71FC3C8CD413369CDE0DA33_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_tBC81A7DE12BDADB43C5817DF67BD79E70CFFFC54 * ___s_Raycasters_0;

public:
	inline static int32_t get_offset_of_s_Raycasters_0() { return static_cast<int32_t>(offsetof(RaycasterManager_t9B5A044582C34098C71FC3C8CD413369CDE0DA33_StaticFields, ___s_Raycasters_0)); }
	inline List_1_tBC81A7DE12BDADB43C5817DF67BD79E70CFFFC54 * get_s_Raycasters_0() const { return ___s_Raycasters_0; }
	inline List_1_tBC81A7DE12BDADB43C5817DF67BD79E70CFFFC54 ** get_address_of_s_Raycasters_0() { return &___s_Raycasters_0; }
	inline void set_s_Raycasters_0(List_1_tBC81A7DE12BDADB43C5817DF67BD79E70CFFFC54 * value)
	{
		___s_Raycasters_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Raycasters_0), (void*)value);
	}
};


// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t27A8B301052E9C6A4A7D38F95293CA129C39373F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t48E12CFDCFDEA0CD7D83F9DDE1E341DBCC855005 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tA4A6A2336A9B9FEE31F8F5344576B3BB0A7B3F34 * ___getRaycastNonAlloc_2;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t125C1CA6D0148380915E597AC8ADBB93EFB0EE29 * ___raycast2D_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t9D6C059892DE030746D2873EB8871415BAC79311 * ___getRayIntersectionAll_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t6DAE64211C37E996B257BF2C54707DAD3474D69C * ___getRayIntersectionAllNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1, ___raycast3D_0)); }
	inline Raycast3DCallback_t27A8B301052E9C6A4A7D38F95293CA129C39373F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t27A8B301052E9C6A4A7D38F95293CA129C39373F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t27A8B301052E9C6A4A7D38F95293CA129C39373F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycast3D_0), (void*)value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t48E12CFDCFDEA0CD7D83F9DDE1E341DBCC855005 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t48E12CFDCFDEA0CD7D83F9DDE1E341DBCC855005 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t48E12CFDCFDEA0CD7D83F9DDE1E341DBCC855005 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycast3DAll_1), (void*)value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1, ___getRaycastNonAlloc_2)); }
	inline GetRaycastNonAllocCallback_tA4A6A2336A9B9FEE31F8F5344576B3BB0A7B3F34 * get_getRaycastNonAlloc_2() const { return ___getRaycastNonAlloc_2; }
	inline GetRaycastNonAllocCallback_tA4A6A2336A9B9FEE31F8F5344576B3BB0A7B3F34 ** get_address_of_getRaycastNonAlloc_2() { return &___getRaycastNonAlloc_2; }
	inline void set_getRaycastNonAlloc_2(GetRaycastNonAllocCallback_tA4A6A2336A9B9FEE31F8F5344576B3BB0A7B3F34 * value)
	{
		___getRaycastNonAlloc_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getRaycastNonAlloc_2), (void*)value);
	}

	inline static int32_t get_offset_of_raycast2D_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1, ___raycast2D_3)); }
	inline Raycast2DCallback_t125C1CA6D0148380915E597AC8ADBB93EFB0EE29 * get_raycast2D_3() const { return ___raycast2D_3; }
	inline Raycast2DCallback_t125C1CA6D0148380915E597AC8ADBB93EFB0EE29 ** get_address_of_raycast2D_3() { return &___raycast2D_3; }
	inline void set_raycast2D_3(Raycast2DCallback_t125C1CA6D0148380915E597AC8ADBB93EFB0EE29 * value)
	{
		___raycast2D_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycast2D_3), (void*)value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1, ___getRayIntersectionAll_4)); }
	inline GetRayIntersectionAllCallback_t9D6C059892DE030746D2873EB8871415BAC79311 * get_getRayIntersectionAll_4() const { return ___getRayIntersectionAll_4; }
	inline GetRayIntersectionAllCallback_t9D6C059892DE030746D2873EB8871415BAC79311 ** get_address_of_getRayIntersectionAll_4() { return &___getRayIntersectionAll_4; }
	inline void set_getRayIntersectionAll_4(GetRayIntersectionAllCallback_t9D6C059892DE030746D2873EB8871415BAC79311 * value)
	{
		___getRayIntersectionAll_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getRayIntersectionAll_4), (void*)value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1, ___getRayIntersectionAllNonAlloc_5)); }
	inline GetRayIntersectionAllNonAllocCallback_t6DAE64211C37E996B257BF2C54707DAD3474D69C * get_getRayIntersectionAllNonAlloc_5() const { return ___getRayIntersectionAllNonAlloc_5; }
	inline GetRayIntersectionAllNonAllocCallback_t6DAE64211C37E996B257BF2C54707DAD3474D69C ** get_address_of_getRayIntersectionAllNonAlloc_5() { return &___getRayIntersectionAllNonAlloc_5; }
	inline void set_getRayIntersectionAllNonAlloc_5(GetRayIntersectionAllNonAllocCallback_t6DAE64211C37E996B257BF2C54707DAD3474D69C * value)
	{
		___getRayIntersectionAllNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getRayIntersectionAllNonAlloc_5), (void*)value);
	}
};

struct ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ReflectionMethodsCache_6), (void*)value);
	}
};


// UnityEngine.UI.SetPropertyUtility
struct  SetPropertyUtility_tA0FD167699990D8AFDA1284FCCFEA03357AD73BB  : public RuntimeObject
{
public:

public:
};


// AsImpL.SingleLoadingProgress
struct  SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70  : public RuntimeObject
{
public:
	// System.String AsImpL.SingleLoadingProgress::fileName
	String_t* ___fileName_0;
	// System.String AsImpL.SingleLoadingProgress::message
	String_t* ___message_1;
	// System.Single AsImpL.SingleLoadingProgress::percentage
	float ___percentage_2;
	// System.Int32 AsImpL.SingleLoadingProgress::numObjects
	int32_t ___numObjects_3;
	// System.Int32 AsImpL.SingleLoadingProgress::numSubObjects
	int32_t ___numSubObjects_4;
	// System.Boolean AsImpL.SingleLoadingProgress::error
	bool ___error_5;

public:
	inline static int32_t get_offset_of_fileName_0() { return static_cast<int32_t>(offsetof(SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70, ___fileName_0)); }
	inline String_t* get_fileName_0() const { return ___fileName_0; }
	inline String_t** get_address_of_fileName_0() { return &___fileName_0; }
	inline void set_fileName_0(String_t* value)
	{
		___fileName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileName_0), (void*)value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___message_1), (void*)value);
	}

	inline static int32_t get_offset_of_percentage_2() { return static_cast<int32_t>(offsetof(SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70, ___percentage_2)); }
	inline float get_percentage_2() const { return ___percentage_2; }
	inline float* get_address_of_percentage_2() { return &___percentage_2; }
	inline void set_percentage_2(float value)
	{
		___percentage_2 = value;
	}

	inline static int32_t get_offset_of_numObjects_3() { return static_cast<int32_t>(offsetof(SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70, ___numObjects_3)); }
	inline int32_t get_numObjects_3() const { return ___numObjects_3; }
	inline int32_t* get_address_of_numObjects_3() { return &___numObjects_3; }
	inline void set_numObjects_3(int32_t value)
	{
		___numObjects_3 = value;
	}

	inline static int32_t get_offset_of_numSubObjects_4() { return static_cast<int32_t>(offsetof(SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70, ___numSubObjects_4)); }
	inline int32_t get_numSubObjects_4() const { return ___numSubObjects_4; }
	inline int32_t* get_address_of_numSubObjects_4() { return &___numSubObjects_4; }
	inline void set_numSubObjects_4(int32_t value)
	{
		___numSubObjects_4 = value;
	}

	inline static int32_t get_offset_of_error_5() { return static_cast<int32_t>(offsetof(SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70, ___error_5)); }
	inline bool get_error_5() const { return ___error_5; }
	inline bool* get_address_of_error_5() { return &___error_5; }
	inline void set_error_5(bool value)
	{
		___error_5 = value;
	}
};


// UnityEngine.UI.StencilMaterial
struct  StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5  : public RuntimeObject
{
public:

public:
};

struct StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry> UnityEngine.UI.StencilMaterial::m_List
	List_1_t81B435AD26EAEDC4948F109696316554CD0DC100 * ___m_List_0;

public:
	inline static int32_t get_offset_of_m_List_0() { return static_cast<int32_t>(offsetof(StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_StaticFields, ___m_List_0)); }
	inline List_1_t81B435AD26EAEDC4948F109696316554CD0DC100 * get_m_List_0() const { return ___m_List_0; }
	inline List_1_t81B435AD26EAEDC4948F109696316554CD0DC100 ** get_address_of_m_List_0() { return &___m_List_0; }
	inline void set_m_List_0(List_1_t81B435AD26EAEDC4948F109696316554CD0DC100 * value)
	{
		___m_List_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_List_0), (void*)value);
	}
};


// Dummiesman.StringExtensions
struct  StringExtensions_t8EE5329B03429310F4FF4C39B400D1A409C8BAA1  : public RuntimeObject
{
public:

public:
};


// Dummiesman.TGALoader
struct  TGALoader_t5B0B0E4659ACA01D093F5E50197BFD041F8B4DED  : public RuntimeObject
{
public:

public:
};


// AsImpL.MathUtil.Triangle
struct  Triangle_t979E22FAC10144EA526A845A9089CD91A7D1842A  : public RuntimeObject
{
public:
	// AsImpL.MathUtil.Vertex AsImpL.MathUtil.Triangle::v1
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * ___v1_0;
	// AsImpL.MathUtil.Vertex AsImpL.MathUtil.Triangle::v2
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * ___v2_1;
	// AsImpL.MathUtil.Vertex AsImpL.MathUtil.Triangle::v3
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * ___v3_2;

public:
	inline static int32_t get_offset_of_v1_0() { return static_cast<int32_t>(offsetof(Triangle_t979E22FAC10144EA526A845A9089CD91A7D1842A, ___v1_0)); }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * get_v1_0() const { return ___v1_0; }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA ** get_address_of_v1_0() { return &___v1_0; }
	inline void set_v1_0(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * value)
	{
		___v1_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___v1_0), (void*)value);
	}

	inline static int32_t get_offset_of_v2_1() { return static_cast<int32_t>(offsetof(Triangle_t979E22FAC10144EA526A845A9089CD91A7D1842A, ___v2_1)); }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * get_v2_1() const { return ___v2_1; }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA ** get_address_of_v2_1() { return &___v2_1; }
	inline void set_v2_1(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * value)
	{
		___v2_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___v2_1), (void*)value);
	}

	inline static int32_t get_offset_of_v3_2() { return static_cast<int32_t>(offsetof(Triangle_t979E22FAC10144EA526A845A9089CD91A7D1842A, ___v3_2)); }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * get_v3_2() const { return ___v3_2; }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA ** get_address_of_v3_2() { return &___v3_2; }
	inline void set_v3_2(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * value)
	{
		___v3_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___v3_2), (void*)value);
	}
};


// AsImpL.MathUtil.Triangulation
struct  Triangulation_t860BA06E2470FB42DACF731FC999E8C958B30D07  : public RuntimeObject
{
public:

public:
};


// AsImpL.Triangulator
struct  Triangulator_t46EC961B7D523B81178578BD4DB1679F98410149  : public RuntimeObject
{
public:

public:
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// UnityEngine.Networking.UnityWebRequestTexture
struct  UnityWebRequestTexture_tAFCA9B4B27B787F08D35CA883AAA2BC3371313E4  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.WWWForm
struct  WWWForm_t078274293DA1BDA9AB5689AF8BCBF0EE17A2BABB  : public RuntimeObject
{
public:

public:
};


// UnityEngine.WWWTranscoder
struct  WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771  : public RuntimeObject
{
public:

public:
};

struct WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields
{
public:
	// System.Byte[] UnityEngine.WWWTranscoder::ucHexChars
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___ucHexChars_0;
	// System.Byte[] UnityEngine.WWWTranscoder::lcHexChars
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___lcHexChars_1;
	// System.Byte UnityEngine.WWWTranscoder::urlEscapeChar
	uint8_t ___urlEscapeChar_2;
	// System.Byte[] UnityEngine.WWWTranscoder::urlSpace
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___urlSpace_3;
	// System.Byte[] UnityEngine.WWWTranscoder::dataSpace
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataSpace_4;
	// System.Byte[] UnityEngine.WWWTranscoder::urlForbidden
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___urlForbidden_5;
	// System.Byte UnityEngine.WWWTranscoder::qpEscapeChar
	uint8_t ___qpEscapeChar_6;
	// System.Byte[] UnityEngine.WWWTranscoder::qpSpace
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___qpSpace_7;
	// System.Byte[] UnityEngine.WWWTranscoder::qpForbidden
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___qpForbidden_8;

public:
	inline static int32_t get_offset_of_ucHexChars_0() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___ucHexChars_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_ucHexChars_0() const { return ___ucHexChars_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_ucHexChars_0() { return &___ucHexChars_0; }
	inline void set_ucHexChars_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___ucHexChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ucHexChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_lcHexChars_1() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___lcHexChars_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_lcHexChars_1() const { return ___lcHexChars_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_lcHexChars_1() { return &___lcHexChars_1; }
	inline void set_lcHexChars_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___lcHexChars_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lcHexChars_1), (void*)value);
	}

	inline static int32_t get_offset_of_urlEscapeChar_2() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___urlEscapeChar_2)); }
	inline uint8_t get_urlEscapeChar_2() const { return ___urlEscapeChar_2; }
	inline uint8_t* get_address_of_urlEscapeChar_2() { return &___urlEscapeChar_2; }
	inline void set_urlEscapeChar_2(uint8_t value)
	{
		___urlEscapeChar_2 = value;
	}

	inline static int32_t get_offset_of_urlSpace_3() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___urlSpace_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_urlSpace_3() const { return ___urlSpace_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_urlSpace_3() { return &___urlSpace_3; }
	inline void set_urlSpace_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___urlSpace_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___urlSpace_3), (void*)value);
	}

	inline static int32_t get_offset_of_dataSpace_4() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___dataSpace_4)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_dataSpace_4() const { return ___dataSpace_4; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_dataSpace_4() { return &___dataSpace_4; }
	inline void set_dataSpace_4(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___dataSpace_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataSpace_4), (void*)value);
	}

	inline static int32_t get_offset_of_urlForbidden_5() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___urlForbidden_5)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_urlForbidden_5() const { return ___urlForbidden_5; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_urlForbidden_5() { return &___urlForbidden_5; }
	inline void set_urlForbidden_5(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___urlForbidden_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___urlForbidden_5), (void*)value);
	}

	inline static int32_t get_offset_of_qpEscapeChar_6() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___qpEscapeChar_6)); }
	inline uint8_t get_qpEscapeChar_6() const { return ___qpEscapeChar_6; }
	inline uint8_t* get_address_of_qpEscapeChar_6() { return &___qpEscapeChar_6; }
	inline void set_qpEscapeChar_6(uint8_t value)
	{
		___qpEscapeChar_6 = value;
	}

	inline static int32_t get_offset_of_qpSpace_7() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___qpSpace_7)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_qpSpace_7() const { return ___qpSpace_7; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_qpSpace_7() { return &___qpSpace_7; }
	inline void set_qpSpace_7(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___qpSpace_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___qpSpace_7), (void*)value);
	}

	inline static int32_t get_offset_of_qpForbidden_8() { return static_cast<int32_t>(offsetof(WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields, ___qpForbidden_8)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_qpForbidden_8() const { return ___qpForbidden_8; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_qpForbidden_8() { return &___qpForbidden_8; }
	inline void set_qpForbidden_8(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___qpForbidden_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___qpForbidden_8), (void*)value);
	}
};


// UnityEngineInternal.WebRequestUtils
struct  WebRequestUtils_t3FE2D9FD71A02CD3AF8C91B81280F59E5CF26392  : public RuntimeObject
{
public:

public:
};

struct WebRequestUtils_t3FE2D9FD71A02CD3AF8C91B81280F59E5CF26392_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_t90F443D398F44965EA241A652ED75DF5BA072A1F * ___domainRegex_0;

public:
	inline static int32_t get_offset_of_domainRegex_0() { return static_cast<int32_t>(offsetof(WebRequestUtils_t3FE2D9FD71A02CD3AF8C91B81280F59E5CF26392_StaticFields, ___domainRegex_0)); }
	inline Regex_t90F443D398F44965EA241A652ED75DF5BA072A1F * get_domainRegex_0() const { return ___domainRegex_0; }
	inline Regex_t90F443D398F44965EA241A652ED75DF5BA072A1F ** get_address_of_domainRegex_0() { return &___domainRegex_0; }
	inline void set_domainRegex_0(Regex_t90F443D398F44965EA241A652ED75DF5BA072A1F * value)
	{
		___domainRegex_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___domainRegex_0), (void*)value);
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// AsImpL.Examples.CustomObjImporter/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t516260B5AD68685A397D253FF6E9898F2AB75C4A  : public RuntimeObject
{
public:
	// UnityEngine.Transform AsImpL.Examples.CustomObjImporter/<>c__DisplayClass11_0::tr
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___tr_0;

public:
	inline static int32_t get_offset_of_tr_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t516260B5AD68685A397D253FF6E9898F2AB75C4A, ___tr_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_tr_0() const { return ___tr_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_tr_0() { return &___tr_0; }
	inline void set_tr_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___tr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tr_0), (void*)value);
	}
};


// AsImpL.DataSet/FaceGroupData
struct  FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3  : public RuntimeObject
{
public:
	// System.String AsImpL.DataSet/FaceGroupData::name
	String_t* ___name_0;
	// System.String AsImpL.DataSet/FaceGroupData::materialName
	String_t* ___materialName_1;
	// System.Collections.Generic.List`1<AsImpL.DataSet/FaceIndices> AsImpL.DataSet/FaceGroupData::faces
	List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 * ___faces_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_materialName_1() { return static_cast<int32_t>(offsetof(FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3, ___materialName_1)); }
	inline String_t* get_materialName_1() const { return ___materialName_1; }
	inline String_t** get_address_of_materialName_1() { return &___materialName_1; }
	inline void set_materialName_1(String_t* value)
	{
		___materialName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materialName_1), (void*)value);
	}

	inline static int32_t get_offset_of_faces_2() { return static_cast<int32_t>(offsetof(FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3, ___faces_2)); }
	inline List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 * get_faces_2() const { return ___faces_2; }
	inline List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 ** get_address_of_faces_2() { return &___faces_2; }
	inline void set_faces_2(List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 * value)
	{
		___faces_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___faces_2), (void*)value);
	}
};


// AsImpL.DataSet/ObjectData
struct  ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71  : public RuntimeObject
{
public:
	// System.String AsImpL.DataSet/ObjectData::name
	String_t* ___name_0;
	// System.Collections.Generic.List`1<AsImpL.DataSet/FaceGroupData> AsImpL.DataSet/ObjectData::faceGroups
	List_1_t138CB7BF5449ED9D4E18705A315F237223D3CF77 * ___faceGroups_1;
	// System.Collections.Generic.List`1<AsImpL.DataSet/FaceIndices> AsImpL.DataSet/ObjectData::allFaces
	List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 * ___allFaces_2;
	// System.Boolean AsImpL.DataSet/ObjectData::hasNormals
	bool ___hasNormals_3;
	// System.Boolean AsImpL.DataSet/ObjectData::hasColors
	bool ___hasColors_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_faceGroups_1() { return static_cast<int32_t>(offsetof(ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71, ___faceGroups_1)); }
	inline List_1_t138CB7BF5449ED9D4E18705A315F237223D3CF77 * get_faceGroups_1() const { return ___faceGroups_1; }
	inline List_1_t138CB7BF5449ED9D4E18705A315F237223D3CF77 ** get_address_of_faceGroups_1() { return &___faceGroups_1; }
	inline void set_faceGroups_1(List_1_t138CB7BF5449ED9D4E18705A315F237223D3CF77 * value)
	{
		___faceGroups_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___faceGroups_1), (void*)value);
	}

	inline static int32_t get_offset_of_allFaces_2() { return static_cast<int32_t>(offsetof(ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71, ___allFaces_2)); }
	inline List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 * get_allFaces_2() const { return ___allFaces_2; }
	inline List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 ** get_address_of_allFaces_2() { return &___allFaces_2; }
	inline void set_allFaces_2(List_1_t05F243568AE41E37205BE6F5CCAC3235FBF51754 * value)
	{
		___allFaces_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allFaces_2), (void*)value);
	}

	inline static int32_t get_offset_of_hasNormals_3() { return static_cast<int32_t>(offsetof(ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71, ___hasNormals_3)); }
	inline bool get_hasNormals_3() const { return ___hasNormals_3; }
	inline bool* get_address_of_hasNormals_3() { return &___hasNormals_3; }
	inline void set_hasNormals_3(bool value)
	{
		___hasNormals_3 = value;
	}

	inline static int32_t get_offset_of_hasColors_4() { return static_cast<int32_t>(offsetof(ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71, ___hasColors_4)); }
	inline bool get_hasColors_4() const { return ___hasColors_4; }
	inline bool* get_address_of_hasColors_4() { return &___hasColors_4; }
	inline void set_hasColors_4(bool value)
	{
		___hasColors_4 = value;
	}
};


// UnityEngine.EventSystems.ExecuteEvents/<>c
struct  U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/<>c UnityEngine.EventSystems.ExecuteEvents/<>c::<>9
	U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<>c
struct  U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields
{
public:
	// SimpleFileBrowser.FileBrowser/<>c SimpleFileBrowser.FileBrowser/<>c::<>9
	U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * ___U3CU3E9_0;
	// System.Comparison`1<SimpleFileBrowser.FileSystemEntry> SimpleFileBrowser.FileBrowser/<>c::<>9__208_0
	Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 * ___U3CU3E9__208_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__208_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields, ___U3CU3E9__208_0_1)); }
	inline Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 * get_U3CU3E9__208_0_1() const { return ___U3CU3E9__208_0_1; }
	inline Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 ** get_address_of_U3CU3E9__208_0_1() { return &___U3CU3E9__208_0_1; }
	inline void set_U3CU3E9__208_0_1(Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 * value)
	{
		___U3CU3E9__208_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__208_0_1), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212
struct  U3CCreateNewFolderCoroutineU3Ed__212_t1B9AE04C7B0DABED86E805273034CE28E705DDF6  : public RuntimeObject
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__212::<>4__this
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCreateNewFolderCoroutineU3Ed__212_t1B9AE04C7B0DABED86E805273034CE28E705DDF6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCreateNewFolderCoroutineU3Ed__212_t1B9AE04C7B0DABED86E805273034CE28E705DDF6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCreateNewFolderCoroutineU3Ed__212_t1B9AE04C7B0DABED86E805273034CE28E705DDF6, ___U3CU3E4__this_2)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/Filter
struct  Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8  : public RuntimeObject
{
public:
	// System.String SimpleFileBrowser.FileBrowser/Filter::name
	String_t* ___name_0;
	// System.Collections.Generic.HashSet`1<System.String> SimpleFileBrowser.FileBrowser/Filter::extensions
	HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * ___extensions_1;
	// System.String SimpleFileBrowser.FileBrowser/Filter::defaultExtension
	String_t* ___defaultExtension_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_1() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___extensions_1)); }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * get_extensions_1() const { return ___extensions_1; }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 ** get_address_of_extensions_1() { return &___extensions_1; }
	inline void set_extensions_1(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * value)
	{
		___extensions_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_1), (void*)value);
	}

	inline static int32_t get_offset_of_defaultExtension_2() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___defaultExtension_2)); }
	inline String_t* get_defaultExtension_2() const { return ___defaultExtension_2; }
	inline String_t** get_address_of_defaultExtension_2() { return &___defaultExtension_2; }
	inline void set_defaultExtension_2(String_t* value)
	{
		___defaultExtension_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultExtension_2), (void*)value);
	}
};


// AsImpL.Loader/<Load>d__32
struct  U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.Loader/<Load>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AsImpL.Loader/<Load>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String AsImpL.Loader/<Load>d__32::absolutePath
	String_t* ___absolutePath_2;
	// System.String AsImpL.Loader/<Load>d__32::objName
	String_t* ___objName_3;
	// AsImpL.Loader AsImpL.Loader/<Load>d__32::<>4__this
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * ___U3CU3E4__this_4;
	// UnityEngine.Transform AsImpL.Loader/<Load>d__32::parentObj
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parentObj_5;
	// System.String AsImpL.Loader/<Load>d__32::<name>5__2
	String_t* ___U3CnameU3E5__2_6;
	// System.Single AsImpL.Loader/<Load>d__32::<lastTime>5__3
	float ___U3ClastTimeU3E5__3_7;
	// System.Single AsImpL.Loader/<Load>d__32::<startTime>5__4
	float ___U3CstartTimeU3E5__4_8;
	// UnityEngine.GameObject AsImpL.Loader/<Load>d__32::<newObj>5__5
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CnewObjU3E5__5_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_absolutePath_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___absolutePath_2)); }
	inline String_t* get_absolutePath_2() const { return ___absolutePath_2; }
	inline String_t** get_address_of_absolutePath_2() { return &___absolutePath_2; }
	inline void set_absolutePath_2(String_t* value)
	{
		___absolutePath_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___absolutePath_2), (void*)value);
	}

	inline static int32_t get_offset_of_objName_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___objName_3)); }
	inline String_t* get_objName_3() const { return ___objName_3; }
	inline String_t** get_address_of_objName_3() { return &___objName_3; }
	inline void set_objName_3(String_t* value)
	{
		___objName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objName_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___U3CU3E4__this_4)); }
	inline Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_4), (void*)value);
	}

	inline static int32_t get_offset_of_parentObj_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___parentObj_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_parentObj_5() const { return ___parentObj_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_parentObj_5() { return &___parentObj_5; }
	inline void set_parentObj_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___parentObj_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parentObj_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CnameU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___U3CnameU3E5__2_6)); }
	inline String_t* get_U3CnameU3E5__2_6() const { return ___U3CnameU3E5__2_6; }
	inline String_t** get_address_of_U3CnameU3E5__2_6() { return &___U3CnameU3E5__2_6; }
	inline void set_U3CnameU3E5__2_6(String_t* value)
	{
		___U3CnameU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3E5__2_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClastTimeU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___U3ClastTimeU3E5__3_7)); }
	inline float get_U3ClastTimeU3E5__3_7() const { return ___U3ClastTimeU3E5__3_7; }
	inline float* get_address_of_U3ClastTimeU3E5__3_7() { return &___U3ClastTimeU3E5__3_7; }
	inline void set_U3ClastTimeU3E5__3_7(float value)
	{
		___U3ClastTimeU3E5__3_7 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___U3CstartTimeU3E5__4_8)); }
	inline float get_U3CstartTimeU3E5__4_8() const { return ___U3CstartTimeU3E5__4_8; }
	inline float* get_address_of_U3CstartTimeU3E5__4_8() { return &___U3CstartTimeU3E5__4_8; }
	inline void set_U3CstartTimeU3E5__4_8(float value)
	{
		___U3CstartTimeU3E5__4_8 = value;
	}

	inline static int32_t get_offset_of_U3CnewObjU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903, ___U3CnewObjU3E5__5_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CnewObjU3E5__5_9() const { return ___U3CnewObjU3E5__5_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CnewObjU3E5__5_9() { return &___U3CnewObjU3E5__5_9; }
	inline void set_U3CnewObjU3E5__5_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CnewObjU3E5__5_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnewObjU3E5__5_9), (void*)value);
	}
};


// AsImpL.Loader/<LoadMaterialTexture>d__42
struct  U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.Loader/<LoadMaterialTexture>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AsImpL.Loader/<LoadMaterialTexture>d__42::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AsImpL.Loader AsImpL.Loader/<LoadMaterialTexture>d__42::<>4__this
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * ___U3CU3E4__this_2;
	// System.String AsImpL.Loader/<LoadMaterialTexture>d__42::basePath
	String_t* ___basePath_3;
	// System.String AsImpL.Loader/<LoadMaterialTexture>d__42::path
	String_t* ___path_4;
	// UnityEngine.Networking.UnityWebRequest AsImpL.Loader/<LoadMaterialTexture>d__42::<uwr>5__2
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CuwrU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7, ___U3CU3E4__this_2)); }
	inline Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_basePath_3() { return static_cast<int32_t>(offsetof(U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7, ___basePath_3)); }
	inline String_t* get_basePath_3() const { return ___basePath_3; }
	inline String_t** get_address_of_basePath_3() { return &___basePath_3; }
	inline void set_basePath_3(String_t* value)
	{
		___basePath_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___basePath_3), (void*)value);
	}

	inline static int32_t get_offset_of_path_4() { return static_cast<int32_t>(offsetof(U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7, ___path_4)); }
	inline String_t* get_path_4() const { return ___path_4; }
	inline String_t** get_address_of_path_4() { return &___path_4; }
	inline void set_path_4(String_t* value)
	{
		___path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuwrU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7, ___U3CuwrU3E5__2_5)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CuwrU3E5__2_5() const { return ___U3CuwrU3E5__2_5; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CuwrU3E5__2_5() { return &___U3CuwrU3E5__2_5; }
	inline void set_U3CuwrU3E5__2_5(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CuwrU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuwrU3E5__2_5), (void*)value);
	}
};


// AsImpL.LoaderObj/<LoadMaterialLibrary>d__4
struct  U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AsImpL.LoaderObj AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::<>4__this
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * ___U3CU3E4__this_2;
	// System.String AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::absolutePath
	String_t* ___absolutePath_3;
	// System.String AsImpL.LoaderObj/<LoadMaterialLibrary>d__4::<basePath>5__2
	String_t* ___U3CbasePathU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F, ___U3CU3E4__this_2)); }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_absolutePath_3() { return static_cast<int32_t>(offsetof(U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F, ___absolutePath_3)); }
	inline String_t* get_absolutePath_3() const { return ___absolutePath_3; }
	inline String_t** get_address_of_absolutePath_3() { return &___absolutePath_3; }
	inline void set_absolutePath_3(String_t* value)
	{
		___absolutePath_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___absolutePath_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbasePathU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F, ___U3CbasePathU3E5__2_4)); }
	inline String_t* get_U3CbasePathU3E5__2_4() const { return ___U3CbasePathU3E5__2_4; }
	inline String_t** get_address_of_U3CbasePathU3E5__2_4() { return &___U3CbasePathU3E5__2_4; }
	inline void set_U3CbasePathU3E5__2_4(String_t* value)
	{
		___U3CbasePathU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbasePathU3E5__2_4), (void*)value);
	}
};


// AsImpL.LoaderObj/<LoadModelFile>d__3
struct  U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.LoaderObj/<LoadModelFile>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AsImpL.LoaderObj/<LoadModelFile>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String AsImpL.LoaderObj/<LoadModelFile>d__3::absolutePath
	String_t* ___absolutePath_2;
	// AsImpL.LoaderObj AsImpL.LoaderObj/<LoadModelFile>d__3::<>4__this
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_absolutePath_2() { return static_cast<int32_t>(offsetof(U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56, ___absolutePath_2)); }
	inline String_t* get_absolutePath_2() const { return ___absolutePath_2; }
	inline String_t** get_address_of_absolutePath_2() { return &___absolutePath_2; }
	inline void set_absolutePath_2(String_t* value)
	{
		___absolutePath_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___absolutePath_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56, ___U3CU3E4__this_3)); }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// AsImpL.LoaderObj/<LoadOrDownloadText>d__16
struct  U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.LoaderObj/<LoadOrDownloadText>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AsImpL.LoaderObj/<LoadOrDownloadText>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AsImpL.LoaderObj AsImpL.LoaderObj/<LoadOrDownloadText>d__16::<>4__this
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * ___U3CU3E4__this_2;
	// System.String AsImpL.LoaderObj/<LoadOrDownloadText>d__16::url
	String_t* ___url_3;
	// System.Boolean AsImpL.LoaderObj/<LoadOrDownloadText>d__16::notifyErrors
	bool ___notifyErrors_4;
	// UnityEngine.Networking.UnityWebRequest AsImpL.LoaderObj/<LoadOrDownloadText>d__16::<uwr>5__2
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CuwrU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF, ___U3CU3E4__this_2)); }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___url_3), (void*)value);
	}

	inline static int32_t get_offset_of_notifyErrors_4() { return static_cast<int32_t>(offsetof(U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF, ___notifyErrors_4)); }
	inline bool get_notifyErrors_4() const { return ___notifyErrors_4; }
	inline bool* get_address_of_notifyErrors_4() { return &___notifyErrors_4; }
	inline void set_notifyErrors_4(bool value)
	{
		___notifyErrors_4 = value;
	}

	inline static int32_t get_offset_of_U3CuwrU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF, ___U3CuwrU3E5__2_5)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CuwrU3E5__2_5() const { return ___U3CuwrU3E5__2_5; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CuwrU3E5__2_5() { return &___U3CuwrU3E5__2_5; }
	inline void set_U3CuwrU3E5__2_5(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CuwrU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuwrU3E5__2_5), (void*)value);
	}
};


// AsImpL.LoaderObj/<ParseGeometryData>d__8
struct  U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.LoaderObj/<ParseGeometryData>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AsImpL.LoaderObj/<ParseGeometryData>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String AsImpL.LoaderObj/<ParseGeometryData>d__8::objDataText
	String_t* ___objDataText_2;
	// AsImpL.LoaderObj AsImpL.LoaderObj/<ParseGeometryData>d__8::<>4__this
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * ___U3CU3E4__this_3;
	// System.String[] AsImpL.LoaderObj/<ParseGeometryData>d__8::<lines>5__2
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3ClinesU3E5__2_4;
	// System.Boolean AsImpL.LoaderObj/<ParseGeometryData>d__8::<isFirstInGroup>5__3
	bool ___U3CisFirstInGroupU3E5__3_5;
	// System.Boolean AsImpL.LoaderObj/<ParseGeometryData>d__8::<isFaceIndexPlus>5__4
	bool ___U3CisFaceIndexPlusU3E5__4_6;
	// System.Char[] AsImpL.LoaderObj/<ParseGeometryData>d__8::<separators>5__5
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___U3CseparatorsU3E5__5_7;
	// System.Int32 AsImpL.LoaderObj/<ParseGeometryData>d__8::<i>5__6
	int32_t ___U3CiU3E5__6_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_objDataText_2() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___objDataText_2)); }
	inline String_t* get_objDataText_2() const { return ___objDataText_2; }
	inline String_t** get_address_of_objDataText_2() { return &___objDataText_2; }
	inline void set_objDataText_2(String_t* value)
	{
		___objDataText_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objDataText_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3CU3E4__this_3)); }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClinesU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3ClinesU3E5__2_4)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3ClinesU3E5__2_4() const { return ___U3ClinesU3E5__2_4; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3ClinesU3E5__2_4() { return &___U3ClinesU3E5__2_4; }
	inline void set_U3ClinesU3E5__2_4(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3ClinesU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClinesU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisFirstInGroupU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3CisFirstInGroupU3E5__3_5)); }
	inline bool get_U3CisFirstInGroupU3E5__3_5() const { return ___U3CisFirstInGroupU3E5__3_5; }
	inline bool* get_address_of_U3CisFirstInGroupU3E5__3_5() { return &___U3CisFirstInGroupU3E5__3_5; }
	inline void set_U3CisFirstInGroupU3E5__3_5(bool value)
	{
		___U3CisFirstInGroupU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CisFaceIndexPlusU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3CisFaceIndexPlusU3E5__4_6)); }
	inline bool get_U3CisFaceIndexPlusU3E5__4_6() const { return ___U3CisFaceIndexPlusU3E5__4_6; }
	inline bool* get_address_of_U3CisFaceIndexPlusU3E5__4_6() { return &___U3CisFaceIndexPlusU3E5__4_6; }
	inline void set_U3CisFaceIndexPlusU3E5__4_6(bool value)
	{
		___U3CisFaceIndexPlusU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CseparatorsU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3CseparatorsU3E5__5_7)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_U3CseparatorsU3E5__5_7() const { return ___U3CseparatorsU3E5__5_7; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_U3CseparatorsU3E5__5_7() { return &___U3CseparatorsU3E5__5_7; }
	inline void set_U3CseparatorsU3E5__5_7(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___U3CseparatorsU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CseparatorsU3E5__5_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5, ___U3CiU3E5__6_8)); }
	inline int32_t get_U3CiU3E5__6_8() const { return ___U3CiU3E5__6_8; }
	inline int32_t* get_address_of_U3CiU3E5__6_8() { return &___U3CiU3E5__6_8; }
	inline void set_U3CiU3E5__6_8(int32_t value)
	{
		___U3CiU3E5__6_8 = value;
	}
};


// AsImpL.LoaderObj/BumpParamDef
struct  BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187  : public RuntimeObject
{
public:
	// System.String AsImpL.LoaderObj/BumpParamDef::optionName
	String_t* ___optionName_0;
	// System.String AsImpL.LoaderObj/BumpParamDef::valueType
	String_t* ___valueType_1;
	// System.Int32 AsImpL.LoaderObj/BumpParamDef::valueNumMin
	int32_t ___valueNumMin_2;
	// System.Int32 AsImpL.LoaderObj/BumpParamDef::valueNumMax
	int32_t ___valueNumMax_3;

public:
	inline static int32_t get_offset_of_optionName_0() { return static_cast<int32_t>(offsetof(BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187, ___optionName_0)); }
	inline String_t* get_optionName_0() const { return ___optionName_0; }
	inline String_t** get_address_of_optionName_0() { return &___optionName_0; }
	inline void set_optionName_0(String_t* value)
	{
		___optionName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___optionName_0), (void*)value);
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187, ___valueType_1)); }
	inline String_t* get_valueType_1() const { return ___valueType_1; }
	inline String_t** get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(String_t* value)
	{
		___valueType_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___valueType_1), (void*)value);
	}

	inline static int32_t get_offset_of_valueNumMin_2() { return static_cast<int32_t>(offsetof(BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187, ___valueNumMin_2)); }
	inline int32_t get_valueNumMin_2() const { return ___valueNumMin_2; }
	inline int32_t* get_address_of_valueNumMin_2() { return &___valueNumMin_2; }
	inline void set_valueNumMin_2(int32_t value)
	{
		___valueNumMin_2 = value;
	}

	inline static int32_t get_offset_of_valueNumMax_3() { return static_cast<int32_t>(offsetof(BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187, ___valueNumMax_3)); }
	inline int32_t get_valueNumMax_3() const { return ___valueNumMax_3; }
	inline int32_t* get_address_of_valueNumMax_3() { return &___valueNumMax_3; }
	inline void set_valueNumMax_3(int32_t value)
	{
		___valueNumMax_3 = value;
	}
};


// Dummiesman.OBJLoader/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Dummiesman.OBJObjectBuilder> Dummiesman.OBJLoader/<>c__DisplayClass7_0::builderDict
	Dictionary_2_t38706229FB7F5DD826CC94AD9B6EEB0049FEF20C * ___builderDict_0;
	// Dummiesman.OBJObjectBuilder Dummiesman.OBJLoader/<>c__DisplayClass7_0::currentBuilder
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA * ___currentBuilder_1;
	// Dummiesman.OBJLoader Dummiesman.OBJLoader/<>c__DisplayClass7_0::<>4__this
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_builderDict_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488, ___builderDict_0)); }
	inline Dictionary_2_t38706229FB7F5DD826CC94AD9B6EEB0049FEF20C * get_builderDict_0() const { return ___builderDict_0; }
	inline Dictionary_2_t38706229FB7F5DD826CC94AD9B6EEB0049FEF20C ** get_address_of_builderDict_0() { return &___builderDict_0; }
	inline void set_builderDict_0(Dictionary_2_t38706229FB7F5DD826CC94AD9B6EEB0049FEF20C * value)
	{
		___builderDict_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___builderDict_0), (void*)value);
	}

	inline static int32_t get_offset_of_currentBuilder_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488, ___currentBuilder_1)); }
	inline OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA * get_currentBuilder_1() const { return ___currentBuilder_1; }
	inline OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA ** get_address_of_currentBuilder_1() { return &___currentBuilder_1; }
	inline void set_currentBuilder_1(OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA * value)
	{
		___currentBuilder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentBuilder_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488, ___U3CU3E4__this_2)); }
	inline OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Dummiesman.OBJObjectBuilder/ObjLoopHash
struct  ObjLoopHash_t74EB66CEFCEC5238360135A50B1E80AC8EA8BAB5  : public RuntimeObject
{
public:
	// System.Int32 Dummiesman.OBJObjectBuilder/ObjLoopHash::vertexIndex
	int32_t ___vertexIndex_0;
	// System.Int32 Dummiesman.OBJObjectBuilder/ObjLoopHash::normalIndex
	int32_t ___normalIndex_1;
	// System.Int32 Dummiesman.OBJObjectBuilder/ObjLoopHash::uvIndex
	int32_t ___uvIndex_2;

public:
	inline static int32_t get_offset_of_vertexIndex_0() { return static_cast<int32_t>(offsetof(ObjLoopHash_t74EB66CEFCEC5238360135A50B1E80AC8EA8BAB5, ___vertexIndex_0)); }
	inline int32_t get_vertexIndex_0() const { return ___vertexIndex_0; }
	inline int32_t* get_address_of_vertexIndex_0() { return &___vertexIndex_0; }
	inline void set_vertexIndex_0(int32_t value)
	{
		___vertexIndex_0 = value;
	}

	inline static int32_t get_offset_of_normalIndex_1() { return static_cast<int32_t>(offsetof(ObjLoopHash_t74EB66CEFCEC5238360135A50B1E80AC8EA8BAB5, ___normalIndex_1)); }
	inline int32_t get_normalIndex_1() const { return ___normalIndex_1; }
	inline int32_t* get_address_of_normalIndex_1() { return &___normalIndex_1; }
	inline void set_normalIndex_1(int32_t value)
	{
		___normalIndex_1 = value;
	}

	inline static int32_t get_offset_of_uvIndex_2() { return static_cast<int32_t>(offsetof(ObjLoopHash_t74EB66CEFCEC5238360135A50B1E80AC8EA8BAB5, ___uvIndex_2)); }
	inline int32_t get_uvIndex_2() const { return ___uvIndex_2; }
	inline int32_t* get_address_of_uvIndex_2() { return &___uvIndex_2; }
	inline void set_uvIndex_2(int32_t value)
	{
		___uvIndex_2 = value;
	}
};


// AsImpL.ObjectBuilder/BuildStatus
struct  BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96  : public RuntimeObject
{
public:
	// System.Boolean AsImpL.ObjectBuilder/BuildStatus::newObject
	bool ___newObject_0;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::objCount
	int32_t ___objCount_1;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::subObjCount
	int32_t ___subObjCount_2;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::idxCount
	int32_t ___idxCount_3;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::grpIdx
	int32_t ___grpIdx_4;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::numGroups
	int32_t ___numGroups_5;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::grpFaceIdx
	int32_t ___grpFaceIdx_6;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::meshPartIdx
	int32_t ___meshPartIdx_7;
	// System.Int32 AsImpL.ObjectBuilder/BuildStatus::totFaceIdxCount
	int32_t ___totFaceIdxCount_8;
	// UnityEngine.GameObject AsImpL.ObjectBuilder/BuildStatus::currObjGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___currObjGameObject_9;
	// UnityEngine.GameObject AsImpL.ObjectBuilder/BuildStatus::subObjParent
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___subObjParent_10;

public:
	inline static int32_t get_offset_of_newObject_0() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___newObject_0)); }
	inline bool get_newObject_0() const { return ___newObject_0; }
	inline bool* get_address_of_newObject_0() { return &___newObject_0; }
	inline void set_newObject_0(bool value)
	{
		___newObject_0 = value;
	}

	inline static int32_t get_offset_of_objCount_1() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___objCount_1)); }
	inline int32_t get_objCount_1() const { return ___objCount_1; }
	inline int32_t* get_address_of_objCount_1() { return &___objCount_1; }
	inline void set_objCount_1(int32_t value)
	{
		___objCount_1 = value;
	}

	inline static int32_t get_offset_of_subObjCount_2() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___subObjCount_2)); }
	inline int32_t get_subObjCount_2() const { return ___subObjCount_2; }
	inline int32_t* get_address_of_subObjCount_2() { return &___subObjCount_2; }
	inline void set_subObjCount_2(int32_t value)
	{
		___subObjCount_2 = value;
	}

	inline static int32_t get_offset_of_idxCount_3() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___idxCount_3)); }
	inline int32_t get_idxCount_3() const { return ___idxCount_3; }
	inline int32_t* get_address_of_idxCount_3() { return &___idxCount_3; }
	inline void set_idxCount_3(int32_t value)
	{
		___idxCount_3 = value;
	}

	inline static int32_t get_offset_of_grpIdx_4() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___grpIdx_4)); }
	inline int32_t get_grpIdx_4() const { return ___grpIdx_4; }
	inline int32_t* get_address_of_grpIdx_4() { return &___grpIdx_4; }
	inline void set_grpIdx_4(int32_t value)
	{
		___grpIdx_4 = value;
	}

	inline static int32_t get_offset_of_numGroups_5() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___numGroups_5)); }
	inline int32_t get_numGroups_5() const { return ___numGroups_5; }
	inline int32_t* get_address_of_numGroups_5() { return &___numGroups_5; }
	inline void set_numGroups_5(int32_t value)
	{
		___numGroups_5 = value;
	}

	inline static int32_t get_offset_of_grpFaceIdx_6() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___grpFaceIdx_6)); }
	inline int32_t get_grpFaceIdx_6() const { return ___grpFaceIdx_6; }
	inline int32_t* get_address_of_grpFaceIdx_6() { return &___grpFaceIdx_6; }
	inline void set_grpFaceIdx_6(int32_t value)
	{
		___grpFaceIdx_6 = value;
	}

	inline static int32_t get_offset_of_meshPartIdx_7() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___meshPartIdx_7)); }
	inline int32_t get_meshPartIdx_7() const { return ___meshPartIdx_7; }
	inline int32_t* get_address_of_meshPartIdx_7() { return &___meshPartIdx_7; }
	inline void set_meshPartIdx_7(int32_t value)
	{
		___meshPartIdx_7 = value;
	}

	inline static int32_t get_offset_of_totFaceIdxCount_8() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___totFaceIdxCount_8)); }
	inline int32_t get_totFaceIdxCount_8() const { return ___totFaceIdxCount_8; }
	inline int32_t* get_address_of_totFaceIdxCount_8() { return &___totFaceIdxCount_8; }
	inline void set_totFaceIdxCount_8(int32_t value)
	{
		___totFaceIdxCount_8 = value;
	}

	inline static int32_t get_offset_of_currObjGameObject_9() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___currObjGameObject_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_currObjGameObject_9() const { return ___currObjGameObject_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_currObjGameObject_9() { return &___currObjGameObject_9; }
	inline void set_currObjGameObject_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___currObjGameObject_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currObjGameObject_9), (void*)value);
	}

	inline static int32_t get_offset_of_subObjParent_10() { return static_cast<int32_t>(offsetof(BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96, ___subObjParent_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_subObjParent_10() const { return ___subObjParent_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_subObjParent_10() { return &___subObjParent_10; }
	inline void set_subObjParent_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___subObjParent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___subObjParent_10), (void*)value);
	}
};


// AsImpL.ObjectBuilder/ProgressInfo
struct  ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.ObjectBuilder/ProgressInfo::materialsLoaded
	int32_t ___materialsLoaded_0;
	// System.Int32 AsImpL.ObjectBuilder/ProgressInfo::objectsLoaded
	int32_t ___objectsLoaded_1;
	// System.Int32 AsImpL.ObjectBuilder/ProgressInfo::groupsLoaded
	int32_t ___groupsLoaded_2;
	// System.Int32 AsImpL.ObjectBuilder/ProgressInfo::numGroups
	int32_t ___numGroups_3;

public:
	inline static int32_t get_offset_of_materialsLoaded_0() { return static_cast<int32_t>(offsetof(ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C, ___materialsLoaded_0)); }
	inline int32_t get_materialsLoaded_0() const { return ___materialsLoaded_0; }
	inline int32_t* get_address_of_materialsLoaded_0() { return &___materialsLoaded_0; }
	inline void set_materialsLoaded_0(int32_t value)
	{
		___materialsLoaded_0 = value;
	}

	inline static int32_t get_offset_of_objectsLoaded_1() { return static_cast<int32_t>(offsetof(ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C, ___objectsLoaded_1)); }
	inline int32_t get_objectsLoaded_1() const { return ___objectsLoaded_1; }
	inline int32_t* get_address_of_objectsLoaded_1() { return &___objectsLoaded_1; }
	inline void set_objectsLoaded_1(int32_t value)
	{
		___objectsLoaded_1 = value;
	}

	inline static int32_t get_offset_of_groupsLoaded_2() { return static_cast<int32_t>(offsetof(ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C, ___groupsLoaded_2)); }
	inline int32_t get_groupsLoaded_2() const { return ___groupsLoaded_2; }
	inline int32_t* get_address_of_groupsLoaded_2() { return &___groupsLoaded_2; }
	inline void set_groupsLoaded_2(int32_t value)
	{
		___groupsLoaded_2 = value;
	}

	inline static int32_t get_offset_of_numGroups_3() { return static_cast<int32_t>(offsetof(ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C, ___numGroups_3)); }
	inline int32_t get_numGroups_3() const { return ___numGroups_3; }
	inline int32_t* get_address_of_numGroups_3() { return &___numGroups_3; }
	inline void set_numGroups_3(int32_t value)
	{
		___numGroups_3 = value;
	}
};


// UnityEngine.EventSystems.PhysicsRaycaster/RaycastHitComparer
struct  RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877  : public RuntimeObject
{
public:

public:
};

struct RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877_StaticFields
{
public:
	// UnityEngine.EventSystems.PhysicsRaycaster/RaycastHitComparer UnityEngine.EventSystems.PhysicsRaycaster/RaycastHitComparer::instance
	RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877_StaticFields, ___instance_0)); }
	inline RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877 * get_instance_0() const { return ___instance_0; }
	inline RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_0), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerInputModule/MouseState
struct  MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState> UnityEngine.EventSystems.PointerInputModule/MouseState::m_TrackedButtons
	List_1_t75FFBEBE24171F12D0459DE4BA90E0FD3E22A60E * ___m_TrackedButtons_0;

public:
	inline static int32_t get_offset_of_m_TrackedButtons_0() { return static_cast<int32_t>(offsetof(MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1, ___m_TrackedButtons_0)); }
	inline List_1_t75FFBEBE24171F12D0459DE4BA90E0FD3E22A60E * get_m_TrackedButtons_0() const { return ___m_TrackedButtons_0; }
	inline List_1_t75FFBEBE24171F12D0459DE4BA90E0FD3E22A60E ** get_address_of_m_TrackedButtons_0() { return &___m_TrackedButtons_0; }
	inline void set_m_TrackedButtons_0(List_1_t75FFBEBE24171F12D0459DE4BA90E0FD3E22A60E * value)
	{
		___m_TrackedButtons_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedButtons_0), (void*)value);
	}
};


// AsImpL.TextureLoader/TgaHeader
struct  TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9  : public RuntimeObject
{
public:
	// System.Byte AsImpL.TextureLoader/TgaHeader::identSize
	uint8_t ___identSize_0;
	// System.Byte AsImpL.TextureLoader/TgaHeader::colorMapType
	uint8_t ___colorMapType_1;
	// System.Byte AsImpL.TextureLoader/TgaHeader::imageType
	uint8_t ___imageType_2;
	// System.UInt16 AsImpL.TextureLoader/TgaHeader::colorMapStart
	uint16_t ___colorMapStart_3;
	// System.UInt16 AsImpL.TextureLoader/TgaHeader::colorMapLength
	uint16_t ___colorMapLength_4;
	// System.Byte AsImpL.TextureLoader/TgaHeader::colorMapBits
	uint8_t ___colorMapBits_5;
	// System.UInt16 AsImpL.TextureLoader/TgaHeader::xStart
	uint16_t ___xStart_6;
	// System.UInt16 AsImpL.TextureLoader/TgaHeader::ySstart
	uint16_t ___ySstart_7;
	// System.UInt16 AsImpL.TextureLoader/TgaHeader::width
	uint16_t ___width_8;
	// System.UInt16 AsImpL.TextureLoader/TgaHeader::height
	uint16_t ___height_9;
	// System.Byte AsImpL.TextureLoader/TgaHeader::bits
	uint8_t ___bits_10;
	// System.Byte AsImpL.TextureLoader/TgaHeader::descriptor
	uint8_t ___descriptor_11;

public:
	inline static int32_t get_offset_of_identSize_0() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___identSize_0)); }
	inline uint8_t get_identSize_0() const { return ___identSize_0; }
	inline uint8_t* get_address_of_identSize_0() { return &___identSize_0; }
	inline void set_identSize_0(uint8_t value)
	{
		___identSize_0 = value;
	}

	inline static int32_t get_offset_of_colorMapType_1() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___colorMapType_1)); }
	inline uint8_t get_colorMapType_1() const { return ___colorMapType_1; }
	inline uint8_t* get_address_of_colorMapType_1() { return &___colorMapType_1; }
	inline void set_colorMapType_1(uint8_t value)
	{
		___colorMapType_1 = value;
	}

	inline static int32_t get_offset_of_imageType_2() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___imageType_2)); }
	inline uint8_t get_imageType_2() const { return ___imageType_2; }
	inline uint8_t* get_address_of_imageType_2() { return &___imageType_2; }
	inline void set_imageType_2(uint8_t value)
	{
		___imageType_2 = value;
	}

	inline static int32_t get_offset_of_colorMapStart_3() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___colorMapStart_3)); }
	inline uint16_t get_colorMapStart_3() const { return ___colorMapStart_3; }
	inline uint16_t* get_address_of_colorMapStart_3() { return &___colorMapStart_3; }
	inline void set_colorMapStart_3(uint16_t value)
	{
		___colorMapStart_3 = value;
	}

	inline static int32_t get_offset_of_colorMapLength_4() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___colorMapLength_4)); }
	inline uint16_t get_colorMapLength_4() const { return ___colorMapLength_4; }
	inline uint16_t* get_address_of_colorMapLength_4() { return &___colorMapLength_4; }
	inline void set_colorMapLength_4(uint16_t value)
	{
		___colorMapLength_4 = value;
	}

	inline static int32_t get_offset_of_colorMapBits_5() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___colorMapBits_5)); }
	inline uint8_t get_colorMapBits_5() const { return ___colorMapBits_5; }
	inline uint8_t* get_address_of_colorMapBits_5() { return &___colorMapBits_5; }
	inline void set_colorMapBits_5(uint8_t value)
	{
		___colorMapBits_5 = value;
	}

	inline static int32_t get_offset_of_xStart_6() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___xStart_6)); }
	inline uint16_t get_xStart_6() const { return ___xStart_6; }
	inline uint16_t* get_address_of_xStart_6() { return &___xStart_6; }
	inline void set_xStart_6(uint16_t value)
	{
		___xStart_6 = value;
	}

	inline static int32_t get_offset_of_ySstart_7() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___ySstart_7)); }
	inline uint16_t get_ySstart_7() const { return ___ySstart_7; }
	inline uint16_t* get_address_of_ySstart_7() { return &___ySstart_7; }
	inline void set_ySstart_7(uint16_t value)
	{
		___ySstart_7 = value;
	}

	inline static int32_t get_offset_of_width_8() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___width_8)); }
	inline uint16_t get_width_8() const { return ___width_8; }
	inline uint16_t* get_address_of_width_8() { return &___width_8; }
	inline void set_width_8(uint16_t value)
	{
		___width_8 = value;
	}

	inline static int32_t get_offset_of_height_9() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___height_9)); }
	inline uint16_t get_height_9() const { return ___height_9; }
	inline uint16_t* get_address_of_height_9() { return &___height_9; }
	inline void set_height_9(uint16_t value)
	{
		___height_9 = value;
	}

	inline static int32_t get_offset_of_bits_10() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___bits_10)); }
	inline uint8_t get_bits_10() const { return ___bits_10; }
	inline uint8_t* get_address_of_bits_10() { return &___bits_10; }
	inline void set_bits_10(uint8_t value)
	{
		___bits_10 = value;
	}

	inline static int32_t get_offset_of_descriptor_11() { return static_cast<int32_t>(offsetof(TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9, ___descriptor_11)); }
	inline uint8_t get_descriptor_11() const { return ___descriptor_11; }
	inline uint8_t* get_address_of_descriptor_11() { return &___descriptor_11; }
	inline void set_descriptor_11(uint8_t value)
	{
		___descriptor_11 = value;
	}
};


// UnityEngine.UI.ToggleGroup/<>c
struct  U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961_StaticFields
{
public:
	// UnityEngine.UI.ToggleGroup/<>c UnityEngine.UI.ToggleGroup/<>c::<>9
	U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961 * ___U3CU3E9_0;
	// System.Predicate`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup/<>c::<>9__13_0
	Predicate_1_t0AABBBAF16CED490518BA49ED7BC02D9A9475166 * ___U3CU3E9__13_0_1;
	// System.Func`2<UnityEngine.UI.Toggle,System.Boolean> UnityEngine.UI.ToggleGroup/<>c::<>9__14_0
	Func_2_tCE3CE3D7F67C20FF5576ED2A6E74518A0756E2DE * ___U3CU3E9__14_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961_StaticFields, ___U3CU3E9__13_0_1)); }
	inline Predicate_1_t0AABBBAF16CED490518BA49ED7BC02D9A9475166 * get_U3CU3E9__13_0_1() const { return ___U3CU3E9__13_0_1; }
	inline Predicate_1_t0AABBBAF16CED490518BA49ED7BC02D9A9475166 ** get_address_of_U3CU3E9__13_0_1() { return &___U3CU3E9__13_0_1; }
	inline void set_U3CU3E9__13_0_1(Predicate_1_t0AABBBAF16CED490518BA49ED7BC02D9A9475166 * value)
	{
		___U3CU3E9__13_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__13_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961_StaticFields, ___U3CU3E9__14_0_2)); }
	inline Func_2_tCE3CE3D7F67C20FF5576ED2A6E74518A0756E2DE * get_U3CU3E9__14_0_2() const { return ___U3CU3E9__14_0_2; }
	inline Func_2_tCE3CE3D7F67C20FF5576ED2A6E74518A0756E2DE ** get_address_of_U3CU3E9__14_0_2() { return &___U3CU3E9__14_0_2; }
	inline void set_U3CU3E9__14_0_2(Func_2_tCE3CE3D7F67C20FF5576ED2A6E74518A0756E2DE * value)
	{
		___U3CU3E9__14_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_0_2), (void*)value);
	}
};


// mainMenu/<>c
struct  U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73_StaticFields
{
public:
	// mainMenu/<>c mainMenu/<>c::<>9
	U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction mainMenu/<>c::<>9__6_3
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__6_3_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_3_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73_StaticFields, ___U3CU3E9__6_3_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__6_3_1() const { return ___U3CU3E9__6_3_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__6_3_1() { return &___U3CU3E9__6_3_1; }
	inline void set_U3CU3E9__6_3_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__6_3_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_3_1), (void*)value);
	}
};


// mainMenu/<ShowLoadDialogCoroutine>d__13
struct  U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50  : public RuntimeObject
{
public:
	// System.Int32 mainMenu/<ShowLoadDialogCoroutine>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object mainMenu/<ShowLoadDialogCoroutine>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean mainMenu/<ShowLoadDialogCoroutine>d__13::input
	bool ___input_2;
	// mainMenu mainMenu/<ShowLoadDialogCoroutine>d__13::<>4__this
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_input_2() { return static_cast<int32_t>(offsetof(U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50, ___input_2)); }
	inline bool get_input_2() const { return ___input_2; }
	inline bool* get_address_of_input_2() { return &___input_2; }
	inline void set_input_2(bool value)
	{
		___input_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50, ___U3CU3E4__this_3)); }
	inline mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<AsImpL.MaterialData>
struct  Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2, ___list_0)); }
	inline List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * get_list_0() const { return ___list_0; }
	inline List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2, ___current_3)); }
	inline MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 * get_current_3() const { return ___current_3; }
	inline MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityEvent_1_t5CD4A65E59B117C339B96E838E5F127A989C5428  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t5CD4A65E59B117C339B96E838E5F127A989C5428, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t10C429A2DAF73A4517568E494115F7503F9E17EB  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t10C429A2DAF73A4517568E494115F7503F9E17EB, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t1238B72D437B572D32DDC7E67B423C2E90691350  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1238B72D437B572D32DDC7E67B423C2E90691350, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// B83.Image.BMP.BMPFileHeader
struct  BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0 
{
public:
	// System.UInt16 B83.Image.BMP.BMPFileHeader::magic
	uint16_t ___magic_0;
	// System.UInt32 B83.Image.BMP.BMPFileHeader::filesize
	uint32_t ___filesize_1;
	// System.UInt32 B83.Image.BMP.BMPFileHeader::reserved
	uint32_t ___reserved_2;
	// System.UInt32 B83.Image.BMP.BMPFileHeader::offset
	uint32_t ___offset_3;

public:
	inline static int32_t get_offset_of_magic_0() { return static_cast<int32_t>(offsetof(BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0, ___magic_0)); }
	inline uint16_t get_magic_0() const { return ___magic_0; }
	inline uint16_t* get_address_of_magic_0() { return &___magic_0; }
	inline void set_magic_0(uint16_t value)
	{
		___magic_0 = value;
	}

	inline static int32_t get_offset_of_filesize_1() { return static_cast<int32_t>(offsetof(BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0, ___filesize_1)); }
	inline uint32_t get_filesize_1() const { return ___filesize_1; }
	inline uint32_t* get_address_of_filesize_1() { return &___filesize_1; }
	inline void set_filesize_1(uint32_t value)
	{
		___filesize_1 = value;
	}

	inline static int32_t get_offset_of_reserved_2() { return static_cast<int32_t>(offsetof(BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0, ___reserved_2)); }
	inline uint32_t get_reserved_2() const { return ___reserved_2; }
	inline uint32_t* get_address_of_reserved_2() { return &___reserved_2; }
	inline void set_reserved_2(uint32_t value)
	{
		___reserved_2 = value;
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0, ___offset_3)); }
	inline uint32_t get_offset_3() const { return ___offset_3; }
	inline uint32_t* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(uint32_t value)
	{
		___offset_3 = value;
	}
};


// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E  : public AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E, ___m_EventSystem_1)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_1), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.UI.CoroutineTween.FloatTween
struct  FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228 
{
public:
	// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback UnityEngine.UI.CoroutineTween.FloatTween::m_Target
	FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23 * ___m_Target_0;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228, ___m_Target_0)); }
	inline FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228_marshaled_pinvoke
{
	FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228_marshaled_com
{
	FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct  LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WWW
struct  WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2, ____uwr_0)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____uwr_0), (void*)value);
	}
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t7F7209CE80E982A37AD0FED34F45A96EFE184746 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t7F7209CE80E982A37AD0FED34F45A96EFE184746__padding[12];
	};

public:
};


// AsImpL.DataSet/FaceIndices
struct  FaceIndices_t20F9E271361AC1CEC1AE901CF52E9AA0595209D5 
{
public:
	// System.Int32 AsImpL.DataSet/FaceIndices::vertIdx
	int32_t ___vertIdx_0;
	// System.Int32 AsImpL.DataSet/FaceIndices::uvIdx
	int32_t ___uvIdx_1;
	// System.Int32 AsImpL.DataSet/FaceIndices::normIdx
	int32_t ___normIdx_2;

public:
	inline static int32_t get_offset_of_vertIdx_0() { return static_cast<int32_t>(offsetof(FaceIndices_t20F9E271361AC1CEC1AE901CF52E9AA0595209D5, ___vertIdx_0)); }
	inline int32_t get_vertIdx_0() const { return ___vertIdx_0; }
	inline int32_t* get_address_of_vertIdx_0() { return &___vertIdx_0; }
	inline void set_vertIdx_0(int32_t value)
	{
		___vertIdx_0 = value;
	}

	inline static int32_t get_offset_of_uvIdx_1() { return static_cast<int32_t>(offsetof(FaceIndices_t20F9E271361AC1CEC1AE901CF52E9AA0595209D5, ___uvIdx_1)); }
	inline int32_t get_uvIdx_1() const { return ___uvIdx_1; }
	inline int32_t* get_address_of_uvIdx_1() { return &___uvIdx_1; }
	inline void set_uvIdx_1(int32_t value)
	{
		___uvIdx_1 = value;
	}

	inline static int32_t get_offset_of_normIdx_2() { return static_cast<int32_t>(offsetof(FaceIndices_t20F9E271361AC1CEC1AE901CF52E9AA0595209D5, ___normIdx_2)); }
	inline int32_t get_normIdx_2() const { return ___normIdx_2; }
	inline int32_t* get_address_of_normIdx_2() { return &___normIdx_2; }
	inline void set_normIdx_2(int32_t value)
	{
		___normIdx_2 = value;
	}
};


// SimpleFileBrowser.FileBrowser/FiletypeIcon
struct  FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A 
{
public:
	// System.String SimpleFileBrowser.FileBrowser/FiletypeIcon::extension
	String_t* ___extension_0;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser/FiletypeIcon::icon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_1;

public:
	inline static int32_t get_offset_of_extension_0() { return static_cast<int32_t>(offsetof(FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A, ___extension_0)); }
	inline String_t* get_extension_0() const { return ___extension_0; }
	inline String_t** get_address_of_extension_0() { return &___extension_0; }
	inline void set_extension_0(String_t* value)
	{
		___extension_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extension_0), (void*)value);
	}

	inline static int32_t get_offset_of_icon_1() { return static_cast<int32_t>(offsetof(FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A, ___icon_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_icon_1() const { return ___icon_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_icon_1() { return &___icon_1; }
	inline void set_icon_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___icon_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of SimpleFileBrowser.FileBrowser/FiletypeIcon
struct FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_pinvoke
{
	char* ___extension_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_1;
};
// Native definition for COM marshalling of SimpleFileBrowser.FileBrowser/FiletypeIcon
struct FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_com
{
	Il2CppChar* ___extension_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_1;
};

// AsImpL.Loader/BuildStats
struct  BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E 
{
public:
	// System.Single AsImpL.Loader/BuildStats::texturesTime
	float ___texturesTime_0;
	// System.Single AsImpL.Loader/BuildStats::materialsTime
	float ___materialsTime_1;
	// System.Single AsImpL.Loader/BuildStats::objectsTime
	float ___objectsTime_2;

public:
	inline static int32_t get_offset_of_texturesTime_0() { return static_cast<int32_t>(offsetof(BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E, ___texturesTime_0)); }
	inline float get_texturesTime_0() const { return ___texturesTime_0; }
	inline float* get_address_of_texturesTime_0() { return &___texturesTime_0; }
	inline void set_texturesTime_0(float value)
	{
		___texturesTime_0 = value;
	}

	inline static int32_t get_offset_of_materialsTime_1() { return static_cast<int32_t>(offsetof(BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E, ___materialsTime_1)); }
	inline float get_materialsTime_1() const { return ___materialsTime_1; }
	inline float* get_address_of_materialsTime_1() { return &___materialsTime_1; }
	inline void set_materialsTime_1(float value)
	{
		___materialsTime_1 = value;
	}

	inline static int32_t get_offset_of_objectsTime_2() { return static_cast<int32_t>(offsetof(BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E, ___objectsTime_2)); }
	inline float get_objectsTime_2() const { return ___objectsTime_2; }
	inline float* get_address_of_objectsTime_2() { return &___objectsTime_2; }
	inline void set_objectsTime_2(float value)
	{
		___objectsTime_2 = value;
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tA4B8E3F98E3B6A41218937C44898DCEE20629F8F  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tA4B8E3F98E3B6A41218937C44898DCEE20629F8F_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::1C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB
	__StaticArrayInitTypeSizeU3D12_t7F7209CE80E982A37AD0FED34F45A96EFE184746  ___1C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0;

public:
	inline static int32_t get_offset_of_U31C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tA4B8E3F98E3B6A41218937C44898DCEE20629F8F_StaticFields, ___1C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t7F7209CE80E982A37AD0FED34F45A96EFE184746  get_U31C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0() const { return ___1C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0; }
	inline __StaticArrayInitTypeSizeU3D12_t7F7209CE80E982A37AD0FED34F45A96EFE184746 * get_address_of_U31C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0() { return &___1C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0; }
	inline void set_U31C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0(__StaticArrayInitTypeSizeU3D12_t7F7209CE80E982A37AD0FED34F45A96EFE184746  value)
	{
		___1C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0 = value;
	}
};


// UnityEngine.AsyncOperation
struct  AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_completeCallback_1)); }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_completeCallback_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// B83.Image.BMP.BMPComressionMode
struct  BMPComressionMode_tDC4995809AD3E520B5B9F36D4452FB1623679454 
{
public:
	// System.Int32 B83.Image.BMP.BMPComressionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BMPComressionMode_tDC4995809AD3E520B5B9F36D4452FB1623679454, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.CertificateHandler
struct  CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Rendering.ColorWriteMask
struct  ColorWriteMask_t3FA3CB36396FDF33FC5192A387BC3E75232299C0 
{
public:
	// System.Int32 UnityEngine.Rendering.ColorWriteMask::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorWriteMask_t3FA3CB36396FDF33FC5192A387BC3E75232299C0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.CompareFunction
struct  CompareFunction_tBF5493E8F362C82B59254A3737D21710E0B70075 
{
public:
	// System.Int32 UnityEngine.Rendering.CompareFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompareFunction_tBF5493E8F362C82B59254A3737D21710E0B70075, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_t2A81C886C0708BC766E39686BBB54121A310F554 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventHandle_t2A81C886C0708BC766E39686BBB54121A310F554, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.EventTriggerType
struct  EventTriggerType_tED9176836ED486B7FEE926108C027C4E2954B9CE 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventTriggerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventTriggerType_tED9176836ED486B7FEE926108C027C4E2954B9CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.FileAttributes
struct  FileAttributes_t47DBB9A73CF80C7CA21C9AAB8D5336C92D32C1AE 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAttributes_t47DBB9A73CF80C7CA21C9AAB8D5336C92D32C1AE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AsImpL.ImportOptions
struct  ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F  : public RuntimeObject
{
public:
	// System.Boolean AsImpL.ImportOptions::zUp
	bool ___zUp_0;
	// System.Boolean AsImpL.ImportOptions::litDiffuse
	bool ___litDiffuse_1;
	// System.Boolean AsImpL.ImportOptions::convertToDoubleSided
	bool ___convertToDoubleSided_2;
	// System.Single AsImpL.ImportOptions::modelScaling
	float ___modelScaling_3;
	// System.Boolean AsImpL.ImportOptions::reuseLoaded
	bool ___reuseLoaded_4;
	// System.Boolean AsImpL.ImportOptions::inheritLayer
	bool ___inheritLayer_5;
	// System.Boolean AsImpL.ImportOptions::buildColliders
	bool ___buildColliders_6;
	// System.Boolean AsImpL.ImportOptions::colliderConvex
	bool ___colliderConvex_7;
	// System.Boolean AsImpL.ImportOptions::colliderTrigger
	bool ___colliderTrigger_8;
	// System.Boolean AsImpL.ImportOptions::use32bitIndices
	bool ___use32bitIndices_9;
	// System.Boolean AsImpL.ImportOptions::hideWhileLoading
	bool ___hideWhileLoading_10;
	// UnityEngine.Vector3 AsImpL.ImportOptions::localPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___localPosition_11;
	// UnityEngine.Vector3 AsImpL.ImportOptions::localEulerAngles
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___localEulerAngles_12;
	// UnityEngine.Vector3 AsImpL.ImportOptions::localScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___localScale_13;

public:
	inline static int32_t get_offset_of_zUp_0() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___zUp_0)); }
	inline bool get_zUp_0() const { return ___zUp_0; }
	inline bool* get_address_of_zUp_0() { return &___zUp_0; }
	inline void set_zUp_0(bool value)
	{
		___zUp_0 = value;
	}

	inline static int32_t get_offset_of_litDiffuse_1() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___litDiffuse_1)); }
	inline bool get_litDiffuse_1() const { return ___litDiffuse_1; }
	inline bool* get_address_of_litDiffuse_1() { return &___litDiffuse_1; }
	inline void set_litDiffuse_1(bool value)
	{
		___litDiffuse_1 = value;
	}

	inline static int32_t get_offset_of_convertToDoubleSided_2() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___convertToDoubleSided_2)); }
	inline bool get_convertToDoubleSided_2() const { return ___convertToDoubleSided_2; }
	inline bool* get_address_of_convertToDoubleSided_2() { return &___convertToDoubleSided_2; }
	inline void set_convertToDoubleSided_2(bool value)
	{
		___convertToDoubleSided_2 = value;
	}

	inline static int32_t get_offset_of_modelScaling_3() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___modelScaling_3)); }
	inline float get_modelScaling_3() const { return ___modelScaling_3; }
	inline float* get_address_of_modelScaling_3() { return &___modelScaling_3; }
	inline void set_modelScaling_3(float value)
	{
		___modelScaling_3 = value;
	}

	inline static int32_t get_offset_of_reuseLoaded_4() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___reuseLoaded_4)); }
	inline bool get_reuseLoaded_4() const { return ___reuseLoaded_4; }
	inline bool* get_address_of_reuseLoaded_4() { return &___reuseLoaded_4; }
	inline void set_reuseLoaded_4(bool value)
	{
		___reuseLoaded_4 = value;
	}

	inline static int32_t get_offset_of_inheritLayer_5() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___inheritLayer_5)); }
	inline bool get_inheritLayer_5() const { return ___inheritLayer_5; }
	inline bool* get_address_of_inheritLayer_5() { return &___inheritLayer_5; }
	inline void set_inheritLayer_5(bool value)
	{
		___inheritLayer_5 = value;
	}

	inline static int32_t get_offset_of_buildColliders_6() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___buildColliders_6)); }
	inline bool get_buildColliders_6() const { return ___buildColliders_6; }
	inline bool* get_address_of_buildColliders_6() { return &___buildColliders_6; }
	inline void set_buildColliders_6(bool value)
	{
		___buildColliders_6 = value;
	}

	inline static int32_t get_offset_of_colliderConvex_7() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___colliderConvex_7)); }
	inline bool get_colliderConvex_7() const { return ___colliderConvex_7; }
	inline bool* get_address_of_colliderConvex_7() { return &___colliderConvex_7; }
	inline void set_colliderConvex_7(bool value)
	{
		___colliderConvex_7 = value;
	}

	inline static int32_t get_offset_of_colliderTrigger_8() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___colliderTrigger_8)); }
	inline bool get_colliderTrigger_8() const { return ___colliderTrigger_8; }
	inline bool* get_address_of_colliderTrigger_8() { return &___colliderTrigger_8; }
	inline void set_colliderTrigger_8(bool value)
	{
		___colliderTrigger_8 = value;
	}

	inline static int32_t get_offset_of_use32bitIndices_9() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___use32bitIndices_9)); }
	inline bool get_use32bitIndices_9() const { return ___use32bitIndices_9; }
	inline bool* get_address_of_use32bitIndices_9() { return &___use32bitIndices_9; }
	inline void set_use32bitIndices_9(bool value)
	{
		___use32bitIndices_9 = value;
	}

	inline static int32_t get_offset_of_hideWhileLoading_10() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___hideWhileLoading_10)); }
	inline bool get_hideWhileLoading_10() const { return ___hideWhileLoading_10; }
	inline bool* get_address_of_hideWhileLoading_10() { return &___hideWhileLoading_10; }
	inline void set_hideWhileLoading_10(bool value)
	{
		___hideWhileLoading_10 = value;
	}

	inline static int32_t get_offset_of_localPosition_11() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___localPosition_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_localPosition_11() const { return ___localPosition_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_localPosition_11() { return &___localPosition_11; }
	inline void set_localPosition_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___localPosition_11 = value;
	}

	inline static int32_t get_offset_of_localEulerAngles_12() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___localEulerAngles_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_localEulerAngles_12() const { return ___localEulerAngles_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_localEulerAngles_12() { return &___localEulerAngles_12; }
	inline void set_localEulerAngles_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___localEulerAngles_12 = value;
	}

	inline static int32_t get_offset_of_localScale_13() { return static_cast<int32_t>(offsetof(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F, ___localScale_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_localScale_13() const { return ___localScale_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_localScale_13() { return &___localScale_13; }
	inline void set_localScale_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___localScale_13 = value;
	}
};


// AsImpL.MaterialData
struct  MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48  : public RuntimeObject
{
public:
	// System.String AsImpL.MaterialData::materialName
	String_t* ___materialName_0;
	// UnityEngine.Color AsImpL.MaterialData::ambientColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___ambientColor_1;
	// UnityEngine.Color AsImpL.MaterialData::diffuseColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___diffuseColor_2;
	// UnityEngine.Color AsImpL.MaterialData::specularColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___specularColor_3;
	// UnityEngine.Color AsImpL.MaterialData::emissiveColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___emissiveColor_4;
	// System.Single AsImpL.MaterialData::shininess
	float ___shininess_5;
	// System.Single AsImpL.MaterialData::overallAlpha
	float ___overallAlpha_6;
	// System.Int32 AsImpL.MaterialData::illumType
	int32_t ___illumType_7;
	// System.Boolean AsImpL.MaterialData::hasReflectionTex
	bool ___hasReflectionTex_8;
	// System.String AsImpL.MaterialData::diffuseTexPath
	String_t* ___diffuseTexPath_9;
	// UnityEngine.Texture2D AsImpL.MaterialData::diffuseTex
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___diffuseTex_10;
	// System.String AsImpL.MaterialData::bumpTexPath
	String_t* ___bumpTexPath_11;
	// UnityEngine.Texture2D AsImpL.MaterialData::bumpTex
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___bumpTex_12;
	// System.String AsImpL.MaterialData::specularTexPath
	String_t* ___specularTexPath_13;
	// UnityEngine.Texture2D AsImpL.MaterialData::specularTex
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___specularTex_14;
	// System.String AsImpL.MaterialData::opacityTexPath
	String_t* ___opacityTexPath_15;
	// UnityEngine.Texture2D AsImpL.MaterialData::opacityTex
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___opacityTex_16;

public:
	inline static int32_t get_offset_of_materialName_0() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___materialName_0)); }
	inline String_t* get_materialName_0() const { return ___materialName_0; }
	inline String_t** get_address_of_materialName_0() { return &___materialName_0; }
	inline void set_materialName_0(String_t* value)
	{
		___materialName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materialName_0), (void*)value);
	}

	inline static int32_t get_offset_of_ambientColor_1() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___ambientColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_ambientColor_1() const { return ___ambientColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_ambientColor_1() { return &___ambientColor_1; }
	inline void set_ambientColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___ambientColor_1 = value;
	}

	inline static int32_t get_offset_of_diffuseColor_2() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___diffuseColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_diffuseColor_2() const { return ___diffuseColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_diffuseColor_2() { return &___diffuseColor_2; }
	inline void set_diffuseColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___diffuseColor_2 = value;
	}

	inline static int32_t get_offset_of_specularColor_3() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___specularColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_specularColor_3() const { return ___specularColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_specularColor_3() { return &___specularColor_3; }
	inline void set_specularColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___specularColor_3 = value;
	}

	inline static int32_t get_offset_of_emissiveColor_4() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___emissiveColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_emissiveColor_4() const { return ___emissiveColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_emissiveColor_4() { return &___emissiveColor_4; }
	inline void set_emissiveColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___emissiveColor_4 = value;
	}

	inline static int32_t get_offset_of_shininess_5() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___shininess_5)); }
	inline float get_shininess_5() const { return ___shininess_5; }
	inline float* get_address_of_shininess_5() { return &___shininess_5; }
	inline void set_shininess_5(float value)
	{
		___shininess_5 = value;
	}

	inline static int32_t get_offset_of_overallAlpha_6() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___overallAlpha_6)); }
	inline float get_overallAlpha_6() const { return ___overallAlpha_6; }
	inline float* get_address_of_overallAlpha_6() { return &___overallAlpha_6; }
	inline void set_overallAlpha_6(float value)
	{
		___overallAlpha_6 = value;
	}

	inline static int32_t get_offset_of_illumType_7() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___illumType_7)); }
	inline int32_t get_illumType_7() const { return ___illumType_7; }
	inline int32_t* get_address_of_illumType_7() { return &___illumType_7; }
	inline void set_illumType_7(int32_t value)
	{
		___illumType_7 = value;
	}

	inline static int32_t get_offset_of_hasReflectionTex_8() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___hasReflectionTex_8)); }
	inline bool get_hasReflectionTex_8() const { return ___hasReflectionTex_8; }
	inline bool* get_address_of_hasReflectionTex_8() { return &___hasReflectionTex_8; }
	inline void set_hasReflectionTex_8(bool value)
	{
		___hasReflectionTex_8 = value;
	}

	inline static int32_t get_offset_of_diffuseTexPath_9() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___diffuseTexPath_9)); }
	inline String_t* get_diffuseTexPath_9() const { return ___diffuseTexPath_9; }
	inline String_t** get_address_of_diffuseTexPath_9() { return &___diffuseTexPath_9; }
	inline void set_diffuseTexPath_9(String_t* value)
	{
		___diffuseTexPath_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___diffuseTexPath_9), (void*)value);
	}

	inline static int32_t get_offset_of_diffuseTex_10() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___diffuseTex_10)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_diffuseTex_10() const { return ___diffuseTex_10; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_diffuseTex_10() { return &___diffuseTex_10; }
	inline void set_diffuseTex_10(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___diffuseTex_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___diffuseTex_10), (void*)value);
	}

	inline static int32_t get_offset_of_bumpTexPath_11() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___bumpTexPath_11)); }
	inline String_t* get_bumpTexPath_11() const { return ___bumpTexPath_11; }
	inline String_t** get_address_of_bumpTexPath_11() { return &___bumpTexPath_11; }
	inline void set_bumpTexPath_11(String_t* value)
	{
		___bumpTexPath_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bumpTexPath_11), (void*)value);
	}

	inline static int32_t get_offset_of_bumpTex_12() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___bumpTex_12)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_bumpTex_12() const { return ___bumpTex_12; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_bumpTex_12() { return &___bumpTex_12; }
	inline void set_bumpTex_12(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___bumpTex_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bumpTex_12), (void*)value);
	}

	inline static int32_t get_offset_of_specularTexPath_13() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___specularTexPath_13)); }
	inline String_t* get_specularTexPath_13() const { return ___specularTexPath_13; }
	inline String_t** get_address_of_specularTexPath_13() { return &___specularTexPath_13; }
	inline void set_specularTexPath_13(String_t* value)
	{
		___specularTexPath_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___specularTexPath_13), (void*)value);
	}

	inline static int32_t get_offset_of_specularTex_14() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___specularTex_14)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_specularTex_14() const { return ___specularTex_14; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_specularTex_14() { return &___specularTex_14; }
	inline void set_specularTex_14(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___specularTex_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___specularTex_14), (void*)value);
	}

	inline static int32_t get_offset_of_opacityTexPath_15() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___opacityTexPath_15)); }
	inline String_t* get_opacityTexPath_15() const { return ___opacityTexPath_15; }
	inline String_t** get_address_of_opacityTexPath_15() { return &___opacityTexPath_15; }
	inline void set_opacityTexPath_15(String_t* value)
	{
		___opacityTexPath_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___opacityTexPath_15), (void*)value);
	}

	inline static int32_t get_offset_of_opacityTex_16() { return static_cast<int32_t>(offsetof(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48, ___opacityTex_16)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_opacityTex_16() const { return ___opacityTex_16; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_opacityTex_16() { return &___opacityTex_16; }
	inline void set_opacityTex_16(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___opacityTex_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___opacityTex_16), (void*)value);
	}
};


// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t740623362F85DF2963BE20C702F7B8EF44E91645 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MoveDirection_t740623362F85DF2963BE20C702F7B8EF44E91645, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Ray
struct  Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Origin_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Direction_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct  RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Centroid_0)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Point_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Normal_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::displayIndex
	int32_t ___displayIndex_10;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___m_GameObject_0)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GameObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___module_1)); }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___module_1), (void*)value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldPosition_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldNormal_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___screenPosition_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___screenPosition_9 = value;
	}

	inline static int32_t get_offset_of_displayIndex_10() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___displayIndex_10)); }
	inline int32_t get_displayIndex_10() const { return ___displayIndex_10; }
	inline int32_t* get_address_of_displayIndex_10() { return &___displayIndex_10; }
	inline void set_displayIndex_10(int32_t value)
	{
		___displayIndex_10 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};

// AsImpL.RootPathEnum
struct  RootPathEnum_tB17BAA3D8A73E632D635A0E33DA0F1FAEB5F1B7D 
{
public:
	// System.Int32 AsImpL.RootPathEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RootPathEnum_tB17BAA3D8A73E632D635A0E33DA0F1FAEB5F1B7D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Dummiesman.SplitMode
struct  SplitMode_t967BD1F9C357CCFC5E67C9DD56784DC2BBD707AE 
{
public:
	// System.Int32 Dummiesman.SplitMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SplitMode_t967BD1F9C357CCFC5E67C9DD56784DC2BBD707AE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.StencilOp
struct  StencilOp_t29403ED1B3D9A0953577E567FA3BF403E13FA6AD 
{
public:
	// System.Int32 UnityEngine.Rendering.StencilOp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StencilOp_t29403ED1B3D9A0953577E567FA3BF403E13FA6AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UploadHandler
struct  UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// AsImpL.MathUtil.Vertex
struct  Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA  : public RuntimeObject
{
public:
	// AsImpL.MathUtil.Vertex AsImpL.MathUtil.Vertex::prevVertex
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * ___prevVertex_0;
	// AsImpL.MathUtil.Vertex AsImpL.MathUtil.Vertex::nextVertex
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * ___nextVertex_1;
	// System.Single AsImpL.MathUtil.Vertex::triangleArea
	float ___triangleArea_2;
	// System.Boolean AsImpL.MathUtil.Vertex::triangleHasChanged
	bool ___triangleHasChanged_3;
	// UnityEngine.Vector3 AsImpL.MathUtil.Vertex::<Position>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionU3Ek__BackingField_4;
	// System.Int32 AsImpL.MathUtil.Vertex::<OriginalIndex>k__BackingField
	int32_t ___U3COriginalIndexU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_prevVertex_0() { return static_cast<int32_t>(offsetof(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA, ___prevVertex_0)); }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * get_prevVertex_0() const { return ___prevVertex_0; }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA ** get_address_of_prevVertex_0() { return &___prevVertex_0; }
	inline void set_prevVertex_0(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * value)
	{
		___prevVertex_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prevVertex_0), (void*)value);
	}

	inline static int32_t get_offset_of_nextVertex_1() { return static_cast<int32_t>(offsetof(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA, ___nextVertex_1)); }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * get_nextVertex_1() const { return ___nextVertex_1; }
	inline Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA ** get_address_of_nextVertex_1() { return &___nextVertex_1; }
	inline void set_nextVertex_1(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA * value)
	{
		___nextVertex_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nextVertex_1), (void*)value);
	}

	inline static int32_t get_offset_of_triangleArea_2() { return static_cast<int32_t>(offsetof(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA, ___triangleArea_2)); }
	inline float get_triangleArea_2() const { return ___triangleArea_2; }
	inline float* get_address_of_triangleArea_2() { return &___triangleArea_2; }
	inline void set_triangleArea_2(float value)
	{
		___triangleArea_2 = value;
	}

	inline static int32_t get_offset_of_triangleHasChanged_3() { return static_cast<int32_t>(offsetof(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA, ___triangleHasChanged_3)); }
	inline bool get_triangleHasChanged_3() const { return ___triangleHasChanged_3; }
	inline bool* get_address_of_triangleHasChanged_3() { return &___triangleHasChanged_3; }
	inline void set_triangleHasChanged_3(bool value)
	{
		___triangleHasChanged_3 = value;
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA, ___U3CPositionU3Ek__BackingField_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CPositionU3Ek__BackingField_4() const { return ___U3CPositionU3Ek__BackingField_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CPositionU3Ek__BackingField_4() { return &___U3CPositionU3Ek__BackingField_4; }
	inline void set_U3CPositionU3Ek__BackingField_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3COriginalIndexU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA, ___U3COriginalIndexU3Ek__BackingField_5)); }
	inline int32_t get_U3COriginalIndexU3Ek__BackingField_5() const { return ___U3COriginalIndexU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3COriginalIndexU3Ek__BackingField_5() { return &___U3COriginalIndexU3Ek__BackingField_5; }
	inline void set_U3COriginalIndexU3Ek__BackingField_5(int32_t value)
	{
		___U3COriginalIndexU3Ek__BackingField_5 = value;
	}
};


// UnityEngine.UI.VertexHelper
struct  VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Positions_0)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Colors_1)); }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv0S_2)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv0S_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv1S_3)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv1S_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv2S_4)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv2S_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv3S_5)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv3S_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Normals_6)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Tangents_7)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tangents_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Indices_8)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Indices_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___s_DefaultNormal_10 = value;
	}
};


// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct  ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026  : public UnityEvent_1_t1238B72D437B572D32DDC7E67B423C2E90691350
{
public:

public:
};


// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
struct  ColorTweenMode_tC8254CFED9F320A1B7A452159F60A143952DFE19 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorTweenMode_tC8254CFED9F320A1B7A452159F60A143952DFE19, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Environment/SpecialFolder
struct  SpecialFolder_t6103ABF21BDF31D4FF825E2761E4616153810B76 
{
public:
	// System.Int32 System.Environment/SpecialFolder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialFolder_t6103ABF21BDF31D4FF825E2761E4616153810B76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct  TriggerEvent_t6C4DB59340B55DE906C54EE3FF7DAE4DE24A1276  : public UnityEvent_1_t5CD4A65E59B117C339B96E838E5F127A989C5428
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/Permission
struct  Permission_t54F789471D32013B167C19F2CD6132E7E49298A2 
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/Permission::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Permission_t54F789471D32013B167C19F2CD6132E7E49298A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SimpleFileBrowser.FileBrowser/PickMode
struct  PickMode_tE2DF59B643089F6FE81412D1C152B52222B0414B 
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/PickMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PickMode_tE2DF59B643089F6FE81412D1C152B52222B0414B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct  FloatTweenCallback_t56E4D48C62B03C68A69708463C2CCF8E02BBFB23  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Dummiesman.ImageLoader/TextureFormat
struct  TextureFormat_tFF539B6329240C7555E512E6855810CE554BF351 
{
public:
	// System.Int32 Dummiesman.ImageLoader/TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tFF539B6329240C7555E512E6855810CE554BF351, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AsImpL.Loader/<Build>d__36
struct  U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9  : public RuntimeObject
{
public:
	// System.Int32 AsImpL.Loader/<Build>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AsImpL.Loader/<Build>d__36::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AsImpL.Loader AsImpL.Loader/<Build>d__36::<>4__this
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * ___U3CU3E4__this_2;
	// System.String AsImpL.Loader/<Build>d__36::absolutePath
	String_t* ___absolutePath_3;
	// System.String AsImpL.Loader/<Build>d__36::objName
	String_t* ___objName_4;
	// UnityEngine.Transform AsImpL.Loader/<Build>d__36::parentTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parentTransform_5;
	// System.Single AsImpL.Loader/<Build>d__36::<prevTime>5__2
	float ___U3CprevTimeU3E5__2_6;
	// AsImpL.ObjectBuilder/ProgressInfo AsImpL.Loader/<Build>d__36::<info>5__3
	ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C * ___U3CinfoU3E5__3_7;
	// System.Single AsImpL.Loader/<Build>d__36::<objInitPerc>5__4
	float ___U3CobjInitPercU3E5__4_8;
	// UnityEngine.GameObject AsImpL.Loader/<Build>d__36::<newObj>5__5
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CnewObjU3E5__5_9;
	// System.Single AsImpL.Loader/<Build>d__36::<initProgress>5__6
	float ___U3CinitProgressU3E5__6_10;
	// System.String AsImpL.Loader/<Build>d__36::<basePath>5__7
	String_t* ___U3CbasePathU3E5__7_11;
	// System.Int32 AsImpL.Loader/<Build>d__36::<count>5__8
	int32_t ___U3CcountU3E5__8_12;
	// System.Collections.Generic.List`1/Enumerator<AsImpL.MaterialData> AsImpL.Loader/<Build>d__36::<>7__wrap8
	Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2  ___U3CU3E7__wrap8_13;
	// AsImpL.MaterialData AsImpL.Loader/<Build>d__36::<mtl>5__10
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 * ___U3CmtlU3E5__10_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CU3E4__this_2)); }
	inline Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_absolutePath_3() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___absolutePath_3)); }
	inline String_t* get_absolutePath_3() const { return ___absolutePath_3; }
	inline String_t** get_address_of_absolutePath_3() { return &___absolutePath_3; }
	inline void set_absolutePath_3(String_t* value)
	{
		___absolutePath_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___absolutePath_3), (void*)value);
	}

	inline static int32_t get_offset_of_objName_4() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___objName_4)); }
	inline String_t* get_objName_4() const { return ___objName_4; }
	inline String_t** get_address_of_objName_4() { return &___objName_4; }
	inline void set_objName_4(String_t* value)
	{
		___objName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objName_4), (void*)value);
	}

	inline static int32_t get_offset_of_parentTransform_5() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___parentTransform_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_parentTransform_5() const { return ___parentTransform_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_parentTransform_5() { return &___parentTransform_5; }
	inline void set_parentTransform_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___parentTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parentTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CprevTimeU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CprevTimeU3E5__2_6)); }
	inline float get_U3CprevTimeU3E5__2_6() const { return ___U3CprevTimeU3E5__2_6; }
	inline float* get_address_of_U3CprevTimeU3E5__2_6() { return &___U3CprevTimeU3E5__2_6; }
	inline void set_U3CprevTimeU3E5__2_6(float value)
	{
		___U3CprevTimeU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CinfoU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CinfoU3E5__3_7)); }
	inline ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C * get_U3CinfoU3E5__3_7() const { return ___U3CinfoU3E5__3_7; }
	inline ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C ** get_address_of_U3CinfoU3E5__3_7() { return &___U3CinfoU3E5__3_7; }
	inline void set_U3CinfoU3E5__3_7(ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C * value)
	{
		___U3CinfoU3E5__3_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinfoU3E5__3_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CobjInitPercU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CobjInitPercU3E5__4_8)); }
	inline float get_U3CobjInitPercU3E5__4_8() const { return ___U3CobjInitPercU3E5__4_8; }
	inline float* get_address_of_U3CobjInitPercU3E5__4_8() { return &___U3CobjInitPercU3E5__4_8; }
	inline void set_U3CobjInitPercU3E5__4_8(float value)
	{
		___U3CobjInitPercU3E5__4_8 = value;
	}

	inline static int32_t get_offset_of_U3CnewObjU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CnewObjU3E5__5_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CnewObjU3E5__5_9() const { return ___U3CnewObjU3E5__5_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CnewObjU3E5__5_9() { return &___U3CnewObjU3E5__5_9; }
	inline void set_U3CnewObjU3E5__5_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CnewObjU3E5__5_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnewObjU3E5__5_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinitProgressU3E5__6_10() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CinitProgressU3E5__6_10)); }
	inline float get_U3CinitProgressU3E5__6_10() const { return ___U3CinitProgressU3E5__6_10; }
	inline float* get_address_of_U3CinitProgressU3E5__6_10() { return &___U3CinitProgressU3E5__6_10; }
	inline void set_U3CinitProgressU3E5__6_10(float value)
	{
		___U3CinitProgressU3E5__6_10 = value;
	}

	inline static int32_t get_offset_of_U3CbasePathU3E5__7_11() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CbasePathU3E5__7_11)); }
	inline String_t* get_U3CbasePathU3E5__7_11() const { return ___U3CbasePathU3E5__7_11; }
	inline String_t** get_address_of_U3CbasePathU3E5__7_11() { return &___U3CbasePathU3E5__7_11; }
	inline void set_U3CbasePathU3E5__7_11(String_t* value)
	{
		___U3CbasePathU3E5__7_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbasePathU3E5__7_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountU3E5__8_12() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CcountU3E5__8_12)); }
	inline int32_t get_U3CcountU3E5__8_12() const { return ___U3CcountU3E5__8_12; }
	inline int32_t* get_address_of_U3CcountU3E5__8_12() { return &___U3CcountU3E5__8_12; }
	inline void set_U3CcountU3E5__8_12(int32_t value)
	{
		___U3CcountU3E5__8_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap8_13() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CU3E7__wrap8_13)); }
	inline Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2  get_U3CU3E7__wrap8_13() const { return ___U3CU3E7__wrap8_13; }
	inline Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2 * get_address_of_U3CU3E7__wrap8_13() { return &___U3CU3E7__wrap8_13; }
	inline void set_U3CU3E7__wrap8_13(Enumerator_t9D9E3A1188532B9F04D54BAA1F9A838A54A769A2  value)
	{
		___U3CU3E7__wrap8_13 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap8_13))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap8_13))->___current_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CmtlU3E5__10_14() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9, ___U3CmtlU3E5__10_14)); }
	inline MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 * get_U3CmtlU3E5__10_14() const { return ___U3CmtlU3E5__10_14; }
	inline MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 ** get_address_of_U3CmtlU3E5__10_14() { return &___U3CmtlU3E5__10_14; }
	inline void set_U3CmtlU3E5__10_14(MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48 * value)
	{
		___U3CmtlU3E5__10_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmtlU3E5__10_14), (void*)value);
	}
};


// AsImpL.Loader/Stats
struct  Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F 
{
public:
	// System.Single AsImpL.Loader/Stats::modelParseTime
	float ___modelParseTime_0;
	// System.Single AsImpL.Loader/Stats::materialsParseTime
	float ___materialsParseTime_1;
	// System.Single AsImpL.Loader/Stats::buildTime
	float ___buildTime_2;
	// AsImpL.Loader/BuildStats AsImpL.Loader/Stats::buildStats
	BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E  ___buildStats_3;
	// System.Single AsImpL.Loader/Stats::totalTime
	float ___totalTime_4;

public:
	inline static int32_t get_offset_of_modelParseTime_0() { return static_cast<int32_t>(offsetof(Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F, ___modelParseTime_0)); }
	inline float get_modelParseTime_0() const { return ___modelParseTime_0; }
	inline float* get_address_of_modelParseTime_0() { return &___modelParseTime_0; }
	inline void set_modelParseTime_0(float value)
	{
		___modelParseTime_0 = value;
	}

	inline static int32_t get_offset_of_materialsParseTime_1() { return static_cast<int32_t>(offsetof(Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F, ___materialsParseTime_1)); }
	inline float get_materialsParseTime_1() const { return ___materialsParseTime_1; }
	inline float* get_address_of_materialsParseTime_1() { return &___materialsParseTime_1; }
	inline void set_materialsParseTime_1(float value)
	{
		___materialsParseTime_1 = value;
	}

	inline static int32_t get_offset_of_buildTime_2() { return static_cast<int32_t>(offsetof(Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F, ___buildTime_2)); }
	inline float get_buildTime_2() const { return ___buildTime_2; }
	inline float* get_address_of_buildTime_2() { return &___buildTime_2; }
	inline void set_buildTime_2(float value)
	{
		___buildTime_2 = value;
	}

	inline static int32_t get_offset_of_buildStats_3() { return static_cast<int32_t>(offsetof(Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F, ___buildStats_3)); }
	inline BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E  get_buildStats_3() const { return ___buildStats_3; }
	inline BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E * get_address_of_buildStats_3() { return &___buildStats_3; }
	inline void set_buildStats_3(BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E  value)
	{
		___buildStats_3 = value;
	}

	inline static int32_t get_offset_of_totalTime_4() { return static_cast<int32_t>(offsetof(Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F, ___totalTime_4)); }
	inline float get_totalTime_4() const { return ___totalTime_4; }
	inline float* get_address_of_totalTime_4() { return &___totalTime_4; }
	inline void set_totalTime_4(float value)
	{
		___totalTime_4 = value;
	}
};


// AsImpL.ModelUtil/MtlBlendMode
struct  MtlBlendMode_t5560844B5D0A9E82A0A09A9697CB795FDABDB025 
{
public:
	// System.Int32 AsImpL.ModelUtil/MtlBlendMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MtlBlendMode_t5560844B5D0A9E82A0A09A9697CB795FDABDB025, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct  Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AsImpL.ObjectImporter/ImportPhase
struct  ImportPhase_t7A13D25EDB4A12A4A7B35354D54AB9697FAF2225 
{
public:
	// System.Int32 AsImpL.ObjectImporter/ImportPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImportPhase_t7A13D25EDB4A12A4A7B35354D54AB9697FAF2225, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.PointerEventData/FramePressState
struct  FramePressState_t4BB461B7704D7F72519B36A0C8B3370AB302E7A7 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/FramePressState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FramePressState_t4BB461B7704D7F72519B36A0C8B3370AB302E7A7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Scrollbar/<ClickRepeat>d__58
struct  U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/<ClickRepeat>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>d__58::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.Scrollbar/<ClickRepeat>d__58::<>4__this
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * ___U3CU3E4__this_2;
	// UnityEngine.Vector2 UnityEngine.UI.Scrollbar/<ClickRepeat>d__58::screenPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_3;
	// UnityEngine.Camera UnityEngine.UI.Scrollbar/<ClickRepeat>d__58::camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE, ___U3CU3E4__this_2)); }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_screenPosition_3() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE, ___screenPosition_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_screenPosition_3() const { return ___screenPosition_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_screenPosition_3() { return &___screenPosition_3; }
	inline void set_screenPosition_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___screenPosition_3 = value;
	}

	inline static int32_t get_offset_of_camera_4() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE, ___camera_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_camera_4() const { return ___camera_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_camera_4() { return &___camera_4; }
	inline void set_camera_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___camera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___camera_4), (void*)value);
	}
};


// UnityEngine.UI.Scrollbar/Axis
struct  Axis_t561E10ABB080BB3C1F7C93C39E8DDD06BE6490B1 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t561E10ABB080BB3C1F7C93C39E8DDD06BE6490B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Scrollbar/Direction
struct  Direction_tCE7C4B78403A18007E901268411DB754E7B784B7 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tCE7C4B78403A18007E901268411DB754E7B784B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Scrollbar/ScrollEvent
struct  ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_tB421C4551CDC64C8EB31158E8C7FF118F46FF72F 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tB421C4551CDC64C8EB31158E8C7FF118F46FF72F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct  Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider/Axis
struct  Axis_t5BFF2AACB2D94E92243ED4EF295A1DCAF2FC52D5 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t5BFF2AACB2D94E92243ED4EF295A1DCAF2FC52D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider/Direction
struct  Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider/SliderEvent
struct  SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// UnityEngine.EventSystems.StandaloneInputModule/InputMode
struct  InputMode_tABD640D064CD823116744F702C9DD0836A7E8972 
{
public:
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule/InputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputMode_tABD640D064CD823116744F702C9DD0836A7E8972, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Toggle/ToggleEvent
struct  ToggleEvent_t7B9EFE80B7D7F16F3E7B8FA75FEF45B00E0C0075  : public UnityEvent_1_t10C429A2DAF73A4517568E494115F7503F9E17EB
{
public:

public:
};


// UnityEngine.UI.Toggle/ToggleTransition
struct  ToggleTransition_t4D1AA30F2BA24242EB9D1DD2E3DF839F0BAC5167 
{
public:
	// System.Int32 UnityEngine.UI.Toggle/ToggleTransition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleTransition_t4D1AA30F2BA24242EB9D1DD2E3DF839F0BAC5167, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest/Result
struct  Result_t3233C0F690EC3844C8E0C4649568659679AFBE75 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest/Result::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Result_t3233C0F690EC3844C8E0C4649568659679AFBE75, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest/UnityWebRequestError
struct  UnityWebRequestError_t01C779C192877A58EBDB44371C42F9A5831EB9F6 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityWebRequestError_t01C779C192877A58EBDB44371C42F9A5831EB9F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod
struct  UnityWebRequestMethod_tF538D9A75B76FFC81710E65697E38C1B12E4F7E5 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityWebRequestMethod_tF538D9A75B76FFC81710E65697E38C1B12E4F7E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.AxisEventData
struct  AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E  : public BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.AxisEventData::<moveVector>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CmoveVectorU3Ek__BackingField_2;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.EventSystems.AxisEventData::<moveDir>k__BackingField
	int32_t ___U3CmoveDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmoveVectorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E, ___U3CmoveVectorU3Ek__BackingField_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CmoveVectorU3Ek__BackingField_2() const { return ___U3CmoveVectorU3Ek__BackingField_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CmoveVectorU3Ek__BackingField_2() { return &___U3CmoveVectorU3Ek__BackingField_2; }
	inline void set_U3CmoveVectorU3Ek__BackingField_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CmoveVectorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmoveDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E, ___U3CmoveDirU3Ek__BackingField_3)); }
	inline int32_t get_U3CmoveDirU3Ek__BackingField_3() const { return ___U3CmoveDirU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmoveDirU3Ek__BackingField_3() { return &___U3CmoveDirU3Ek__BackingField_3; }
	inline void set_U3CmoveDirU3Ek__BackingField_3(int32_t value)
	{
		___U3CmoveDirU3Ek__BackingField_3 = value;
	}
};


// B83.Image.BMP.BitmapInfoHeader
struct  BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476 
{
public:
	// System.UInt32 B83.Image.BMP.BitmapInfoHeader::size
	uint32_t ___size_0;
	// System.Int32 B83.Image.BMP.BitmapInfoHeader::width
	int32_t ___width_1;
	// System.Int32 B83.Image.BMP.BitmapInfoHeader::height
	int32_t ___height_2;
	// System.UInt16 B83.Image.BMP.BitmapInfoHeader::nColorPlanes
	uint16_t ___nColorPlanes_3;
	// System.UInt16 B83.Image.BMP.BitmapInfoHeader::nBitsPerPixel
	uint16_t ___nBitsPerPixel_4;
	// B83.Image.BMP.BMPComressionMode B83.Image.BMP.BitmapInfoHeader::compressionMethod
	int32_t ___compressionMethod_5;
	// System.UInt32 B83.Image.BMP.BitmapInfoHeader::rawImageSize
	uint32_t ___rawImageSize_6;
	// System.Int32 B83.Image.BMP.BitmapInfoHeader::xPPM
	int32_t ___xPPM_7;
	// System.Int32 B83.Image.BMP.BitmapInfoHeader::yPPM
	int32_t ___yPPM_8;
	// System.UInt32 B83.Image.BMP.BitmapInfoHeader::nPaletteColors
	uint32_t ___nPaletteColors_9;
	// System.UInt32 B83.Image.BMP.BitmapInfoHeader::nImportantColors
	uint32_t ___nImportantColors_10;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___size_0)); }
	inline uint32_t get_size_0() const { return ___size_0; }
	inline uint32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(uint32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_nColorPlanes_3() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___nColorPlanes_3)); }
	inline uint16_t get_nColorPlanes_3() const { return ___nColorPlanes_3; }
	inline uint16_t* get_address_of_nColorPlanes_3() { return &___nColorPlanes_3; }
	inline void set_nColorPlanes_3(uint16_t value)
	{
		___nColorPlanes_3 = value;
	}

	inline static int32_t get_offset_of_nBitsPerPixel_4() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___nBitsPerPixel_4)); }
	inline uint16_t get_nBitsPerPixel_4() const { return ___nBitsPerPixel_4; }
	inline uint16_t* get_address_of_nBitsPerPixel_4() { return &___nBitsPerPixel_4; }
	inline void set_nBitsPerPixel_4(uint16_t value)
	{
		___nBitsPerPixel_4 = value;
	}

	inline static int32_t get_offset_of_compressionMethod_5() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___compressionMethod_5)); }
	inline int32_t get_compressionMethod_5() const { return ___compressionMethod_5; }
	inline int32_t* get_address_of_compressionMethod_5() { return &___compressionMethod_5; }
	inline void set_compressionMethod_5(int32_t value)
	{
		___compressionMethod_5 = value;
	}

	inline static int32_t get_offset_of_rawImageSize_6() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___rawImageSize_6)); }
	inline uint32_t get_rawImageSize_6() const { return ___rawImageSize_6; }
	inline uint32_t* get_address_of_rawImageSize_6() { return &___rawImageSize_6; }
	inline void set_rawImageSize_6(uint32_t value)
	{
		___rawImageSize_6 = value;
	}

	inline static int32_t get_offset_of_xPPM_7() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___xPPM_7)); }
	inline int32_t get_xPPM_7() const { return ___xPPM_7; }
	inline int32_t* get_address_of_xPPM_7() { return &___xPPM_7; }
	inline void set_xPPM_7(int32_t value)
	{
		___xPPM_7 = value;
	}

	inline static int32_t get_offset_of_yPPM_8() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___yPPM_8)); }
	inline int32_t get_yPPM_8() const { return ___yPPM_8; }
	inline int32_t* get_address_of_yPPM_8() { return &___yPPM_8; }
	inline void set_yPPM_8(int32_t value)
	{
		___yPPM_8 = value;
	}

	inline static int32_t get_offset_of_nPaletteColors_9() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___nPaletteColors_9)); }
	inline uint32_t get_nPaletteColors_9() const { return ___nPaletteColors_9; }
	inline uint32_t* get_address_of_nPaletteColors_9() { return &___nPaletteColors_9; }
	inline void set_nPaletteColors_9(uint32_t value)
	{
		___nPaletteColors_9 = value;
	}

	inline static int32_t get_offset_of_nImportantColors_10() { return static_cast<int32_t>(offsetof(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476, ___nImportantColors_10)); }
	inline uint32_t get_nImportantColors_10() const { return ___nImportantColors_10; }
	inline uint32_t* get_address_of_nImportantColors_10() { return &___nImportantColors_10; }
	inline void set_nImportantColors_10(uint32_t value)
	{
		___nImportantColors_10 = value;
	}
};


// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339, ___m_Target_0)); }
	inline ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339, ___m_StartColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339, ___m_TargetColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339_marshaled_pinvoke
{
	ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026 * ___m_Target_0;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_StartColor_1;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339_marshaled_com
{
	ColorTweenCallback_tFD140F68C9A5F1C9799A2A82FA463C4EF56F9026 * ___m_Target_0;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_StartColor_1;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};

// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Networking.DownloadHandlerBuffer
struct  DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D  : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_marshaled_pinvoke : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_marshaled_com : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
};

// UnityEngine.Networking.DownloadHandlerTexture
struct  DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142  : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB
{
public:
	// UnityEngine.Texture2D UnityEngine.Networking.DownloadHandlerTexture::mTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___mTexture_1;
	// System.Boolean UnityEngine.Networking.DownloadHandlerTexture::mHasTexture
	bool ___mHasTexture_2;
	// System.Boolean UnityEngine.Networking.DownloadHandlerTexture::mNonReadable
	bool ___mNonReadable_3;

public:
	inline static int32_t get_offset_of_mTexture_1() { return static_cast<int32_t>(offsetof(DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142, ___mTexture_1)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_mTexture_1() const { return ___mTexture_1; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_mTexture_1() { return &___mTexture_1; }
	inline void set_mTexture_1(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___mTexture_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTexture_1), (void*)value);
	}

	inline static int32_t get_offset_of_mHasTexture_2() { return static_cast<int32_t>(offsetof(DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142, ___mHasTexture_2)); }
	inline bool get_mHasTexture_2() const { return ___mHasTexture_2; }
	inline bool* get_address_of_mHasTexture_2() { return &___mHasTexture_2; }
	inline void set_mHasTexture_2(bool value)
	{
		___mHasTexture_2 = value;
	}

	inline static int32_t get_offset_of_mNonReadable_3() { return static_cast<int32_t>(offsetof(DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142, ___mNonReadable_3)); }
	inline bool get_mNonReadable_3() const { return ___mNonReadable_3; }
	inline bool* get_address_of_mNonReadable_3() { return &___mNonReadable_3; }
	inline void set_mNonReadable_3(bool value)
	{
		___mNonReadable_3 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142_marshaled_pinvoke : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___mTexture_1;
	int32_t ___mHasTexture_2;
	int32_t ___mNonReadable_3;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142_marshaled_com : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___mTexture_1;
	int32_t ___mHasTexture_2;
	int32_t ___mNonReadable_3;
};

// SimpleFileBrowser.FileSystemEntry
struct  FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 
{
public:
	// System.String SimpleFileBrowser.FileSystemEntry::Path
	String_t* ___Path_0;
	// System.String SimpleFileBrowser.FileSystemEntry::Name
	String_t* ___Name_1;
	// System.String SimpleFileBrowser.FileSystemEntry::Extension
	String_t* ___Extension_2;
	// System.IO.FileAttributes SimpleFileBrowser.FileSystemEntry::Attributes
	int32_t ___Attributes_3;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Path_0), (void*)value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_1), (void*)value);
	}

	inline static int32_t get_offset_of_Extension_2() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Extension_2)); }
	inline String_t* get_Extension_2() const { return ___Extension_2; }
	inline String_t** get_address_of_Extension_2() { return &___Extension_2; }
	inline void set_Extension_2(String_t* value)
	{
		___Extension_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Extension_2), (void*)value);
	}

	inline static int32_t get_offset_of_Attributes_3() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Attributes_3)); }
	inline int32_t get_Attributes_3() const { return ___Attributes_3; }
	inline int32_t* get_address_of_Attributes_3() { return &___Attributes_3; }
	inline void set_Attributes_3(int32_t value)
	{
		___Attributes_3 = value;
	}
};

// Native definition for P/Invoke marshalling of SimpleFileBrowser.FileSystemEntry
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke
{
	char* ___Path_0;
	char* ___Name_1;
	char* ___Extension_2;
	int32_t ___Attributes_3;
};
// Native definition for COM marshalling of SimpleFileBrowser.FileSystemEntry
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_com
{
	Il2CppChar* ___Path_0;
	Il2CppChar* ___Name_1;
	Il2CppChar* ___Extension_2;
	int32_t ___Attributes_3;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct  Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// Dummiesman.OBJLoader
struct  OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896  : public RuntimeObject
{
public:
	// Dummiesman.SplitMode Dummiesman.OBJLoader::SplitMode
	int32_t ___SplitMode_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Dummiesman.OBJLoader::Vertices
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___Vertices_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Dummiesman.OBJLoader::Normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___Normals_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Dummiesman.OBJLoader::UVs
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___UVs_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> Dummiesman.OBJLoader::Materials
	Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F * ___Materials_4;
	// System.IO.FileInfo Dummiesman.OBJLoader::_objInfo
	FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * ____objInfo_5;

public:
	inline static int32_t get_offset_of_SplitMode_0() { return static_cast<int32_t>(offsetof(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896, ___SplitMode_0)); }
	inline int32_t get_SplitMode_0() const { return ___SplitMode_0; }
	inline int32_t* get_address_of_SplitMode_0() { return &___SplitMode_0; }
	inline void set_SplitMode_0(int32_t value)
	{
		___SplitMode_0 = value;
	}

	inline static int32_t get_offset_of_Vertices_1() { return static_cast<int32_t>(offsetof(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896, ___Vertices_1)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_Vertices_1() const { return ___Vertices_1; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_Vertices_1() { return &___Vertices_1; }
	inline void set_Vertices_1(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___Vertices_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Vertices_1), (void*)value);
	}

	inline static int32_t get_offset_of_Normals_2() { return static_cast<int32_t>(offsetof(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896, ___Normals_2)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_Normals_2() const { return ___Normals_2; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_Normals_2() { return &___Normals_2; }
	inline void set_Normals_2(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___Normals_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Normals_2), (void*)value);
	}

	inline static int32_t get_offset_of_UVs_3() { return static_cast<int32_t>(offsetof(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896, ___UVs_3)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_UVs_3() const { return ___UVs_3; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_UVs_3() { return &___UVs_3; }
	inline void set_UVs_3(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___UVs_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UVs_3), (void*)value);
	}

	inline static int32_t get_offset_of_Materials_4() { return static_cast<int32_t>(offsetof(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896, ___Materials_4)); }
	inline Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F * get_Materials_4() const { return ___Materials_4; }
	inline Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F ** get_address_of_Materials_4() { return &___Materials_4; }
	inline void set_Materials_4(Dictionary_2_tDEB9074ADF852EC40CE0399CE36596B7AE79316F * value)
	{
		___Materials_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Materials_4), (void*)value);
	}

	inline static int32_t get_offset_of__objInfo_5() { return static_cast<int32_t>(offsetof(OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896, ____objInfo_5)); }
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * get__objInfo_5() const { return ____objInfo_5; }
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 ** get_address_of__objInfo_5() { return &____objInfo_5; }
	inline void set__objInfo_5(FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * value)
	{
		____objInfo_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____objInfo_5), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954  : public BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerClick>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerClickU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerCurrentRaycastU3Ek__BackingField_8;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerPressRaycastU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___hovered_10;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_11;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpositionU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaU3Ek__BackingField_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldPositionU3Ek__BackingField_16;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldNormalU3Ek__BackingField_17;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_18;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CscrollDeltaU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_21;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_22;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerEnterU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___m_PointerPress_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerPress_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClastPressU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrawPointerPressU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerDragU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerClickU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerClickU3Ek__BackingField_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerClickU3Ek__BackingField_7() const { return ___U3CpointerClickU3Ek__BackingField_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerClickU3Ek__BackingField_7() { return &___U3CpointerClickU3Ek__BackingField_7; }
	inline void set_U3CpointerClickU3Ek__BackingField_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerClickU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerClickU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerCurrentRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerCurrentRaycastU3Ek__BackingField_8() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_8(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerPressRaycastU3Ek__BackingField_9)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerPressRaycastU3Ek__BackingField_9() const { return ___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return &___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_9(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_hovered_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___hovered_10)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_hovered_10() const { return ___hovered_10; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_hovered_10() { return &___hovered_10; }
	inline void set_hovered_10(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___hovered_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hovered_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CeligibleForClickU3Ek__BackingField_11)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_11() const { return ___U3CeligibleForClickU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_11() { return &___U3CeligibleForClickU3Ek__BackingField_11; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_11(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerIdU3Ek__BackingField_12)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_12() const { return ___U3CpointerIdU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_12() { return &___U3CpointerIdU3Ek__BackingField_12; }
	inline void set_U3CpointerIdU3Ek__BackingField_12(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpositionU3Ek__BackingField_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpositionU3Ek__BackingField_13() const { return ___U3CpositionU3Ek__BackingField_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpositionU3Ek__BackingField_13() { return &___U3CpositionU3Ek__BackingField_13; }
	inline void set_U3CpositionU3Ek__BackingField_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpositionU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdeltaU3Ek__BackingField_14)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CdeltaU3Ek__BackingField_14() const { return ___U3CdeltaU3Ek__BackingField_14; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CdeltaU3Ek__BackingField_14() { return &___U3CdeltaU3Ek__BackingField_14; }
	inline void set_U3CdeltaU3Ek__BackingField_14(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CdeltaU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpressPositionU3Ek__BackingField_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpressPositionU3Ek__BackingField_15() const { return ___U3CpressPositionU3Ek__BackingField_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpressPositionU3Ek__BackingField_15() { return &___U3CpressPositionU3Ek__BackingField_15; }
	inline void set_U3CpressPositionU3Ek__BackingField_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpressPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldPositionU3Ek__BackingField_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldPositionU3Ek__BackingField_16() const { return ___U3CworldPositionU3Ek__BackingField_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldPositionU3Ek__BackingField_16() { return &___U3CworldPositionU3Ek__BackingField_16; }
	inline void set_U3CworldPositionU3Ek__BackingField_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldPositionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldNormalU3Ek__BackingField_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldNormalU3Ek__BackingField_17() const { return ___U3CworldNormalU3Ek__BackingField_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldNormalU3Ek__BackingField_17() { return &___U3CworldNormalU3Ek__BackingField_17; }
	inline void set_U3CworldNormalU3Ek__BackingField_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldNormalU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickTimeU3Ek__BackingField_18)); }
	inline float get_U3CclickTimeU3Ek__BackingField_18() const { return ___U3CclickTimeU3Ek__BackingField_18; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_18() { return &___U3CclickTimeU3Ek__BackingField_18; }
	inline void set_U3CclickTimeU3Ek__BackingField_18(float value)
	{
		___U3CclickTimeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickCountU3Ek__BackingField_19)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_19() const { return ___U3CclickCountU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_19() { return &___U3CclickCountU3Ek__BackingField_19; }
	inline void set_U3CclickCountU3Ek__BackingField_19(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CscrollDeltaU3Ek__BackingField_20)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CscrollDeltaU3Ek__BackingField_20() const { return ___U3CscrollDeltaU3Ek__BackingField_20; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CscrollDeltaU3Ek__BackingField_20() { return &___U3CscrollDeltaU3Ek__BackingField_20; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_20(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CuseDragThresholdU3Ek__BackingField_21)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_21() const { return ___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_21() { return &___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_21(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdraggingU3Ek__BackingField_22)); }
	inline bool get_U3CdraggingU3Ek__BackingField_22() const { return ___U3CdraggingU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_22() { return &___U3CdraggingU3Ek__BackingField_22; }
	inline void set_U3CdraggingU3Ek__BackingField_22(bool value)
	{
		___U3CdraggingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CbuttonU3Ek__BackingField_23)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_23() const { return ___U3CbuttonU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_23() { return &___U3CbuttonU3Ek__BackingField_23; }
	inline void set_U3CbuttonU3Ek__BackingField_23(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_23 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest
struct  UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_DownloadHandler_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_DownloadHandler_1)); }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * get_m_DownloadHandler_1() const { return ___m_DownloadHandler_1; }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB ** get_address_of_m_DownloadHandler_1() { return &___m_DownloadHandler_1; }
	inline void set_m_DownloadHandler_1(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * value)
	{
		___m_DownloadHandler_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DownloadHandler_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_UploadHandler_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_UploadHandler_2)); }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * get_m_UploadHandler_2() const { return ___m_UploadHandler_2; }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA ** get_address_of_m_UploadHandler_2() { return &___m_UploadHandler_2; }
	inline void set_m_UploadHandler_2(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * value)
	{
		___m_UploadHandler_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UploadHandler_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CertificateHandler_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_CertificateHandler_3)); }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * get_m_CertificateHandler_3() const { return ___m_CertificateHandler_3; }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E ** get_address_of_m_CertificateHandler_3() { return &___m_CertificateHandler_3; }
	inline void set_m_CertificateHandler_3(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * value)
	{
		___m_CertificateHandler_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CertificateHandler_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uri_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Uri_4)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_m_Uri_4() const { return ___m_Uri_4; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_m_Uri_4() { return &___m_Uri_4; }
	inline void set_m_Uri_4(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___m_Uri_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uri_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5)); }
	inline bool get_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() const { return ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return &___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline void set_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5(bool value)
	{
		___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com* ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct  UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396  : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CwebRequestU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CwebRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396, ___U3CwebRequestU3Ek__BackingField_2)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CwebRequestU3Ek__BackingField_2() const { return ___U3CwebRequestU3Ek__BackingField_2; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CwebRequestU3Ek__BackingField_2() { return &___U3CwebRequestU3Ek__BackingField_2; }
	inline void set_U3CwebRequestU3Ek__BackingField_2(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CwebRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwebRequestU3Ek__BackingField_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396_marshaled_pinvoke : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke
{
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396_marshaled_com : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com
{
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// UnityEngine.EventSystems.EventTrigger/Entry
struct  Entry_t9C594CD634607709CF020BE9C8A469E1C9033D36  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UnityEngine.EventSystems.EventTrigger/Entry::eventID
	int32_t ___eventID_0;
	// UnityEngine.EventSystems.EventTrigger/TriggerEvent UnityEngine.EventSystems.EventTrigger/Entry::callback
	TriggerEvent_t6C4DB59340B55DE906C54EE3FF7DAE4DE24A1276 * ___callback_1;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(Entry_t9C594CD634607709CF020BE9C8A469E1C9033D36, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Entry_t9C594CD634607709CF020BE9C8A469E1C9033D36, ___callback_1)); }
	inline TriggerEvent_t6C4DB59340B55DE906C54EE3FF7DAE4DE24A1276 * get_callback_1() const { return ___callback_1; }
	inline TriggerEvent_t6C4DB59340B55DE906C54EE3FF7DAE4DE24A1276 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(TriggerEvent_t6C4DB59340B55DE906C54EE3FF7DAE4DE24A1276 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_1), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232
struct  U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B  : public RuntimeObject
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::pickMode
	int32_t ___pickMode_2;
	// System.Boolean SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::allowMultiSelection
	bool ___allowMultiSelection_3;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::initialPath
	String_t* ___initialPath_4;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::initialFilename
	String_t* ___initialFilename_5;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::title
	String_t* ___title_6;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__232::loadButtonText
	String_t* ___loadButtonText_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_pickMode_2() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___pickMode_2)); }
	inline int32_t get_pickMode_2() const { return ___pickMode_2; }
	inline int32_t* get_address_of_pickMode_2() { return &___pickMode_2; }
	inline void set_pickMode_2(int32_t value)
	{
		___pickMode_2 = value;
	}

	inline static int32_t get_offset_of_allowMultiSelection_3() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___allowMultiSelection_3)); }
	inline bool get_allowMultiSelection_3() const { return ___allowMultiSelection_3; }
	inline bool* get_address_of_allowMultiSelection_3() { return &___allowMultiSelection_3; }
	inline void set_allowMultiSelection_3(bool value)
	{
		___allowMultiSelection_3 = value;
	}

	inline static int32_t get_offset_of_initialPath_4() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___initialPath_4)); }
	inline String_t* get_initialPath_4() const { return ___initialPath_4; }
	inline String_t** get_address_of_initialPath_4() { return &___initialPath_4; }
	inline void set_initialPath_4(String_t* value)
	{
		___initialPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialPath_4), (void*)value);
	}

	inline static int32_t get_offset_of_initialFilename_5() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___initialFilename_5)); }
	inline String_t* get_initialFilename_5() const { return ___initialFilename_5; }
	inline String_t** get_address_of_initialFilename_5() { return &___initialFilename_5; }
	inline void set_initialFilename_5(String_t* value)
	{
		___initialFilename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialFilename_5), (void*)value);
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___title_6)); }
	inline String_t* get_title_6() const { return ___title_6; }
	inline String_t** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(String_t* value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_6), (void*)value);
	}

	inline static int32_t get_offset_of_loadButtonText_7() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B, ___loadButtonText_7)); }
	inline String_t* get_loadButtonText_7() const { return ___loadButtonText_7; }
	inline String_t** get_address_of_loadButtonText_7() { return &___loadButtonText_7; }
	inline void set_loadButtonText_7(String_t* value)
	{
		___loadButtonText_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadButtonText_7), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231
struct  U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389  : public RuntimeObject
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::pickMode
	int32_t ___pickMode_2;
	// System.Boolean SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::allowMultiSelection
	bool ___allowMultiSelection_3;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::initialPath
	String_t* ___initialPath_4;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::initialFilename
	String_t* ___initialFilename_5;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::title
	String_t* ___title_6;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__231::saveButtonText
	String_t* ___saveButtonText_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_pickMode_2() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___pickMode_2)); }
	inline int32_t get_pickMode_2() const { return ___pickMode_2; }
	inline int32_t* get_address_of_pickMode_2() { return &___pickMode_2; }
	inline void set_pickMode_2(int32_t value)
	{
		___pickMode_2 = value;
	}

	inline static int32_t get_offset_of_allowMultiSelection_3() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___allowMultiSelection_3)); }
	inline bool get_allowMultiSelection_3() const { return ___allowMultiSelection_3; }
	inline bool* get_address_of_allowMultiSelection_3() { return &___allowMultiSelection_3; }
	inline void set_allowMultiSelection_3(bool value)
	{
		___allowMultiSelection_3 = value;
	}

	inline static int32_t get_offset_of_initialPath_4() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___initialPath_4)); }
	inline String_t* get_initialPath_4() const { return ___initialPath_4; }
	inline String_t** get_address_of_initialPath_4() { return &___initialPath_4; }
	inline void set_initialPath_4(String_t* value)
	{
		___initialPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialPath_4), (void*)value);
	}

	inline static int32_t get_offset_of_initialFilename_5() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___initialFilename_5)); }
	inline String_t* get_initialFilename_5() const { return ___initialFilename_5; }
	inline String_t** get_address_of_initialFilename_5() { return &___initialFilename_5; }
	inline void set_initialFilename_5(String_t* value)
	{
		___initialFilename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialFilename_5), (void*)value);
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___title_6)); }
	inline String_t* get_title_6() const { return ___title_6; }
	inline String_t** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(String_t* value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_6), (void*)value);
	}

	inline static int32_t get_offset_of_saveButtonText_7() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389, ___saveButtonText_7)); }
	inline String_t* get_saveButtonText_7() const { return ___saveButtonText_7; }
	inline String_t** get_address_of_saveButtonText_7() { return &___saveButtonText_7; }
	inline void set_saveButtonText_7(String_t* value)
	{
		___saveButtonText_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___saveButtonText_7), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/QuickLink
struct  QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7 
{
public:
	// System.Environment/SpecialFolder SimpleFileBrowser.FileBrowser/QuickLink::target
	int32_t ___target_0;
	// System.String SimpleFileBrowser.FileBrowser/QuickLink::name
	String_t* ___name_1;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser/QuickLink::icon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_1), (void*)value);
	}

	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7, ___icon_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_icon_2() const { return ___icon_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of SimpleFileBrowser.FileBrowser/QuickLink
struct QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_pinvoke
{
	int32_t ___target_0;
	char* ___name_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_2;
};
// Native definition for COM marshalling of SimpleFileBrowser.FileBrowser/QuickLink
struct QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_com
{
	int32_t ___target_0;
	Il2CppChar* ___name_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_2;
};

// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct  ButtonState_t49AF0FCF7DF429002E167972B40DC5A2A3804562  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerInputModule/ButtonState::m_Button
	int32_t ___m_Button_0;
	// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData UnityEngine.EventSystems.PointerInputModule/ButtonState::m_EventData
	MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6 * ___m_EventData_1;

public:
	inline static int32_t get_offset_of_m_Button_0() { return static_cast<int32_t>(offsetof(ButtonState_t49AF0FCF7DF429002E167972B40DC5A2A3804562, ___m_Button_0)); }
	inline int32_t get_m_Button_0() const { return ___m_Button_0; }
	inline int32_t* get_address_of_m_Button_0() { return &___m_Button_0; }
	inline void set_m_Button_0(int32_t value)
	{
		___m_Button_0 = value;
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(ButtonState_t49AF0FCF7DF429002E167972B40DC5A2A3804562, ___m_EventData_1)); }
	inline MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventData_1), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct  MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/FramePressState UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonState
	int32_t ___buttonState_0;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonData
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___buttonData_1;

public:
	inline static int32_t get_offset_of_buttonState_0() { return static_cast<int32_t>(offsetof(MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6, ___buttonState_0)); }
	inline int32_t get_buttonState_0() const { return ___buttonState_0; }
	inline int32_t* get_address_of_buttonState_0() { return &___buttonState_0; }
	inline void set_buttonState_0(int32_t value)
	{
		___buttonState_0 = value;
	}

	inline static int32_t get_offset_of_buttonData_1() { return static_cast<int32_t>(offsetof(MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6, ___buttonData_1)); }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * get_buttonData_1() const { return ___buttonData_1; }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 ** get_address_of_buttonData_1() { return &___buttonData_1; }
	inline void set_buttonData_1(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * value)
	{
		___buttonData_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonData_1), (void*)value);
	}
};


// UnityEngine.UI.StencilMaterial/MatEntry
struct  MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityEngine.UI.StencilMaterial/MatEntry::baseMat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMat_0;
	// UnityEngine.Material UnityEngine.UI.StencilMaterial/MatEntry::customMat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___customMat_1;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::count
	int32_t ___count_2;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::stencilId
	int32_t ___stencilId_3;
	// UnityEngine.Rendering.StencilOp UnityEngine.UI.StencilMaterial/MatEntry::operation
	int32_t ___operation_4;
	// UnityEngine.Rendering.CompareFunction UnityEngine.UI.StencilMaterial/MatEntry::compareFunction
	int32_t ___compareFunction_5;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::readMask
	int32_t ___readMask_6;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::writeMask
	int32_t ___writeMask_7;
	// System.Boolean UnityEngine.UI.StencilMaterial/MatEntry::useAlphaClip
	bool ___useAlphaClip_8;
	// UnityEngine.Rendering.ColorWriteMask UnityEngine.UI.StencilMaterial/MatEntry::colorMask
	int32_t ___colorMask_9;

public:
	inline static int32_t get_offset_of_baseMat_0() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___baseMat_0)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_baseMat_0() const { return ___baseMat_0; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_baseMat_0() { return &___baseMat_0; }
	inline void set_baseMat_0(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___baseMat_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseMat_0), (void*)value);
	}

	inline static int32_t get_offset_of_customMat_1() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___customMat_1)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_customMat_1() const { return ___customMat_1; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_customMat_1() { return &___customMat_1; }
	inline void set_customMat_1(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___customMat_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customMat_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_stencilId_3() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___stencilId_3)); }
	inline int32_t get_stencilId_3() const { return ___stencilId_3; }
	inline int32_t* get_address_of_stencilId_3() { return &___stencilId_3; }
	inline void set_stencilId_3(int32_t value)
	{
		___stencilId_3 = value;
	}

	inline static int32_t get_offset_of_operation_4() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___operation_4)); }
	inline int32_t get_operation_4() const { return ___operation_4; }
	inline int32_t* get_address_of_operation_4() { return &___operation_4; }
	inline void set_operation_4(int32_t value)
	{
		___operation_4 = value;
	}

	inline static int32_t get_offset_of_compareFunction_5() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___compareFunction_5)); }
	inline int32_t get_compareFunction_5() const { return ___compareFunction_5; }
	inline int32_t* get_address_of_compareFunction_5() { return &___compareFunction_5; }
	inline void set_compareFunction_5(int32_t value)
	{
		___compareFunction_5 = value;
	}

	inline static int32_t get_offset_of_readMask_6() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___readMask_6)); }
	inline int32_t get_readMask_6() const { return ___readMask_6; }
	inline int32_t* get_address_of_readMask_6() { return &___readMask_6; }
	inline void set_readMask_6(int32_t value)
	{
		___readMask_6 = value;
	}

	inline static int32_t get_offset_of_writeMask_7() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___writeMask_7)); }
	inline int32_t get_writeMask_7() const { return ___writeMask_7; }
	inline int32_t* get_address_of_writeMask_7() { return &___writeMask_7; }
	inline void set_writeMask_7(int32_t value)
	{
		___writeMask_7 = value;
	}

	inline static int32_t get_offset_of_useAlphaClip_8() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___useAlphaClip_8)); }
	inline bool get_useAlphaClip_8() const { return ___useAlphaClip_8; }
	inline bool* get_address_of_useAlphaClip_8() { return &___useAlphaClip_8; }
	inline void set_useAlphaClip_8(bool value)
	{
		___useAlphaClip_8 = value;
	}

	inline static int32_t get_offset_of_colorMask_9() { return static_cast<int32_t>(offsetof(MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E, ___colorMask_9)); }
	inline int32_t get_colorMask_9() const { return ___colorMask_9; }
	inline int32_t* get_address_of_colorMask_9() { return &___colorMask_9; }
	inline void set_colorMask_9(int32_t value)
	{
		___colorMask_9 = value;
	}
};


// B83.Image.BMP.BMPImage
struct  BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0  : public RuntimeObject
{
public:
	// B83.Image.BMP.BMPFileHeader B83.Image.BMP.BMPImage::header
	BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0  ___header_0;
	// B83.Image.BMP.BitmapInfoHeader B83.Image.BMP.BMPImage::info
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476  ___info_1;
	// System.UInt32 B83.Image.BMP.BMPImage::rMask
	uint32_t ___rMask_2;
	// System.UInt32 B83.Image.BMP.BMPImage::gMask
	uint32_t ___gMask_3;
	// System.UInt32 B83.Image.BMP.BMPImage::bMask
	uint32_t ___bMask_4;
	// System.UInt32 B83.Image.BMP.BMPImage::aMask
	uint32_t ___aMask_5;
	// System.Collections.Generic.List`1<UnityEngine.Color32> B83.Image.BMP.BMPImage::palette
	List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * ___palette_6;
	// UnityEngine.Color32[] B83.Image.BMP.BMPImage::imageData
	Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* ___imageData_7;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___header_0)); }
	inline BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0  get_header_0() const { return ___header_0; }
	inline BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0 * get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0  value)
	{
		___header_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___info_1)); }
	inline BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476  get_info_1() const { return ___info_1; }
	inline BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476 * get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476  value)
	{
		___info_1 = value;
	}

	inline static int32_t get_offset_of_rMask_2() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___rMask_2)); }
	inline uint32_t get_rMask_2() const { return ___rMask_2; }
	inline uint32_t* get_address_of_rMask_2() { return &___rMask_2; }
	inline void set_rMask_2(uint32_t value)
	{
		___rMask_2 = value;
	}

	inline static int32_t get_offset_of_gMask_3() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___gMask_3)); }
	inline uint32_t get_gMask_3() const { return ___gMask_3; }
	inline uint32_t* get_address_of_gMask_3() { return &___gMask_3; }
	inline void set_gMask_3(uint32_t value)
	{
		___gMask_3 = value;
	}

	inline static int32_t get_offset_of_bMask_4() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___bMask_4)); }
	inline uint32_t get_bMask_4() const { return ___bMask_4; }
	inline uint32_t* get_address_of_bMask_4() { return &___bMask_4; }
	inline void set_bMask_4(uint32_t value)
	{
		___bMask_4 = value;
	}

	inline static int32_t get_offset_of_aMask_5() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___aMask_5)); }
	inline uint32_t get_aMask_5() const { return ___aMask_5; }
	inline uint32_t* get_address_of_aMask_5() { return &___aMask_5; }
	inline void set_aMask_5(uint32_t value)
	{
		___aMask_5 = value;
	}

	inline static int32_t get_offset_of_palette_6() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___palette_6)); }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * get_palette_6() const { return ___palette_6; }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 ** get_address_of_palette_6() { return &___palette_6; }
	inline void set_palette_6(List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * value)
	{
		___palette_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___palette_6), (void*)value);
	}

	inline static int32_t get_offset_of_imageData_7() { return static_cast<int32_t>(offsetof(BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0, ___imageData_7)); }
	inline Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* get_imageData_7() const { return ___imageData_7; }
	inline Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2** get_address_of_imageData_7() { return &___imageData_7; }
	inline void set_imageData_7(Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* value)
	{
		___imageData_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imageData_7), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// SimpleFileBrowser.OnItemClickedHandler
struct  OnItemClickedHandler_tB35D42D0B84155AAC5D63832D33201C548080A0D  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/<>c__DisplayClass213_0
struct  U3CU3Ec__DisplayClass213_0_tE83CD523E9B1680A5A376728628E6A0125228E14  : public RuntimeObject
{
public:
	// SimpleFileBrowser.FileSystemEntry SimpleFileBrowser.FileBrowser/<>c__DisplayClass213_0::fileInfo
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  ___fileInfo_0;
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser/<>c__DisplayClass213_0::<>4__this
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_fileInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass213_0_tE83CD523E9B1680A5A376728628E6A0125228E14, ___fileInfo_0)); }
	inline FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  get_fileInfo_0() const { return ___fileInfo_0; }
	inline FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * get_address_of_fileInfo_0() { return &___fileInfo_0; }
	inline void set_fileInfo_0(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  value)
	{
		___fileInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___fileInfo_0))->___Path_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___fileInfo_0))->___Name_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___fileInfo_0))->___Extension_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass213_0_tE83CD523E9B1680A5A376728628E6A0125228E14, ___U3CU3E4__this_1)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/OnCancel
struct  OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/OnSuccess
struct  OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed
struct  OnDeletionConfirmed_t54FFE37F4F1E4680F19B875155B0796C976BE677  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted
struct  OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t9D6C059892DE030746D2873EB8871415BAC79311  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t6DAE64211C37E996B257BF2C54707DAD3474D69C  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tA4A6A2336A9B9FEE31F8F5344576B3BB0A7B3F34  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t125C1CA6D0148380915E597AC8ADBB93EFB0EE29  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t27A8B301052E9C6A4A7D38F95293CA129C39373F  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t48E12CFDCFDEA0CD7D83F9DDE1E341DBCC855005  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// AsImpL.Examples.AsImpLSample
struct  AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String AsImpL.Examples.AsImpLSample::filePath
	String_t* ___filePath_4;
	// System.String AsImpL.Examples.AsImpLSample::objectName
	String_t* ___objectName_5;
	// AsImpL.ImportOptions AsImpL.Examples.AsImpLSample::importOptions
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * ___importOptions_6;
	// AsImpL.PathSettings AsImpL.Examples.AsImpLSample::pathSettings
	PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 * ___pathSettings_7;
	// AsImpL.ObjectImporter AsImpL.Examples.AsImpLSample::objImporter
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D * ___objImporter_8;

public:
	inline static int32_t get_offset_of_filePath_4() { return static_cast<int32_t>(offsetof(AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A, ___filePath_4)); }
	inline String_t* get_filePath_4() const { return ___filePath_4; }
	inline String_t** get_address_of_filePath_4() { return &___filePath_4; }
	inline void set_filePath_4(String_t* value)
	{
		___filePath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filePath_4), (void*)value);
	}

	inline static int32_t get_offset_of_objectName_5() { return static_cast<int32_t>(offsetof(AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A, ___objectName_5)); }
	inline String_t* get_objectName_5() const { return ___objectName_5; }
	inline String_t** get_address_of_objectName_5() { return &___objectName_5; }
	inline void set_objectName_5(String_t* value)
	{
		___objectName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectName_5), (void*)value);
	}

	inline static int32_t get_offset_of_importOptions_6() { return static_cast<int32_t>(offsetof(AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A, ___importOptions_6)); }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * get_importOptions_6() const { return ___importOptions_6; }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F ** get_address_of_importOptions_6() { return &___importOptions_6; }
	inline void set_importOptions_6(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * value)
	{
		___importOptions_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___importOptions_6), (void*)value);
	}

	inline static int32_t get_offset_of_pathSettings_7() { return static_cast<int32_t>(offsetof(AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A, ___pathSettings_7)); }
	inline PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 * get_pathSettings_7() const { return ___pathSettings_7; }
	inline PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 ** get_address_of_pathSettings_7() { return &___pathSettings_7; }
	inline void set_pathSettings_7(PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 * value)
	{
		___pathSettings_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathSettings_7), (void*)value);
	}

	inline static int32_t get_offset_of_objImporter_8() { return static_cast<int32_t>(offsetof(AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A, ___objImporter_8)); }
	inline ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D * get_objImporter_8() const { return ___objImporter_8; }
	inline ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D ** get_address_of_objImporter_8() { return &___objImporter_8; }
	inline void set_objImporter_8(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D * value)
	{
		___objImporter_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objImporter_8), (void*)value);
	}
};


// AsImpL.Examples.EditorLikeCameraController
struct  EditorLikeCameraController_tF9B42E63797E09C95BA414AED094B9A66E700BD2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.EventSystems.EventTrigger
struct  EventTrigger_tA136EB086A23F8BBDC2D547223F1AA9CBA9A2563  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry> UnityEngine.EventSystems.EventTrigger::m_Delegates
	List_1_t88A4BE98895C19A1F134BA69882646898AC2BD70 * ___m_Delegates_4;

public:
	inline static int32_t get_offset_of_m_Delegates_4() { return static_cast<int32_t>(offsetof(EventTrigger_tA136EB086A23F8BBDC2D547223F1AA9CBA9A2563, ___m_Delegates_4)); }
	inline List_1_t88A4BE98895C19A1F134BA69882646898AC2BD70 * get_m_Delegates_4() const { return ___m_Delegates_4; }
	inline List_1_t88A4BE98895C19A1F134BA69882646898AC2BD70 ** get_address_of_m_Delegates_4() { return &___m_Delegates_4; }
	inline void set_m_Delegates_4(List_1_t88A4BE98895C19A1F134BA69882646898AC2BD70 * value)
	{
		___m_Delegates_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Delegates_4), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser
struct  FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Color SimpleFileBrowser.FileBrowser::normalFileColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___normalFileColor_17;
	// UnityEngine.Color SimpleFileBrowser.FileBrowser::hoveredFileColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___hoveredFileColor_18;
	// UnityEngine.Color SimpleFileBrowser.FileBrowser::selectedFileColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___selectedFileColor_19;
	// UnityEngine.Color SimpleFileBrowser.FileBrowser::wrongFilenameColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___wrongFilenameColor_20;
	// System.Int32 SimpleFileBrowser.FileBrowser::minWidth
	int32_t ___minWidth_21;
	// System.Int32 SimpleFileBrowser.FileBrowser::minHeight
	int32_t ___minHeight_22;
	// System.Single SimpleFileBrowser.FileBrowser::narrowScreenWidth
	float ___narrowScreenWidth_23;
	// System.Single SimpleFileBrowser.FileBrowser::quickLinksMaxWidthPercentage
	float ___quickLinksMaxWidthPercentage_24;
	// System.Boolean SimpleFileBrowser.FileBrowser::sortFilesByName
	bool ___sortFilesByName_25;
	// System.String[] SimpleFileBrowser.FileBrowser::excludeExtensions
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___excludeExtensions_26;
	// SimpleFileBrowser.FileBrowser/QuickLink[] SimpleFileBrowser.FileBrowser::quickLinks
	QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC* ___quickLinks_27;
	// System.Collections.Generic.HashSet`1<System.String> SimpleFileBrowser.FileBrowser::excludedExtensionsSet
	HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * ___excludedExtensionsSet_29;
	// System.Boolean SimpleFileBrowser.FileBrowser::generateQuickLinksForDrives
	bool ___generateQuickLinksForDrives_30;
	// System.Boolean SimpleFileBrowser.FileBrowser::contextMenuShowDeleteButton
	bool ___contextMenuShowDeleteButton_31;
	// System.Boolean SimpleFileBrowser.FileBrowser::contextMenuShowRenameButton
	bool ___contextMenuShowRenameButton_32;
	// System.Boolean SimpleFileBrowser.FileBrowser::showResizeCursor
	bool ___showResizeCursor_33;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser::folderIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___folderIcon_34;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser::driveIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___driveIcon_35;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser::defaultIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___defaultIcon_36;
	// SimpleFileBrowser.FileBrowser/FiletypeIcon[] SimpleFileBrowser.FileBrowser::filetypeIcons
	FiletypeIconU5BU5D_tCB87AC1C627D7517B2694613417FDB428E27F395* ___filetypeIcons_37;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite> SimpleFileBrowser.FileBrowser::filetypeToIcon
	Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D * ___filetypeToIcon_38;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser::multiSelectionToggleOffIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___multiSelectionToggleOffIcon_39;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser::multiSelectionToggleOnIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___multiSelectionToggleOnIcon_40;
	// SimpleFileBrowser.FileBrowserMovement SimpleFileBrowser.FileBrowser::window
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 * ___window_41;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::windowTR
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___windowTR_42;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::topViewNarrowScreen
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___topViewNarrowScreen_43;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleView
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleView_44;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowser::middleViewOriginalPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___middleViewOriginalPosition_45;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowser::middleViewOriginalSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___middleViewOriginalSize_46;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleViewQuickLinks
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleViewQuickLinks_47;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowser::middleViewQuickLinksOriginalSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___middleViewQuickLinksOriginalSize_48;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleViewFiles
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleViewFiles_49;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleViewSeparator
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleViewSeparator_50;
	// SimpleFileBrowser.FileBrowserItem SimpleFileBrowser.FileBrowser::itemPrefab
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 * ___itemPrefab_51;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserItem> SimpleFileBrowser.FileBrowser::allItems
	List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 * ___allItems_52;
	// System.Single SimpleFileBrowser.FileBrowser::itemHeight
	float ___itemHeight_53;
	// SimpleFileBrowser.FileBrowserQuickLink SimpleFileBrowser.FileBrowser::quickLinkPrefab
	FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C * ___quickLinkPrefab_54;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserQuickLink> SimpleFileBrowser.FileBrowser::allQuickLinks
	List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F * ___allQuickLinks_55;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::titleText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___titleText_56;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowser::backButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___backButton_57;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowser::forwardButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___forwardButton_58;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowser::upButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___upButton_59;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowser::pathInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___pathInputField_60;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::pathInputFieldSlotTop
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___pathInputFieldSlotTop_61;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::pathInputFieldSlotBottom
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___pathInputFieldSlotBottom_62;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowser::searchInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___searchInputField_63;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::quickLinksContainer
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___quickLinksContainer_64;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::filesContainer
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___filesContainer_65;
	// UnityEngine.UI.ScrollRect SimpleFileBrowser.FileBrowser::filesScrollRect
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___filesScrollRect_66;
	// SimpleFileBrowser.RecycledListView SimpleFileBrowser.FileBrowser::listView
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * ___listView_67;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowser::filenameInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___filenameInputField_68;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::filenameInputFieldOverlayText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___filenameInputFieldOverlayText_69;
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowser::filenameImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___filenameImage_70;
	// UnityEngine.UI.Dropdown SimpleFileBrowser.FileBrowser::filtersDropdown
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___filtersDropdown_71;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::filtersDropdownContainer
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___filtersDropdownContainer_72;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::filterItemTemplate
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___filterItemTemplate_73;
	// UnityEngine.UI.Toggle SimpleFileBrowser.FileBrowser::showHiddenFilesToggle
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ___showHiddenFilesToggle_74;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::submitButtonText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___submitButtonText_75;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::moreOptionsContextMenuPosition
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___moreOptionsContextMenuPosition_76;
	// SimpleFileBrowser.FileBrowserRenamedItem SimpleFileBrowser.FileBrowser::renameItem
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * ___renameItem_77;
	// SimpleFileBrowser.FileBrowserContextMenu SimpleFileBrowser.FileBrowser::contextMenu
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC * ___contextMenu_78;
	// SimpleFileBrowser.FileBrowserDeleteConfirmationPanel SimpleFileBrowser.FileBrowser::deleteConfirmationPanel
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C * ___deleteConfirmationPanel_79;
	// SimpleFileBrowser.FileBrowserCursorHandler SimpleFileBrowser.FileBrowser::resizeCursorHandler
	FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C * ___resizeCursorHandler_80;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rectTransform_81;
	// UnityEngine.Canvas SimpleFileBrowser.FileBrowser::canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___canvas_82;
	// System.IO.FileAttributes SimpleFileBrowser.FileBrowser::ignoredFileAttributes
	int32_t ___ignoredFileAttributes_83;
	// SimpleFileBrowser.FileSystemEntry[] SimpleFileBrowser.FileBrowser::allFileEntries
	FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3* ___allFileEntries_84;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry> SimpleFileBrowser.FileBrowser::validFileEntries
	List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * ___validFileEntries_85;
	// System.Collections.Generic.List`1<System.Int32> SimpleFileBrowser.FileBrowser::selectedFileEntries
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___selectedFileEntries_86;
	// System.Collections.Generic.List`1<System.String> SimpleFileBrowser.FileBrowser::pendingFileEntrySelection
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___pendingFileEntrySelection_87;
	// System.Int32 SimpleFileBrowser.FileBrowser::multiSelectionPivotFileEntry
	int32_t ___multiSelectionPivotFileEntry_88;
	// System.Text.StringBuilder SimpleFileBrowser.FileBrowser::multiSelectionFilenameBuilder
	StringBuilder_t * ___multiSelectionFilenameBuilder_89;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowser/Filter> SimpleFileBrowser.FileBrowser::filters
	List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A * ___filters_90;
	// SimpleFileBrowser.FileBrowser/Filter SimpleFileBrowser.FileBrowser::allFilesFilter
	Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * ___allFilesFilter_91;
	// System.Boolean SimpleFileBrowser.FileBrowser::showAllFilesFilter
	bool ___showAllFilesFilter_92;
	// System.String SimpleFileBrowser.FileBrowser::defaultInitialPath
	String_t* ___defaultInitialPath_93;
	// System.Int32 SimpleFileBrowser.FileBrowser::currentPathIndex
	int32_t ___currentPathIndex_94;
	// System.Collections.Generic.List`1<System.String> SimpleFileBrowser.FileBrowser::pathsFollowed
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___pathsFollowed_95;
	// System.Collections.Generic.HashSet`1<System.Char> SimpleFileBrowser.FileBrowser::invalidFilenameChars
	HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 * ___invalidFilenameChars_96;
	// System.Single SimpleFileBrowser.FileBrowser::drivesNextRefreshTime
	float ___drivesNextRefreshTime_97;
	// System.String[] SimpleFileBrowser.FileBrowser::driveQuickLinks
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___driveQuickLinks_98;
	// System.Int32 SimpleFileBrowser.FileBrowser::numberOfDriveQuickLinks
	int32_t ___numberOfDriveQuickLinks_99;
	// System.Boolean SimpleFileBrowser.FileBrowser::canvasDimensionsChanged
	bool ___canvasDimensionsChanged_100;
	// UnityEngine.EventSystems.PointerEventData SimpleFileBrowser.FileBrowser::nullPointerEventData
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___nullPointerEventData_101;
	// System.String SimpleFileBrowser.FileBrowser::m_currentPath
	String_t* ___m_currentPath_102;
	// System.String SimpleFileBrowser.FileBrowser::m_searchString
	String_t* ___m_searchString_103;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_acceptNonExistingFilename
	bool ___m_acceptNonExistingFilename_104;
	// SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser::m_pickerMode
	int32_t ___m_pickerMode_105;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_allowMultiSelection
	bool ___m_allowMultiSelection_106;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_multiSelectionToggleSelectionMode
	bool ___m_multiSelectionToggleSelectionMode_107;
	// SimpleFileBrowser.FileBrowser/OnSuccess SimpleFileBrowser.FileBrowser::onSuccess
	OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * ___onSuccess_108;
	// SimpleFileBrowser.FileBrowser/OnCancel SimpleFileBrowser.FileBrowser::onCancel
	OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * ___onCancel_109;

public:
	inline static int32_t get_offset_of_normalFileColor_17() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___normalFileColor_17)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_normalFileColor_17() const { return ___normalFileColor_17; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_normalFileColor_17() { return &___normalFileColor_17; }
	inline void set_normalFileColor_17(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___normalFileColor_17 = value;
	}

	inline static int32_t get_offset_of_hoveredFileColor_18() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___hoveredFileColor_18)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_hoveredFileColor_18() const { return ___hoveredFileColor_18; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_hoveredFileColor_18() { return &___hoveredFileColor_18; }
	inline void set_hoveredFileColor_18(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___hoveredFileColor_18 = value;
	}

	inline static int32_t get_offset_of_selectedFileColor_19() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___selectedFileColor_19)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_selectedFileColor_19() const { return ___selectedFileColor_19; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_selectedFileColor_19() { return &___selectedFileColor_19; }
	inline void set_selectedFileColor_19(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___selectedFileColor_19 = value;
	}

	inline static int32_t get_offset_of_wrongFilenameColor_20() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___wrongFilenameColor_20)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_wrongFilenameColor_20() const { return ___wrongFilenameColor_20; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_wrongFilenameColor_20() { return &___wrongFilenameColor_20; }
	inline void set_wrongFilenameColor_20(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___wrongFilenameColor_20 = value;
	}

	inline static int32_t get_offset_of_minWidth_21() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___minWidth_21)); }
	inline int32_t get_minWidth_21() const { return ___minWidth_21; }
	inline int32_t* get_address_of_minWidth_21() { return &___minWidth_21; }
	inline void set_minWidth_21(int32_t value)
	{
		___minWidth_21 = value;
	}

	inline static int32_t get_offset_of_minHeight_22() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___minHeight_22)); }
	inline int32_t get_minHeight_22() const { return ___minHeight_22; }
	inline int32_t* get_address_of_minHeight_22() { return &___minHeight_22; }
	inline void set_minHeight_22(int32_t value)
	{
		___minHeight_22 = value;
	}

	inline static int32_t get_offset_of_narrowScreenWidth_23() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___narrowScreenWidth_23)); }
	inline float get_narrowScreenWidth_23() const { return ___narrowScreenWidth_23; }
	inline float* get_address_of_narrowScreenWidth_23() { return &___narrowScreenWidth_23; }
	inline void set_narrowScreenWidth_23(float value)
	{
		___narrowScreenWidth_23 = value;
	}

	inline static int32_t get_offset_of_quickLinksMaxWidthPercentage_24() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinksMaxWidthPercentage_24)); }
	inline float get_quickLinksMaxWidthPercentage_24() const { return ___quickLinksMaxWidthPercentage_24; }
	inline float* get_address_of_quickLinksMaxWidthPercentage_24() { return &___quickLinksMaxWidthPercentage_24; }
	inline void set_quickLinksMaxWidthPercentage_24(float value)
	{
		___quickLinksMaxWidthPercentage_24 = value;
	}

	inline static int32_t get_offset_of_sortFilesByName_25() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___sortFilesByName_25)); }
	inline bool get_sortFilesByName_25() const { return ___sortFilesByName_25; }
	inline bool* get_address_of_sortFilesByName_25() { return &___sortFilesByName_25; }
	inline void set_sortFilesByName_25(bool value)
	{
		___sortFilesByName_25 = value;
	}

	inline static int32_t get_offset_of_excludeExtensions_26() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___excludeExtensions_26)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_excludeExtensions_26() const { return ___excludeExtensions_26; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_excludeExtensions_26() { return &___excludeExtensions_26; }
	inline void set_excludeExtensions_26(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___excludeExtensions_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___excludeExtensions_26), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinks_27() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinks_27)); }
	inline QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC* get_quickLinks_27() const { return ___quickLinks_27; }
	inline QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC** get_address_of_quickLinks_27() { return &___quickLinks_27; }
	inline void set_quickLinks_27(QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC* value)
	{
		___quickLinks_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickLinks_27), (void*)value);
	}

	inline static int32_t get_offset_of_excludedExtensionsSet_29() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___excludedExtensionsSet_29)); }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * get_excludedExtensionsSet_29() const { return ___excludedExtensionsSet_29; }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 ** get_address_of_excludedExtensionsSet_29() { return &___excludedExtensionsSet_29; }
	inline void set_excludedExtensionsSet_29(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * value)
	{
		___excludedExtensionsSet_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___excludedExtensionsSet_29), (void*)value);
	}

	inline static int32_t get_offset_of_generateQuickLinksForDrives_30() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___generateQuickLinksForDrives_30)); }
	inline bool get_generateQuickLinksForDrives_30() const { return ___generateQuickLinksForDrives_30; }
	inline bool* get_address_of_generateQuickLinksForDrives_30() { return &___generateQuickLinksForDrives_30; }
	inline void set_generateQuickLinksForDrives_30(bool value)
	{
		___generateQuickLinksForDrives_30 = value;
	}

	inline static int32_t get_offset_of_contextMenuShowDeleteButton_31() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___contextMenuShowDeleteButton_31)); }
	inline bool get_contextMenuShowDeleteButton_31() const { return ___contextMenuShowDeleteButton_31; }
	inline bool* get_address_of_contextMenuShowDeleteButton_31() { return &___contextMenuShowDeleteButton_31; }
	inline void set_contextMenuShowDeleteButton_31(bool value)
	{
		___contextMenuShowDeleteButton_31 = value;
	}

	inline static int32_t get_offset_of_contextMenuShowRenameButton_32() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___contextMenuShowRenameButton_32)); }
	inline bool get_contextMenuShowRenameButton_32() const { return ___contextMenuShowRenameButton_32; }
	inline bool* get_address_of_contextMenuShowRenameButton_32() { return &___contextMenuShowRenameButton_32; }
	inline void set_contextMenuShowRenameButton_32(bool value)
	{
		___contextMenuShowRenameButton_32 = value;
	}

	inline static int32_t get_offset_of_showResizeCursor_33() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___showResizeCursor_33)); }
	inline bool get_showResizeCursor_33() const { return ___showResizeCursor_33; }
	inline bool* get_address_of_showResizeCursor_33() { return &___showResizeCursor_33; }
	inline void set_showResizeCursor_33(bool value)
	{
		___showResizeCursor_33 = value;
	}

	inline static int32_t get_offset_of_folderIcon_34() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___folderIcon_34)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_folderIcon_34() const { return ___folderIcon_34; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_folderIcon_34() { return &___folderIcon_34; }
	inline void set_folderIcon_34(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___folderIcon_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___folderIcon_34), (void*)value);
	}

	inline static int32_t get_offset_of_driveIcon_35() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___driveIcon_35)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_driveIcon_35() const { return ___driveIcon_35; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_driveIcon_35() { return &___driveIcon_35; }
	inline void set_driveIcon_35(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___driveIcon_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___driveIcon_35), (void*)value);
	}

	inline static int32_t get_offset_of_defaultIcon_36() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___defaultIcon_36)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_defaultIcon_36() const { return ___defaultIcon_36; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_defaultIcon_36() { return &___defaultIcon_36; }
	inline void set_defaultIcon_36(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___defaultIcon_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultIcon_36), (void*)value);
	}

	inline static int32_t get_offset_of_filetypeIcons_37() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filetypeIcons_37)); }
	inline FiletypeIconU5BU5D_tCB87AC1C627D7517B2694613417FDB428E27F395* get_filetypeIcons_37() const { return ___filetypeIcons_37; }
	inline FiletypeIconU5BU5D_tCB87AC1C627D7517B2694613417FDB428E27F395** get_address_of_filetypeIcons_37() { return &___filetypeIcons_37; }
	inline void set_filetypeIcons_37(FiletypeIconU5BU5D_tCB87AC1C627D7517B2694613417FDB428E27F395* value)
	{
		___filetypeIcons_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filetypeIcons_37), (void*)value);
	}

	inline static int32_t get_offset_of_filetypeToIcon_38() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filetypeToIcon_38)); }
	inline Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D * get_filetypeToIcon_38() const { return ___filetypeToIcon_38; }
	inline Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D ** get_address_of_filetypeToIcon_38() { return &___filetypeToIcon_38; }
	inline void set_filetypeToIcon_38(Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D * value)
	{
		___filetypeToIcon_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filetypeToIcon_38), (void*)value);
	}

	inline static int32_t get_offset_of_multiSelectionToggleOffIcon_39() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___multiSelectionToggleOffIcon_39)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_multiSelectionToggleOffIcon_39() const { return ___multiSelectionToggleOffIcon_39; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_multiSelectionToggleOffIcon_39() { return &___multiSelectionToggleOffIcon_39; }
	inline void set_multiSelectionToggleOffIcon_39(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___multiSelectionToggleOffIcon_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiSelectionToggleOffIcon_39), (void*)value);
	}

	inline static int32_t get_offset_of_multiSelectionToggleOnIcon_40() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___multiSelectionToggleOnIcon_40)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_multiSelectionToggleOnIcon_40() const { return ___multiSelectionToggleOnIcon_40; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_multiSelectionToggleOnIcon_40() { return &___multiSelectionToggleOnIcon_40; }
	inline void set_multiSelectionToggleOnIcon_40(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___multiSelectionToggleOnIcon_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiSelectionToggleOnIcon_40), (void*)value);
	}

	inline static int32_t get_offset_of_window_41() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___window_41)); }
	inline FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 * get_window_41() const { return ___window_41; }
	inline FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 ** get_address_of_window_41() { return &___window_41; }
	inline void set_window_41(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 * value)
	{
		___window_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___window_41), (void*)value);
	}

	inline static int32_t get_offset_of_windowTR_42() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___windowTR_42)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_windowTR_42() const { return ___windowTR_42; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_windowTR_42() { return &___windowTR_42; }
	inline void set_windowTR_42(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___windowTR_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___windowTR_42), (void*)value);
	}

	inline static int32_t get_offset_of_topViewNarrowScreen_43() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___topViewNarrowScreen_43)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_topViewNarrowScreen_43() const { return ___topViewNarrowScreen_43; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_topViewNarrowScreen_43() { return &___topViewNarrowScreen_43; }
	inline void set_topViewNarrowScreen_43(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___topViewNarrowScreen_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___topViewNarrowScreen_43), (void*)value);
	}

	inline static int32_t get_offset_of_middleView_44() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleView_44)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleView_44() const { return ___middleView_44; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleView_44() { return &___middleView_44; }
	inline void set_middleView_44(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleView_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleView_44), (void*)value);
	}

	inline static int32_t get_offset_of_middleViewOriginalPosition_45() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewOriginalPosition_45)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_middleViewOriginalPosition_45() const { return ___middleViewOriginalPosition_45; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_middleViewOriginalPosition_45() { return &___middleViewOriginalPosition_45; }
	inline void set_middleViewOriginalPosition_45(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___middleViewOriginalPosition_45 = value;
	}

	inline static int32_t get_offset_of_middleViewOriginalSize_46() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewOriginalSize_46)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_middleViewOriginalSize_46() const { return ___middleViewOriginalSize_46; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_middleViewOriginalSize_46() { return &___middleViewOriginalSize_46; }
	inline void set_middleViewOriginalSize_46(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___middleViewOriginalSize_46 = value;
	}

	inline static int32_t get_offset_of_middleViewQuickLinks_47() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewQuickLinks_47)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleViewQuickLinks_47() const { return ___middleViewQuickLinks_47; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleViewQuickLinks_47() { return &___middleViewQuickLinks_47; }
	inline void set_middleViewQuickLinks_47(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleViewQuickLinks_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleViewQuickLinks_47), (void*)value);
	}

	inline static int32_t get_offset_of_middleViewQuickLinksOriginalSize_48() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewQuickLinksOriginalSize_48)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_middleViewQuickLinksOriginalSize_48() const { return ___middleViewQuickLinksOriginalSize_48; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_middleViewQuickLinksOriginalSize_48() { return &___middleViewQuickLinksOriginalSize_48; }
	inline void set_middleViewQuickLinksOriginalSize_48(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___middleViewQuickLinksOriginalSize_48 = value;
	}

	inline static int32_t get_offset_of_middleViewFiles_49() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewFiles_49)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleViewFiles_49() const { return ___middleViewFiles_49; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleViewFiles_49() { return &___middleViewFiles_49; }
	inline void set_middleViewFiles_49(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleViewFiles_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleViewFiles_49), (void*)value);
	}

	inline static int32_t get_offset_of_middleViewSeparator_50() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewSeparator_50)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleViewSeparator_50() const { return ___middleViewSeparator_50; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleViewSeparator_50() { return &___middleViewSeparator_50; }
	inline void set_middleViewSeparator_50(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleViewSeparator_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleViewSeparator_50), (void*)value);
	}

	inline static int32_t get_offset_of_itemPrefab_51() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___itemPrefab_51)); }
	inline FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 * get_itemPrefab_51() const { return ___itemPrefab_51; }
	inline FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 ** get_address_of_itemPrefab_51() { return &___itemPrefab_51; }
	inline void set_itemPrefab_51(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 * value)
	{
		___itemPrefab_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemPrefab_51), (void*)value);
	}

	inline static int32_t get_offset_of_allItems_52() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allItems_52)); }
	inline List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 * get_allItems_52() const { return ___allItems_52; }
	inline List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 ** get_address_of_allItems_52() { return &___allItems_52; }
	inline void set_allItems_52(List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 * value)
	{
		___allItems_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allItems_52), (void*)value);
	}

	inline static int32_t get_offset_of_itemHeight_53() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___itemHeight_53)); }
	inline float get_itemHeight_53() const { return ___itemHeight_53; }
	inline float* get_address_of_itemHeight_53() { return &___itemHeight_53; }
	inline void set_itemHeight_53(float value)
	{
		___itemHeight_53 = value;
	}

	inline static int32_t get_offset_of_quickLinkPrefab_54() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinkPrefab_54)); }
	inline FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C * get_quickLinkPrefab_54() const { return ___quickLinkPrefab_54; }
	inline FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C ** get_address_of_quickLinkPrefab_54() { return &___quickLinkPrefab_54; }
	inline void set_quickLinkPrefab_54(FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C * value)
	{
		___quickLinkPrefab_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickLinkPrefab_54), (void*)value);
	}

	inline static int32_t get_offset_of_allQuickLinks_55() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allQuickLinks_55)); }
	inline List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F * get_allQuickLinks_55() const { return ___allQuickLinks_55; }
	inline List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F ** get_address_of_allQuickLinks_55() { return &___allQuickLinks_55; }
	inline void set_allQuickLinks_55(List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F * value)
	{
		___allQuickLinks_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allQuickLinks_55), (void*)value);
	}

	inline static int32_t get_offset_of_titleText_56() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___titleText_56)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_titleText_56() const { return ___titleText_56; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_titleText_56() { return &___titleText_56; }
	inline void set_titleText_56(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___titleText_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___titleText_56), (void*)value);
	}

	inline static int32_t get_offset_of_backButton_57() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___backButton_57)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_backButton_57() const { return ___backButton_57; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_backButton_57() { return &___backButton_57; }
	inline void set_backButton_57(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___backButton_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backButton_57), (void*)value);
	}

	inline static int32_t get_offset_of_forwardButton_58() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___forwardButton_58)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_forwardButton_58() const { return ___forwardButton_58; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_forwardButton_58() { return &___forwardButton_58; }
	inline void set_forwardButton_58(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___forwardButton_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___forwardButton_58), (void*)value);
	}

	inline static int32_t get_offset_of_upButton_59() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___upButton_59)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_upButton_59() const { return ___upButton_59; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_upButton_59() { return &___upButton_59; }
	inline void set_upButton_59(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___upButton_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___upButton_59), (void*)value);
	}

	inline static int32_t get_offset_of_pathInputField_60() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathInputField_60)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_pathInputField_60() const { return ___pathInputField_60; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_pathInputField_60() { return &___pathInputField_60; }
	inline void set_pathInputField_60(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___pathInputField_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathInputField_60), (void*)value);
	}

	inline static int32_t get_offset_of_pathInputFieldSlotTop_61() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathInputFieldSlotTop_61)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_pathInputFieldSlotTop_61() const { return ___pathInputFieldSlotTop_61; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_pathInputFieldSlotTop_61() { return &___pathInputFieldSlotTop_61; }
	inline void set_pathInputFieldSlotTop_61(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___pathInputFieldSlotTop_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathInputFieldSlotTop_61), (void*)value);
	}

	inline static int32_t get_offset_of_pathInputFieldSlotBottom_62() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathInputFieldSlotBottom_62)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_pathInputFieldSlotBottom_62() const { return ___pathInputFieldSlotBottom_62; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_pathInputFieldSlotBottom_62() { return &___pathInputFieldSlotBottom_62; }
	inline void set_pathInputFieldSlotBottom_62(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___pathInputFieldSlotBottom_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathInputFieldSlotBottom_62), (void*)value);
	}

	inline static int32_t get_offset_of_searchInputField_63() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___searchInputField_63)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_searchInputField_63() const { return ___searchInputField_63; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_searchInputField_63() { return &___searchInputField_63; }
	inline void set_searchInputField_63(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___searchInputField_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___searchInputField_63), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinksContainer_64() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinksContainer_64)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_quickLinksContainer_64() const { return ___quickLinksContainer_64; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_quickLinksContainer_64() { return &___quickLinksContainer_64; }
	inline void set_quickLinksContainer_64(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___quickLinksContainer_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickLinksContainer_64), (void*)value);
	}

	inline static int32_t get_offset_of_filesContainer_65() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filesContainer_65)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_filesContainer_65() const { return ___filesContainer_65; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_filesContainer_65() { return &___filesContainer_65; }
	inline void set_filesContainer_65(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___filesContainer_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filesContainer_65), (void*)value);
	}

	inline static int32_t get_offset_of_filesScrollRect_66() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filesScrollRect_66)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get_filesScrollRect_66() const { return ___filesScrollRect_66; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of_filesScrollRect_66() { return &___filesScrollRect_66; }
	inline void set_filesScrollRect_66(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		___filesScrollRect_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filesScrollRect_66), (void*)value);
	}

	inline static int32_t get_offset_of_listView_67() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___listView_67)); }
	inline RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * get_listView_67() const { return ___listView_67; }
	inline RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 ** get_address_of_listView_67() { return &___listView_67; }
	inline void set_listView_67(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * value)
	{
		___listView_67 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___listView_67), (void*)value);
	}

	inline static int32_t get_offset_of_filenameInputField_68() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filenameInputField_68)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_filenameInputField_68() const { return ___filenameInputField_68; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_filenameInputField_68() { return &___filenameInputField_68; }
	inline void set_filenameInputField_68(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___filenameInputField_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filenameInputField_68), (void*)value);
	}

	inline static int32_t get_offset_of_filenameInputFieldOverlayText_69() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filenameInputFieldOverlayText_69)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_filenameInputFieldOverlayText_69() const { return ___filenameInputFieldOverlayText_69; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_filenameInputFieldOverlayText_69() { return &___filenameInputFieldOverlayText_69; }
	inline void set_filenameInputFieldOverlayText_69(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___filenameInputFieldOverlayText_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filenameInputFieldOverlayText_69), (void*)value);
	}

	inline static int32_t get_offset_of_filenameImage_70() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filenameImage_70)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_filenameImage_70() const { return ___filenameImage_70; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_filenameImage_70() { return &___filenameImage_70; }
	inline void set_filenameImage_70(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___filenameImage_70 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filenameImage_70), (void*)value);
	}

	inline static int32_t get_offset_of_filtersDropdown_71() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filtersDropdown_71)); }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * get_filtersDropdown_71() const { return ___filtersDropdown_71; }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 ** get_address_of_filtersDropdown_71() { return &___filtersDropdown_71; }
	inline void set_filtersDropdown_71(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * value)
	{
		___filtersDropdown_71 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filtersDropdown_71), (void*)value);
	}

	inline static int32_t get_offset_of_filtersDropdownContainer_72() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filtersDropdownContainer_72)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_filtersDropdownContainer_72() const { return ___filtersDropdownContainer_72; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_filtersDropdownContainer_72() { return &___filtersDropdownContainer_72; }
	inline void set_filtersDropdownContainer_72(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___filtersDropdownContainer_72 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filtersDropdownContainer_72), (void*)value);
	}

	inline static int32_t get_offset_of_filterItemTemplate_73() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filterItemTemplate_73)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_filterItemTemplate_73() const { return ___filterItemTemplate_73; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_filterItemTemplate_73() { return &___filterItemTemplate_73; }
	inline void set_filterItemTemplate_73(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___filterItemTemplate_73 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filterItemTemplate_73), (void*)value);
	}

	inline static int32_t get_offset_of_showHiddenFilesToggle_74() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___showHiddenFilesToggle_74)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get_showHiddenFilesToggle_74() const { return ___showHiddenFilesToggle_74; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of_showHiddenFilesToggle_74() { return &___showHiddenFilesToggle_74; }
	inline void set_showHiddenFilesToggle_74(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		___showHiddenFilesToggle_74 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___showHiddenFilesToggle_74), (void*)value);
	}

	inline static int32_t get_offset_of_submitButtonText_75() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___submitButtonText_75)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_submitButtonText_75() const { return ___submitButtonText_75; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_submitButtonText_75() { return &___submitButtonText_75; }
	inline void set_submitButtonText_75(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___submitButtonText_75 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submitButtonText_75), (void*)value);
	}

	inline static int32_t get_offset_of_moreOptionsContextMenuPosition_76() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___moreOptionsContextMenuPosition_76)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_moreOptionsContextMenuPosition_76() const { return ___moreOptionsContextMenuPosition_76; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_moreOptionsContextMenuPosition_76() { return &___moreOptionsContextMenuPosition_76; }
	inline void set_moreOptionsContextMenuPosition_76(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___moreOptionsContextMenuPosition_76 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moreOptionsContextMenuPosition_76), (void*)value);
	}

	inline static int32_t get_offset_of_renameItem_77() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___renameItem_77)); }
	inline FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * get_renameItem_77() const { return ___renameItem_77; }
	inline FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 ** get_address_of_renameItem_77() { return &___renameItem_77; }
	inline void set_renameItem_77(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * value)
	{
		___renameItem_77 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___renameItem_77), (void*)value);
	}

	inline static int32_t get_offset_of_contextMenu_78() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___contextMenu_78)); }
	inline FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC * get_contextMenu_78() const { return ___contextMenu_78; }
	inline FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC ** get_address_of_contextMenu_78() { return &___contextMenu_78; }
	inline void set_contextMenu_78(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC * value)
	{
		___contextMenu_78 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contextMenu_78), (void*)value);
	}

	inline static int32_t get_offset_of_deleteConfirmationPanel_79() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___deleteConfirmationPanel_79)); }
	inline FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C * get_deleteConfirmationPanel_79() const { return ___deleteConfirmationPanel_79; }
	inline FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C ** get_address_of_deleteConfirmationPanel_79() { return &___deleteConfirmationPanel_79; }
	inline void set_deleteConfirmationPanel_79(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C * value)
	{
		___deleteConfirmationPanel_79 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deleteConfirmationPanel_79), (void*)value);
	}

	inline static int32_t get_offset_of_resizeCursorHandler_80() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___resizeCursorHandler_80)); }
	inline FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C * get_resizeCursorHandler_80() const { return ___resizeCursorHandler_80; }
	inline FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C ** get_address_of_resizeCursorHandler_80() { return &___resizeCursorHandler_80; }
	inline void set_resizeCursorHandler_80(FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C * value)
	{
		___resizeCursorHandler_80 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resizeCursorHandler_80), (void*)value);
	}

	inline static int32_t get_offset_of_rectTransform_81() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___rectTransform_81)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_rectTransform_81() const { return ___rectTransform_81; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_rectTransform_81() { return &___rectTransform_81; }
	inline void set_rectTransform_81(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___rectTransform_81 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rectTransform_81), (void*)value);
	}

	inline static int32_t get_offset_of_canvas_82() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___canvas_82)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_canvas_82() const { return ___canvas_82; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_canvas_82() { return &___canvas_82; }
	inline void set_canvas_82(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___canvas_82 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_82), (void*)value);
	}

	inline static int32_t get_offset_of_ignoredFileAttributes_83() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___ignoredFileAttributes_83)); }
	inline int32_t get_ignoredFileAttributes_83() const { return ___ignoredFileAttributes_83; }
	inline int32_t* get_address_of_ignoredFileAttributes_83() { return &___ignoredFileAttributes_83; }
	inline void set_ignoredFileAttributes_83(int32_t value)
	{
		___ignoredFileAttributes_83 = value;
	}

	inline static int32_t get_offset_of_allFileEntries_84() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allFileEntries_84)); }
	inline FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3* get_allFileEntries_84() const { return ___allFileEntries_84; }
	inline FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3** get_address_of_allFileEntries_84() { return &___allFileEntries_84; }
	inline void set_allFileEntries_84(FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3* value)
	{
		___allFileEntries_84 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allFileEntries_84), (void*)value);
	}

	inline static int32_t get_offset_of_validFileEntries_85() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___validFileEntries_85)); }
	inline List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * get_validFileEntries_85() const { return ___validFileEntries_85; }
	inline List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB ** get_address_of_validFileEntries_85() { return &___validFileEntries_85; }
	inline void set_validFileEntries_85(List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * value)
	{
		___validFileEntries_85 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___validFileEntries_85), (void*)value);
	}

	inline static int32_t get_offset_of_selectedFileEntries_86() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___selectedFileEntries_86)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_selectedFileEntries_86() const { return ___selectedFileEntries_86; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_selectedFileEntries_86() { return &___selectedFileEntries_86; }
	inline void set_selectedFileEntries_86(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___selectedFileEntries_86 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedFileEntries_86), (void*)value);
	}

	inline static int32_t get_offset_of_pendingFileEntrySelection_87() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pendingFileEntrySelection_87)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_pendingFileEntrySelection_87() const { return ___pendingFileEntrySelection_87; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_pendingFileEntrySelection_87() { return &___pendingFileEntrySelection_87; }
	inline void set_pendingFileEntrySelection_87(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___pendingFileEntrySelection_87 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pendingFileEntrySelection_87), (void*)value);
	}

	inline static int32_t get_offset_of_multiSelectionPivotFileEntry_88() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___multiSelectionPivotFileEntry_88)); }
	inline int32_t get_multiSelectionPivotFileEntry_88() const { return ___multiSelectionPivotFileEntry_88; }
	inline int32_t* get_address_of_multiSelectionPivotFileEntry_88() { return &___multiSelectionPivotFileEntry_88; }
	inline void set_multiSelectionPivotFileEntry_88(int32_t value)
	{
		___multiSelectionPivotFileEntry_88 = value;
	}

	inline static int32_t get_offset_of_multiSelectionFilenameBuilder_89() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___multiSelectionFilenameBuilder_89)); }
	inline StringBuilder_t * get_multiSelectionFilenameBuilder_89() const { return ___multiSelectionFilenameBuilder_89; }
	inline StringBuilder_t ** get_address_of_multiSelectionFilenameBuilder_89() { return &___multiSelectionFilenameBuilder_89; }
	inline void set_multiSelectionFilenameBuilder_89(StringBuilder_t * value)
	{
		___multiSelectionFilenameBuilder_89 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiSelectionFilenameBuilder_89), (void*)value);
	}

	inline static int32_t get_offset_of_filters_90() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filters_90)); }
	inline List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A * get_filters_90() const { return ___filters_90; }
	inline List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A ** get_address_of_filters_90() { return &___filters_90; }
	inline void set_filters_90(List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A * value)
	{
		___filters_90 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filters_90), (void*)value);
	}

	inline static int32_t get_offset_of_allFilesFilter_91() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allFilesFilter_91)); }
	inline Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * get_allFilesFilter_91() const { return ___allFilesFilter_91; }
	inline Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 ** get_address_of_allFilesFilter_91() { return &___allFilesFilter_91; }
	inline void set_allFilesFilter_91(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * value)
	{
		___allFilesFilter_91 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allFilesFilter_91), (void*)value);
	}

	inline static int32_t get_offset_of_showAllFilesFilter_92() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___showAllFilesFilter_92)); }
	inline bool get_showAllFilesFilter_92() const { return ___showAllFilesFilter_92; }
	inline bool* get_address_of_showAllFilesFilter_92() { return &___showAllFilesFilter_92; }
	inline void set_showAllFilesFilter_92(bool value)
	{
		___showAllFilesFilter_92 = value;
	}

	inline static int32_t get_offset_of_defaultInitialPath_93() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___defaultInitialPath_93)); }
	inline String_t* get_defaultInitialPath_93() const { return ___defaultInitialPath_93; }
	inline String_t** get_address_of_defaultInitialPath_93() { return &___defaultInitialPath_93; }
	inline void set_defaultInitialPath_93(String_t* value)
	{
		___defaultInitialPath_93 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultInitialPath_93), (void*)value);
	}

	inline static int32_t get_offset_of_currentPathIndex_94() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___currentPathIndex_94)); }
	inline int32_t get_currentPathIndex_94() const { return ___currentPathIndex_94; }
	inline int32_t* get_address_of_currentPathIndex_94() { return &___currentPathIndex_94; }
	inline void set_currentPathIndex_94(int32_t value)
	{
		___currentPathIndex_94 = value;
	}

	inline static int32_t get_offset_of_pathsFollowed_95() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathsFollowed_95)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_pathsFollowed_95() const { return ___pathsFollowed_95; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_pathsFollowed_95() { return &___pathsFollowed_95; }
	inline void set_pathsFollowed_95(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___pathsFollowed_95 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathsFollowed_95), (void*)value);
	}

	inline static int32_t get_offset_of_invalidFilenameChars_96() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___invalidFilenameChars_96)); }
	inline HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 * get_invalidFilenameChars_96() const { return ___invalidFilenameChars_96; }
	inline HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 ** get_address_of_invalidFilenameChars_96() { return &___invalidFilenameChars_96; }
	inline void set_invalidFilenameChars_96(HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 * value)
	{
		___invalidFilenameChars_96 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invalidFilenameChars_96), (void*)value);
	}

	inline static int32_t get_offset_of_drivesNextRefreshTime_97() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___drivesNextRefreshTime_97)); }
	inline float get_drivesNextRefreshTime_97() const { return ___drivesNextRefreshTime_97; }
	inline float* get_address_of_drivesNextRefreshTime_97() { return &___drivesNextRefreshTime_97; }
	inline void set_drivesNextRefreshTime_97(float value)
	{
		___drivesNextRefreshTime_97 = value;
	}

	inline static int32_t get_offset_of_driveQuickLinks_98() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___driveQuickLinks_98)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_driveQuickLinks_98() const { return ___driveQuickLinks_98; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_driveQuickLinks_98() { return &___driveQuickLinks_98; }
	inline void set_driveQuickLinks_98(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___driveQuickLinks_98 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___driveQuickLinks_98), (void*)value);
	}

	inline static int32_t get_offset_of_numberOfDriveQuickLinks_99() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___numberOfDriveQuickLinks_99)); }
	inline int32_t get_numberOfDriveQuickLinks_99() const { return ___numberOfDriveQuickLinks_99; }
	inline int32_t* get_address_of_numberOfDriveQuickLinks_99() { return &___numberOfDriveQuickLinks_99; }
	inline void set_numberOfDriveQuickLinks_99(int32_t value)
	{
		___numberOfDriveQuickLinks_99 = value;
	}

	inline static int32_t get_offset_of_canvasDimensionsChanged_100() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___canvasDimensionsChanged_100)); }
	inline bool get_canvasDimensionsChanged_100() const { return ___canvasDimensionsChanged_100; }
	inline bool* get_address_of_canvasDimensionsChanged_100() { return &___canvasDimensionsChanged_100; }
	inline void set_canvasDimensionsChanged_100(bool value)
	{
		___canvasDimensionsChanged_100 = value;
	}

	inline static int32_t get_offset_of_nullPointerEventData_101() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___nullPointerEventData_101)); }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * get_nullPointerEventData_101() const { return ___nullPointerEventData_101; }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 ** get_address_of_nullPointerEventData_101() { return &___nullPointerEventData_101; }
	inline void set_nullPointerEventData_101(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * value)
	{
		___nullPointerEventData_101 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nullPointerEventData_101), (void*)value);
	}

	inline static int32_t get_offset_of_m_currentPath_102() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_currentPath_102)); }
	inline String_t* get_m_currentPath_102() const { return ___m_currentPath_102; }
	inline String_t** get_address_of_m_currentPath_102() { return &___m_currentPath_102; }
	inline void set_m_currentPath_102(String_t* value)
	{
		___m_currentPath_102 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_currentPath_102), (void*)value);
	}

	inline static int32_t get_offset_of_m_searchString_103() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_searchString_103)); }
	inline String_t* get_m_searchString_103() const { return ___m_searchString_103; }
	inline String_t** get_address_of_m_searchString_103() { return &___m_searchString_103; }
	inline void set_m_searchString_103(String_t* value)
	{
		___m_searchString_103 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_searchString_103), (void*)value);
	}

	inline static int32_t get_offset_of_m_acceptNonExistingFilename_104() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_acceptNonExistingFilename_104)); }
	inline bool get_m_acceptNonExistingFilename_104() const { return ___m_acceptNonExistingFilename_104; }
	inline bool* get_address_of_m_acceptNonExistingFilename_104() { return &___m_acceptNonExistingFilename_104; }
	inline void set_m_acceptNonExistingFilename_104(bool value)
	{
		___m_acceptNonExistingFilename_104 = value;
	}

	inline static int32_t get_offset_of_m_pickerMode_105() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_pickerMode_105)); }
	inline int32_t get_m_pickerMode_105() const { return ___m_pickerMode_105; }
	inline int32_t* get_address_of_m_pickerMode_105() { return &___m_pickerMode_105; }
	inline void set_m_pickerMode_105(int32_t value)
	{
		___m_pickerMode_105 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiSelection_106() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_allowMultiSelection_106)); }
	inline bool get_m_allowMultiSelection_106() const { return ___m_allowMultiSelection_106; }
	inline bool* get_address_of_m_allowMultiSelection_106() { return &___m_allowMultiSelection_106; }
	inline void set_m_allowMultiSelection_106(bool value)
	{
		___m_allowMultiSelection_106 = value;
	}

	inline static int32_t get_offset_of_m_multiSelectionToggleSelectionMode_107() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_multiSelectionToggleSelectionMode_107)); }
	inline bool get_m_multiSelectionToggleSelectionMode_107() const { return ___m_multiSelectionToggleSelectionMode_107; }
	inline bool* get_address_of_m_multiSelectionToggleSelectionMode_107() { return &___m_multiSelectionToggleSelectionMode_107; }
	inline void set_m_multiSelectionToggleSelectionMode_107(bool value)
	{
		___m_multiSelectionToggleSelectionMode_107 = value;
	}

	inline static int32_t get_offset_of_onSuccess_108() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___onSuccess_108)); }
	inline OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * get_onSuccess_108() const { return ___onSuccess_108; }
	inline OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C ** get_address_of_onSuccess_108() { return &___onSuccess_108; }
	inline void set_onSuccess_108(OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * value)
	{
		___onSuccess_108 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onSuccess_108), (void*)value);
	}

	inline static int32_t get_offset_of_onCancel_109() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___onCancel_109)); }
	inline OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * get_onCancel_109() const { return ___onCancel_109; }
	inline OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E ** get_address_of_onCancel_109() { return &___onCancel_109; }
	inline void set_onCancel_109(OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * value)
	{
		___onCancel_109 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onCancel_109), (void*)value);
	}
};

struct FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields
{
public:
	// System.Boolean SimpleFileBrowser.FileBrowser::<IsOpen>k__BackingField
	bool ___U3CIsOpenU3Ek__BackingField_6;
	// System.Boolean SimpleFileBrowser.FileBrowser::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_7;
	// System.String[] SimpleFileBrowser.FileBrowser::<Result>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3CResultU3Ek__BackingField_8;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_askPermissions
	bool ___m_askPermissions_9;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_singleClickMode
	bool ___m_singleClickMode_10;
	// System.Single SimpleFileBrowser.FileBrowser::m_drivesRefreshInterval
	float ___m_drivesRefreshInterval_11;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_displayHiddenFilesToggle
	bool ___m_displayHiddenFilesToggle_12;
	// System.String SimpleFileBrowser.FileBrowser::m_allFilesFilterText
	String_t* ___m_allFilesFilterText_13;
	// System.String SimpleFileBrowser.FileBrowser::m_foldersFilterText
	String_t* ___m_foldersFilterText_14;
	// System.String SimpleFileBrowser.FileBrowser::m_pickFolderQuickLinkText
	String_t* ___m_pickFolderQuickLinkText_15;
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser::m_instance
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___m_instance_16;
	// System.Boolean SimpleFileBrowser.FileBrowser::quickLinksInitialized
	bool ___quickLinksInitialized_28;

public:
	inline static int32_t get_offset_of_U3CIsOpenU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___U3CIsOpenU3Ek__BackingField_6)); }
	inline bool get_U3CIsOpenU3Ek__BackingField_6() const { return ___U3CIsOpenU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsOpenU3Ek__BackingField_6() { return &___U3CIsOpenU3Ek__BackingField_6; }
	inline void set_U3CIsOpenU3Ek__BackingField_6(bool value)
	{
		___U3CIsOpenU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___U3CSuccessU3Ek__BackingField_7)); }
	inline bool get_U3CSuccessU3Ek__BackingField_7() const { return ___U3CSuccessU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_7() { return &___U3CSuccessU3Ek__BackingField_7; }
	inline void set_U3CSuccessU3Ek__BackingField_7(bool value)
	{
		___U3CSuccessU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___U3CResultU3Ek__BackingField_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3CResultU3Ek__BackingField_8() const { return ___U3CResultU3Ek__BackingField_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3CResultU3Ek__BackingField_8() { return &___U3CResultU3Ek__BackingField_8; }
	inline void set_U3CResultU3Ek__BackingField_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3CResultU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CResultU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_askPermissions_9() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_askPermissions_9)); }
	inline bool get_m_askPermissions_9() const { return ___m_askPermissions_9; }
	inline bool* get_address_of_m_askPermissions_9() { return &___m_askPermissions_9; }
	inline void set_m_askPermissions_9(bool value)
	{
		___m_askPermissions_9 = value;
	}

	inline static int32_t get_offset_of_m_singleClickMode_10() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_singleClickMode_10)); }
	inline bool get_m_singleClickMode_10() const { return ___m_singleClickMode_10; }
	inline bool* get_address_of_m_singleClickMode_10() { return &___m_singleClickMode_10; }
	inline void set_m_singleClickMode_10(bool value)
	{
		___m_singleClickMode_10 = value;
	}

	inline static int32_t get_offset_of_m_drivesRefreshInterval_11() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_drivesRefreshInterval_11)); }
	inline float get_m_drivesRefreshInterval_11() const { return ___m_drivesRefreshInterval_11; }
	inline float* get_address_of_m_drivesRefreshInterval_11() { return &___m_drivesRefreshInterval_11; }
	inline void set_m_drivesRefreshInterval_11(float value)
	{
		___m_drivesRefreshInterval_11 = value;
	}

	inline static int32_t get_offset_of_m_displayHiddenFilesToggle_12() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_displayHiddenFilesToggle_12)); }
	inline bool get_m_displayHiddenFilesToggle_12() const { return ___m_displayHiddenFilesToggle_12; }
	inline bool* get_address_of_m_displayHiddenFilesToggle_12() { return &___m_displayHiddenFilesToggle_12; }
	inline void set_m_displayHiddenFilesToggle_12(bool value)
	{
		___m_displayHiddenFilesToggle_12 = value;
	}

	inline static int32_t get_offset_of_m_allFilesFilterText_13() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_allFilesFilterText_13)); }
	inline String_t* get_m_allFilesFilterText_13() const { return ___m_allFilesFilterText_13; }
	inline String_t** get_address_of_m_allFilesFilterText_13() { return &___m_allFilesFilterText_13; }
	inline void set_m_allFilesFilterText_13(String_t* value)
	{
		___m_allFilesFilterText_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_allFilesFilterText_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_foldersFilterText_14() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_foldersFilterText_14)); }
	inline String_t* get_m_foldersFilterText_14() const { return ___m_foldersFilterText_14; }
	inline String_t** get_address_of_m_foldersFilterText_14() { return &___m_foldersFilterText_14; }
	inline void set_m_foldersFilterText_14(String_t* value)
	{
		___m_foldersFilterText_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_foldersFilterText_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_pickFolderQuickLinkText_15() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_pickFolderQuickLinkText_15)); }
	inline String_t* get_m_pickFolderQuickLinkText_15() const { return ___m_pickFolderQuickLinkText_15; }
	inline String_t** get_address_of_m_pickFolderQuickLinkText_15() { return &___m_pickFolderQuickLinkText_15; }
	inline void set_m_pickFolderQuickLinkText_15(String_t* value)
	{
		___m_pickFolderQuickLinkText_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_pickFolderQuickLinkText_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_instance_16() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_instance_16)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_m_instance_16() const { return ___m_instance_16; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_m_instance_16() { return &___m_instance_16; }
	inline void set_m_instance_16(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___m_instance_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_instance_16), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinksInitialized_28() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___quickLinksInitialized_28)); }
	inline bool get_quickLinksInitialized_28() const { return ___quickLinksInitialized_28; }
	inline bool* get_address_of_quickLinksInitialized_28() { return &___quickLinksInitialized_28; }
	inline void set_quickLinksInitialized_28(bool value)
	{
		___quickLinksInitialized_28 = value;
	}
};


// SimpleFileBrowser.FileBrowserContextMenu
struct  FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowserContextMenu::fileBrowser
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___fileBrowser_4;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowserContextMenu::rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rectTransform_5;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowserContextMenu::selectAllButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___selectAllButton_6;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowserContextMenu::deselectAllButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___deselectAllButton_7;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowserContextMenu::deleteButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___deleteButton_8;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowserContextMenu::renameButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___renameButton_9;
	// UnityEngine.GameObject SimpleFileBrowser.FileBrowserContextMenu::selectAllButtonSeparator
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___selectAllButtonSeparator_10;
	// System.Single SimpleFileBrowser.FileBrowserContextMenu::minDistanceToEdges
	float ___minDistanceToEdges_11;

public:
	inline static int32_t get_offset_of_fileBrowser_4() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___fileBrowser_4)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_fileBrowser_4() const { return ___fileBrowser_4; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_fileBrowser_4() { return &___fileBrowser_4; }
	inline void set_fileBrowser_4(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___fileBrowser_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileBrowser_4), (void*)value);
	}

	inline static int32_t get_offset_of_rectTransform_5() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___rectTransform_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_rectTransform_5() const { return ___rectTransform_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_rectTransform_5() { return &___rectTransform_5; }
	inline void set_rectTransform_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rectTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_selectAllButton_6() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___selectAllButton_6)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_selectAllButton_6() const { return ___selectAllButton_6; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_selectAllButton_6() { return &___selectAllButton_6; }
	inline void set_selectAllButton_6(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___selectAllButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectAllButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_deselectAllButton_7() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___deselectAllButton_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_deselectAllButton_7() const { return ___deselectAllButton_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_deselectAllButton_7() { return &___deselectAllButton_7; }
	inline void set_deselectAllButton_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___deselectAllButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deselectAllButton_7), (void*)value);
	}

	inline static int32_t get_offset_of_deleteButton_8() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___deleteButton_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_deleteButton_8() const { return ___deleteButton_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_deleteButton_8() { return &___deleteButton_8; }
	inline void set_deleteButton_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___deleteButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deleteButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_renameButton_9() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___renameButton_9)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_renameButton_9() const { return ___renameButton_9; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_renameButton_9() { return &___renameButton_9; }
	inline void set_renameButton_9(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___renameButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___renameButton_9), (void*)value);
	}

	inline static int32_t get_offset_of_selectAllButtonSeparator_10() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___selectAllButtonSeparator_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_selectAllButtonSeparator_10() const { return ___selectAllButtonSeparator_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_selectAllButtonSeparator_10() { return &___selectAllButtonSeparator_10; }
	inline void set_selectAllButtonSeparator_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___selectAllButtonSeparator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectAllButtonSeparator_10), (void*)value);
	}

	inline static int32_t get_offset_of_minDistanceToEdges_11() { return static_cast<int32_t>(offsetof(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC, ___minDistanceToEdges_11)); }
	inline float get_minDistanceToEdges_11() const { return ___minDistanceToEdges_11; }
	inline float* get_address_of_minDistanceToEdges_11() { return &___minDistanceToEdges_11; }
	inline void set_minDistanceToEdges_11(float value)
	{
		___minDistanceToEdges_11 = value;
	}
};


// SimpleFileBrowser.FileBrowserCursorHandler
struct  FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Texture2D SimpleFileBrowser.FileBrowserCursorHandler::resizeCursor
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___resizeCursor_4;
	// System.Boolean SimpleFileBrowser.FileBrowserCursorHandler::isHovering
	bool ___isHovering_5;
	// System.Boolean SimpleFileBrowser.FileBrowserCursorHandler::isResizing
	bool ___isResizing_6;

public:
	inline static int32_t get_offset_of_resizeCursor_4() { return static_cast<int32_t>(offsetof(FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C, ___resizeCursor_4)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_resizeCursor_4() const { return ___resizeCursor_4; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_resizeCursor_4() { return &___resizeCursor_4; }
	inline void set_resizeCursor_4(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___resizeCursor_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resizeCursor_4), (void*)value);
	}

	inline static int32_t get_offset_of_isHovering_5() { return static_cast<int32_t>(offsetof(FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C, ___isHovering_5)); }
	inline bool get_isHovering_5() const { return ___isHovering_5; }
	inline bool* get_address_of_isHovering_5() { return &___isHovering_5; }
	inline void set_isHovering_5(bool value)
	{
		___isHovering_5 = value;
	}

	inline static int32_t get_offset_of_isResizing_6() { return static_cast<int32_t>(offsetof(FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C, ___isResizing_6)); }
	inline bool get_isResizing_6() const { return ___isResizing_6; }
	inline bool* get_address_of_isResizing_6() { return &___isResizing_6; }
	inline void set_isResizing_6(bool value)
	{
		___isResizing_6 = value;
	}
};


// SimpleFileBrowser.FileBrowserDeleteConfirmationPanel
struct  FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::deletedItems
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___deletedItems_4;
	// UnityEngine.UI.Image[] SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::deletedItemIcons
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___deletedItemIcons_5;
	// UnityEngine.UI.Text[] SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::deletedItemNames
	TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* ___deletedItemNames_6;
	// UnityEngine.GameObject SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::deletedItemsRest
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___deletedItemsRest_7;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::deletedItemsRestLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___deletedItemsRestLabel_8;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::yesButtonTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___yesButtonTransform_9;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::noButtonTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___noButtonTransform_10;
	// System.Single SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::narrowScreenWidth
	float ___narrowScreenWidth_11;
	// SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::onDeletionConfirmed
	OnDeletionConfirmed_t54FFE37F4F1E4680F19B875155B0796C976BE677 * ___onDeletionConfirmed_12;

public:
	inline static int32_t get_offset_of_deletedItems_4() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___deletedItems_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_deletedItems_4() const { return ___deletedItems_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_deletedItems_4() { return &___deletedItems_4; }
	inline void set_deletedItems_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___deletedItems_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deletedItems_4), (void*)value);
	}

	inline static int32_t get_offset_of_deletedItemIcons_5() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___deletedItemIcons_5)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_deletedItemIcons_5() const { return ___deletedItemIcons_5; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_deletedItemIcons_5() { return &___deletedItemIcons_5; }
	inline void set_deletedItemIcons_5(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___deletedItemIcons_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deletedItemIcons_5), (void*)value);
	}

	inline static int32_t get_offset_of_deletedItemNames_6() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___deletedItemNames_6)); }
	inline TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* get_deletedItemNames_6() const { return ___deletedItemNames_6; }
	inline TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F** get_address_of_deletedItemNames_6() { return &___deletedItemNames_6; }
	inline void set_deletedItemNames_6(TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* value)
	{
		___deletedItemNames_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deletedItemNames_6), (void*)value);
	}

	inline static int32_t get_offset_of_deletedItemsRest_7() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___deletedItemsRest_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_deletedItemsRest_7() const { return ___deletedItemsRest_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_deletedItemsRest_7() { return &___deletedItemsRest_7; }
	inline void set_deletedItemsRest_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___deletedItemsRest_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deletedItemsRest_7), (void*)value);
	}

	inline static int32_t get_offset_of_deletedItemsRestLabel_8() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___deletedItemsRestLabel_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_deletedItemsRestLabel_8() const { return ___deletedItemsRestLabel_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_deletedItemsRestLabel_8() { return &___deletedItemsRestLabel_8; }
	inline void set_deletedItemsRestLabel_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___deletedItemsRestLabel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deletedItemsRestLabel_8), (void*)value);
	}

	inline static int32_t get_offset_of_yesButtonTransform_9() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___yesButtonTransform_9)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_yesButtonTransform_9() const { return ___yesButtonTransform_9; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_yesButtonTransform_9() { return &___yesButtonTransform_9; }
	inline void set_yesButtonTransform_9(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___yesButtonTransform_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___yesButtonTransform_9), (void*)value);
	}

	inline static int32_t get_offset_of_noButtonTransform_10() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___noButtonTransform_10)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_noButtonTransform_10() const { return ___noButtonTransform_10; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_noButtonTransform_10() { return &___noButtonTransform_10; }
	inline void set_noButtonTransform_10(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___noButtonTransform_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___noButtonTransform_10), (void*)value);
	}

	inline static int32_t get_offset_of_narrowScreenWidth_11() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___narrowScreenWidth_11)); }
	inline float get_narrowScreenWidth_11() const { return ___narrowScreenWidth_11; }
	inline float* get_address_of_narrowScreenWidth_11() { return &___narrowScreenWidth_11; }
	inline void set_narrowScreenWidth_11(float value)
	{
		___narrowScreenWidth_11 = value;
	}

	inline static int32_t get_offset_of_onDeletionConfirmed_12() { return static_cast<int32_t>(offsetof(FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C, ___onDeletionConfirmed_12)); }
	inline OnDeletionConfirmed_t54FFE37F4F1E4680F19B875155B0796C976BE677 * get_onDeletionConfirmed_12() const { return ___onDeletionConfirmed_12; }
	inline OnDeletionConfirmed_t54FFE37F4F1E4680F19B875155B0796C976BE677 ** get_address_of_onDeletionConfirmed_12() { return &___onDeletionConfirmed_12; }
	inline void set_onDeletionConfirmed_12(OnDeletionConfirmed_t54FFE37F4F1E4680F19B875155B0796C976BE677 * value)
	{
		___onDeletionConfirmed_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onDeletionConfirmed_12), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowserMovement
struct  FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowserMovement::fileBrowser
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___fileBrowser_4;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowserMovement::canvasTR
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___canvasTR_5;
	// UnityEngine.Camera SimpleFileBrowser.FileBrowserMovement::canvasCam
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___canvasCam_6;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowserMovement::window
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___window_7;
	// SimpleFileBrowser.RecycledListView SimpleFileBrowser.FileBrowserMovement::listView
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * ___listView_8;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowserMovement::initialTouchPos
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___initialTouchPos_9;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowserMovement::initialAnchoredPos
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___initialAnchoredPos_10;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowserMovement::initialSizeDelta
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___initialSizeDelta_11;

public:
	inline static int32_t get_offset_of_fileBrowser_4() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___fileBrowser_4)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_fileBrowser_4() const { return ___fileBrowser_4; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_fileBrowser_4() { return &___fileBrowser_4; }
	inline void set_fileBrowser_4(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___fileBrowser_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileBrowser_4), (void*)value);
	}

	inline static int32_t get_offset_of_canvasTR_5() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___canvasTR_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_canvasTR_5() const { return ___canvasTR_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_canvasTR_5() { return &___canvasTR_5; }
	inline void set_canvasTR_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___canvasTR_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasTR_5), (void*)value);
	}

	inline static int32_t get_offset_of_canvasCam_6() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___canvasCam_6)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_canvasCam_6() const { return ___canvasCam_6; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_canvasCam_6() { return &___canvasCam_6; }
	inline void set_canvasCam_6(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___canvasCam_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasCam_6), (void*)value);
	}

	inline static int32_t get_offset_of_window_7() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___window_7)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_window_7() const { return ___window_7; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_window_7() { return &___window_7; }
	inline void set_window_7(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___window_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___window_7), (void*)value);
	}

	inline static int32_t get_offset_of_listView_8() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___listView_8)); }
	inline RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * get_listView_8() const { return ___listView_8; }
	inline RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 ** get_address_of_listView_8() { return &___listView_8; }
	inline void set_listView_8(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * value)
	{
		___listView_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___listView_8), (void*)value);
	}

	inline static int32_t get_offset_of_initialTouchPos_9() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___initialTouchPos_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_initialTouchPos_9() const { return ___initialTouchPos_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_initialTouchPos_9() { return &___initialTouchPos_9; }
	inline void set_initialTouchPos_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___initialTouchPos_9 = value;
	}

	inline static int32_t get_offset_of_initialAnchoredPos_10() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___initialAnchoredPos_10)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_initialAnchoredPos_10() const { return ___initialAnchoredPos_10; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_initialAnchoredPos_10() { return &___initialAnchoredPos_10; }
	inline void set_initialAnchoredPos_10(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___initialAnchoredPos_10 = value;
	}

	inline static int32_t get_offset_of_initialSizeDelta_11() { return static_cast<int32_t>(offsetof(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19, ___initialSizeDelta_11)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_initialSizeDelta_11() const { return ___initialSizeDelta_11; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_initialSizeDelta_11() { return &___initialSizeDelta_11; }
	inline void set_initialSizeDelta_11(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___initialSizeDelta_11 = value;
	}
};


// SimpleFileBrowser.FileBrowserRenamedItem
struct  FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowserRenamedItem::background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___background_4;
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowserRenamedItem::icon
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___icon_5;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowserRenamedItem::nameInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___nameInputField_6;
	// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted SimpleFileBrowser.FileBrowserRenamedItem::onRenameCompleted
	OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * ___onRenameCompleted_7;

public:
	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___background_4)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_background_4() const { return ___background_4; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___background_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background_4), (void*)value);
	}

	inline static int32_t get_offset_of_icon_5() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___icon_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_icon_5() const { return ___icon_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_icon_5() { return &___icon_5; }
	inline void set_icon_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___icon_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_5), (void*)value);
	}

	inline static int32_t get_offset_of_nameInputField_6() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___nameInputField_6)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_nameInputField_6() const { return ___nameInputField_6; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_nameInputField_6() { return &___nameInputField_6; }
	inline void set_nameInputField_6(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___nameInputField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameInputField_6), (void*)value);
	}

	inline static int32_t get_offset_of_onRenameCompleted_7() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___onRenameCompleted_7)); }
	inline OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * get_onRenameCompleted_7() const { return ___onRenameCompleted_7; }
	inline OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 ** get_address_of_onRenameCompleted_7() { return &___onRenameCompleted_7; }
	inline void set_onRenameCompleted_7(OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * value)
	{
		___onRenameCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRenameCompleted_7), (void*)value);
	}
};


// SimpleFileBrowser.ListItem
struct  ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Object SimpleFileBrowser.ListItem::<Tag>k__BackingField
	RuntimeObject * ___U3CTagU3Ek__BackingField_4;
	// System.Int32 SimpleFileBrowser.ListItem::<Position>k__BackingField
	int32_t ___U3CPositionU3Ek__BackingField_5;
	// SimpleFileBrowser.IListViewAdapter SimpleFileBrowser.ListItem::adapter
	RuntimeObject* ___adapter_6;

public:
	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED, ___U3CTagU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CTagU3Ek__BackingField_4() const { return ___U3CTagU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CTagU3Ek__BackingField_4() { return &___U3CTagU3Ek__BackingField_4; }
	inline void set_U3CTagU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CTagU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTagU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED, ___U3CPositionU3Ek__BackingField_5)); }
	inline int32_t get_U3CPositionU3Ek__BackingField_5() const { return ___U3CPositionU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPositionU3Ek__BackingField_5() { return &___U3CPositionU3Ek__BackingField_5; }
	inline void set_U3CPositionU3Ek__BackingField_5(int32_t value)
	{
		___U3CPositionU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_adapter_6() { return static_cast<int32_t>(offsetof(ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED, ___adapter_6)); }
	inline RuntimeObject* get_adapter_6() const { return ___adapter_6; }
	inline RuntimeObject** get_address_of_adapter_6() { return &___adapter_6; }
	inline void set_adapter_6(RuntimeObject* value)
	{
		___adapter_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adapter_6), (void*)value);
	}
};


// AsImpL.Loader
struct  Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// AsImpL.ImportOptions AsImpL.Loader::buildOptions
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * ___buildOptions_5;
	// AsImpL.DataSet AsImpL.Loader::dataSet
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 * ___dataSet_12;
	// AsImpL.ObjectBuilder AsImpL.Loader::objectBuilder
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D * ___objectBuilder_13;
	// System.Collections.Generic.List`1<AsImpL.MaterialData> AsImpL.Loader::materialData
	List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * ___materialData_14;
	// AsImpL.SingleLoadingProgress AsImpL.Loader::objLoadingProgress
	SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70 * ___objLoadingProgress_15;
	// AsImpL.Loader/Stats AsImpL.Loader::loadStats
	Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F  ___loadStats_16;
	// UnityEngine.Texture2D AsImpL.Loader::loadedTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___loadedTexture_17;
	// System.Action`2<UnityEngine.GameObject,System.String> AsImpL.Loader::ModelCreated
	Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * ___ModelCreated_18;
	// System.Action`2<UnityEngine.GameObject,System.String> AsImpL.Loader::ModelLoaded
	Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * ___ModelLoaded_19;
	// System.Action`1<System.String> AsImpL.Loader::ModelError
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___ModelError_20;

public:
	inline static int32_t get_offset_of_buildOptions_5() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___buildOptions_5)); }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * get_buildOptions_5() const { return ___buildOptions_5; }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F ** get_address_of_buildOptions_5() { return &___buildOptions_5; }
	inline void set_buildOptions_5(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * value)
	{
		___buildOptions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buildOptions_5), (void*)value);
	}

	inline static int32_t get_offset_of_dataSet_12() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___dataSet_12)); }
	inline DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 * get_dataSet_12() const { return ___dataSet_12; }
	inline DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 ** get_address_of_dataSet_12() { return &___dataSet_12; }
	inline void set_dataSet_12(DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863 * value)
	{
		___dataSet_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataSet_12), (void*)value);
	}

	inline static int32_t get_offset_of_objectBuilder_13() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___objectBuilder_13)); }
	inline ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D * get_objectBuilder_13() const { return ___objectBuilder_13; }
	inline ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D ** get_address_of_objectBuilder_13() { return &___objectBuilder_13; }
	inline void set_objectBuilder_13(ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D * value)
	{
		___objectBuilder_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectBuilder_13), (void*)value);
	}

	inline static int32_t get_offset_of_materialData_14() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___materialData_14)); }
	inline List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * get_materialData_14() const { return ___materialData_14; }
	inline List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 ** get_address_of_materialData_14() { return &___materialData_14; }
	inline void set_materialData_14(List_1_t57E3526FD1E5A9623D23A2DBF5B7C2DB1A624A76 * value)
	{
		___materialData_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materialData_14), (void*)value);
	}

	inline static int32_t get_offset_of_objLoadingProgress_15() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___objLoadingProgress_15)); }
	inline SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70 * get_objLoadingProgress_15() const { return ___objLoadingProgress_15; }
	inline SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70 ** get_address_of_objLoadingProgress_15() { return &___objLoadingProgress_15; }
	inline void set_objLoadingProgress_15(SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70 * value)
	{
		___objLoadingProgress_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objLoadingProgress_15), (void*)value);
	}

	inline static int32_t get_offset_of_loadStats_16() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___loadStats_16)); }
	inline Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F  get_loadStats_16() const { return ___loadStats_16; }
	inline Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F * get_address_of_loadStats_16() { return &___loadStats_16; }
	inline void set_loadStats_16(Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F  value)
	{
		___loadStats_16 = value;
	}

	inline static int32_t get_offset_of_loadedTexture_17() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___loadedTexture_17)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_loadedTexture_17() const { return ___loadedTexture_17; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_loadedTexture_17() { return &___loadedTexture_17; }
	inline void set_loadedTexture_17(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___loadedTexture_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadedTexture_17), (void*)value);
	}

	inline static int32_t get_offset_of_ModelCreated_18() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___ModelCreated_18)); }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * get_ModelCreated_18() const { return ___ModelCreated_18; }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 ** get_address_of_ModelCreated_18() { return &___ModelCreated_18; }
	inline void set_ModelCreated_18(Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * value)
	{
		___ModelCreated_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModelCreated_18), (void*)value);
	}

	inline static int32_t get_offset_of_ModelLoaded_19() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___ModelLoaded_19)); }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * get_ModelLoaded_19() const { return ___ModelLoaded_19; }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 ** get_address_of_ModelLoaded_19() { return &___ModelLoaded_19; }
	inline void set_ModelLoaded_19(Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * value)
	{
		___ModelLoaded_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModelLoaded_19), (void*)value);
	}

	inline static int32_t get_offset_of_ModelError_20() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B, ___ModelError_20)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_ModelError_20() const { return ___ModelError_20; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_ModelError_20() { return &___ModelError_20; }
	inline void set_ModelError_20(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___ModelError_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModelError_20), (void*)value);
	}
};

struct Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields
{
public:
	// AsImpL.LoadingProgress AsImpL.Loader::totalProgress
	LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5 * ___totalProgress_4;
	// System.Single AsImpL.Loader::LOAD_PHASE_PERC
	float ___LOAD_PHASE_PERC_6;
	// System.Single AsImpL.Loader::TEXTURE_PHASE_PERC
	float ___TEXTURE_PHASE_PERC_7;
	// System.Single AsImpL.Loader::MATERIAL_PHASE_PERC
	float ___MATERIAL_PHASE_PERC_8;
	// System.Single AsImpL.Loader::BUILD_PHASE_PERC
	float ___BUILD_PHASE_PERC_9;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> AsImpL.Loader::loadedModels
	Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * ___loadedModels_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AsImpL.Loader::instanceCount
	Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * ___instanceCount_11;

public:
	inline static int32_t get_offset_of_totalProgress_4() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields, ___totalProgress_4)); }
	inline LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5 * get_totalProgress_4() const { return ___totalProgress_4; }
	inline LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5 ** get_address_of_totalProgress_4() { return &___totalProgress_4; }
	inline void set_totalProgress_4(LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5 * value)
	{
		___totalProgress_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___totalProgress_4), (void*)value);
	}

	inline static int32_t get_offset_of_LOAD_PHASE_PERC_6() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields, ___LOAD_PHASE_PERC_6)); }
	inline float get_LOAD_PHASE_PERC_6() const { return ___LOAD_PHASE_PERC_6; }
	inline float* get_address_of_LOAD_PHASE_PERC_6() { return &___LOAD_PHASE_PERC_6; }
	inline void set_LOAD_PHASE_PERC_6(float value)
	{
		___LOAD_PHASE_PERC_6 = value;
	}

	inline static int32_t get_offset_of_TEXTURE_PHASE_PERC_7() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields, ___TEXTURE_PHASE_PERC_7)); }
	inline float get_TEXTURE_PHASE_PERC_7() const { return ___TEXTURE_PHASE_PERC_7; }
	inline float* get_address_of_TEXTURE_PHASE_PERC_7() { return &___TEXTURE_PHASE_PERC_7; }
	inline void set_TEXTURE_PHASE_PERC_7(float value)
	{
		___TEXTURE_PHASE_PERC_7 = value;
	}

	inline static int32_t get_offset_of_MATERIAL_PHASE_PERC_8() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields, ___MATERIAL_PHASE_PERC_8)); }
	inline float get_MATERIAL_PHASE_PERC_8() const { return ___MATERIAL_PHASE_PERC_8; }
	inline float* get_address_of_MATERIAL_PHASE_PERC_8() { return &___MATERIAL_PHASE_PERC_8; }
	inline void set_MATERIAL_PHASE_PERC_8(float value)
	{
		___MATERIAL_PHASE_PERC_8 = value;
	}

	inline static int32_t get_offset_of_BUILD_PHASE_PERC_9() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields, ___BUILD_PHASE_PERC_9)); }
	inline float get_BUILD_PHASE_PERC_9() const { return ___BUILD_PHASE_PERC_9; }
	inline float* get_address_of_BUILD_PHASE_PERC_9() { return &___BUILD_PHASE_PERC_9; }
	inline void set_BUILD_PHASE_PERC_9(float value)
	{
		___BUILD_PHASE_PERC_9 = value;
	}

	inline static int32_t get_offset_of_loadedModels_10() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields, ___loadedModels_10)); }
	inline Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * get_loadedModels_10() const { return ___loadedModels_10; }
	inline Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C ** get_address_of_loadedModels_10() { return &___loadedModels_10; }
	inline void set_loadedModels_10(Dictionary_2_tADDBF4C67A82C92CD16099CD166D7F72E8DCB13C * value)
	{
		___loadedModels_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadedModels_10), (void*)value);
	}

	inline static int32_t get_offset_of_instanceCount_11() { return static_cast<int32_t>(offsetof(Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields, ___instanceCount_11)); }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * get_instanceCount_11() const { return ___instanceCount_11; }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 ** get_address_of_instanceCount_11() { return &___instanceCount_11; }
	inline void set_instanceCount_11(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * value)
	{
		___instanceCount_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instanceCount_11), (void*)value);
	}
};


// ObjFromFile
struct  ObjFromFile_tA35B91E55B4B729F6B47DE8BEB212203D489B788  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String ObjFromFile::objPath
	String_t* ___objPath_4;
	// System.String ObjFromFile::error
	String_t* ___error_5;
	// UnityEngine.GameObject ObjFromFile::loadedObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___loadedObject_6;

public:
	inline static int32_t get_offset_of_objPath_4() { return static_cast<int32_t>(offsetof(ObjFromFile_tA35B91E55B4B729F6B47DE8BEB212203D489B788, ___objPath_4)); }
	inline String_t* get_objPath_4() const { return ___objPath_4; }
	inline String_t** get_address_of_objPath_4() { return &___objPath_4; }
	inline void set_objPath_4(String_t* value)
	{
		___objPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objPath_4), (void*)value);
	}

	inline static int32_t get_offset_of_error_5() { return static_cast<int32_t>(offsetof(ObjFromFile_tA35B91E55B4B729F6B47DE8BEB212203D489B788, ___error_5)); }
	inline String_t* get_error_5() const { return ___error_5; }
	inline String_t** get_address_of_error_5() { return &___error_5; }
	inline void set_error_5(String_t* value)
	{
		___error_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___error_5), (void*)value);
	}

	inline static int32_t get_offset_of_loadedObject_6() { return static_cast<int32_t>(offsetof(ObjFromFile_tA35B91E55B4B729F6B47DE8BEB212203D489B788, ___loadedObject_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_loadedObject_6() const { return ___loadedObject_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_loadedObject_6() { return &___loadedObject_6; }
	inline void set_loadedObject_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___loadedObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadedObject_6), (void*)value);
	}
};


// ObjFromStream
struct  ObjFromStream_tECDE28DABB6B6709B55B4268FF22BA5E8BE11FB8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// AsImpL.ObjectImporter
struct  ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 AsImpL.ObjectImporter::numTotalImports
	int32_t ___numTotalImports_4;
	// System.Boolean AsImpL.ObjectImporter::allLoaded
	bool ___allLoaded_5;
	// AsImpL.ImportOptions AsImpL.ObjectImporter::buildOptions
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * ___buildOptions_6;
	// System.Collections.Generic.List`1<AsImpL.Loader> AsImpL.ObjectImporter::loaderList
	List_1_tF8B1775B627E4B4F8DA9A10979A72AA4553651A8 * ___loaderList_7;
	// AsImpL.ObjectImporter/ImportPhase AsImpL.ObjectImporter::importPhase
	int32_t ___importPhase_8;
	// System.Action AsImpL.ObjectImporter::ImportingStart
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___ImportingStart_9;
	// System.Action AsImpL.ObjectImporter::ImportingComplete
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___ImportingComplete_10;
	// System.Action`2<UnityEngine.GameObject,System.String> AsImpL.ObjectImporter::CreatedModel
	Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * ___CreatedModel_11;
	// System.Action`2<UnityEngine.GameObject,System.String> AsImpL.ObjectImporter::ImportedModel
	Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * ___ImportedModel_12;
	// System.Action`1<System.String> AsImpL.ObjectImporter::ImportError
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___ImportError_13;

public:
	inline static int32_t get_offset_of_numTotalImports_4() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___numTotalImports_4)); }
	inline int32_t get_numTotalImports_4() const { return ___numTotalImports_4; }
	inline int32_t* get_address_of_numTotalImports_4() { return &___numTotalImports_4; }
	inline void set_numTotalImports_4(int32_t value)
	{
		___numTotalImports_4 = value;
	}

	inline static int32_t get_offset_of_allLoaded_5() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___allLoaded_5)); }
	inline bool get_allLoaded_5() const { return ___allLoaded_5; }
	inline bool* get_address_of_allLoaded_5() { return &___allLoaded_5; }
	inline void set_allLoaded_5(bool value)
	{
		___allLoaded_5 = value;
	}

	inline static int32_t get_offset_of_buildOptions_6() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___buildOptions_6)); }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * get_buildOptions_6() const { return ___buildOptions_6; }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F ** get_address_of_buildOptions_6() { return &___buildOptions_6; }
	inline void set_buildOptions_6(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * value)
	{
		___buildOptions_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buildOptions_6), (void*)value);
	}

	inline static int32_t get_offset_of_loaderList_7() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___loaderList_7)); }
	inline List_1_tF8B1775B627E4B4F8DA9A10979A72AA4553651A8 * get_loaderList_7() const { return ___loaderList_7; }
	inline List_1_tF8B1775B627E4B4F8DA9A10979A72AA4553651A8 ** get_address_of_loaderList_7() { return &___loaderList_7; }
	inline void set_loaderList_7(List_1_tF8B1775B627E4B4F8DA9A10979A72AA4553651A8 * value)
	{
		___loaderList_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loaderList_7), (void*)value);
	}

	inline static int32_t get_offset_of_importPhase_8() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___importPhase_8)); }
	inline int32_t get_importPhase_8() const { return ___importPhase_8; }
	inline int32_t* get_address_of_importPhase_8() { return &___importPhase_8; }
	inline void set_importPhase_8(int32_t value)
	{
		___importPhase_8 = value;
	}

	inline static int32_t get_offset_of_ImportingStart_9() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___ImportingStart_9)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_ImportingStart_9() const { return ___ImportingStart_9; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_ImportingStart_9() { return &___ImportingStart_9; }
	inline void set_ImportingStart_9(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___ImportingStart_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ImportingStart_9), (void*)value);
	}

	inline static int32_t get_offset_of_ImportingComplete_10() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___ImportingComplete_10)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_ImportingComplete_10() const { return ___ImportingComplete_10; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_ImportingComplete_10() { return &___ImportingComplete_10; }
	inline void set_ImportingComplete_10(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___ImportingComplete_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ImportingComplete_10), (void*)value);
	}

	inline static int32_t get_offset_of_CreatedModel_11() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___CreatedModel_11)); }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * get_CreatedModel_11() const { return ___CreatedModel_11; }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 ** get_address_of_CreatedModel_11() { return &___CreatedModel_11; }
	inline void set_CreatedModel_11(Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * value)
	{
		___CreatedModel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CreatedModel_11), (void*)value);
	}

	inline static int32_t get_offset_of_ImportedModel_12() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___ImportedModel_12)); }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * get_ImportedModel_12() const { return ___ImportedModel_12; }
	inline Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 ** get_address_of_ImportedModel_12() { return &___ImportedModel_12; }
	inline void set_ImportedModel_12(Action_2_tAEA12E6EF35A63D6AB0AF79B17B4343733EBCDE0 * value)
	{
		___ImportedModel_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ImportedModel_12), (void*)value);
	}

	inline static int32_t get_offset_of_ImportError_13() { return static_cast<int32_t>(offsetof(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D, ___ImportError_13)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_ImportError_13() const { return ___ImportError_13; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_ImportError_13() { return &___ImportError_13; }
	inline void set_ImportError_13(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___ImportError_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ImportError_13), (void*)value);
	}
};


// AsImpL.ObjectImporterUI
struct  ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text AsImpL.ObjectImporterUI::progressText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___progressText_4;
	// UnityEngine.UI.Slider AsImpL.ObjectImporterUI::progressSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___progressSlider_5;
	// UnityEngine.UI.Image AsImpL.ObjectImporterUI::progressImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___progressImage_6;
	// AsImpL.ObjectImporter AsImpL.ObjectImporterUI::objImporter
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D * ___objImporter_7;

public:
	inline static int32_t get_offset_of_progressText_4() { return static_cast<int32_t>(offsetof(ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875, ___progressText_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_progressText_4() const { return ___progressText_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_progressText_4() { return &___progressText_4; }
	inline void set_progressText_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___progressText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___progressText_4), (void*)value);
	}

	inline static int32_t get_offset_of_progressSlider_5() { return static_cast<int32_t>(offsetof(ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875, ___progressSlider_5)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_progressSlider_5() const { return ___progressSlider_5; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_progressSlider_5() { return &___progressSlider_5; }
	inline void set_progressSlider_5(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___progressSlider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___progressSlider_5), (void*)value);
	}

	inline static int32_t get_offset_of_progressImage_6() { return static_cast<int32_t>(offsetof(ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875, ___progressImage_6)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_progressImage_6() const { return ___progressImage_6; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_progressImage_6() { return &___progressImage_6; }
	inline void set_progressImage_6(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___progressImage_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___progressImage_6), (void*)value);
	}

	inline static int32_t get_offset_of_objImporter_7() { return static_cast<int32_t>(offsetof(ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875, ___objImporter_7)); }
	inline ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D * get_objImporter_7() const { return ___objImporter_7; }
	inline ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D ** get_address_of_objImporter_7() { return &___objImporter_7; }
	inline void set_objImporter_7(ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D * value)
	{
		___objImporter_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objImporter_7), (void*)value);
	}
};


// AsImpL.PathSettings
struct  PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// AsImpL.RootPathEnum AsImpL.PathSettings::defaultRootPath
	int32_t ___defaultRootPath_4;
	// AsImpL.RootPathEnum AsImpL.PathSettings::mobileRootPath
	int32_t ___mobileRootPath_5;

public:
	inline static int32_t get_offset_of_defaultRootPath_4() { return static_cast<int32_t>(offsetof(PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346, ___defaultRootPath_4)); }
	inline int32_t get_defaultRootPath_4() const { return ___defaultRootPath_4; }
	inline int32_t* get_address_of_defaultRootPath_4() { return &___defaultRootPath_4; }
	inline void set_defaultRootPath_4(int32_t value)
	{
		___defaultRootPath_4 = value;
	}

	inline static int32_t get_offset_of_mobileRootPath_5() { return static_cast<int32_t>(offsetof(PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346, ___mobileRootPath_5)); }
	inline int32_t get_mobileRootPath_5() const { return ___mobileRootPath_5; }
	inline int32_t* get_address_of_mobileRootPath_5() { return &___mobileRootPath_5; }
	inline void set_mobileRootPath_5(int32_t value)
	{
		___mobileRootPath_5 = value;
	}
};


// SimpleFileBrowser.RecycledListView
struct  RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform SimpleFileBrowser.RecycledListView::viewportTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___viewportTransform_4;
	// UnityEngine.RectTransform SimpleFileBrowser.RecycledListView::contentTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___contentTransform_5;
	// System.Single SimpleFileBrowser.RecycledListView::itemHeight
	float ___itemHeight_6;
	// System.Single SimpleFileBrowser.RecycledListView::_1OverItemHeight
	float ____1OverItemHeight_7;
	// System.Single SimpleFileBrowser.RecycledListView::viewportHeight
	float ___viewportHeight_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,SimpleFileBrowser.ListItem> SimpleFileBrowser.RecycledListView::items
	Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 * ___items_9;
	// System.Collections.Generic.Stack`1<SimpleFileBrowser.ListItem> SimpleFileBrowser.RecycledListView::pooledItems
	Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A * ___pooledItems_10;
	// SimpleFileBrowser.IListViewAdapter SimpleFileBrowser.RecycledListView::adapter
	RuntimeObject* ___adapter_11;
	// System.Int32 SimpleFileBrowser.RecycledListView::currentTopIndex
	int32_t ___currentTopIndex_12;
	// System.Int32 SimpleFileBrowser.RecycledListView::currentBottomIndex
	int32_t ___currentBottomIndex_13;

public:
	inline static int32_t get_offset_of_viewportTransform_4() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___viewportTransform_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_viewportTransform_4() const { return ___viewportTransform_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_viewportTransform_4() { return &___viewportTransform_4; }
	inline void set_viewportTransform_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___viewportTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewportTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_contentTransform_5() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___contentTransform_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_contentTransform_5() const { return ___contentTransform_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_contentTransform_5() { return &___contentTransform_5; }
	inline void set_contentTransform_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___contentTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contentTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_itemHeight_6() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___itemHeight_6)); }
	inline float get_itemHeight_6() const { return ___itemHeight_6; }
	inline float* get_address_of_itemHeight_6() { return &___itemHeight_6; }
	inline void set_itemHeight_6(float value)
	{
		___itemHeight_6 = value;
	}

	inline static int32_t get_offset_of__1OverItemHeight_7() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ____1OverItemHeight_7)); }
	inline float get__1OverItemHeight_7() const { return ____1OverItemHeight_7; }
	inline float* get_address_of__1OverItemHeight_7() { return &____1OverItemHeight_7; }
	inline void set__1OverItemHeight_7(float value)
	{
		____1OverItemHeight_7 = value;
	}

	inline static int32_t get_offset_of_viewportHeight_8() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___viewportHeight_8)); }
	inline float get_viewportHeight_8() const { return ___viewportHeight_8; }
	inline float* get_address_of_viewportHeight_8() { return &___viewportHeight_8; }
	inline void set_viewportHeight_8(float value)
	{
		___viewportHeight_8 = value;
	}

	inline static int32_t get_offset_of_items_9() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___items_9)); }
	inline Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 * get_items_9() const { return ___items_9; }
	inline Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 ** get_address_of_items_9() { return &___items_9; }
	inline void set_items_9(Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 * value)
	{
		___items_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___items_9), (void*)value);
	}

	inline static int32_t get_offset_of_pooledItems_10() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___pooledItems_10)); }
	inline Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A * get_pooledItems_10() const { return ___pooledItems_10; }
	inline Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A ** get_address_of_pooledItems_10() { return &___pooledItems_10; }
	inline void set_pooledItems_10(Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A * value)
	{
		___pooledItems_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pooledItems_10), (void*)value);
	}

	inline static int32_t get_offset_of_adapter_11() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___adapter_11)); }
	inline RuntimeObject* get_adapter_11() const { return ___adapter_11; }
	inline RuntimeObject** get_address_of_adapter_11() { return &___adapter_11; }
	inline void set_adapter_11(RuntimeObject* value)
	{
		___adapter_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adapter_11), (void*)value);
	}

	inline static int32_t get_offset_of_currentTopIndex_12() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___currentTopIndex_12)); }
	inline int32_t get_currentTopIndex_12() const { return ___currentTopIndex_12; }
	inline int32_t* get_address_of_currentTopIndex_12() { return &___currentTopIndex_12; }
	inline void set_currentTopIndex_12(int32_t value)
	{
		___currentTopIndex_12 = value;
	}

	inline static int32_t get_offset_of_currentBottomIndex_13() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___currentBottomIndex_13)); }
	inline int32_t get_currentBottomIndex_13() const { return ___currentBottomIndex_13; }
	inline int32_t* get_address_of_currentBottomIndex_13() { return &___currentBottomIndex_13; }
	inline void set_currentBottomIndex_13(int32_t value)
	{
		___currentBottomIndex_13 = value;
	}
};


// AsImpL.TextureLoader
struct  TextureLoader_t35FA96AF45E04053CBF58389A1F0296B38EA1C28  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// mainMenu
struct  mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String mainMenu::input_path
	String_t* ___input_path_4;
	// System.String mainMenu::output_path
	String_t* ___output_path_5;
	// UnityEngine.UI.Text mainMenu::m_Text
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Text_6;
	// System.Boolean mainMenu::m_SceneLoaded
	bool ___m_SceneLoaded_7;
	// UnityEngine.UI.Dropdown mainMenu::dropdown
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___dropdown_8;

public:
	inline static int32_t get_offset_of_input_path_4() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___input_path_4)); }
	inline String_t* get_input_path_4() const { return ___input_path_4; }
	inline String_t** get_address_of_input_path_4() { return &___input_path_4; }
	inline void set_input_path_4(String_t* value)
	{
		___input_path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_path_4), (void*)value);
	}

	inline static int32_t get_offset_of_output_path_5() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___output_path_5)); }
	inline String_t* get_output_path_5() const { return ___output_path_5; }
	inline String_t** get_address_of_output_path_5() { return &___output_path_5; }
	inline void set_output_path_5(String_t* value)
	{
		___output_path_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___output_path_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_6() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___m_Text_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Text_6() const { return ___m_Text_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Text_6() { return &___m_Text_6; }
	inline void set_m_Text_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Text_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_SceneLoaded_7() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___m_SceneLoaded_7)); }
	inline bool get_m_SceneLoaded_7() const { return ___m_SceneLoaded_7; }
	inline bool* get_address_of_m_SceneLoaded_7() { return &___m_SceneLoaded_7; }
	inline void set_m_SceneLoaded_7(bool value)
	{
		___m_SceneLoaded_7 = value;
	}

	inline static int32_t get_offset_of_dropdown_8() { return static_cast<int32_t>(offsetof(mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339, ___dropdown_8)); }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * get_dropdown_8() const { return ___dropdown_8; }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 ** get_address_of_dropdown_8() { return &___dropdown_8; }
	inline void set_dropdown_8(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * value)
	{
		___dropdown_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dropdown_8), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseInput
struct  BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:

public:
};


// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_RaycastResultCache_4)); }
	inline List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastResultCache_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_AxisEventData_5)); }
	inline AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AxisEventData_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_EventSystem_6)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_BaseEventData_7)); }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BaseEventData_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_InputOverride_8)); }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputOverride_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_DefaultInput_9)); }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultInput_9), (void*)value);
	}
};


// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_tC7D44B0AC6406BAC3E4FC4579A43FC135BDB6FDA  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_tC7D44B0AC6406BAC3E4FC4579A43FC135BDB6FDA, ___m_Graphic_4)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Graphic_4), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.BaseRaycaster::m_RootRaycaster
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___m_RootRaycaster_4;

public:
	inline static int32_t get_offset_of_m_RootRaycaster_4() { return static_cast<int32_t>(offsetof(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876, ___m_RootRaycaster_4)); }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * get_m_RootRaycaster_4() const { return ___m_RootRaycaster_4; }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 ** get_address_of_m_RootRaycaster_4() { return &___m_RootRaycaster_4; }
	inline void set_m_RootRaycaster_4(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * value)
	{
		___m_RootRaycaster_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RootRaycaster_4), (void*)value);
	}
};


// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SystemInputModules_4)); }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SystemInputModules_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentInputModule_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_FirstSelected_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FirstSelected_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentSelected_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentSelected_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DummyData_13)); }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DummyData_13), (void*)value);
	}
};

struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * ___s_RaycastComparer_14;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___m_EventSystems_6)); }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystems_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastComparer_14), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowserItem
struct  FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025  : public ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED
{
public:
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowserItem::fileBrowser
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___fileBrowser_9;
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowserItem::background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___background_10;
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowserItem::icon
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___icon_11;
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowserItem::multiSelectionToggle
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___multiSelectionToggle_12;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowserItem::nameText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___nameText_13;
	// System.Boolean SimpleFileBrowser.FileBrowserItem::isSelected
	bool ___isSelected_14;
	// System.Single SimpleFileBrowser.FileBrowserItem::pressTime
	float ___pressTime_15;
	// System.Single SimpleFileBrowser.FileBrowserItem::prevClickTime
	float ___prevClickTime_16;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowserItem::m_transform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_transform_17;
	// System.Boolean SimpleFileBrowser.FileBrowserItem::<IsDirectory>k__BackingField
	bool ___U3CIsDirectoryU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_fileBrowser_9() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___fileBrowser_9)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_fileBrowser_9() const { return ___fileBrowser_9; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_fileBrowser_9() { return &___fileBrowser_9; }
	inline void set_fileBrowser_9(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___fileBrowser_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileBrowser_9), (void*)value);
	}

	inline static int32_t get_offset_of_background_10() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___background_10)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_background_10() const { return ___background_10; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_background_10() { return &___background_10; }
	inline void set_background_10(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___background_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background_10), (void*)value);
	}

	inline static int32_t get_offset_of_icon_11() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___icon_11)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_icon_11() const { return ___icon_11; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_icon_11() { return &___icon_11; }
	inline void set_icon_11(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___icon_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_11), (void*)value);
	}

	inline static int32_t get_offset_of_multiSelectionToggle_12() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___multiSelectionToggle_12)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_multiSelectionToggle_12() const { return ___multiSelectionToggle_12; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_multiSelectionToggle_12() { return &___multiSelectionToggle_12; }
	inline void set_multiSelectionToggle_12(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___multiSelectionToggle_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiSelectionToggle_12), (void*)value);
	}

	inline static int32_t get_offset_of_nameText_13() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___nameText_13)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_nameText_13() const { return ___nameText_13; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_nameText_13() { return &___nameText_13; }
	inline void set_nameText_13(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___nameText_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameText_13), (void*)value);
	}

	inline static int32_t get_offset_of_isSelected_14() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___isSelected_14)); }
	inline bool get_isSelected_14() const { return ___isSelected_14; }
	inline bool* get_address_of_isSelected_14() { return &___isSelected_14; }
	inline void set_isSelected_14(bool value)
	{
		___isSelected_14 = value;
	}

	inline static int32_t get_offset_of_pressTime_15() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___pressTime_15)); }
	inline float get_pressTime_15() const { return ___pressTime_15; }
	inline float* get_address_of_pressTime_15() { return &___pressTime_15; }
	inline void set_pressTime_15(float value)
	{
		___pressTime_15 = value;
	}

	inline static int32_t get_offset_of_prevClickTime_16() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___prevClickTime_16)); }
	inline float get_prevClickTime_16() const { return ___prevClickTime_16; }
	inline float* get_address_of_prevClickTime_16() { return &___prevClickTime_16; }
	inline void set_prevClickTime_16(float value)
	{
		___prevClickTime_16 = value;
	}

	inline static int32_t get_offset_of_m_transform_17() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___m_transform_17)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_transform_17() const { return ___m_transform_17; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_transform_17() { return &___m_transform_17; }
	inline void set_m_transform_17(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_transform_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_transform_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsDirectoryU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025, ___U3CIsDirectoryU3Ek__BackingField_18)); }
	inline bool get_U3CIsDirectoryU3Ek__BackingField_18() const { return ___U3CIsDirectoryU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CIsDirectoryU3Ek__BackingField_18() { return &___U3CIsDirectoryU3Ek__BackingField_18; }
	inline void set_U3CIsDirectoryU3Ek__BackingField_18(bool value)
	{
		___U3CIsDirectoryU3Ek__BackingField_18 = value;
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// AsImpL.LoaderObj
struct  LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543  : public Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B
{
public:
	// System.String AsImpL.LoaderObj::mtlLib
	String_t* ___mtlLib_21;
	// System.String AsImpL.LoaderObj::loadedText
	String_t* ___loadedText_22;

public:
	inline static int32_t get_offset_of_mtlLib_21() { return static_cast<int32_t>(offsetof(LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543, ___mtlLib_21)); }
	inline String_t* get_mtlLib_21() const { return ___mtlLib_21; }
	inline String_t** get_address_of_mtlLib_21() { return &___mtlLib_21; }
	inline void set_mtlLib_21(String_t* value)
	{
		___mtlLib_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mtlLib_21), (void*)value);
	}

	inline static int32_t get_offset_of_loadedText_22() { return static_cast<int32_t>(offsetof(LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543, ___loadedText_22)); }
	inline String_t* get_loadedText_22() const { return ___loadedText_22; }
	inline String_t** get_address_of_loadedText_22() { return &___loadedText_22; }
	inline void set_loadedText_22(String_t* value)
	{
		___loadedText_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadedText_22), (void*)value);
	}
};


// AsImpL.MultiObjectImporter
struct  MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C  : public ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D
{
public:
	// System.Boolean AsImpL.MultiObjectImporter::autoLoadOnStart
	bool ___autoLoadOnStart_14;
	// System.Collections.Generic.List`1<AsImpL.ModelImportInfo> AsImpL.MultiObjectImporter::objectsList
	List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 * ___objectsList_15;
	// AsImpL.ImportOptions AsImpL.MultiObjectImporter::defaultImportOptions
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * ___defaultImportOptions_16;
	// AsImpL.PathSettings AsImpL.MultiObjectImporter::pathSettings
	PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 * ___pathSettings_17;

public:
	inline static int32_t get_offset_of_autoLoadOnStart_14() { return static_cast<int32_t>(offsetof(MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C, ___autoLoadOnStart_14)); }
	inline bool get_autoLoadOnStart_14() const { return ___autoLoadOnStart_14; }
	inline bool* get_address_of_autoLoadOnStart_14() { return &___autoLoadOnStart_14; }
	inline void set_autoLoadOnStart_14(bool value)
	{
		___autoLoadOnStart_14 = value;
	}

	inline static int32_t get_offset_of_objectsList_15() { return static_cast<int32_t>(offsetof(MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C, ___objectsList_15)); }
	inline List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 * get_objectsList_15() const { return ___objectsList_15; }
	inline List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 ** get_address_of_objectsList_15() { return &___objectsList_15; }
	inline void set_objectsList_15(List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 * value)
	{
		___objectsList_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectsList_15), (void*)value);
	}

	inline static int32_t get_offset_of_defaultImportOptions_16() { return static_cast<int32_t>(offsetof(MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C, ___defaultImportOptions_16)); }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * get_defaultImportOptions_16() const { return ___defaultImportOptions_16; }
	inline ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F ** get_address_of_defaultImportOptions_16() { return &___defaultImportOptions_16; }
	inline void set_defaultImportOptions_16(ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F * value)
	{
		___defaultImportOptions_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultImportOptions_16), (void*)value);
	}

	inline static int32_t get_offset_of_pathSettings_17() { return static_cast<int32_t>(offsetof(MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C, ___pathSettings_17)); }
	inline PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 * get_pathSettings_17() const { return ___pathSettings_17; }
	inline PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 ** get_address_of_pathSettings_17() { return &___pathSettings_17; }
	inline void set_pathSettings_17(PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346 * value)
	{
		___pathSettings_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathSettings_17), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.ToggleGroup
struct  ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.ToggleGroup::m_AllowSwitchOff
	bool ___m_AllowSwitchOff_4;
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::m_Toggles
	List_1_tECEEA56321275CFF8DECB929786CE364F743B07D * ___m_Toggles_5;

public:
	inline static int32_t get_offset_of_m_AllowSwitchOff_4() { return static_cast<int32_t>(offsetof(ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95, ___m_AllowSwitchOff_4)); }
	inline bool get_m_AllowSwitchOff_4() const { return ___m_AllowSwitchOff_4; }
	inline bool* get_address_of_m_AllowSwitchOff_4() { return &___m_AllowSwitchOff_4; }
	inline void set_m_AllowSwitchOff_4(bool value)
	{
		___m_AllowSwitchOff_4 = value;
	}

	inline static int32_t get_offset_of_m_Toggles_5() { return static_cast<int32_t>(offsetof(ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95, ___m_Toggles_5)); }
	inline List_1_tECEEA56321275CFF8DECB929786CE364F743B07D * get_m_Toggles_5() const { return ___m_Toggles_5; }
	inline List_1_tECEEA56321275CFF8DECB929786CE364F743B07D ** get_address_of_m_Toggles_5() { return &___m_Toggles_5; }
	inline void set_m_Toggles_5(List_1_tECEEA56321275CFF8DECB929786CE364F743B07D * value)
	{
		___m_Toggles_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Toggles_5), (void*)value);
	}
};


// AsImpL.Examples.CustomObjImporter
struct  CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE  : public MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C
{
public:
	// UnityEngine.UI.Text AsImpL.Examples.CustomObjImporter::objScalingText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___objScalingText_18;
	// System.String AsImpL.Examples.CustomObjImporter::configFile
	String_t* ___configFile_19;
	// System.Collections.Generic.List`1<AsImpL.ModelImportInfo> AsImpL.Examples.CustomObjImporter::modelsToImport
	List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 * ___modelsToImport_20;

public:
	inline static int32_t get_offset_of_objScalingText_18() { return static_cast<int32_t>(offsetof(CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE, ___objScalingText_18)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_objScalingText_18() const { return ___objScalingText_18; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_objScalingText_18() { return &___objScalingText_18; }
	inline void set_objScalingText_18(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___objScalingText_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objScalingText_18), (void*)value);
	}

	inline static int32_t get_offset_of_configFile_19() { return static_cast<int32_t>(offsetof(CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE, ___configFile_19)); }
	inline String_t* get_configFile_19() const { return ___configFile_19; }
	inline String_t** get_address_of_configFile_19() { return &___configFile_19; }
	inline void set_configFile_19(String_t* value)
	{
		___configFile_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___configFile_19), (void*)value);
	}

	inline static int32_t get_offset_of_modelsToImport_20() { return static_cast<int32_t>(offsetof(CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE, ___modelsToImport_20)); }
	inline List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 * get_modelsToImport_20() const { return ___modelsToImport_20; }
	inline List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 ** get_address_of_modelsToImport_20() { return &___modelsToImport_20; }
	inline void set_modelsToImport_20(List_1_tE20112D72A39658DBC578AE1FC61DF1187334A02 * value)
	{
		___modelsToImport_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modelsToImport_20), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowserQuickLink
struct  FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C  : public FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025
{
public:
	// System.String SimpleFileBrowser.FileBrowserQuickLink::m_targetPath
	String_t* ___m_targetPath_19;

public:
	inline static int32_t get_offset_of_m_targetPath_19() { return static_cast<int32_t>(offsetof(FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C, ___m_targetPath_19)); }
	inline String_t* get_m_targetPath_19() const { return ___m_targetPath_19; }
	inline String_t** get_address_of_m_targetPath_19() { return &___m_targetPath_19; }
	inline void set_m_targetPath_19(String_t* value)
	{
		___m_targetPath_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_targetPath_19), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// SimpleFileBrowser.NonDrawingGraphic
struct  NonDrawingGraphic_t14C8D2E57B2CC0F4160CBB2D73DA16E0D0966345  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:

public:
};


// UnityEngine.EventSystems.PhysicsRaycaster
struct  PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823  : public BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876
{
public:
	// UnityEngine.Camera UnityEngine.EventSystems.PhysicsRaycaster::m_EventCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_EventCamera_6;
	// UnityEngine.LayerMask UnityEngine.EventSystems.PhysicsRaycaster::m_EventMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_EventMask_7;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_MaxRayIntersections
	int32_t ___m_MaxRayIntersections_8;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_LastMaxRayIntersections
	int32_t ___m_LastMaxRayIntersections_9;
	// UnityEngine.RaycastHit[] UnityEngine.EventSystems.PhysicsRaycaster::m_Hits
	RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* ___m_Hits_10;

public:
	inline static int32_t get_offset_of_m_EventCamera_6() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823, ___m_EventCamera_6)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_EventCamera_6() const { return ___m_EventCamera_6; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_EventCamera_6() { return &___m_EventCamera_6; }
	inline void set_m_EventCamera_6(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_EventCamera_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventCamera_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventMask_7() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823, ___m_EventMask_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_EventMask_7() const { return ___m_EventMask_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_EventMask_7() { return &___m_EventMask_7; }
	inline void set_m_EventMask_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_EventMask_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxRayIntersections_8() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823, ___m_MaxRayIntersections_8)); }
	inline int32_t get_m_MaxRayIntersections_8() const { return ___m_MaxRayIntersections_8; }
	inline int32_t* get_address_of_m_MaxRayIntersections_8() { return &___m_MaxRayIntersections_8; }
	inline void set_m_MaxRayIntersections_8(int32_t value)
	{
		___m_MaxRayIntersections_8 = value;
	}

	inline static int32_t get_offset_of_m_LastMaxRayIntersections_9() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823, ___m_LastMaxRayIntersections_9)); }
	inline int32_t get_m_LastMaxRayIntersections_9() const { return ___m_LastMaxRayIntersections_9; }
	inline int32_t* get_address_of_m_LastMaxRayIntersections_9() { return &___m_LastMaxRayIntersections_9; }
	inline void set_m_LastMaxRayIntersections_9(int32_t value)
	{
		___m_LastMaxRayIntersections_9 = value;
	}

	inline static int32_t get_offset_of_m_Hits_10() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823, ___m_Hits_10)); }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* get_m_Hits_10() const { return ___m_Hits_10; }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09** get_address_of_m_Hits_10() { return &___m_Hits_10; }
	inline void set_m_Hits_10(RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* value)
	{
		___m_Hits_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Hits_10), (void*)value);
	}
};


// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_tD7460503C6A4E1060914FFD213535AEF6AE2F421  : public BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 * ___m_PointerData_14;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1 * ___m_MouseState_15;

public:
	inline static int32_t get_offset_of_m_PointerData_14() { return static_cast<int32_t>(offsetof(PointerInputModule_tD7460503C6A4E1060914FFD213535AEF6AE2F421, ___m_PointerData_14)); }
	inline Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 * get_m_PointerData_14() const { return ___m_PointerData_14; }
	inline Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 ** get_address_of_m_PointerData_14() { return &___m_PointerData_14; }
	inline void set_m_PointerData_14(Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 * value)
	{
		___m_PointerData_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerData_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_MouseState_15() { return static_cast<int32_t>(offsetof(PointerInputModule_tD7460503C6A4E1060914FFD213535AEF6AE2F421, ___m_MouseState_15)); }
	inline MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1 * get_m_MouseState_15() const { return ___m_MouseState_15; }
	inline MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1 ** get_address_of_m_MouseState_15() { return &___m_MouseState_15; }
	inline void set_m_MouseState_15(MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1 * value)
	{
		___m_MouseState_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MouseState_15), (void*)value);
	}
};


// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t6C9AD80A60E2C2526C5E5E04403D9B6DDC9C9725  : public BaseMeshEffect_tC7D44B0AC6406BAC3E4FC4579A43FC135BDB6FDA
{
public:

public:
};


// UnityEngine.UI.Scrollbar
struct  Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_HandleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleRect_20;
	// UnityEngine.UI.Scrollbar/Direction UnityEngine.UI.Scrollbar::m_Direction
	int32_t ___m_Direction_21;
	// System.Single UnityEngine.UI.Scrollbar::m_Value
	float ___m_Value_22;
	// System.Single UnityEngine.UI.Scrollbar::m_Size
	float ___m_Size_23;
	// System.Int32 UnityEngine.UI.Scrollbar::m_NumberOfSteps
	int32_t ___m_NumberOfSteps_24;
	// UnityEngine.UI.Scrollbar/ScrollEvent UnityEngine.UI.Scrollbar::m_OnValueChanged
	ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED * ___m_OnValueChanged_25;
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_ContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_ContainerRect_26;
	// UnityEngine.Vector2 UnityEngine.UI.Scrollbar::m_Offset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Offset_27;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Scrollbar::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_28;
	// UnityEngine.Coroutine UnityEngine.UI.Scrollbar::m_PointerDownRepeat
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_PointerDownRepeat_29;
	// System.Boolean UnityEngine.UI.Scrollbar::isPointerDownAndNotDragging
	bool ___isPointerDownAndNotDragging_30;
	// System.Boolean UnityEngine.UI.Scrollbar::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_31;

public:
	inline static int32_t get_offset_of_m_HandleRect_20() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_HandleRect_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleRect_20() const { return ___m_HandleRect_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleRect_20() { return &___m_HandleRect_20; }
	inline void set_m_HandleRect_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_21() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Direction_21)); }
	inline int32_t get_m_Direction_21() const { return ___m_Direction_21; }
	inline int32_t* get_address_of_m_Direction_21() { return &___m_Direction_21; }
	inline void set_m_Direction_21(int32_t value)
	{
		___m_Direction_21 = value;
	}

	inline static int32_t get_offset_of_m_Value_22() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Value_22)); }
	inline float get_m_Value_22() const { return ___m_Value_22; }
	inline float* get_address_of_m_Value_22() { return &___m_Value_22; }
	inline void set_m_Value_22(float value)
	{
		___m_Value_22 = value;
	}

	inline static int32_t get_offset_of_m_Size_23() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Size_23)); }
	inline float get_m_Size_23() const { return ___m_Size_23; }
	inline float* get_address_of_m_Size_23() { return &___m_Size_23; }
	inline void set_m_Size_23(float value)
	{
		___m_Size_23 = value;
	}

	inline static int32_t get_offset_of_m_NumberOfSteps_24() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_NumberOfSteps_24)); }
	inline int32_t get_m_NumberOfSteps_24() const { return ___m_NumberOfSteps_24; }
	inline int32_t* get_address_of_m_NumberOfSteps_24() { return &___m_NumberOfSteps_24; }
	inline void set_m_NumberOfSteps_24(int32_t value)
	{
		___m_NumberOfSteps_24 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_OnValueChanged_25)); }
	inline ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(ScrollEvent_tD181ECDC6DDCEE9E32FBEFB0E657F0001E3099ED * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContainerRect_26() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_ContainerRect_26)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_ContainerRect_26() const { return ___m_ContainerRect_26; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_ContainerRect_26() { return &___m_ContainerRect_26; }
	inline void set_m_ContainerRect_26(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_ContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ContainerRect_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_27() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Offset_27)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Offset_27() const { return ___m_Offset_27; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Offset_27() { return &___m_Offset_27; }
	inline void set_m_Offset_27(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Offset_27 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_28() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_Tracker_28)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_28() const { return ___m_Tracker_28; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_28() { return &___m_Tracker_28; }
	inline void set_m_Tracker_28(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_28 = value;
	}

	inline static int32_t get_offset_of_m_PointerDownRepeat_29() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_PointerDownRepeat_29)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_PointerDownRepeat_29() const { return ___m_PointerDownRepeat_29; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_PointerDownRepeat_29() { return &___m_PointerDownRepeat_29; }
	inline void set_m_PointerDownRepeat_29(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_PointerDownRepeat_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerDownRepeat_29), (void*)value);
	}

	inline static int32_t get_offset_of_isPointerDownAndNotDragging_30() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___isPointerDownAndNotDragging_30)); }
	inline bool get_isPointerDownAndNotDragging_30() const { return ___isPointerDownAndNotDragging_30; }
	inline bool* get_address_of_isPointerDownAndNotDragging_30() { return &___isPointerDownAndNotDragging_30; }
	inline void set_isPointerDownAndNotDragging_30(bool value)
	{
		___isPointerDownAndNotDragging_30 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_31() { return static_cast<int32_t>(offsetof(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28, ___m_DelayedUpdateVisuals_31)); }
	inline bool get_m_DelayedUpdateVisuals_31() const { return ___m_DelayedUpdateVisuals_31; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_31() { return &___m_DelayedUpdateVisuals_31; }
	inline void set_m_DelayedUpdateVisuals_31(bool value)
	{
		___m_DelayedUpdateVisuals_31 = value;
	}
};


// UnityEngine.UI.Shadow
struct  Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E  : public BaseMeshEffect_tC7D44B0AC6406BAC3E4FC4579A43FC135BDB6FDA
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E, ___m_EffectColor_5)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E, ___m_EffectDistance_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};


// UnityEngine.UI.Slider
struct  Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillRect_20;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleRect_21;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_22;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_23;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_24;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_25;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_26;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * ___m_OnValueChanged_27;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_FillImage_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_FillTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillContainerRect_30;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_HandleTransform_31;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleContainerRect_32;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Offset_33;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_34;
	// System.Boolean UnityEngine.UI.Slider::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_35;

public:
	inline static int32_t get_offset_of_m_FillRect_20() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillRect_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillRect_20() const { return ___m_FillRect_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillRect_20() { return &___m_FillRect_20; }
	inline void set_m_FillRect_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleRect_21() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleRect_21)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleRect_21() const { return ___m_HandleRect_21; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleRect_21() { return &___m_HandleRect_21; }
	inline void set_m_HandleRect_21(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleRect_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_22() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Direction_22)); }
	inline int32_t get_m_Direction_22() const { return ___m_Direction_22; }
	inline int32_t* get_address_of_m_Direction_22() { return &___m_Direction_22; }
	inline void set_m_Direction_22(int32_t value)
	{
		___m_Direction_22 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_23() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MinValue_23)); }
	inline float get_m_MinValue_23() const { return ___m_MinValue_23; }
	inline float* get_address_of_m_MinValue_23() { return &___m_MinValue_23; }
	inline void set_m_MinValue_23(float value)
	{
		___m_MinValue_23 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_24() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MaxValue_24)); }
	inline float get_m_MaxValue_24() const { return ___m_MaxValue_24; }
	inline float* get_address_of_m_MaxValue_24() { return &___m_MaxValue_24; }
	inline void set_m_MaxValue_24(float value)
	{
		___m_MaxValue_24 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_25() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_WholeNumbers_25)); }
	inline bool get_m_WholeNumbers_25() const { return ___m_WholeNumbers_25; }
	inline bool* get_address_of_m_WholeNumbers_25() { return &___m_WholeNumbers_25; }
	inline void set_m_WholeNumbers_25(bool value)
	{
		___m_WholeNumbers_25 = value;
	}

	inline static int32_t get_offset_of_m_Value_26() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Value_26)); }
	inline float get_m_Value_26() const { return ___m_Value_26; }
	inline float* get_address_of_m_Value_26() { return &___m_Value_26; }
	inline void set_m_Value_26(float value)
	{
		___m_Value_26 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_OnValueChanged_27)); }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillImage_28() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillImage_28)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_FillImage_28() const { return ___m_FillImage_28; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_FillImage_28() { return &___m_FillImage_28; }
	inline void set_m_FillImage_28(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_FillImage_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillImage_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillTransform_29() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillTransform_29)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_FillTransform_29() const { return ___m_FillTransform_29; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_FillTransform_29() { return &___m_FillTransform_29; }
	inline void set_m_FillTransform_29(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_FillTransform_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillTransform_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillContainerRect_30)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillContainerRect_30() const { return ___m_FillContainerRect_30; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillContainerRect_30() { return &___m_FillContainerRect_30; }
	inline void set_m_FillContainerRect_30(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillContainerRect_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_31() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleTransform_31)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_HandleTransform_31() const { return ___m_HandleTransform_31; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_HandleTransform_31() { return &___m_HandleTransform_31; }
	inline void set_m_HandleTransform_31(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_HandleTransform_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleTransform_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_32() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleContainerRect_32)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleContainerRect_32() const { return ___m_HandleContainerRect_32; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleContainerRect_32() { return &___m_HandleContainerRect_32; }
	inline void set_m_HandleContainerRect_32(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleContainerRect_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleContainerRect_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_33() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Offset_33)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Offset_33() const { return ___m_Offset_33; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Offset_33() { return &___m_Offset_33; }
	inline void set_m_Offset_33(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Offset_33 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_34() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Tracker_34)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_34() const { return ___m_Tracker_34; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_34() { return &___m_Tracker_34; }
	inline void set_m_Tracker_34(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_34 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_35() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_DelayedUpdateVisuals_35)); }
	inline bool get_m_DelayedUpdateVisuals_35() const { return ___m_DelayedUpdateVisuals_35; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_35() { return &___m_DelayedUpdateVisuals_35; }
	inline void set_m_DelayedUpdateVisuals_35(bool value)
	{
		___m_DelayedUpdateVisuals_35 = value;
	}
};


// UnityEngine.UI.Toggle
struct  Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Toggle/ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_20;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic_21;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 * ___m_Group_22;
	// UnityEngine.UI.Toggle/ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t7B9EFE80B7D7F16F3E7B8FA75FEF45B00E0C0075 * ___onValueChanged_23;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_24;

public:
	inline static int32_t get_offset_of_toggleTransition_20() { return static_cast<int32_t>(offsetof(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E, ___toggleTransition_20)); }
	inline int32_t get_toggleTransition_20() const { return ___toggleTransition_20; }
	inline int32_t* get_address_of_toggleTransition_20() { return &___toggleTransition_20; }
	inline void set_toggleTransition_20(int32_t value)
	{
		___toggleTransition_20 = value;
	}

	inline static int32_t get_offset_of_graphic_21() { return static_cast<int32_t>(offsetof(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E, ___graphic_21)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_graphic_21() const { return ___graphic_21; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_graphic_21() { return &___graphic_21; }
	inline void set_graphic_21(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___graphic_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___graphic_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Group_22() { return static_cast<int32_t>(offsetof(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E, ___m_Group_22)); }
	inline ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 * get_m_Group_22() const { return ___m_Group_22; }
	inline ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 ** get_address_of_m_Group_22() { return &___m_Group_22; }
	inline void set_m_Group_22(ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 * value)
	{
		___m_Group_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Group_22), (void*)value);
	}

	inline static int32_t get_offset_of_onValueChanged_23() { return static_cast<int32_t>(offsetof(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E, ___onValueChanged_23)); }
	inline ToggleEvent_t7B9EFE80B7D7F16F3E7B8FA75FEF45B00E0C0075 * get_onValueChanged_23() const { return ___onValueChanged_23; }
	inline ToggleEvent_t7B9EFE80B7D7F16F3E7B8FA75FEF45B00E0C0075 ** get_address_of_onValueChanged_23() { return &___onValueChanged_23; }
	inline void set_onValueChanged_23(ToggleEvent_t7B9EFE80B7D7F16F3E7B8FA75FEF45B00E0C0075 * value)
	{
		___onValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onValueChanged_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsOn_24() { return static_cast<int32_t>(offsetof(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E, ___m_IsOn_24)); }
	inline bool get_m_IsOn_24() const { return ___m_IsOn_24; }
	inline bool* get_address_of_m_IsOn_24() { return &___m_IsOn_24; }
	inline void set_m_IsOn_24(bool value)
	{
		___m_IsOn_24 = value;
	}
};


// UnityEngine.UI.Outline
struct  Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2  : public Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E
{
public:

public:
};


// UnityEngine.EventSystems.Physics2DRaycaster
struct  Physics2DRaycaster_t0A86A26E1B770FECE956F4B4FD773887AF66C4C3  : public PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823
{
public:
	// UnityEngine.RaycastHit2D[] UnityEngine.EventSystems.Physics2DRaycaster::m_Hits
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ___m_Hits_11;

public:
	inline static int32_t get_offset_of_m_Hits_11() { return static_cast<int32_t>(offsetof(Physics2DRaycaster_t0A86A26E1B770FECE956F4B4FD773887AF66C4C3, ___m_Hits_11)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get_m_Hits_11() const { return ___m_Hits_11; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of_m_Hits_11() { return &___m_Hits_11; }
	inline void set_m_Hits_11(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		___m_Hits_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Hits_11), (void*)value);
	}
};


// UnityEngine.EventSystems.StandaloneInputModule
struct  StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD  : public PointerInputModule_tD7460503C6A4E1060914FFD213535AEF6AE2F421
{
public:
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_PrevActionTime
	float ___m_PrevActionTime_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMoveVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_LastMoveVector_17;
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMousePosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_LastMousePosition_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_MousePosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_MousePosition_20;
	// UnityEngine.GameObject UnityEngine.EventSystems.StandaloneInputModule::m_CurrentFocusedGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CurrentFocusedGameObject_21;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.StandaloneInputModule::m_InputPointerEvent
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___m_InputPointerEvent_22;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_23;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_24;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_25;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_CancelButton
	String_t* ___m_CancelButton_26;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_27;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_RepeatDelay
	float ___m_RepeatDelay_28;
	// System.Boolean UnityEngine.EventSystems.StandaloneInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_29;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_16() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_PrevActionTime_16)); }
	inline float get_m_PrevActionTime_16() const { return ___m_PrevActionTime_16; }
	inline float* get_address_of_m_PrevActionTime_16() { return &___m_PrevActionTime_16; }
	inline void set_m_PrevActionTime_16(float value)
	{
		___m_PrevActionTime_16 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_17() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_LastMoveVector_17)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_LastMoveVector_17() const { return ___m_LastMoveVector_17; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_LastMoveVector_17() { return &___m_LastMoveVector_17; }
	inline void set_m_LastMoveVector_17(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_LastMoveVector_17 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_18() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_ConsecutiveMoveCount_18)); }
	inline int32_t get_m_ConsecutiveMoveCount_18() const { return ___m_ConsecutiveMoveCount_18; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_18() { return &___m_ConsecutiveMoveCount_18; }
	inline void set_m_ConsecutiveMoveCount_18(int32_t value)
	{
		___m_ConsecutiveMoveCount_18 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_19() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_LastMousePosition_19)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_LastMousePosition_19() const { return ___m_LastMousePosition_19; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_LastMousePosition_19() { return &___m_LastMousePosition_19; }
	inline void set_m_LastMousePosition_19(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_LastMousePosition_19 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_20() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_MousePosition_20)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_MousePosition_20() const { return ___m_MousePosition_20; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_MousePosition_20() { return &___m_MousePosition_20; }
	inline void set_m_MousePosition_20(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_MousePosition_20 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFocusedGameObject_21() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_CurrentFocusedGameObject_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CurrentFocusedGameObject_21() const { return ___m_CurrentFocusedGameObject_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CurrentFocusedGameObject_21() { return &___m_CurrentFocusedGameObject_21; }
	inline void set_m_CurrentFocusedGameObject_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CurrentFocusedGameObject_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentFocusedGameObject_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_22() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_InputPointerEvent_22)); }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * get_m_InputPointerEvent_22() const { return ___m_InputPointerEvent_22; }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 ** get_address_of_m_InputPointerEvent_22() { return &___m_InputPointerEvent_22; }
	inline void set_m_InputPointerEvent_22(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * value)
	{
		___m_InputPointerEvent_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputPointerEvent_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_23() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_HorizontalAxis_23)); }
	inline String_t* get_m_HorizontalAxis_23() const { return ___m_HorizontalAxis_23; }
	inline String_t** get_address_of_m_HorizontalAxis_23() { return &___m_HorizontalAxis_23; }
	inline void set_m_HorizontalAxis_23(String_t* value)
	{
		___m_HorizontalAxis_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalAxis_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_24() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_VerticalAxis_24)); }
	inline String_t* get_m_VerticalAxis_24() const { return ___m_VerticalAxis_24; }
	inline String_t** get_address_of_m_VerticalAxis_24() { return &___m_VerticalAxis_24; }
	inline void set_m_VerticalAxis_24(String_t* value)
	{
		___m_VerticalAxis_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalAxis_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_25() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_SubmitButton_25)); }
	inline String_t* get_m_SubmitButton_25() const { return ___m_SubmitButton_25; }
	inline String_t** get_address_of_m_SubmitButton_25() { return &___m_SubmitButton_25; }
	inline void set_m_SubmitButton_25(String_t* value)
	{
		___m_SubmitButton_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SubmitButton_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_CancelButton_26() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_CancelButton_26)); }
	inline String_t* get_m_CancelButton_26() const { return ___m_CancelButton_26; }
	inline String_t** get_address_of_m_CancelButton_26() { return &___m_CancelButton_26; }
	inline void set_m_CancelButton_26(String_t* value)
	{
		___m_CancelButton_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CancelButton_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_27() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_InputActionsPerSecond_27)); }
	inline float get_m_InputActionsPerSecond_27() const { return ___m_InputActionsPerSecond_27; }
	inline float* get_address_of_m_InputActionsPerSecond_27() { return &___m_InputActionsPerSecond_27; }
	inline void set_m_InputActionsPerSecond_27(float value)
	{
		___m_InputActionsPerSecond_27 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_28() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_RepeatDelay_28)); }
	inline float get_m_RepeatDelay_28() const { return ___m_RepeatDelay_28; }
	inline float* get_address_of_m_RepeatDelay_28() { return &___m_RepeatDelay_28; }
	inline void set_m_RepeatDelay_28(float value)
	{
		___m_RepeatDelay_28 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_29() { return static_cast<int32_t>(offsetof(StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD, ___m_ForceModuleActive_29)); }
	inline bool get_m_ForceModuleActive_29() const { return ___m_ForceModuleActive_29; }
	inline bool* get_address_of_m_ForceModuleActive_29() { return &___m_ForceModuleActive_29; }
	inline void set_m_ForceModuleActive_29(bool value)
	{
		___m_ForceModuleActive_29 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};


// UnityEngine.EventSystems.TouchInputModule
struct  TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B  : public PointerInputModule_tD7460503C6A4E1060914FFD213535AEF6AE2F421
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_LastMousePosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_LastMousePosition_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_MousePosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_MousePosition_17;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.TouchInputModule::m_InputPointerEvent
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___m_InputPointerEvent_18;
	// System.Boolean UnityEngine.EventSystems.TouchInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_19;

public:
	inline static int32_t get_offset_of_m_LastMousePosition_16() { return static_cast<int32_t>(offsetof(TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B, ___m_LastMousePosition_16)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_LastMousePosition_16() const { return ___m_LastMousePosition_16; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_LastMousePosition_16() { return &___m_LastMousePosition_16; }
	inline void set_m_LastMousePosition_16(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_LastMousePosition_16 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_17() { return static_cast<int32_t>(offsetof(TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B, ___m_MousePosition_17)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_MousePosition_17() const { return ___m_MousePosition_17; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_MousePosition_17() { return &___m_MousePosition_17; }
	inline void set_m_MousePosition_17(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_MousePosition_17 = value;
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_18() { return static_cast<int32_t>(offsetof(TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B, ___m_InputPointerEvent_18)); }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * get_m_InputPointerEvent_18() const { return ___m_InputPointerEvent_18; }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 ** get_address_of_m_InputPointerEvent_18() { return &___m_InputPointerEvent_18; }
	inline void set_m_InputPointerEvent_18(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * value)
	{
		___m_InputPointerEvent_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputPointerEvent_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_19() { return static_cast<int32_t>(offsetof(TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B, ___m_ForceModuleActive_19)); }
	inline bool get_m_ForceModuleActive_19() const { return ___m_ForceModuleActive_19; }
	inline bool* get_address_of_m_ForceModuleActive_19() { return &___m_ForceModuleActive_19; }
	inline void set_m_ForceModuleActive_19(bool value)
	{
		___m_ForceModuleActive_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3348[3] = 
{
	Axis_t561E10ABB080BB3C1F7C93C39E8DDD06BE6490B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3349[5] = 
{
	U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE::get_offset_of_U3CU3E1__state_0(),
	U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE::get_offset_of_U3CU3E2__current_1(),
	U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE::get_offset_of_U3CU3E4__this_2(),
	U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE::get_offset_of_screenPosition_3(),
	U3CClickRepeatU3Ed__58_t4A7572863E83E4FDDB7EC44F38E5C0055224BDCE::get_offset_of_camera_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3350[12] = 
{
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_HandleRect_20(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_Direction_21(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_Value_22(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_Size_23(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_NumberOfSteps_24(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_OnValueChanged_25(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_ContainerRect_26(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_Offset_27(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_Tracker_28(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_PointerDownRepeat_29(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_isPointerDownAndNotDragging_30(),
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28::get_offset_of_m_DelayedUpdateVisuals_31(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3351[5] = 
{
	Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3352[6] = 
{
	SelectionState_tB421C4551CDC64C8EB31158E8C7FF118F46FF72F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3353[16] = 
{
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields::get_offset_of_s_Selectables_4(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields::get_offset_of_s_SelectableCount_5(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_EnableCalled_6(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_Navigation_7(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_Transition_8(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_Colors_9(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_SpriteState_10(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_AnimationTriggers_11(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_Interactable_12(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_TargetGraphic_13(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_GroupsAllowInteraction_14(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_CurrentIndex_15(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_U3CisPointerInsideU3Ek__BackingField_16(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_U3CisPointerDownU3Ek__BackingField_17(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_U3ChasSelectionU3Ek__BackingField_18(),
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD::get_offset_of_m_CanvasGroupCache_19(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3355[5] = 
{
	Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3357[3] = 
{
	Axis_t5BFF2AACB2D94E92243ED4EF295A1DCAF2FC52D5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3358[16] = 
{
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_FillRect_20(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_HandleRect_21(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_Direction_22(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_MinValue_23(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_MaxValue_24(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_WholeNumbers_25(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_Value_26(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_OnValueChanged_27(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_FillImage_28(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_FillTransform_29(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_FillContainerRect_30(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_HandleTransform_31(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_HandleContainerRect_32(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_Offset_33(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_Tracker_34(),
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A::get_offset_of_m_DelayedUpdateVisuals_35(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3359[4] = 
{
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E::get_offset_of_m_SelectedSprite_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E::get_offset_of_m_DisabledSprite_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3360[10] = 
{
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_baseMat_0(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_customMat_1(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_count_2(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_stencilId_3(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_operation_4(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_compareFunction_5(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_readMask_6(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_writeMask_7(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_useAlphaClip_8(),
	MatEntry_t94DD6F2A201E3EF569EE31B20C3EC8274A9E022E::get_offset_of_colorMask_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3361[1] = 
{
	StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_StaticFields::get_offset_of_m_List_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3362[7] = 
{
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1::get_offset_of_m_FontData_36(),
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1::get_offset_of_m_Text_37(),
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1::get_offset_of_m_TextCache_38(),
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1::get_offset_of_m_TextCacheForLayout_39(),
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields::get_offset_of_s_DefaultText_40(),
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1::get_offset_of_m_DisableFontTextureRebuiltCallback_41(),
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1::get_offset_of_m_TempVerts_42(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3363[3] = 
{
	ToggleTransition_t4D1AA30F2BA24242EB9D1DD2E3DF839F0BAC5167::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3365[5] = 
{
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E::get_offset_of_toggleTransition_20(),
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E::get_offset_of_graphic_21(),
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E::get_offset_of_m_Group_22(),
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E::get_offset_of_onValueChanged_23(),
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E::get_offset_of_m_IsOn_24(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3366[3] = 
{
	U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961_StaticFields::get_offset_of_U3CU3E9__13_0_1(),
	U3CU3Ec_t6FADCC9ADE15B1BB28A4FA9CDCE1340EFAEB9961_StaticFields::get_offset_of_U3CU3E9__14_0_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3367[2] = 
{
	ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95::get_offset_of_m_AllowSwitchOff_4(),
	ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95::get_offset_of_m_Toggles_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3368[1] = 
{
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3369[4] = 
{
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3376[7] = 
{
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1::get_offset_of_getRaycastNonAlloc_2(),
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1::get_offset_of_raycast2D_3(),
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1::get_offset_of_getRayIntersectionAll_4(),
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1::get_offset_of_getRayIntersectionAllNonAlloc_5(),
	ReflectionMethodsCache_t315D4F7C3E58291AD340D6CBFD1A09B2B1EAE8D1_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3377[12] = 
{
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Positions_0(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Colors_1(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Uv0S_2(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Uv1S_3(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Uv2S_4(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Uv3S_5(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Normals_6(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Tangents_7(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_Indices_8(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55::get_offset_of_m_ListsInitalized_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3379[1] = 
{
	BaseMeshEffect_tC7D44B0AC6406BAC3E4FC4579A43FC135BDB6FDA::get_offset_of_m_Graphic_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3384[4] = 
{
	Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E::get_offset_of_m_EffectColor_5(),
	Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E::get_offset_of_m_EffectDistance_6(),
	Shadow_t96D9C6FC7BB4D9CBEB5788F2333125365DE12F8E::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3385[2] = 
{
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3387[4] = 
{
	ColorTweenMode_tC8254CFED9F320A1B7A452159F60A143952DFE19::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3389[6] = 
{
	ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_tB608DC1CF7A7F226B0D4DD8B269798F27CECE339::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3391[5] = 
{
	FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_tFC6A79CB4DD9D51D99523093925F926E12D2F228::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3392[4] = 
{
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3393[2] = 
{
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3394[2] = 
{
	AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3395[1] = 
{
	AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF::get_offset_of_m_Used_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3396[1] = 
{
	BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E::get_offset_of_m_EventSystem_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3397[4] = 
{
	InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3398[5] = 
{
	FramePressState_t4BB461B7704D7F72519B36A0C8B3370AB302E7A7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3399[22] = 
{
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_m_PointerPress_3(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpointerClickU3Ek__BackingField_7(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_hovered_10(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CeligibleForClickU3Ek__BackingField_11(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpointerIdU3Ek__BackingField_12(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpositionU3Ek__BackingField_13(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CdeltaU3Ek__BackingField_14(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CpressPositionU3Ek__BackingField_15(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CworldPositionU3Ek__BackingField_16(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CworldNormalU3Ek__BackingField_17(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CclickTimeU3Ek__BackingField_18(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CclickCountU3Ek__BackingField_19(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CscrollDeltaU3Ek__BackingField_20(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_21(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CdraggingU3Ek__BackingField_22(),
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954::get_offset_of_U3CbuttonU3Ek__BackingField_23(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3400[3] = 
{
	EventHandle_t2A81C886C0708BC766E39686BBB54121A310F554::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3419[11] = 
{
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_SystemInputModules_4(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_CurrentInputModule_5(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields::get_offset_of_m_EventSystems_6(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_FirstSelected_7(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_sendNavigationEvents_8(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_DragThreshold_9(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_CurrentSelected_10(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_HasFocus_11(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_SelectionGuard_12(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C::get_offset_of_m_DummyData_13(),
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields::get_offset_of_s_RaycastComparer_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3421[2] = 
{
	Entry_t9C594CD634607709CF020BE9C8A469E1C9033D36::get_offset_of_eventID_0(),
	Entry_t9C594CD634607709CF020BE9C8A469E1C9033D36::get_offset_of_callback_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3422[1] = 
{
	EventTrigger_tA136EB086A23F8BBDC2D547223F1AA9CBA9A2563::get_offset_of_m_Delegates_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3423[18] = 
{
	EventTriggerType_tED9176836ED486B7FEE926108C027C4E2954B9CE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3425[1] = 
{
	U3CU3Ec_t20C9A4C48478BFCA11C0533F07831530FE1782BB_StaticFields::get_offset_of_U3CU3E9_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3426[19] = 
{
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_tEA324150A01AFB802974FA8B7DB1C19F83FECA68_StaticFields::get_offset_of_s_InternalTransformList_18(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3428[6] = 
{
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924::get_offset_of_m_RaycastResultCache_4(),
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924::get_offset_of_m_AxisEventData_5(),
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924::get_offset_of_m_EventSystem_6(),
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924::get_offset_of_m_BaseEventData_7(),
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924::get_offset_of_m_InputOverride_8(),
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924::get_offset_of_m_DefaultInput_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3429[2] = 
{
	ButtonState_t49AF0FCF7DF429002E167972B40DC5A2A3804562::get_offset_of_m_Button_0(),
	ButtonState_t49AF0FCF7DF429002E167972B40DC5A2A3804562::get_offset_of_m_EventData_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3430[1] = 
{
	MouseState_tD62A64A795CF964D179003BB566EF667DB7DACC1::get_offset_of_m_TrackedButtons_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3431[2] = 
{
	MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6::get_offset_of_buttonState_0(),
	MouseButtonEventData_tE8E157C9D47E03160B193B4853B5DF5AA3FA65B6::get_offset_of_buttonData_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3432[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_tD7460503C6A4E1060914FFD213535AEF6AE2F421::get_offset_of_m_PointerData_14(),
	PointerInputModule_tD7460503C6A4E1060914FFD213535AEF6AE2F421::get_offset_of_m_MouseState_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3433[3] = 
{
	InputMode_tABD640D064CD823116744F702C9DD0836A7E8972::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3434[14] = 
{
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_PrevActionTime_16(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_LastMoveVector_17(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_ConsecutiveMoveCount_18(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_LastMousePosition_19(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_MousePosition_20(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_CurrentFocusedGameObject_21(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_InputPointerEvent_22(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_HorizontalAxis_23(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_VerticalAxis_24(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_SubmitButton_25(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_CancelButton_26(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_InputActionsPerSecond_27(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_RepeatDelay_28(),
	StandaloneInputModule_tA1F0F27C9314CBB9B5E3E583D455DD97355E8BAD::get_offset_of_m_ForceModuleActive_29(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3435[4] = 
{
	TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B::get_offset_of_m_LastMousePosition_16(),
	TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B::get_offset_of_m_MousePosition_17(),
	TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B::get_offset_of_m_InputPointerEvent_18(),
	TouchInputModule_tC92ADD4A36C73348565AD94F128327F6D44DBB9B::get_offset_of_m_ForceModuleActive_19(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3436[6] = 
{
	MoveDirection_t740623362F85DF2963BE20C702F7B8EF44E91645::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3437[11] = 
{
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_module_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_index_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE::get_offset_of_displayIndex_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3438[1] = 
{
	RaycasterManager_t9B5A044582C34098C71FC3C8CD413369CDE0DA33_StaticFields::get_offset_of_s_Raycasters_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3439[1] = 
{
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876::get_offset_of_m_RootRaycaster_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3440[1] = 
{
	Physics2DRaycaster_t0A86A26E1B770FECE956F4B4FD773887AF66C4C3::get_offset_of_m_Hits_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3441[1] = 
{
	RaycastHitComparer_tC1146CEF99040544A2E1034A40CA0E4747E83877_StaticFields::get_offset_of_instance_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3442[6] = 
{
	0,
	PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823::get_offset_of_m_EventCamera_6(),
	PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823::get_offset_of_m_EventMask_7(),
	PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823::get_offset_of_m_MaxRayIntersections_8(),
	PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823::get_offset_of_m_LastMaxRayIntersections_9(),
	PhysicsRaycaster_t30CAABC8B439EB2F455D320192635CFD2BD89823::get_offset_of_m_Hits_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3445[1] = 
{
	U3CPrivateImplementationDetailsU3E_tA4B8E3F98E3B6A41218937C44898DCEE20629F8F_StaticFields::get_offset_of_U31C3635C112D556F4C11A4FE6BDE6ED3F126C4B2B546811BDB64DE7BDED3A05CB_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3447[1] = 
{
	WebRequestUtils_t3FE2D9FD71A02CD3AF8C91B81280F59E5CF26392_StaticFields::get_offset_of_domainRegex_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3449[9] = 
{
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_dataSpace_4(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_urlForbidden_5(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_qpEscapeChar_6(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_qpSpace_7(),
	WWWTranscoder_t61D467EE2097E0FE6FA215AAEA4D3BF4216CB771_StaticFields::get_offset_of_qpForbidden_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3450[1] = 
{
	UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396::get_offset_of_U3CwebRequestU3Ek__BackingField_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3451[6] = 
{
	UnityWebRequestMethod_tF538D9A75B76FFC81710E65697E38C1B12E4F7E5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3452[30] = 
{
	UnityWebRequestError_t01C779C192877A58EBDB44371C42F9A5831EB9F6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3453[6] = 
{
	Result_t3233C0F690EC3844C8E0C4649568659679AFBE75::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3454[8] = 
{
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_m_Ptr_0(),
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_m_DownloadHandler_1(),
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_m_UploadHandler_2(),
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_m_CertificateHandler_3(),
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_m_Uri_4(),
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5(),
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6(),
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E::get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3455[1] = 
{
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E::get_offset_of_m_Ptr_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3456[1] = 
{
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB::get_offset_of_m_Ptr_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3458[1] = 
{
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA::get_offset_of_m_Ptr_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3460[4] = 
{
	Permission_t54F789471D32013B167C19F2CD6132E7E49298A2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3461[4] = 
{
	PickMode_tE2DF59B643089F6FE81412D1C152B52222B0414B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3462[2] = 
{
	FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A::get_offset_of_extension_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A::get_offset_of_icon_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3463[3] = 
{
	QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7::get_offset_of_target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7::get_offset_of_name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7::get_offset_of_icon_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3464[3] = 
{
	Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8::get_offset_of_name_0(),
	Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8::get_offset_of_extensions_1(),
	Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8::get_offset_of_defaultExtension_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3467[2] = 
{
	U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields::get_offset_of_U3CU3E9__208_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3468[3] = 
{
	U3CCreateNewFolderCoroutineU3Ed__212_t1B9AE04C7B0DABED86E805273034CE28E705DDF6::get_offset_of_U3CU3E1__state_0(),
	U3CCreateNewFolderCoroutineU3Ed__212_t1B9AE04C7B0DABED86E805273034CE28E705DDF6::get_offset_of_U3CU3E2__current_1(),
	U3CCreateNewFolderCoroutineU3Ed__212_t1B9AE04C7B0DABED86E805273034CE28E705DDF6::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3469[2] = 
{
	U3CU3Ec__DisplayClass213_0_tE83CD523E9B1680A5A376728628E6A0125228E14::get_offset_of_fileInfo_0(),
	U3CU3Ec__DisplayClass213_0_tE83CD523E9B1680A5A376728628E6A0125228E14::get_offset_of_U3CU3E4__this_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3470[8] = 
{
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_pickMode_2(),
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_allowMultiSelection_3(),
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_initialPath_4(),
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_initialFilename_5(),
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_title_6(),
	U3CWaitForSaveDialogU3Ed__231_t6AC2F688AA7EDBB61264895B03C6FA2E0770E389::get_offset_of_saveButtonText_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3471[8] = 
{
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_pickMode_2(),
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_allowMultiSelection_3(),
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_initialPath_4(),
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_initialFilename_5(),
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_title_6(),
	U3CWaitForLoadDialogU3Ed__232_t0E070E63F7FEA5092B2630E3E1A682F01CFBA28B::get_offset_of_loadButtonText_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3472[106] = 
{
	0,
	0,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_U3CIsOpenU3Ek__BackingField_6(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_U3CSuccessU3Ek__BackingField_7(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_U3CResultU3Ek__BackingField_8(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_askPermissions_9(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_singleClickMode_10(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_drivesRefreshInterval_11(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_displayHiddenFilesToggle_12(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_allFilesFilterText_13(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_foldersFilterText_14(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_pickFolderQuickLinkText_15(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_m_instance_16(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_normalFileColor_17(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_hoveredFileColor_18(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_selectedFileColor_19(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_wrongFilenameColor_20(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_minWidth_21(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_minHeight_22(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_narrowScreenWidth_23(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_quickLinksMaxWidthPercentage_24(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_sortFilesByName_25(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_excludeExtensions_26(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_quickLinks_27(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields::get_offset_of_quickLinksInitialized_28(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_excludedExtensionsSet_29(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_generateQuickLinksForDrives_30(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_contextMenuShowDeleteButton_31(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_contextMenuShowRenameButton_32(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_showResizeCursor_33(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_folderIcon_34(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_driveIcon_35(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_defaultIcon_36(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filetypeIcons_37(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filetypeToIcon_38(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_multiSelectionToggleOffIcon_39(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_multiSelectionToggleOnIcon_40(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_window_41(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_windowTR_42(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_topViewNarrowScreen_43(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_middleView_44(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_middleViewOriginalPosition_45(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_middleViewOriginalSize_46(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_middleViewQuickLinks_47(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_middleViewQuickLinksOriginalSize_48(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_middleViewFiles_49(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_middleViewSeparator_50(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_itemPrefab_51(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_allItems_52(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_itemHeight_53(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_quickLinkPrefab_54(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_allQuickLinks_55(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_titleText_56(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_backButton_57(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_forwardButton_58(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_upButton_59(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_pathInputField_60(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_pathInputFieldSlotTop_61(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_pathInputFieldSlotBottom_62(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_searchInputField_63(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_quickLinksContainer_64(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filesContainer_65(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filesScrollRect_66(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_listView_67(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filenameInputField_68(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filenameInputFieldOverlayText_69(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filenameImage_70(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filtersDropdown_71(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filtersDropdownContainer_72(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filterItemTemplate_73(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_showHiddenFilesToggle_74(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_submitButtonText_75(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_moreOptionsContextMenuPosition_76(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_renameItem_77(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_contextMenu_78(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_deleteConfirmationPanel_79(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_resizeCursorHandler_80(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_rectTransform_81(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_canvas_82(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_ignoredFileAttributes_83(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_allFileEntries_84(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_validFileEntries_85(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_selectedFileEntries_86(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_pendingFileEntrySelection_87(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_multiSelectionPivotFileEntry_88(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_multiSelectionFilenameBuilder_89(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_filters_90(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_allFilesFilter_91(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_showAllFilesFilter_92(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_defaultInitialPath_93(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_currentPathIndex_94(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_pathsFollowed_95(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_invalidFilenameChars_96(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_drivesNextRefreshTime_97(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_driveQuickLinks_98(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_numberOfDriveQuickLinks_99(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_canvasDimensionsChanged_100(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_nullPointerEventData_101(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_m_currentPath_102(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_m_searchString_103(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_m_acceptNonExistingFilename_104(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_m_pickerMode_105(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_m_allowMultiSelection_106(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_m_multiSelectionToggleSelectionMode_107(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_onSuccess_108(),
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8::get_offset_of_onCancel_109(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3473[8] = 
{
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_fileBrowser_4(),
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_rectTransform_5(),
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_selectAllButton_6(),
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_deselectAllButton_7(),
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_deleteButton_8(),
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_renameButton_9(),
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_selectAllButtonSeparator_10(),
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC::get_offset_of_minDistanceToEdges_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3474[3] = 
{
	FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C::get_offset_of_resizeCursor_4(),
	FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C::get_offset_of_isHovering_5(),
	FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C::get_offset_of_isResizing_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3476[9] = 
{
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_deletedItems_4(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_deletedItemIcons_5(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_deletedItemNames_6(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_deletedItemsRest_7(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_deletedItemsRestLabel_8(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_yesButtonTransform_9(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_noButtonTransform_10(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_narrowScreenWidth_11(),
	FileBrowserDeleteConfirmationPanel_t41CFCECFB30E9A78DC3E5463100CD94B080A6A7C::get_offset_of_onDeletionConfirmed_12(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3477[4] = 
{
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9::get_offset_of_Path_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9::get_offset_of_Name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9::get_offset_of_Extension_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9::get_offset_of_Attributes_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3479[12] = 
{
	0,
	0,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_fileBrowser_9(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_background_10(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_icon_11(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_multiSelectionToggle_12(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_nameText_13(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_isSelected_14(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_pressTime_15(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_prevClickTime_16(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_m_transform_17(),
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025::get_offset_of_U3CIsDirectoryU3Ek__BackingField_18(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3480[8] = 
{
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_fileBrowser_4(),
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_canvasTR_5(),
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_canvasCam_6(),
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_window_7(),
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_listView_8(),
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_initialTouchPos_9(),
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_initialAnchoredPos_10(),
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19::get_offset_of_initialSizeDelta_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3481[1] = 
{
	FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C::get_offset_of_m_targetPath_19(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3483[4] = 
{
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4::get_offset_of_background_4(),
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4::get_offset_of_icon_5(),
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4::get_offset_of_nameInputField_6(),
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4::get_offset_of_onRenameCompleted_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3487[3] = 
{
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED::get_offset_of_U3CTagU3Ek__BackingField_4(),
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED::get_offset_of_U3CPositionU3Ek__BackingField_5(),
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED::get_offset_of_adapter_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3488[10] = 
{
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_viewportTransform_4(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_contentTransform_5(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_itemHeight_6(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of__1OverItemHeight_7(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_viewportHeight_8(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_items_9(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_pooledItems_10(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_adapter_11(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_currentTopIndex_12(),
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767::get_offset_of_currentBottomIndex_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3491[3] = 
{
	DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142::get_offset_of_mTexture_1(),
	DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142::get_offset_of_mHasTexture_2(),
	DownloadHandlerTexture_tA2708E80049AA58E9B0DBE9F5325CC9A1B487142::get_offset_of_mNonReadable_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3493[1] = 
{
	WWW_tCC46D6E5A368D4A83A3D6FAFF00B19700C5373E2::get_offset_of__uwr_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3496[2] = 
{
	MTLLoader_tDE3C3EB24E2C96C6A4C6F1D2D19AAABC9EC93E82::get_offset_of_SearchPaths_0(),
	MTLLoader_tDE3C3EB24E2C96C6A4C6F1D2D19AAABC9EC93E82::get_offset_of__objFileInfo_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3497[3] = 
{
	ObjFromFile_tA35B91E55B4B729F6B47DE8BEB212203D489B788::get_offset_of_objPath_4(),
	ObjFromFile_tA35B91E55B4B729F6B47DE8BEB212203D489B788::get_offset_of_error_5(),
	ObjFromFile_tA35B91E55B4B729F6B47DE8BEB212203D489B788::get_offset_of_loadedObject_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3499[2] = 
{
	U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tDA8BDFBDDF3A8A4432A732B0FDC7D3C946DFEB73_StaticFields::get_offset_of_U3CU3E9__6_3_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3500[4] = 
{
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50::get_offset_of_U3CU3E1__state_0(),
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50::get_offset_of_U3CU3E2__current_1(),
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50::get_offset_of_input_2(),
	U3CShowLoadDialogCoroutineU3Ed__13_t20B92FD99BDD758F9302A08A3C9DC8A853C95A50::get_offset_of_U3CU3E4__this_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3501[5] = 
{
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339::get_offset_of_input_path_4(),
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339::get_offset_of_output_path_5(),
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339::get_offset_of_m_Text_6(),
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339::get_offset_of_m_SceneLoaded_7(),
	mainMenu_t5BDB19E71956BBB9059721EEFA5F232E14F03339::get_offset_of_dropdown_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3502[11] = 
{
	BMPComressionMode_tDC4995809AD3E520B5B9F36D4452FB1623679454::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3503[4] = 
{
	BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0::get_offset_of_magic_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0::get_offset_of_filesize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0::get_offset_of_reserved_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BMPFileHeader_tA86362404C5C18798ABDBF97880346140BBA49A0::get_offset_of_offset_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3504[11] = 
{
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_size_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_width_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_height_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_nColorPlanes_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_nBitsPerPixel_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_compressionMethod_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_rawImageSize_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_xPPM_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_yPPM_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_nPaletteColors_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BitmapInfoHeader_tBBB712DCC439AB3C9940A0C938FF548973D11476::get_offset_of_nImportantColors_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3505[8] = 
{
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_header_0(),
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_info_1(),
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_rMask_2(),
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_gMask_3(),
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_bMask_4(),
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_aMask_5(),
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_palette_6(),
	BMPImage_tDD7CD67966633D8E970C33C7F8FB746A98D26AA0::get_offset_of_imageData_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3506[3] = 
{
	0,
	BMPLoader_t7887312A90AEBE67CF364032B76634D8E4E5C828::get_offset_of_ReadPaletteAlpha_1(),
	BMPLoader_t7887312A90AEBE67CF364032B76634D8E4E5C828::get_offset_of_ForceAlphaReadWhenPossible_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3507[3] = 
{
	BitStreamReader_t346F29AEEFCB12190328AD5372F70B676DA34EC3::get_offset_of_m_Reader_0(),
	BitStreamReader_t346F29AEEFCB12190328AD5372F70B676DA34EC3::get_offset_of_m_Data_1(),
	BitStreamReader_t346F29AEEFCB12190328AD5372F70B676DA34EC3::get_offset_of_m_Bits_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3508[9] = 
{
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_word_0(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_wordSize_1(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_endReached_2(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_reader_3(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_bufferSize_4(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_buffer_5(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_currentChar_6(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_currentPosition_7(),
	CharWordReader_tAC7EB7D2ECCEF3932D11D0E91F09CD71820837A9::get_offset_of_maxPosition_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3509[4] = 
{
	SplitMode_t967BD1F9C357CCFC5E67C9DD56784DC2BBD707AE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3510[3] = 
{
	U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488::get_offset_of_builderDict_0(),
	U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488::get_offset_of_currentBuilder_1(),
	U3CU3Ec__DisplayClass7_0_tF8ECEAB064759E2905CEDA0088A9108AF5FB0488::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3511[6] = 
{
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896::get_offset_of_SplitMode_0(),
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896::get_offset_of_Vertices_1(),
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896::get_offset_of_Normals_2(),
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896::get_offset_of_UVs_3(),
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896::get_offset_of_Materials_4(),
	OBJLoader_tDEAA2E2DECA36AE97E7CD7FCDF4D5F0F4520A896::get_offset_of__objInfo_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3513[3] = 
{
	ObjLoopHash_t74EB66CEFCEC5238360135A50B1E80AC8EA8BAB5::get_offset_of_vertexIndex_0(),
	ObjLoopHash_t74EB66CEFCEC5238360135A50B1E80AC8EA8BAB5::get_offset_of_normalIndex_1(),
	ObjLoopHash_t74EB66CEFCEC5238360135A50B1E80AC8EA8BAB5::get_offset_of_uvIndex_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3514[11] = 
{
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of_U3CPushedFaceCountU3Ek__BackingField_0(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__loader_1(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__name_2(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__globalIndexRemap_3(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__materialIndices_4(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__currentIndexList_5(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__lastMaterial_6(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__vertices_7(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__normals_8(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of__uvs_9(),
	OBJObjectBuilder_t303D02198229601B64EFC6BCC7B2A2A56B4D1DAA::get_offset_of_recalculateNormals_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3518[7] = 
{
	TextureFormat_tFF539B6329240C7555E512E6855810CE554BF351::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3524[3] = 
{
	FaceIndices_t20F9E271361AC1CEC1AE901CF52E9AA0595209D5::get_offset_of_vertIdx_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceIndices_t20F9E271361AC1CEC1AE901CF52E9AA0595209D5::get_offset_of_uvIdx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceIndices_t20F9E271361AC1CEC1AE901CF52E9AA0595209D5::get_offset_of_normIdx_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3525[5] = 
{
	ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71::get_offset_of_name_0(),
	ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71::get_offset_of_faceGroups_1(),
	ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71::get_offset_of_allFaces_2(),
	ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71::get_offset_of_hasNormals_3(),
	ObjectData_t28A4B1067C49504AC0FA478BFF0D291B7A4C6F71::get_offset_of_hasColors_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3526[3] = 
{
	FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3::get_offset_of_name_0(),
	FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3::get_offset_of_materialName_1(),
	FaceGroupData_t7E9D386EB054E3F554E6871CFB49E1F11A0BA0F3::get_offset_of_faces_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3527[9] = 
{
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_objectList_0(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_vertList_1(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_uvList_2(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_normalList_3(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_colorList_4(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_unnamedGroupIndex_5(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_currObjData_6(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_currGroup_7(),
	DataSet_t9E544E9FB871D89700F89C0AA9F8ED3BD9395863::get_offset_of_noFaceDefined_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3528[17] = 
{
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_materialName_0(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_ambientColor_1(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_diffuseColor_2(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_specularColor_3(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_emissiveColor_4(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_shininess_5(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_overallAlpha_6(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_illumType_7(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_hasReflectionTex_8(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_diffuseTexPath_9(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_diffuseTex_10(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_bumpTexPath_11(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_bumpTex_12(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_specularTexPath_13(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_specularTex_14(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_opacityTexPath_15(),
	MaterialData_t9CE636B81614FFD4289D4C51DA2DFCE867C51E48::get_offset_of_opacityTex_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3529[5] = 
{
	MtlBlendMode_t5560844B5D0A9E82A0A09A9697CB795FDABDB025::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3531[4] = 
{
	ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C::get_offset_of_materialsLoaded_0(),
	ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C::get_offset_of_objectsLoaded_1(),
	ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C::get_offset_of_groupsLoaded_2(),
	ProgressInfo_t7B5820AD3D6408E1B5C9330BF81D958746A8B92C::get_offset_of_numGroups_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3532[11] = 
{
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_newObject_0(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_objCount_1(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_subObjCount_2(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_idxCount_3(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_grpIdx_4(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_numGroups_5(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_grpFaceIdx_6(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_meshPartIdx_7(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_totFaceIdxCount_8(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_currObjGameObject_9(),
	BuildStatus_t82BFCF7016B526D6E9479E9F003CB2A9A5A55B96::get_offset_of_subObjParent_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3533[9] = 
{
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D::get_offset_of_buildOptions_0(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D::get_offset_of_buildStatus_1(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D::get_offset_of_currDataSet_2(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D::get_offset_of_currParentObj_3(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D::get_offset_of_currMaterials_4(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D::get_offset_of_materialData_5(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D_StaticFields::get_offset_of_MAX_VERTICES_LIMIT_FOR_A_MESH_6(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D_StaticFields::get_offset_of_MAX_INDICES_LIMIT_FOR_A_MESH_7(),
	ObjectBuilder_t2ABBFE0B2BD514D95B6DA92904C56B966416D86D_StaticFields::get_offset_of_MAX_VERT_COUNT_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3535[14] = 
{
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_zUp_0(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_litDiffuse_1(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_convertToDoubleSided_2(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_modelScaling_3(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_reuseLoaded_4(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_inheritLayer_5(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_buildColliders_6(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_colliderConvex_7(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_colliderTrigger_8(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_use32bitIndices_9(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_hideWhileLoading_10(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_localPosition_11(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_localEulerAngles_12(),
	ImportOptions_t1D316F48C50596172DC57EA3AB3B5532BE80952F::get_offset_of_localScale_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3536[3] = 
{
	BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E::get_offset_of_texturesTime_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E::get_offset_of_materialsTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BuildStats_tAD28E84DC7B795553C76D18A3C886EE22F48655E::get_offset_of_objectsTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3537[5] = 
{
	Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F::get_offset_of_modelParseTime_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F::get_offset_of_materialsParseTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F::get_offset_of_buildTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F::get_offset_of_buildStats_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Stats_t7AEEEEC00E152723CC754E94A16FE13DD594192F::get_offset_of_totalTime_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3538[10] = 
{
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_absolutePath_2(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_objName_3(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_U3CU3E4__this_4(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_parentObj_5(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_U3CnameU3E5__2_6(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_U3ClastTimeU3E5__3_7(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_U3CstartTimeU3E5__4_8(),
	U3CLoadU3Ed__32_t9E1CD58B4A50D02D088C1547AECAA6C695B51903::get_offset_of_U3CnewObjU3E5__5_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3539[15] = 
{
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CU3E1__state_0(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CU3E2__current_1(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CU3E4__this_2(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_absolutePath_3(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_objName_4(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_parentTransform_5(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CprevTimeU3E5__2_6(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CinfoU3E5__3_7(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CobjInitPercU3E5__4_8(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CnewObjU3E5__5_9(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CinitProgressU3E5__6_10(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CbasePathU3E5__7_11(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CcountU3E5__8_12(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CU3E7__wrap8_13(),
	U3CBuildU3Ed__36_t73EB0602B49F2E5D11B7315689F58F52E8AB0CF9::get_offset_of_U3CmtlU3E5__10_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3540[6] = 
{
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7::get_offset_of_U3CU3E1__state_0(),
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7::get_offset_of_U3CU3E2__current_1(),
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7::get_offset_of_U3CU3E4__this_2(),
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7::get_offset_of_basePath_3(),
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7::get_offset_of_path_4(),
	U3CLoadMaterialTextureU3Ed__42_t2ED4AD5569B0957AE344EF6234B18037CBC10AD7::get_offset_of_U3CuwrU3E5__2_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3541[17] = 
{
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields::get_offset_of_totalProgress_4(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_buildOptions_5(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields::get_offset_of_LOAD_PHASE_PERC_6(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields::get_offset_of_TEXTURE_PHASE_PERC_7(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields::get_offset_of_MATERIAL_PHASE_PERC_8(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields::get_offset_of_BUILD_PHASE_PERC_9(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields::get_offset_of_loadedModels_10(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B_StaticFields::get_offset_of_instanceCount_11(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_dataSet_12(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_objectBuilder_13(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_materialData_14(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_objLoadingProgress_15(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_loadStats_16(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_loadedTexture_17(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_ModelCreated_18(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_ModelLoaded_19(),
	Loader_tCF151A378DB925EFDCD43B30674CC2D0F3327D0B::get_offset_of_ModelError_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3542[4] = 
{
	BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187::get_offset_of_optionName_0(),
	BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187::get_offset_of_valueType_1(),
	BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187::get_offset_of_valueNumMin_2(),
	BumpParamDef_t080D0D570387B59571840C1E802386C71F6B8187::get_offset_of_valueNumMax_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3543[4] = 
{
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56::get_offset_of_U3CU3E1__state_0(),
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56::get_offset_of_U3CU3E2__current_1(),
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56::get_offset_of_absolutePath_2(),
	U3CLoadModelFileU3Ed__3_tAB5EA6120710D2172B96E210AD9F966DA5514F56::get_offset_of_U3CU3E4__this_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3544[5] = 
{
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F::get_offset_of_U3CU3E1__state_0(),
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F::get_offset_of_U3CU3E2__current_1(),
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F::get_offset_of_U3CU3E4__this_2(),
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F::get_offset_of_absolutePath_3(),
	U3CLoadMaterialLibraryU3Ed__4_t6A2472E8EE05396BEACB1A3E5E5EAD15ABBF1B1F::get_offset_of_U3CbasePathU3E5__2_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3545[9] = 
{
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3CU3E1__state_0(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3CU3E2__current_1(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_objDataText_2(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3CU3E4__this_3(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3ClinesU3E5__2_4(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3CisFirstInGroupU3E5__3_5(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3CisFaceIndexPlusU3E5__4_6(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3CseparatorsU3E5__5_7(),
	U3CParseGeometryDataU3Ed__8_t85F9E234DCAAC948F4D9F2DF3DEF1DB84A4E7ED5::get_offset_of_U3CiU3E5__6_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3546[6] = 
{
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF::get_offset_of_U3CU3E1__state_0(),
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF::get_offset_of_U3CU3E2__current_1(),
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF::get_offset_of_U3CU3E4__this_2(),
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF::get_offset_of_url_3(),
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF::get_offset_of_notifyErrors_4(),
	U3CLoadOrDownloadTextU3Ed__16_t2B2AAD0798621DE6CB15D752D965F940EF1541EF::get_offset_of_U3CuwrU3E5__2_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3547[2] = 
{
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543::get_offset_of_mtlLib_21(),
	LoaderObj_t3D5C153CA0FA652243D8BF7AB80216022F6B8543::get_offset_of_loadedText_22(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3548[12] = 
{
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_identSize_0(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_colorMapType_1(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_imageType_2(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_colorMapStart_3(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_colorMapLength_4(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_colorMapBits_5(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_xStart_6(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_ySstart_7(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_width_8(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_height_9(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_bits_10(),
	TgaHeader_t040C3E1E40E8FF57964C1BBAD8DA1532982D50F9::get_offset_of_descriptor_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3550[6] = 
{
	SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70::get_offset_of_fileName_0(),
	SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70::get_offset_of_message_1(),
	SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70::get_offset_of_percentage_2(),
	SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70::get_offset_of_numObjects_3(),
	SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70::get_offset_of_numSubObjects_4(),
	SingleLoadingProgress_t2B4D1E3BF547387BBDE2BC780C7A58361EA98C70::get_offset_of_error_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3551[1] = 
{
	LoadingProgress_t8DD0C03C0D0F201468D8E669C09911F8ED7B87C5::get_offset_of_singleProgress_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3552[4] = 
{
	ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6::get_offset_of_name_0(),
	ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6::get_offset_of_path_1(),
	ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6::get_offset_of_skip_2(),
	ModelImportInfo_t8D7145EDECC62C818D269E226BC4FB14589452B6::get_offset_of_loaderOptions_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3553[4] = 
{
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C::get_offset_of_autoLoadOnStart_14(),
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C::get_offset_of_objectsList_15(),
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C::get_offset_of_defaultImportOptions_16(),
	MultiObjectImporter_t0FF022681578AEF93FA623B7244C2BA15A10ED8C::get_offset_of_pathSettings_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3554[6] = 
{
	ImportPhase_t7A13D25EDB4A12A4A7B35354D54AB9697FAF2225::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3555[10] = 
{
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_numTotalImports_4(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_allLoaded_5(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_buildOptions_6(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_loaderList_7(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_importPhase_8(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_ImportingStart_9(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_ImportingComplete_10(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_CreatedModel_11(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_ImportedModel_12(),
	ObjectImporter_t226E96E900D3C58F19D37A72CDE9C699D420F40D::get_offset_of_ImportError_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3556[4] = 
{
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875::get_offset_of_progressText_4(),
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875::get_offset_of_progressSlider_5(),
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875::get_offset_of_progressImage_6(),
	ObjectImporterUI_t6D49463305C6B700434F52621160D5CC967AC875::get_offset_of_objImporter_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3557[6] = 
{
	RootPathEnum_tB17BAA3D8A73E632D635A0E33DA0F1FAEB5F1B7D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3558[2] = 
{
	PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346::get_offset_of_defaultRootPath_4(),
	PathSettings_t455F6BE9B5E17286C05FCD069FE7CB4C23514346::get_offset_of_mobileRootPath_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3559[3] = 
{
	Triangle_t979E22FAC10144EA526A845A9089CD91A7D1842A::get_offset_of_v1_0(),
	Triangle_t979E22FAC10144EA526A845A9089CD91A7D1842A::get_offset_of_v2_1(),
	Triangle_t979E22FAC10144EA526A845A9089CD91A7D1842A::get_offset_of_v3_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3560[6] = 
{
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA::get_offset_of_prevVertex_0(),
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA::get_offset_of_nextVertex_1(),
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA::get_offset_of_triangleArea_2(),
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA::get_offset_of_triangleHasChanged_3(),
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA::get_offset_of_U3CPositionU3Ek__BackingField_4(),
	Vertex_tF7C701CC21D3C413A1D4911A838E238F222F92DA::get_offset_of_U3COriginalIndexU3Ek__BackingField_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3563[5] = 
{
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A::get_offset_of_filePath_4(),
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A::get_offset_of_objectName_5(),
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A::get_offset_of_importOptions_6(),
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A::get_offset_of_pathSettings_7(),
	AsImpLSample_t161AB94C68CEFEC2FC4B7F013781E06214C9589A::get_offset_of_objImporter_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3564[1] = 
{
	U3CU3Ec__DisplayClass11_0_t516260B5AD68685A397D253FF6E9898F2AB75C4A::get_offset_of_tr_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3565[3] = 
{
	CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE::get_offset_of_objScalingText_18(),
	CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE::get_offset_of_configFile_19(),
	CustomObjImporter_tA8CE762703AC9CE2914EF9897CC683F6D71EDCBE::get_offset_of_modelsToImport_20(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
