using Dummiesman;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Parabox.CSG;
using UnityEngine.UI;

public class modelViewer : MonoBehaviour
{
    string model_path;
    public GameObject mainObj;

    // Start is called before the first frame update
    void Start()
    {
        loadSelectedModel();
    }

    private void OnEnable()
    {
        model_path = PlayerPrefs.GetString("modelPath"); 
    }

	private void loadSelectedModel()
	{
		Debug.Log("Start object loading: " + model_path);
		mainObj = new OBJLoader().Load(model_path);

        //To center the object
        mainObj.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 4, Screen.height / 4, Camera.main.nearClipPlane));

        //Add meshfilter
        MeshFilter meshFilter = mainObj.AddComponent<MeshFilter>();
        Mesh mesh = meshFilter.mesh;

        //Add meshrenderer
        mainObj.AddComponent<MeshRenderer>();

        Debug.Log("Finished object loading");
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
