using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using SimpleFileBrowser;
using UnityEngine.UI;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using System;
using UnityEngine.SceneManagement;
using Dummiesman;

public class mainMenu : MonoBehaviour
{
	string input_path;
	string output_path;
	string model_Path; 

	public Text m_Text;
	bool m_SceneLoaded;

	Dropdown dropdown;
	InputField model_name_input;

	// Start is called before the first frame update
	void Start()
    {
		dropdown = GameObject.Find("Dropdown").GetComponent<Dropdown>();
		model_name_input = GameObject.Find("model_name_input").GetComponent<InputField>();

		configureListeners();
		updateDropdown();
	}

	public void configureListeners() {

		Button input_path_button = GameObject.Find("input_path_button").GetComponent<Button>();
		input_path_button.onClick.AddListener(delegate { OpenFileChooser(true); });

		Button generate_button = GameObject.Find("Generate_button").GetComponent<Button>();
		generate_button.onClick.AddListener(delegate { 
			generateModel();
		});

		Button close_button = GameObject.Find("close_button").GetComponent<Button>();
		close_button.onClick.AddListener(delegate { Application.Quit(); });

		Button load_selected_model = GameObject.Find("load_selected_model").GetComponent<Button>();
		load_selected_model.onClick.AddListener(delegate { changeScene(); });

		Slider iso_slider = GameObject.Find("iso_slider").GetComponent<Slider>();
		iso_slider.onValueChanged.AddListener(delegate {
			GameObject.Find("iso_text").GetComponent<Text>().text = iso_slider.value.ToString("F0");
		});

		Slider mesh_slider = GameObject.Find("mesh_slider").GetComponent<Slider>();
		mesh_slider.onValueChanged.AddListener(delegate {
			GameObject.Find("mesh_text").GetComponent<Text>().text = mesh_slider.value.ToString("F2");
		});

		Slider filter_slider = GameObject.Find("filter_slider").GetComponent<Slider>();
		filter_slider.onValueChanged.AddListener(delegate {
			GameObject.Find("filter_text").GetComponent<Text>().text = filter_slider.value.ToString("F2");
		});
	}

	private void changeScene() {

		//Guardar modelo que se tiene que cargar.
		model_Path = @"Assets\3D_models\" + dropdown.options[dropdown.value].text;

		Debug.Log("Active: " + SceneManager.GetActiveScene().name);

		//Change scene
		Scene modelViewer = SceneManager.CreateScene("modelViewer");
		SceneManager.LoadScene("modelViewer", LoadSceneMode.Single);
		SceneManager.SetActiveScene(modelViewer);
		//SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("menu")); 

		Debug.Log("Active: " + SceneManager.GetActiveScene().name);
	}

	private void updateDropdown() {

		dropdown = GameObject.Find("Dropdown").GetComponent<Dropdown>();

		//Create a List of new Dropdown options
		List<string> dropdown_options = new List<string>();

		DirectoryInfo directory = new DirectoryInfo(@"Assets\3D_models\");
		FileInfo[] Files = directory.GetFiles("*.obj"); //Getting obj files
		
		foreach (FileInfo file in Files)
		{
			//Create list
			dropdown_options.Add(file.Name); 
		}

		//Clear dropdown
		for (int x = 0; x < dropdown.options.Count; x++)
		{
				dropdown.options.RemoveAt(x);
		}

		//Load into dropdown
		dropdown.AddOptions(dropdown_options);
	}

	public void OpenFileChooser(bool input)
	{
		FileBrowser.SetFilters(true, new FileBrowser.Filter("Images", ".jpg", ".png"), new FileBrowser.Filter("Text Files", ".txt", ".pdf"));
		FileBrowser.SetDefaultFilter(".jpg");
		FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
		FileBrowser.AddQuickLink("Users", "C:\\Users", null);

		StartCoroutine(ShowLoadDialogCoroutine(input));
	}

	public void generateModel() {

		//Obtener datos de los elementos de la interfaz gr�fica. 
		float iso_value = GameObject.Find("iso_slider").GetComponent<Slider>().value;
		float mesh_reduction = GameObject.Find("mesh_slider").GetComponent<Slider>().value;
		float filter_value = GameObject.Find("filter_slider").GetComponent<Slider>().value;
		
		bool smoothed = GameObject.Find("smoothed_toggle").GetComponent<Toggle>().isOn;

		//Llamada a la librer�a dicom2mesh. 
		dicom2Mesh(input_path, output_path, (int) iso_value, mesh_reduction, filter_value, smoothed);
		System.Threading.Thread.Sleep(15000);
		updateDropdown();
	}

	IEnumerator ShowLoadDialogCoroutine(bool input)
	{
		// Show a load file dialog and wait for a response from user
		// Load file/folder: both, Allow multiple selection: true
		// Initial path: default (Documents), Initial filename: empty
		// Title: "Load File", Submit button text: "Load"
		yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.FilesAndFolders, true, null, null, "Load Files and Folders", "Load");

		// Dialog is closed
		// Print whether the user has selected some files/folders or cancelled the operation (FileBrowser.Success)
		//Debug.Log(FileBrowser.Success);

		if (FileBrowser.Success)
		{
			// Print paths of the selected files (FileBrowser.Result) (null, if FileBrowser.Success is false)
			for (int i = 0; i < FileBrowser.Result.Length; i++)
				if (input)
				{
					input_path = FileBrowser.Result[i];

					//Show in screen
					Text input_path_text = GameObject.Find("input_path_text").GetComponent<Text>();
					input_path_text.text = input_path; 
				}
				else {
					output_path = FileBrowser.Result[i];
				}

			// Read the bytes of the first file via FileBrowserHelpers
			// Contrary to File.ReadAllBytes, this function works on Android 10+, as well
			byte[] bytes = FileBrowserHelpers.ReadBytesFromFile(FileBrowser.Result[0]);

			// Or, copy the first file to persistentDataPath
			string destinationPath = Path.Combine(Application.persistentDataPath, FileBrowserHelpers.GetFilename(FileBrowser.Result[0]));
			FileBrowserHelpers.CopyFile(FileBrowser.Result[0], destinationPath);
		}
	}

	private void dicom2Mesh(string input, string output, int iso, float reduction, float filter, bool smoothed)
	{
		// Use ProcessStartInfo class (definir propiedades del proceso externo). 
		Process dicom2mesh_process = new Process();

		dicom2mesh_process.StartInfo.UseShellExecute = false;
		dicom2mesh_process.StartInfo.FileName = "dicom2mesh.exe";
		dicom2mesh_process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
		//dicom2mesh_process_si.CreateNoWindow = true; 

		//Construcci�n de los par�metros para la llamada. 
		dicom2mesh_process.StartInfo.Arguments = constructParameters(input, output, iso, reduction, filter, smoothed);

		dicom2mesh_process.StartInfo.RedirectStandardOutput = true;

		//Redireccionar salida del proceso. 
		dicom2mesh_process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
		dicom2mesh_process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

		try
		{
			dicom2mesh_process.Start();

			dicom2mesh_process.BeginOutputReadLine();
			dicom2mesh_process.BeginErrorReadLine();

			dicom2mesh_process.WaitForExit();
        }
		catch (Exception e)
		{
			Debug.Log(e.Message); 
		}
	}

	private string constructParameters(string input, string output, int iso, float reduction, float filter, bool smoothed) {

		string parameters = " -i " + input;

		string[] flags = { "-e", "-r", "-t" };
		float[] parametersSet = { filter, reduction, iso };


		for (int i = 0; i < flags.Length; i++)
		{
			if (parametersSet[i] != 0)
			{
				parameters += " " + flags[i] + " " + parametersSet[i];
			}
		}

		if (model_name_input.text == null) {
			model_name_input.text = "untitled";
		}

		if (smoothed)
		{
			parameters += " -s -c -o Assets\\3D_Models\\" + model_name_input.text + ".obj";
		}
		else
		{
			parameters += " -c -o Assets\\3D_Models\\" + model_name_input.text + ".obj";
		}

		Debug.Log(parameters);

		return parameters;
	}

	static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
	{
		Debug.Log(outLine.Data);

		/*Text console_output = GameObject.Find("console_output").GetComponent<Text>();
		console_output.text = outLine.Data;*/
	}

	// Update is called once per frame
	void Update()
    {

	}

	private void OnDisable()
    {
		//Guardar ruta para que las otras escenas puedan acceder a ella.
		PlayerPrefs.SetString("modelPath", model_Path); 
    }
}
