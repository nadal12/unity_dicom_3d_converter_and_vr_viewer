using GetSocialSdk.Capture.Scripts;
using RockVR.Video;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;


public class mouseInteraction : MonoBehaviour
{
    private Camera camera;

    private float rotationSensitivity = 6f;
    private float movementSensibility = 0.1f;
    private float zoomSpeed = 1f; 

    private float maxYAngle = 80f;
    private Vector2 currentRotation;

    private bool isRightDown = false;
    private bool isCentralDown = false;

    public GameObject linePrefab;

    public GetSocialCapture capture;
    bool recording = false; 

    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
        configureListeners();
    }

    void configureListeners() {

        Slider movement_slider = GameObject.Find("movement_slider").GetComponent<Slider>();
        movement_slider.onValueChanged.AddListener(delegate {
            movementSensibility = movement_slider.value;
        });

        Slider zoom_slider = GameObject.Find("zoom_slider").GetComponent<Slider>();
        zoom_slider.onValueChanged.AddListener(delegate {
            zoomSpeed = zoom_slider.value;
        });

        Button screenshot_button = GameObject.Find("screenshot").GetComponent<Button>();
        screenshot_button.onClick.AddListener(delegate {
            UnityEngine.ScreenCapture.CaptureScreenshot("Assets/Screenshots/screenshot_" + Regex.Replace(Regex.Replace(Regex.Replace(DateTime.Now.ToString(), @"\s", "_"), ":", ""), "/", "") + ".png");
        });

        Button video_button = GameObject.Find("video").GetComponent<Button>();
        video_button.onClick.AddListener(delegate {
            if (!recording)
            {
                VideoCaptureCtrl.instance.StartCapture();
                GameObject.Find("video").GetComponentInChildren<Text>().text = "Stop recording";
                recording = !recording;
            } else {
                VideoCaptureCtrl.instance.StopCapture();
                GameObject.Find("video").GetComponentInChildren<Text>().text = "Start recording";
                recording = !recording;
            }
        });
    }

    void HandleMouse()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            ZoomIn();
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            ZoomOut();
        }

        if (isRightDown)
        {
            RotateCamera();
        }

        if (isCentralDown)
        {
            MoveCamera();
        }
    }

    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.SetColors(color, color);
        lr.SetWidth(0.1f, 0.1f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }

    /* https://www.gamasutra.com/blogs/HectorXiang/20190906/350177/Rotate_zoom_and_move_your_camera_with_your_mouse_in_unity3d.php */
    void MoveCamera()
    {
        Vector3 NewPosition = new Vector3(Input.GetAxis("Mouse X"), 0, Input.GetAxis("Mouse Y"));
        Vector3 position = camera.transform.position;
        if (NewPosition.x > 0.0f)
        {
            position += camera.transform.right * movementSensibility; 
        }
        else if (NewPosition.x < 0.0f)
        {
            position -= camera.transform.right * movementSensibility;
        }
        if (NewPosition.z > 0.0f)
        {
            position += camera.transform.up * movementSensibility;
        }
        if (NewPosition.z < 0.0f)
        {
            position -= camera.transform.up * movementSensibility;
        }
        position.z = camera.transform.position.z;
        camera.transform.position = position;
    }

    void RotateCamera()
    {
         currentRotation.x += Input.GetAxis("Mouse X") * rotationSensitivity;
         currentRotation.y -= Input.GetAxis("Mouse Y") * rotationSensitivity;
         currentRotation.x = Mathf.Repeat(currentRotation.x, 360);
         currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle);
         camera.transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);
    }

    /* https://www.gamasutra.com/blogs/HectorXiang/20190906/350177/Rotate_zoom_and_move_your_camera_with_your_mouse_in_unity3d.php */
    private void ZoomOut()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            transform.position -= transform.forward * zoomSpeed;
        }
    }

    /* https://www.gamasutra.com/blogs/HectorXiang/20190906/350177/Rotate_zoom_and_move_your_camera_with_your_mouse_in_unity3d.php */
    private void ZoomIn()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            transform.position += transform.forward * zoomSpeed;
        }
    }

    private void CheckHeldDownButtons()
    {
        if (Input.GetMouseButtonDown(1))
        {
            isRightDown = true;
        }

        if (Input.GetMouseButtonUp(1))
        {
            isRightDown = false;
        }

        if (Input.GetMouseButtonDown(2))
        {
            isCentralDown = true;
        }

        if (Input.GetMouseButtonUp(2))
        {
            isCentralDown = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckHeldDownButtons();
        HandleMouse();
    }
}
